from flask import flash, render_template, request, jsonify
from flask_login import current_user

from intranet import portal
from utils import db, parse_date

@portal.route('/')
def home():
    return render_template('portal/home.html')

@portal.route('/bookings')
def show_bookings():
    return 'My Bookings'

@portal.route('/pets')
def show_pets():
    return 'My Pets'

@portal.route('/profile')
def show_profile():
    return 'My Profile'

@portal.route('/password')
def change_password():
    return 'Change Password'

@portal.route('/login')
def login():
    return render_template('portal/login.html')

@portal.route('/logout')
def logout():
    return 'Logout'

