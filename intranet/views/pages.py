from intranet import app, csrf
from utils import db, get_settings, parse_date
from flask import render_template, flash, redirect, url_for, request, Response, Response
from decimal import Decimal
from collections import defaultdict
from json import dumps
from typing import cast, Optional, List
from datetime import datetime, date, timedelta
from dateutil.relativedelta import relativedelta
from intranet.utilities import pet_button, inventory_row, date2words, day_emp_html, get_revenue_wages, weekly_shifts_common
from intranet.utilities import get_availability_url, populate_pet_form, leaf2html, row2dict, pet_request_html
from intranet.utilities import populate_pet_form_choices, pet_documents, is_date, get_booking_time, day_events_html, checkin_form_form, day_emp, questionnaire_fields
from intranet.utilities import get_weight_report_data, populate_role_choices, get_employees_for_date, dates_between_dates
from intranet.utilities import weekly_common, get_notice_html, get_employees, parse_date, get_daycare_runs, get_daycare_dogs

# from intranet.views.components import weekly_shifts

from urllib.parse import quote
from intranet.forms import CheckAvailabilityForm, CheckinForm, PaymentForm, CancelBookingForm, BookRequestForm
from intranet.forms import CheckoutForm, BookingForm, NoShowForm, UncancelForm, ConfirmForm, Deposit2VoucherForm, ChangeRequestCustomerForm
from intranet.forms import VetBillForm, PetForm, VisitForm, CustomerForm, EmployeeForm, LoginForm, PasswordForm, PetSubform
from intranet.actions import process_payment, process_cancellation, process_checkin, process_book_request
from intranet.actions import process_checkout, process_booking_form, process_noshow, process_uncancel, process_confirm, process_deposit2voucher
from intranet.actions import process_change_request_customer, process_vetbill_form, process_customer_form
from intranet.actions import process_pet_form, process_visit_form, process_employee_form, populate_employee_form_from_starter, populate_employee_form
from os import scandir
from intranet.utilities import pet_medic, date_range
from dateutil.parser import parse
import re
from models import Period, Customer, Employee, EmployeeStarter, NoteType, DaycareCandidate, Inventory
from models.booking import Booking, load_pet_bookings
from models.availability import get_availability
from models.daycare_candidate import DaycareCandidateStatus
import intranet.filters
from models import Slot, Period, Booking, Pet, Customer, Note
from models.availability import get_availablity_data, get_availability, get_spec_availability
from logic.staffing import required_staffing, staffing_class
from logic import get_cashflow_report
from models.runtype import RunType
from intranet.user import EmployeeUser, User, role_required
from flask_login import login_user, logout_user, login_required, current_user

@app.route('/')
@app.route('/home/<string:date_arg>')
def home_page(date_arg = None):
    def form_class(has_form, attachments):
        if has_form:
            if attachments:
                return 'primary'
            else:
                return 'success'
        else:
            return 'light'

    if date_arg:
        the_date = parse_date(date_arg)
    else:
        the_date = date.today()

    datestr = the_date.strftime('%Y-%m-%d')
    
    # day off?
    
    day_off = db.safeselect_value('select ts_off from vwdayoff where ts_date = ?', datestr)

    emps = get_employees_for_date(datestr)
    emp_desc = []    
    if emps:
        emp_desc = [{
            'employee': emp.ed_emp_nickname + (' (Working from Home)' 
                    if emp.ed_am == 'W' or emp.ed_pm == 'W' else ('' if emp.ed_am == 'X' and emp.ed_pm == 'X' else (
                    ' (AM only)' if emp.ed_am == 'X' else ' (PM only)'))),
            'class': emp.ed_rank_class,
            'emp_no': emp.ed_emp_no,
            'order': emp.ed_order,
            'approved': emp.ed_approved,
            'comment': emp.ed_comment if emp.ed_comment else '',
            'start_punch': emp.ed_start_punch,
            'end_punch': emp.ed_end_punch,
            'start_status': emp.ed_start_status,
            'end_status': emp.ed_end_status, 
            'lunch': (1 if emp.ed_am == 'X' and emp.ed_pm == 'X' else 0) if emp.ed_lunch is None else emp.ed_lunch
        } for emp in emps]

# get events, ins and outs

    visits = db.safeselect("""select visit_no, visit_time, visit_name, visit_type, visit_completed from vwvisitor
where visit_date = ? and visit_type not in ('in', 'out') and visit_first_slot=1 order by visit_time""", datestr)
 
    visit_html = day_events_html(the_date)
    
    next_visit = db.safeselect("select visit_time, visit_desc from vwnext_visit where visit_date = ?", datestr)
    
# get weight exceptions
    weight_data = get_weight_report_data()
    weight_status = weight_data['weight_status']
    
# get checkins and outs
    sql = """select bk_no, cust_no, bk_time, surname, pet_desc, done, pet_no, tooltip, run, is_deluxe, form,
is_first, has_meds, pet_warning, pv_status, vet_clinic, attachments, has_questionnaire, kennel_notes,
ro_run_code, rm_c, bk_status
from vwcheckinouts_in_new where bk_date = ? order by done, bk_time"""
    pets_in_rows = db.safeselect(sql, datestr)
    
    pets_in = {}
    ins_c = 0
    
    if pets_in_rows:
        for pet_row in pets_in_rows:
            pet = Pet.fetch(pet_row.pet_no)
            if not pet_row.bk_no in pets_in:
                pets_in[pet_row.bk_no] = {
                    'cust_no': pet.customer.no,
                    'bk_time': pet_row.bk_time,
                    'surname': pet.customer.surname,
                    'status': pet_row.bk_status,
                    'pets': {pet.no: {
                        'desc': pet.description,
                        'form': pet_row.form,
                        'button': pet_button(pet.no),
                        'has_form': pet_row.form,
                        'is_first': pet_row.is_first,
                        'has_meds': pet_row.has_meds,
                        'warning': pet.warning,
                        'vaccinations': pet_row.pv_status,
                        'vet_clinic': pet.vet.practice_name if pet.vet else 'N/A',
                        'attachments': pet_row.attachments,
                        'form_class': form_class(pet_row.form, pet_row.attachments),
                        'kennel_notes': pet_row.kennel_notes,
                        'run_code': pet_row.ro_run_code,
                        'run_count': pet_row.rm_c
                    }},
                    'done': pet_row.done,
                    'tooltip': pet_row.tooltip,
                    'run': pet_row.run,
                    'is_deluxe': pet_row.is_deluxe
                }
                if not pet_row.done:
                    ins_c += 1
            else:
                pets_in[pet_row.bk_no]['pets'][pet.no] = {
                    'desc': pet.description,
                    'form': pet_row.form,
                    'button': pet_button(pet.no),
                    'has_form': pet_row.form,
                    'is_first': pet_row.is_first,
                    'has_meds': pet_row.has_meds,
                    'warning': pet.warning,
                    'vaccinations': pet_row.pv_status,
                    'vet_clinic': pet.vet.practice_name if pet.vet else 'N/A',
                    'attachments': pet_row.attachments,
                    'form_class': form_class(pet_row.form, pet_row.attachments),
                    'has_questionnaire': pet_row.has_questionnaire,
                    'run_code': pet_row.ro_run_code,
                    'run_count': pet_row.rm_c
                }
            
    sql = """select bk_no, cust_no, bk_time, surname, pet_desc, done, pet_no, tooltip, balance
from vwcheckinouts_out_new where bk_date = ? order by done, bk_time"""

    pets_out_rows = db.safeselect(sql, datestr)
    
    outs_c = 0
    pets_out = {}
    if pets_out_rows:
        for pet_row in pets_out_rows:
            pet = Pet.fetch(pet_row.pet_no)
            if not pet_row.bk_no in pets_out:
                pets_out[pet_row.bk_no] = {
                    'cust_no': pet.customer.no,
                    'bk_time': pet_row.bk_time,
                    'surname': pet.customer.surname,
                    'pets': {pet.no: {'desc': pet.description, 'button': pet_button(pet.no)}},
                    'done': pet_row.done,
                    'balance': pet_row.balance,
                    'tooltip': pet_row.tooltip,
                }
                if not pet_row.done:
                    outs_c += 1
            else:
                pets_out[pet_row.bk_no]['pets'][pet.no] = {'desc': pet.description, 'button': pet_button(pet.no)}

    sql = 'select bk_no, cust_no, surname, pet_name, breed_desc, done, pet_no, tooltip, slot, is_scheduled from vwcheckinouts_in_day_new where bk_date = ?'
    
    pets_day_rows = db.safeselect(sql, datestr)
    
    pets_day = {}
    
    if pets_day_rows:
        for pet_row in pets_day_rows:
            pet = Pet.fetch(pet_row.pet_no)
            assert(pet.breed)
            pets_day[pet_row.bk_no] = {
                'bk_no': pet_row.bk_no,
                'pet': pet,
                'button': pet_button(pet.no),
                'tooltip': pet_row.tooltip,
                'done': pet_row.done,
                'slot': pet_row.slot,
                'scheduled': pet_row.is_scheduled
            }
    
    sql = 'select pet_no from vwdaycare_available where date = ?'
    pet_nos = db.safeselect(sql, datestr)
    
    pets_daycare_available = list(Pet.fetch(row.pet_no) for row in pet_nos) if pet_nos else []
    
# get inventory levels
    kennel_inv = Inventory.get(the_date, 'Dog')
    
    cattery_inv = Inventory.get(the_date, 'Cat')
    
# cash collected

    payments = None
    total_pay = 0.0    
    expected = 0.0
    cu = cast(User, current_user)

    if cu.is_authenticated and cu.role >= User.roles['manager']:
        type_limit = ''
        if cu.role < User.roles['owner']:
            type_limit = " and pay_type = 'Cash'"
        
        sql = f"""select pay_type, count(*) pay_count, convert(float, sum(pay_amount)) pay_total
from pa..tblpayment where pay_date=?{type_limit} group by pay_type"""
        payments = db.safeselect(sql, datestr)

        sql = f"""select count(*) pay_count, convert(float, sum(pay_amount)) pay_total
from pa..tblpayment where pay_date=?{type_limit}"""
        total_pay = db.safeselect_one(sql, datestr)

        sql = """select sum(bk_amt_outstanding)
from pa..tblbooking
where bk_start_date = ?
and bk_status in ('', 'V')
and bk_no in (select bi_bk_no from pa..tblbookingitem where bi_checkin_time = '')
"""
        expected = db.safeselect_value(sql, datestr)
        if not expected:
            expected = 0.0
# get availability
    kennel_availability = {
        'am': get_spec_availability(Slot(the_date, 'am'), 1),
        'pm': get_spec_availability(Slot(the_date, 'pm'), 1)
    }
    cattery_availability = {
        'am': get_spec_availability(Slot(the_date, 'am'), 2),
        'pm': get_spec_availability(Slot(the_date, 'pm'), 2)
    }

    audits = db.safeselect("select * from vwaudit_python where aud_date = ? order by aud_time, aud_no", datestr)
    
    sql = """select count(*) from vwformdata_quick where fd_open = 1 and datediff(day, fd_date_created, getdate()) < 31"""

    requests = db.safeselect_value(sql)
    
    sql = """select count(*) from tblconfirm_form where cf_status = 0 and cf_action = 'amend'"""
    
    amendments = db.safeselect_value(sql)

    sql = "select bk_no, pet_no, pet_name, cust_no, cust_surname, start_date, food_comment from vwraw_warning"
    raw_warnings = db.safeselect(sql)
    
    sql = """select dw_pet_no, dw_pet_name, dw_breed_desc, dw_cust_surname
from vwdogs_to_weigh where dw_date = ?
order by dw_pet_name, dw_cust_surname"""

    dogs_to_weigh = db.safeselect(sql, datestr)

    sql = "select tbc_spec_desc, tbc_run_code, tbc_leave_time, tbc_arrive_time, tbc_type, tbc_overlap from vwtbc where tbc_type <> 'tbc-unallocated' and tbc_date = ?"
    tbc = db.safeselect(sql, datestr)
    
    note_count = Note.get_daily_count(the_date)
    
    return render_template('home.html', datestr=datestr, the_date=the_date, day_off=day_off, emps=emp_desc,
                           visits=visits, visit_html=visit_html, pets_out=pets_out, pets_in=pets_in, pets_day=pets_day,
                           kennel_inv=kennel_inv, cattery_inv=cattery_inv, note_count=note_count,
                           kennel_availability=kennel_availability, cattery_availability=cattery_availability,
                           audits=audits, requests=requests, raw_warnings=raw_warnings, amendments=amendments,
                           payments=payments, total_pay=total_pay, weight_status=weight_status, expected=expected,
                           ins_c=ins_c, outs_c=outs_c, next_visit=next_visit, dogs_to_weigh=dogs_to_weigh, tbc=tbc,
                           pets_daycare_available=pets_daycare_available)


@app.route('/weekly')
@app.route('/weekly/<string:date_arg>')
def show_weekly(date_arg = None):
    return weekly_common(date_arg)

@app.route('/timesheet')
@app.route('/timesheet/<string:date_arg>')
@login_required
def show_timesheet(date_arg = None):
    if date_arg:
        the_date = parse_date(date_arg)
    else:
        the_date = date.today()

    wd = the_date.weekday()
    
    start_date = the_date - timedelta(wd)
    end_date = start_date + timedelta(6)
    
    dates = dates_between_dates(start_date, end_date)
    date_dict = {d.strftime("%y%m%d"): d for d in dates}
    
    recs = db.safeselect('select ts_date, ts_off from vwdayoff where ts_date between ? and ?',
                            start_date, end_date)

    days_off = {rec.ts_date.strftime("%y%m%d") : rec.ts_off for rec in recs} if recs else {}
    
       # get employees scheduled to work
    recs = db.safeselect("""
select ts_date, ts_emp_no, ts_emp_nickname, ts_rank_desc, ts_am, ts_pm, ts_rank_class from vwtimesheet
where ts_no is not null and (ts_am in ('X', 'W') or ts_pm in ('X', 'W')) and ts_date between ? and ?""",
        start_date, end_date)
    
    emps = {}
    for ds, d in date_dict.items():
        emps[ds] = day_emp_html(d, False)
    
    
    kennel_inv = {d : Inventory.get(d, 'Dog') for d in dates}
    cattery_inv = {d : Inventory.get(d, 'Cat') for d in dates}
    
    recs = db.safeselect("""select visit_date, visit_no, visit_time, visit_name, visit_type from vwvisitor
where visit_date between ? and ? and visit_type not in ('in', 'out') and visit_first_slot=1 order by visit_date, visit_time""", start_date, end_date)

    visits = {}
    
    if recs:
        for rec in recs:
            d = rec.visit_date
            if d not in visits:
                visits[d] = []
            visits[d].append(rec)

    wages, revenue, ratio, ratioClass, ratioDesc = get_revenue_wages(start_date)
    
    summary = { d: { 'ins': Inventory.ins(d), 'outs': Inventory.outs(d),
                    'visits': len(visits[d]) if d in visits else 0,
                    'staff': day_emp(d)}
                    for d in dates}
    
    for d in summary:
        summary[d]['req_staffing'] = required_staffing(Inventory.get(d, 'Dog').morning,
                 summary[d]['ins'] + summary[d]['outs'], 1)
        summary[d]['staff_class'] = staffing_class(summary[d]['req_staffing'], summary[d]['staff'])
    
    return render_template('timesheet.html', start_date=start_date, end_date=end_date, dates=date_dict, 
                           kennel_inv=kennel_inv, cattery_inv=cattery_inv, visits=visits, wages=wages,
                           revenue=revenue, ratio=ratio, ratioClass=ratioClass, ratioDesc=ratioDesc, emps=emps, summary=summary)

@app.route('/bookings')
def show_bookings():
    today = date.today()
    show_all = int(request.values.get('show_all', 0))
    
    where = '1=1' if show_all else 'datediff(day, bk_end_date, getdate()) < 60'
    sql = f"""select bk_no from pa..tblbooking where {where}"""
    booking_nos = db.safeselect(sql)
    bookings = [Booking.fetch(row.bk_no) for row in booking_nos]
    return render_template('bookings.html', bookings=bookings, today=today)
   
@app.route('/booking/<int:bk_no>', methods = ['GET', 'POST'])
def show_booking(bk_no):
    today = date.today()
    booking = Booking.fetch(bk_no)
    sideurl = ''
    
    cancel_form = CancelBookingForm()
    noshow_form = NoShowForm()
    pay_form = PaymentForm()
    uncancel_form = UncancelForm()
    confirm_form = ConfirmForm()
    deposit2voucher_form = Deposit2VoucherForm()
    bill_form = VetBillForm()
    bill_form.bk_no.data = bk_no
    
    pet_numbers = [pet.no for pet in booking.pets] if booking.pets else []
    pet_choices = [(pet.no, pet.name) for pet in booking.pets] if booking.pets else []
    
    bill_form.pet.choices = pet_choices
    reread = False
    
    reread = process_payment(pay_form)
    
    reread = reread or process_uncancel(uncancel_form)
    reread = reread or process_confirm(confirm_form)
    
    reread = reread or process_deposit2voucher(deposit2voucher_form)
    
    if len(pet_choices) == 1:
        pet_choice = str(pet_choices[0][0])
        bill_form.pet.data = pet_choice

    reread = reread or process_vetbill_form(bill_form)
    
    if not reread:
        send = 0
        reread = process_cancellation(cancel_form)
        if reread:
            send = cancel_form.notice.data
        if not reread:
            reread = process_noshow(noshow_form)
            send = noshow_form.notice.data
        if reread and send:
            sideurl = url_for('show_doc', the_view='vwemail_cancellation_notice', bk_no=bk_no) # Open a new window

    if reread:
        booking = Booking.get(bk_no)

    if not booking:
        flash(f'Could not find booking #{bk_no}', 'error')
        return redirect(url_for('home_page'))
    
    run_alloc = db.safeselect_value('select bk_runs from vwbooking_runs where bk_no = ?', bk_no)
    
    pay_form.booking.data = bk_no
    pay_form.voucher_balance.data = booking.customer.voucher_balance
    pay_form.pay_date.data = today
    pay_form.amount.data = booking.amt_outstanding

    cancel_form.notice.data = True
    cancel_form.booking.data = bk_no
    refund_status = booking.refund_status()
    noshow_form.booking.data = bk_no
    confirm_form.booking.data = bk_no
    uncancel_form.booking.data = bk_no
    
    pet_numbers = db.safeselect("select bi_pet_no, bi_pet_name from vwbookingitem where bi_bk_no = ?", bk_no)
    pet_choices = [(pet.bi_pet_no, pet.bi_pet_name) for pet in pet_numbers] if pet_numbers else []
    
    bill_form.pet.choices = pet_choices
    bill_form.date.data = today
   
    pets = (pet_button(row.bi_pet_no) for row in pet_numbers) if pet_numbers else []

    audits = db.safeselect('select * from vwaudit_python where aud_bk_no = ? order by aud_date, aud_time, aud_no', bk_no)

    imminent = 0
    uncancel = 0
    confirm = 0
    if booking.status.code in ('', 'P'):
        confirm = 1
        
    if booking.status.code in ('C', 'Z'):
        uncancel_form.booking.data = bk_no
        uncancel = 1
    else:
        if (booking.start_date - today).days < 14:
            imminent = 1
            cancel_form.discount.data = 100
        else:
            cancel_form.discount.data = 100

    noshow = 0
    if booking.start_date <= today and booking.status.code != 'C':
        noshow = 1
        noshow_form.discount.data = 0
    
    deposit2voucher = 0
    if booking.paid_amt > 0:
        deposit2voucher = 1
        deposit2voucher_form.booking.data = bk_no
    
    comment = booking.notes.replace('\r\n', '<br>')
    return render_template('booking.html', booking=booking, pets=pets, comment=comment, today=today,
                           invoice=booking.invoice,
                           cancel_form=cancel_form, pay_form=pay_form, confirm_form=confirm_form, confirm=confirm,
                           imminent=imminent, uncancel=uncancel, run_alloc=run_alloc,
                           deposit2voucher=deposit2voucher, sideurl=sideurl, audits=audits,
                           noshow=noshow, noshow_form=noshow_form, uncancel_form=uncancel_form, refund_status=refund_status,
                           deposit2voucher_form=deposit2voucher_form, bill_form=bill_form, pet_count=len(pet_choices))

@app.route('/inventory/<string:date_arg>')
def show_inventory(date_arg = None):
    if date_arg:
        the_date = parse_date(date_arg)
    else:
        the_date = datetime.now()

    datestr = the_date.strftime('%Y-%m-%d')

    sql = """select pi_bk_no, pi_rt, pi_species, pi_cust_surname, pi_cust_no, pi_start_date,
pi_end_date, pi_in_out, pi_order, pi_pets, pi_run_code
from vwpetinventory_flat
where pi_date=?
order by pi_species desc,
case pi_rt
	when 'Home' then 1
	when 'Deluxe' then 2
	when 'Double' then 3
	when 'GAL' then 4
	when 'Standard' then 5
    when 'Cat' then 6
    when 'Unalloc' then 7
end, pi_order"""

    runs = db.safeselect(sql, datestr)
    dog_runs = [inventory_row(run) for run in runs if run.pi_species == 'Dog'] if runs else []
    cat_runs = [inventory_row(run) for run in runs if run.pi_species == 'Cat'] if runs else []
    return render_template('inventory.html', datestr=datestr, the_date=the_date, dog_runs=dog_runs, cat_runs=cat_runs )


@app.route('/customer')
def show_customers(): 
    sql = """select cust_no, cust_display_name, cust_telno_home, cust_telno_mobile, cust_telno_mobile2, cust_email, cust_email2, cust_pets
from vwcustomer where cust_archived = 0"""
    customers = db.safeselect(sql)
    return render_template('customers.html', customers=customers, active=1)

@app.route('/customer/all')
def show_all_customers(): 
    sql = """select cust_no, cust_display_name, cust_telno_home, cust_telno_mobile, cust_telno_mobile2, cust_email, cust_email2, cust_pets
from vwcustomer"""
    customers = db.safeselect(sql)
    return render_template('customers.html', customers=customers, active=0)

@app.route('/customer/<string:customer_desc>', methods=['GET', 'POST'])
def show_customer(customer_desc):
   sql = 'select cust_no from dbo.fnfind_customers(?)'
   cust_nos = db.safeselect(sql, customer_desc)
   
   if not cust_nos:
       return redirect(url_for('show_customers'))
   
   if len(cust_nos) > 1:
        sql = """select cust_no, cust_display_name, cust_telno_home, cust_telno_mobile, cust_telno_mobile2, cust_email, cust_email2, cust_pets
    from vwcustomer where cust_no in (select cust_no from dbo.fnfind_customers(?))"""
        customers = db.safeselect(sql, customer_desc)
        return render_template('customers.html', customers=customers, active=0)

   cust_no = cust_nos[0][0]
           
#   customer = db.safeselect_one("""
#select cust_display_name name, cust_addr1, cust_addr3, cust_postcode, cust_telno_home, cust_telno_mobile, cust_telno_mobile2, cust_email, cust_email2, cust_banned
#from vwcustomer where cust_no = ?""", cust_no)

   customer = Customer.fetch(cust_no)
   pets = [pet_button(pet_no) for pet_no in customer.pet_numbers] if customer.pet_numbers else []
   
   booking_nos = db.safeselect("select bk_no from pa..tblbooking where bk_cust_no = ? order by bk_start_date desc", cust_no)
   bookings = [Booking.get(row.bk_no) for row in booking_nos] if booking_nos else []

   vouchers = db.safeselect("select v_no, v_amount, v_date, v_action, v_start_date from vwvoucher where v_cust_no=?", cust_no)
   voucher_total = db.safeselect_value("select v_amount from vwvoucher_balance where v_cust_no=?", cust_no)
   
   requests = db.safeselect("select fd_no, fd_date_created, fd_bk_dates, fd_status, fd_bk_no from vwformdata_6 where fd_cust_no = ? order by fd_date_created desc", cust_no)

   visits = db.safeselect('select visit_no, visit_date, visit_time, visit_duration_desc, visit_subject, visit_notes, visit_kt, visit_ct, visit_mg, visit_dca from vwvisit where visit_cust_no=?', cust_no)
   history = db.safeselect("""
                           select source, hist_no, hist_bk_no, hist_date, hist_destination, hist_subject, hist_msg
from (
 select 'history' source, hist_no, hist_bk_no, hist_date, hist_destination, hist_subject, hist_msg
                          from pa..tblhistory
                          where hist_cust_no=? and hist_type='Email Client'
	union all
	select 'mailing', cm_no, null, cm_date, null, mail_subject, mail_content
	from tblcustmailing
	join tblmailing on cm_mail_no = mail_no
	where cm_cust_no = ?) x
order by hist_date desc
""", cust_no, cust_no)
   return render_template('customer.html', cust_no=cust_no, customer=customer, history=history, pets=pets,
                          bookings=bookings, vouchers=vouchers,
                          voucher_total=voucher_total, requests=requests, visits=visits)

@app.route('/edit_customer', methods=['GET', 'POST'])
@app.route('/edit_customer/<int:cust_no>', methods=['GET', 'POST'])
def edit_customer(cust_no = 0):
    form = CustomerForm()
    new_cust_no = process_customer_form(form)
        
    if new_cust_no:
        return redirect(url_for('show_customer', customer_desc=new_cust_no))
    
    customer = None
    if cust_no:       
        customer = Customer.get(cust_no)
        
    if customer:
        form.cust_no.data = cust_no
        form.cust_surname.data = customer.surname
        form.cust_forename.data = customer.forename
        form.cust_email.data = customer.email
        form.cust_email2.data = customer.email2
        form.cust_addr1.data = customer.addr1
        form.cust_addr3.data = customer.addr3
        form.cust_postcode.data = customer.postcode
        form.cust_telno_home.data = customer.telno_home
        form.cust_telno_mobile.data = customer.telno_mobile
        form.cust_telno_mobile2.data = customer.telno_mobile2
        form.cust_notes.data = customer.notes
        form.cust_deposit.data = customer.deposit
        form.cust_sms.data = customer.sms
        form.cust_fb_review.data = customer.fb_review
        form.cust_approved.data = customer.approved
        form.cust_banned.data = customer.banned        

    return render_template('customer_edit.html', form=form, cust_no=cust_no,
                           customer=customer)

@app.route('/new_employee', methods=['GET', 'POST'] )
@login_required
def new_employee():
    form = EmployeeForm()
    populate_role_choices(form)    

    form.emp_role.data = str(User.roles['employee'])
    new_emp_no = process_employee_form(form)    
    
    if new_emp_no:
        return redirect(url_for('show_employee', emp_no=new_emp_no))    
    
    return render_template('employee.html', form=form)

@app.route('/starters')
def starters():
    return render_template('starters.html', starters=EmployeeStarter.get_active())

@app.route('/new_employee_from_starter/<int:sf_no>', methods=['GET', 'POST'])
def new_employee_from_starter(sf_no: int):
    form = EmployeeForm()
    populate_role_choices(form)
    
    new_emp_no = process_employee_form(form)
        
    if new_emp_no:
        return redirect(url_for('show_employee', emp_no=new_emp_no))    
    
    starter = EmployeeStarter(sf_no)
    
    assert(starter)
    
    populate_employee_form_from_starter(starter, form)
    
    return render_template('employee.html', form=form)

@app.route('/employee/<int:emp_no>', methods=['GET', 'POST'])
@login_required
def show_employee(emp_no):
    form = EmployeeForm()
    populate_role_choices(form)
    
    new_emp_no = process_employee_form(form)
        
    if new_emp_no:
        return redirect(url_for('show_employee', emp_no=new_emp_no))    
    
    employee = Employee.fetch(emp_no)
    
    populate_employee_form(employee, form)
    
    return render_template('employee.html', employee=employee, form=form)

@app.route('/employee')
def show_employees():
    sql = """select emp_order, emp_no, emp_nickname, emp_fullname, emp_rank_desc, emp_start_date, emp_facebook, emp_email,
emp_addr1, emp_addr3, emp_postcode, emp_telno_mobile, emp_telno_emergency, emp_telno_home, emp_rank_class
from vwemployee
where emp_current_or_future = 1 order by emp_order"""
    
    employees = db.safeselect(sql)
    return render_template('employees.html', employees=employees)


@app.route('/amendments')
def show_amendments():
    sql = """select cf_no, cf_created, cf_bk_no, cf_action, cf_status, cf_comment, bk_pets cf_pets,
bk_cust_surname cf_surname, bk_cust_no cf_cust_no, bk_start_date cf_start_date, bk_end_date cf_end_date, cf_comment
from tblconfirm_form
join vwbooking on bk_no = cf_bk_no
order by cf_no desc
"""
    amendments = db.safeselect(sql)
    return render_template('amendments.html', amendments=amendments)

@app.route('/request')
def show_requests():
    sql = """select fd_no, fd_date_created, fd_bk_start_date, fd_bk_start_time, fd_bk_end_date, fd_bk_end_time,
fd_surname, fd_cust_no, fd_period, fd_status, fd_email, fd_open, fd_dogs, fd_cats, fd_prior_bookings
from vwformdata_quick
where (fd_open = 1 or datediff(day, fd_date_created, getdate()) <=144)
order by fd_date_created desc"""

    rows = db.safeselect(sql)
    
    requests = list(map(row2dict, rows)) if rows else []
    
    any_rt = RunType.get_by_code('any')
    assert(any_rt)
    cat_rt = RunType.get_by_code('cat')
    assert(cat_rt)
    for request in requests:
        if request['fd_open']:
            period = Period(request['fd_bk_start_date'],
                            request['fd_bk_start_time'],
                            request['fd_bk_end_date'],
                            request['fd_bk_end_time'])
            avail_dogs = get_availability(period, any_rt) if request['fd_dogs'] else 0
            avail_cats = get_availability(period, cat_rt) if request['fd_cats'] else 0
            
            avail = avail_dogs
            if avail_cats > avail:
                avail = avail_cats
        else:
            avail = -1
        request['fd_availability'] = avail
    
    return render_template('requests.html', requests=requests)

@app.route('/request/<int:fd_no>', methods=['GET', 'POST'])
def show_request(fd_no):
    book_request_form = BookRequestForm()
    change_request_customer_form = ChangeRequestCustomerForm()
    booking_form = BookingForm()
    
    request_status_list = [
        ('booked', 'Booked'),
        ('open', 'Open'),
        ('waiting', 'Waiting'),
        ('rejected', 'Rejected'),
        ('visit', 'Arrange Visit'),
        ('scheduled', 'Visit Scheduled'),
        ('duplicate', 'Duplicate'),
        ('withdrawn', 'Withdrawn'),
        ('standby', 'Standby')
    ]

    process_book_request(book_request_form)
    process_change_request_customer(change_request_customer_form)
    new_bk_no = process_booking_form(booking_form)

    if new_bk_no and new_bk_no < 0:
        return redirect(url_for('show_requests'))
    
    book_request_form.req.data = fd_no
    change_request_customer_form.fd_no.data = fd_no
    
    sql = 'select rs_code, rs_description from tblrequeststatus'
    status_choices = db.safeselect(sql)
    
    
    rts = db.safeselect_value("select dbo.fnreq_rts(?)", fd_no)
    
    if new_bk_no:
        # Possibly modify request to reflect booking
        sql = 'exec passociate_booking_with_request ?, ?'
        db.safeexec(sql, fd_no, new_bk_no)
        return redirect(url_for('show_requests'))
#        return redirect(url_for('show_booking', bk_no=new_bk_no))
    
    sql = """select fd_no, fd_period, fd_surname, fd_date_created, fd_status, fd_issues, fd_comments,
fd_bk_no, fd_bk_status, fd_cust_no, fd_html_body, fd_email, fd_pets, fd_period, fd_be_expiry, fd_tooltip,
fd_bk_start_date, fd_bk_end_date, fd_bk_start_time_slot, fd_bk_end_time_slot, fd_visit
from vwformdata_6 where fd_no = ?"""

    req = db.safeselect_one(sql, fd_no)
    comment = ''

    if not req:
        flash(f"Unable to find request {fd_no}", 'error')
        return render_template('alert.html')

    if req.fd_comments:
        comment = req.fd_comments.replace('\r\n', '<br>')
    
    
    subject = quote("Your Crowbank Booking Inquiry")
        
    body = quote(f"You inquired about booking {req.fd_pets} for the period {req.fd_period}.")
    pet_rows = db.safeselect(f"""select distinct pet_name, pet_no, pet_breed from vwformdata_pet where fd_no = ?""", fd_no)
    
    pets = []
    pet_buttons = []
    missing_pets = False
    dogs = 0
    cats = 0
    
    if pet_rows:
        i = 0
        for row in pet_rows:
            pet_no = int(row.pet_no) if row.pet_no else None
            if pet_no:
                pet = Pet.fetch(pet_no)
                pets.append(pet)
                pet_buttons.append(pet_button(pet_no))
                if len(booking_form.pets) < len(pets):
                    booking_form.pets.append_entry()
                    
                booking_form.pets[i].pet_no.data = pet.no
                booking_form.pets[i].pet_name.data = pet.name
             
                i += 1   
                if pet.is_dog:
                    dogs += 1
                else:
                    cats += 1
            else:
                breed = row.pet_breed if row.pet_breed else 'N/A'
                pet_buttons.append(f"<span>{row.pet_name} ({breed})</span>")
                missing_pets = True
    
    if req.fd_issues:
        issues = req.fd_issues.split('/')
    else:
        issues = []
    
    templates = db.safeselect("select mail_no, mail_short_desc, mail_content, mail_subject from tblmailing where mail_active = 1")
    
    booking_form.cust_no.data = req.fd_cust_no
    booking_form.req_no.data = req.fd_no
    
    booking_form.req_no = fd_no
    booking_form.date_range.data = date_range(req.fd_bk_start_date, req.fd_bk_end_date)
    booking_form.start_time.data = get_booking_time(req.fd_bk_start_date, req.fd_bk_start_time_slot, 'in')
    booking_form.end_time.data = get_booking_time(req.fd_bk_end_date, req.fd_bk_end_time_slot, 'out')
    booking_form.start_period.data = req.fd_bk_start_time_slot
    booking_form.end_period.data = req.fd_bk_end_time_slot
    start_date = req.fd_bk_start_date
    end_date = req.fd_bk_end_date
    booking_form.comment.data = req.fd_comments
    booking_form.deluxe.data = False
    booking_form.daycare.data = False
    booking_form.trial.data = False
    
    do_separate = (dogs > 1 or cats > 1)
    do_deluxe = (dogs > 0)
    do_daycare = (dogs > 0)
    
    do_trial = 0
    if do_deluxe and req.fd_bk_start_date == req.fd_bk_end_date:
        do_trial = db.safeselect_value("select dbo.fntrial_eligible(?)", req.fd_cust_no)

    availability_url = get_availability_url(rts, req.fd_bk_start_date, req.fd_bk_end_date)

    return render_template('request.html', req=req, pet_data=pets, pets=pet_buttons, issues=issues, subject=subject, body=body,
                           book_request_form=book_request_form, request_status_list=request_status_list,
                           change_customer_form=change_request_customer_form,
                           booking_form=booking_form, comment=comment, rts=rts,
                           do_separate=do_separate, do_deluxe=do_deluxe, do_trial=do_trial,
                           do_daycare=do_daycare, availability_url=availability_url, start_date=start_date,
                           end_date=end_date, email_templates=templates, missing_pets=missing_pets)


@app.route('/availability_dump')
def availability_dump():
    availability_data = get_availablity_data()

    return render_template('availability_dump.html', availability=availability_data)


# url_for('availability', run_types=rts, from_date=booking_form.start_date.data, to_date=booking_form.end_date.data )
@app.route('/availability_orig')
def availability_orig():
    start_date_arg = request.args.get('start_date')
    end_date_arg = request.args.get('end_date')
    species = request.args.get('species')
    
    if not species:
        species = 'dog'
        
    if start_date_arg:
        start_date = parse_date(start_date_arg)
    else:
        start_date = date.today()
        
    if end_date_arg:
        end_date = parse_date(end_date_arg)
    else:
        end_date = start_date + timedelta(days=7)
    
    date_range = f"{start_date:%d/%m/%Y} - {end_date:%d/%m/%Y}"
    return render_template('availability.html', date_range=date_range, species=species)

@app.route('/checkout/<int:bk_no>', methods=['GET', 'POST'])
def checkout(bk_no):
    booking = Booking.fetch(bk_no)
    pay_form = PaymentForm()
    reread = False
    
    if process_payment(pay_form):
        booking = Booking.fetch(bk_no)
        reread = True

    today = date.today()
    pay_form.booking.data = bk_no
    pay_form.amount.data = booking.invoice.outstanding
    pay_form.pay_date.data = today
    checkout_form = CheckoutForm()
    
    checkout_form.bk_no.data = bk_no
    checkout_form.checkout_date.data = today

    if process_checkout(checkout_form):
        return redirect(url_for('home_page'))
        
    if reread:
        booking = Booking.get(bk_no)

    return render_template('checkout.html', bk_no=bk_no, booking=booking, pay_form=pay_form, form=checkout_form)

@app.route('/checkin/<int:bk_no>', methods=['GET', 'POST'])
def checkin(bk_no):
    today = date.today()
    checkin_form = CheckinForm()
    
    checkin_form.checkin_date.data = today
    checkin_form.bk_no.data = bk_no

    booking = Booking.fetch(bk_no)
    reread = False

    for i in range(0, len(booking.pets)):
        if len(checkin_form.pet_forms) < len(booking.pets):
            checkin_form.pet_forms.append_entry()

        populate_pet_form_choices(checkin_form.pet_forms[i])

    if process_checkin(checkin_form):
        return redirect(url_for('home_page', ))
        
    pay_form = PaymentForm()
    if process_payment(pay_form):
        booking = Booking.fetch(bk_no)
        reread = True
        
    pay_form.booking.data = bk_no
    pay_form.pay_date.data = today

    pay_form.amount.data = booking.amt_outstanding

    if reread:
        booking = Booking.fetch(bk_no)

    pet_att = {}
    rows = db.safeselect('select cif_pet_no, cif_attachments from vwcheckin_form_attachment where cif_bk_no = ?', bk_no)
    if rows:
        for row in rows:
            pet_att[row.cif_pet_no] = row.cif_attachments.split(',')

    docs = []
    questionnaires = []
    if booking.pets:
        for i, pet in enumerate(booking.pets):
            populate_pet_form(checkin_form.pet_forms[i], pet)
            
            d, q = pet_documents(pet.no)
            docs.append(d)
            questionnaires.append(q)
    
    comment = booking.notes.replace('\r\n', '<br>')
    return render_template('checkin.html', booking=booking, pets=booking.pets, comment=comment, pay_form=pay_form,
                           form=checkin_form, docs=docs, first_pet_no=booking.pets[0].no, pet_att=pet_att,
                           questionnaires=questionnaires)

@app.route('/email_docs')
@role_required('admin')
def email_docs():
    reports = db.safeselect("select ev_view, ev_desc from tblemail_view where ev_active = 1")
    options = [{'view': e.ev_view, 'desc': e.ev_desc} for e in reports] if reports else []
    choices = dumps(options)

    return render_template('multifile.html', view_options=choices)

@app.route('/calendar')
def calendar():
    start_date = date.today()
    end_date = start_date + timedelta(7)
    sdate = start_date.strftime("%d/%m/%y")
    edate = end_date.strftime("%d/%m/%y")
    date_range = f'{sdate}-{edate}'
    return render_template('calendar.html', date_range=date_range)

@app.route('/add_pet/<int:cust_no>', methods=['GET', 'POST'])
def add_pet(cust_no):
    return pet_edit(cust_no, 0)

@app.route('/doc/<string:the_view>/<int:bk_no>')
def show_doc(the_view, bk_no):
    sql = f"select * from {the_view} where email_bk_no = ?"
    record = db.safeselect_one(sql, bk_no)
    pets = []
    if record:
        if record.email_pet_view:
            pets = db.safeselect(f'select * from {record.email_pet_view} where bk_no=?', record.email_bk_no)
            
        return render_template(record.email_template + '.html', record=record, as_email=True, pets=pets)
    else:
        flash('No email generated', 'error')
    return redirect(url_for('home_page'))

@app.route('/pet/<int:pet_no>', methods=['GET', 'POST'])
@app.route('/edit_pet/<int:pet_no>', methods=['GET', 'POST'])
def show_pet(pet_no):
    cust_no = db.safeselect_value('select pet_cust_no from pa..tblpet where pet_no=?', pet_no)
    return pet_edit(cust_no, pet_no)

def pet_edit(cust_no, pet_no):
    form = PetForm()
    
    pet_form = cast(PetSubform, form.pet_form)
    
    populate_pet_form_choices(pet_form)
    new_pet_no = process_pet_form(form)
    
    if new_pet_no < 0:
        return redirect(url_for('show_customer', customer_desc=cust_no))
    
    if new_pet_no:
        return redirect(url_for('show_pet', pet_no=new_pet_no))
    
    customer = Customer.fetch(cust_no)
    if pet_no:
        pet = Pet.fetch(pet_no)

        sql = "select pw_date, pw_weight, pw_notes from pa..tblpetweight where pw_pet_no = ? order by pw_date desc"
        weights = db.safeselect(sql, pet_no)
        
        load_pet_bookings(pet)
        bookings = sorted(pet.bookings, key=lambda b: b.start_date)

        docs, questionnaire = pet_documents(pet_no)
        meds = pet_medic(pet_no)
    else:
        pet = Pet(customer.no)
        docs = []
        questionnaire = None
        bookings = []
        meds = []
        weights = []
    
    populate_pet_form(form.pet_form, pet)
    
    return render_template('pet_edit.html', form=form, pet_no=pet_no, cust_no=cust_no, customer=customer,
                           pet=pet, docs=docs, bookings=bookings, meds=meds, weights=weights, questionnaire=questionnaire)

@app.route('/edit_booking/<int:bk_no>', methods=['GET', 'POST'])
def edit_booking(bk_no):
    cust_no = db.safeselect_value(f"select bk_cust_no from pa..tblbooking where bk_no=?", bk_no)
    rts = db.safeselect_value("select dbo.fnbk_rts(?)", bk_no)
    return booking_edit(cust_no, bk_no, rts)

@app.route('/book/<int:cust_no>', methods=['GET', 'POST'])
def book(cust_no):
    rts = db.safeselect_value(f"select dbo.fncust_rts({cust_no})")
    return booking_edit(cust_no, 0, rts)

def booking_edit(cust_no, bk_no, rts):
    form = BookingForm()

    customer = Customer.fetch(cust_no)
    new_bk_no = process_booking_form(form)
    if new_bk_no:
         return redirect(url_for('show_booking', bk_no=new_bk_no))

    booking = None
    pets = customer.pets
    form.cust_no.data = cust_no
    if bk_no:
        booking = Booking.fetch(bk_no)

        for pet in pets:
            pet.check = pet in booking.pets
        form.bk_no.data = bk_no
        
    dogs = 0
    cats = 0
    do_daycare = False
    if pets:
        for i in range(0, len(pets)):
            if pets[i].species == 'Dog':
                dogs += 1
                if pets[0].daycare_approved:
                    do_daycare = True
            if pets[i].species == 'Cat':
                cats += 1
                
            if len(form.pets) < len(pets):
                form.pets.append_entry()
            
            form.pets[i].pet_no.data = pets[i].no
            form.pets[i].pet_name.data = pets[i].name

    # form.pets.choices = [(pet.no, pet.name) for pet in pets]
    
    customer = Customer.fetch(cust_no)

    form.email.data = customer.email
    
    is_amending = 0
    amend_text = ''
    if booking:
        is_amending = booking.is_amending
        if is_amending:
            sql = "select cf_comment, cf_created from tblconfirm_form where cf_bk_no = ? and cf_action = 'amend' and cf_status = 0"
            rec = db.safeselect_one(sql, bk_no)
            assert(rec)
            comment = rec.cf_comment
            comment_time = date2words(rec.cf_created)
            amend_text = f"""{comment_time} you wrote:

"{comment}"

"""
        else:
            amend_text = f"""Regarding your Crowbank booking #{bk_no}:

"""
        form.start_time.data = booking.start_time
        form.start_period.data = booking.start_period
        form.end_time.data = booking.end_time
        form.end_period.data = booking.end_period
        form.comment.data = booking.notes
        form.deluxe.data = booking.is_deluxe
        form.daycare.data = booking.is_daycare
        form.slot.data = booking.dc_slot
        form.trial.data = booking.is_trial
        start_date = booking.start_date
        end_date = booking.end_date
    else:
        start_date = datetime.now()
        end_date = start_date
        form.start_period.data = 'am'
        form.end_period.data = 'am'
                
    availability_url = get_availability_url(rts, start_date, end_date)
    form.date_range.data = date_range(start_date, end_date)
    do_separate = (dogs > 1 or cats > 1)
    do_deluxe = (dogs > 0)
    
    do_trial = 0
    if do_deluxe:
        do_trial = db.safeselect_value("select dbo.fntrial_eligible(?)", cust_no)
    
    return render_template('book.html', form=form, customer=customer, pets=pets, do_separate=do_separate,
                           do_deluxe=do_deluxe, do_trial=do_trial, do_daycare=do_daycare, booking=booking, rts=rts,
                           availability_url=availability_url, start_date=start_date, end_date = end_date,
                           is_amending=is_amending, amend_text=amend_text, amend_rows=amend_text.count('\n'))


@app.route('/runcard/<int:bk_no>/<int:pet_no>')
def runcard(bk_no: int, pet_no: int):   
    booking = Booking.get(bk_no)
    
    pet = Pet.get(pet_no)
    
    rows = db.safeselect("select pi_type, pi_content, pi_date, pi_source from vwpetinfo_current where pi_pet_no = ?", pet_no)
    petinfo = {}
    
    if rows:
        for row in rows:
            petinfo[row.pi_type] = row.pi_content    
        
    return render_template('runcard.html', pet=pet, petinfo=petinfo, booking=booking) 

@app.route('/search', methods = ['GET'])
def search():
    weekdays = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat']
    
    content = request.args.get('search', '').replace('/', '-').strip().lower()

    m = re.fullmatch(r'\+(\d+)', content)
    if m or content in weekdays:
        if m:
            days = int(m.groups()[0])
        else:
            wd = weekdays.index(content) - 1
            wd0 = date.today().weekday()
            days = wd - wd0
            if days <= 0:
                days += 7
        new_date = date.today() + timedelta(days)
        return redirect(url_for('home_page', date_arg=new_date.strftime('%d-%m-%Y')))

    if not content.isdigit or len(content) <= 5 and is_date(content):
        date_arg = parse_date(content)
        return redirect(url_for('home_page', date_arg=date_arg.strftime('%d-%m-%Y')))
    
    if re.fullmatch(r'\d+-\d+', content):
        y = date.today().year
        candidate = f'{content}-{y}'
        if is_date(candidate):
            return redirect(url_for('home_page', date_arg=candidate))
    
    if content.isdigit():
        no = int(content)
        sql = 'select count(*) from pa..tblbooking where bk_no = ? and datediff(M, bk_end_date, getdate()) < 6'
        is_booking = db.safeselect_value(sql, no)
        if is_booking:
            return redirect(url_for('show_booking', bk_no=no))
        
        sql = 'select count(*) from pa..tblcustomer where cust_no = ?'
        is_customer = db.safeselect_value(sql, no)
        if is_customer:
            return redirect(url_for('show_customer', customer_desc=no))
        
        sql = 'select count(*) from pa..tblpet where pet_no = ?'
        is_pet = db.safeselect_value(sql, no)
        if is_pet:
            return redirect(url_for('show_pet', pet_no=no))
        
        sql = 'select count(*) from tblformdata_6 where fd_no = ?'
        is_request = db.safeselect_value(sql, no)
        if is_request:
            return redirect(url_for('show_request', fd_no=no))
  
    sql = 'select cust_no, priority, priority_desc, bk_no, visit_no from dbo.fnfind_customers_prioritized(?)'
    res = db.safeselect(sql, content)
    
    if not res:
        flash('Could not find anything', 'error')
        return redirect(url_for('home_page'))
    
    if len(res) == 1:
        cust = res[0]
        if cust.bk_no:
            return redirect(url_for('show_booking', bk_no=cust.bk_no))
        if cust.visit_no:
            return redirect(url_for('show_visit', visit_no=cust.visit_no))
        pet_no = db.safeselect_value('select pet_no from pa..tblpet where pet_name = ? and pet_cust_no = ?', content, cust.cust_no)
        if pet_no:
            return redirect(url_for('show_pet', pet_no=pet_no))
        return redirect(url_for('show_customer', customer_desc=cust.cust_no))
    
    results = []
    for row in res:
        sql = """select cust_surname, cust_forename, bk_start_date, bk_end_date, visit_date
from pa..tblcustomer
left join pa..tblbooking on bk_cust_no = cust_no and bk_no = ?
left join vwvisit on visit_cust_no = cust_no and visit_no = ?
where cust_no = ?"""
        details = db.safeselect_one(sql, row.bk_no, row.visit_no, row.cust_no)
        assert(details)
        sql = "select pet_no from pa..tblpet where pet_deceased <> 'Y' and pet_cust_no = ?"
        pet_nos = db.safeselect(sql, row.cust_no)
        pets = [Pet.get(p.pet_no) for p in pet_nos] if pet_nos else []
        
        results.append({
            'priority': row.priority,
            'priority_desc': row.priority_desc,
            'cust_no': row.cust_no,
            'cust_surname': details.cust_surname,
            'cust_forename': details.cust_forename,
            'pets': pets,
            'bk_no': row.bk_no,
            'bk_start_date': details.bk_start_date,
            'bk_end_date': details.bk_end_date,
            'visit_no': row.visit_no,
            'visit_date': details.visit_date
        })
    return render_template('search_results.html', results=results)

@app.route('/checkin_report')
def checkin_report():
    start_date_arg = request.args.get('start_date')
    end_date_arg = request.args.get('end_date')
    species = request.args.get('species')
    
    if start_date_arg:
        start_date = parse_date(start_date_arg)
    else:
        start_date = date.today()
        
    if end_date_arg:
        end_date = parse_date(end_date_arg)
    else:
        end_date = start_date + timedelta(days=7)
    
    if not species:
        species = 'dog'
    
    date_range = f"{start_date:%d/%m/%Y} - {end_date:%d/%m/%Y}"
    return render_template('checkin_report.html', date_range=date_range, species=species)

@app.route('/checkout_report')
def checkout_report():
    start_date_arg = request.args.get('start_date')
    end_date_arg = request.args.get('end_date')
    species = request.args.get('species')
    
    if start_date_arg:
        start_date = parse_date(start_date_arg)
    else:
        start_date = date.today()
        
    if end_date_arg:
        end_date = parse_date(end_date_arg)
    else:
        end_date = start_date + timedelta(days=7)
    
    if not species:
        species = 'dog'
    
    date_range = f"{start_date:%d/%m/%Y} - {end_date:%d/%m/%Y}"
    return render_template('checkout_report.html', date_range=date_range, species=species)

@app.route('/add_event', methods=['GET', 'POST'])
@app.route('/add_event/<string:datearg>', methods=['GET', 'POST'])
def add_event(datearg = ''):
    if datearg:
        d = parse_date(datearg)
    else:
        d = None
    return visit_edit(None, 0, d)

@app.route('/edit_visit/<int:visit_no>', methods=['GET', 'POST'])
def edit_visit(visit_no):
    return visit_edit(None, visit_no)

@app.route('/pricetable')
def show_pricetable():
    def date2rates(d):
        sql = """with rates (tense, occ_count, r_desc, r_srv_rate) as
    (select iif(r_date is null, 'current', 'future'), 1, iif(srv_code = 'DELUXE', 'Deluxe', isnull(spec_desc, billcat_desc)), r_srv_rate
    from pa..tblrate
    join pa..tblservice on srv_no = r_srv_no
    left join pa..tblspecies on spec_no = r_key and r_billtype='A'
    left join pa..tblbillcategory on billcat_no = r_key and r_billtype='C'
    where r_srv_rate > 0 and r_peak = '' and srv_code in ('DELUXE', 'BOARD')
    union all
    select iif(r_date is null, 'current', 'future'), 3, iif(srv_code = 'DELUXE2', 'Deluxe', isnull(spec_desc, billcat_desc)), r_srv_rate
    from pa..tblrate
    join pa..tblservice on srv_no = r_srv_no
    left join pa..tblspecies on spec_no = r_key and r_billtype='A'
    left join pa..tblbillcategory on billcat_no = r_key and r_billtype='C'
    where r_srv_rate > 0 and r_peak = '' and srv_code in ('DELUXE2', 'BOARD2'))
    select lower(r_desc) r_desc, sum(iif(occ_count=1, r_srv_rate, 0)) single, sum(r_srv_rate) [double]
    from rates where tense = ?
    group by lower(r_desc)"""
        
        rows = db.safeselect(sql, d)
        day_rates = {}
        
        if rows:
            for row in rows:
                day_rates[row.r_desc.lower()] = (row.single, row.double)
        
        rate_table = {
            i: {
                k: (v[0] * i, v[1] * i)
                for (k, v) in day_rates.items()            
            } for i in range(1, 22)
        }
        return rate_table
        
    current_rates = date2rates('current')
    future_rates = date2rates('future')
       
    return render_template('pricetable.html', current=current_rates, future=future_rates)

@app.route('/daily')
def show_daily():
    base = 'intranet/static/daily'
    html = ''
    latest_path = ''
    latest_name = ''
    for x in scandir(base):
        h, latest_path, latest_name = leaf2html(x)
        html += h
    return render_template('daily.html', folders=html, latest_path=latest_path, latest_name=latest_name)

@app.route('/runview')
def runview():
    start_date_arg = request.args.get('start_date')
    end_date_arg = request.args.get('end_date')
    species = request.args.get('species')
    
    if start_date_arg:
        start_date = parse_date(start_date_arg)
    else:
        start_date = date.today()
        
    if end_date_arg:
        end_date = parse_date(end_date_arg)
    else:
        end_date = start_date + timedelta(days=7)
    
    if not species:
        species = 'dog'
    
    date_range = f"{start_date:%d/%m/%Y} - {end_date:%d/%m/%Y}"
    return render_template('runview.html', date_range=date_range, species=species)

@app.route('/outstanding')
def outstanding_report():
    sql = """select bk_no from vwoutstanding order by bk_end_date"""
    booking_records = db.safeselect(sql)
    
    bookings = {}
    
    total = Decimal(0.0)
    
    if booking_records:
        for record in booking_records:
            booking = Booking.fetch(record.bk_no)
            bookings[record.bk_no] = booking
            total += booking.amt_outstanding
        
    sql = 'select hist_no, hist_bk_no, hist_date, hist_subject, hist_msg from vwoutstanding_history'
    rows = db.safeselect(sql)
    
    histories = defaultdict(list)
    if rows:
        for h in rows:
            if h.hist_bk_no in bookings:
                histories[h.hist_bk_no].append({
                    'date': h.hist_date,
                    'subject': h.hist_subject,
                    'msg': h.hist_msg
                })
        
    return render_template('outstanding_report.html', bookings=bookings, total=total, histories=histories)

@app.route('/allocate_table')
def allocate_table():
    return allocate('table')

@app.route('/allocate')
def allocate(style='map'):
    start_date_arg = request.args.get('start_date')
    end_date_arg = request.args.get('end_date')
    
    if start_date_arg:
        start_date = parse_date(start_date_arg)
    else:
        start_date = date.today()
        
    if end_date_arg:
        end_date = parse_date(end_date_arg)
    else:
        end_date = start_date + timedelta(days=7)
    
#    date_range = f"{start_date:%d/%m/%Y} - {end_date:%d/%m/%Y}"
    return render_template('allocate.html', allocate_style=style, start_date=f"{start_date:%d/%m/%Y}", end_date=f"{end_date:%d/%m/%Y}")

@app.route('/checkin_forms')
def checkin_forms():
    sql = """select cif_bk_no, cif_bk_status, cif_bk_start_date, cif_cust_no, cif_cust_surname, cif_pet_no, cif_pet_name, cif_pet_breed, cif_pet_species,
cif_prior_bookings, iif(cif_no is null, 0, 1) cif_form, iif(cif_attachments is null, 0, 1) cif_attachments
from vwcheckin_detail_all where cif_imminent = 1"""

    rows = db.safeselect(sql)
    dates = {}
    if rows:
        for row in rows:
            if row.cif_bk_start_date not in dates.keys():
                dates[row.cif_bk_start_date] = []
            dates[row.cif_bk_start_date].append(row)
    
    return render_template('checkin_forms.html', dates=dates)

@app.route('/checkin_form/<int:bk_no>/<int:pet_no>')
def checkin_form(bk_no, pet_no):
    form = checkin_form_form(bk_no, pet_no)
    if form:
        return render_template('checkin_form.html', form=form)
    else:
        return 'Form not found'

@app.route('/logout')
@login_required
def logout():
    logout_user()
    flash('Logged Out')
    return redirect(url_for('home_page'))

@app.route('/login', methods=['GET', 'POST'])
def login():
    # Here we use a class of some kind to represent and validate our
    # client-side form data. For example, WTForms is a library that will
    # handle this for us, and we use a custom LoginForm to validate.
    form = LoginForm()
    if form.validate_on_submit():
        # Login and validate the user.
        # user should be an instance of your `User` class
        user = EmployeeUser.get_by_name(form.name.data)
        if user and user.check_pwd(form.password.data):
            login_user(user)

            flash('Logged in successfully.')

            next = request.args.get('next')

            return redirect(next or url_for('home_page'))
        else:
            flash('Login Failed', 'error')
    return render_template('login.html', form=form)


@app.route('/password/<int:user_id>', methods = ['GET', 'POST'])
@role_required('admin')
def reset_password(user_id):
    return password(user_id)


@app.route('/password', methods=['GET', 'POST'])
@login_required
def password(user_id = 0):
    user = current_user
    cu = cast(EmployeeUser, current_user)

    if user_id and cu.is_authenticated and cu.role >= User.roles['admin']:
        user = EmployeeUser.get(user_id)
        
    form = PasswordForm()
    if form.validate_on_submit():
        if user_id or cu.check_pwd(form.current_pwd.data):
            cu.update_pwd(form.new_pwd.data)
            flash('Password Updated', 'info')
            return redirect(url_for('home_page'))
        flash('Wrong Password', 'error')
    
    return render_template('password.html', form=form, user=user)

@app.route('/new-dog-questionnaire')
def new_dog_questionnaires():
    sql = """select ndf_no, ndf_open, ndf_date_created, ndf_first_date, ndf_cust_no,
ndf_surname, ndf_pet_no, ndf_pet_name, ndf_status, ndf_visit_status, ndf_breed_desc, ndf_visit_desc, ndf_visit_no,
ndf_health, ndf_strangers, ndf_behaviour
from vwquestionnaire
order by ndf_first_date"""
    questionnaires = db.safeselect(sql)
    return render_template('questionnaires.html', questionnaires=questionnaires)

@app.route('/new-dog-questionnaire/<int:ndf_no>')
def new_dog_questionnaire(ndf_no):
    sql = """select ndf_no, ndf_open, ndf_date_created, ndf_cust_no,
ndf_surname, ndf_pet_no, ndf_pet_name, ndf_status, ndf_visit_status, ndf_breed_desc,
ndf_first_date, ndf_bk_no, ndf_visit_no, ndf_visit_date, ndf_cust_email, ndf_pet_visit,
ndf_pet_notes, ndf_pet_warning, ndf_visit_desc
from vwquestionnaire where ndf_no = ?"""
    questionnaire = db.safeselect_one(sql, ndf_no)
    
    notes = questionnaire.ndf_pet_notes if questionnaire else ''
    if notes:
        notes = notes.replace('\\n', '\n').lstrip()
    
    fields = questionnaire_fields(ndf_no)
    
    email_subject = f"""Thank you for completing our New Dog Questionnaire for {questionnaire.ndf_pet_name}.


""" if questionnaire else ''
    
    return render_template('questionnaire.html', notes=notes, questionnaire=questionnaire, fields=fields, email_subject=email_subject)

@app.route('/visit_request')
def visitrequest():
    sql = """select visit_no, visit_cust_no, visit_cust_surname, visit_cust_email, visit_start_date,
visit_kt, visit_ct, visit_mg, visit_duration, visit_status,
visit_pets, visit_pet_nos, visit_open, visit_desc, visit_task_no, visit_bk_status,
visit_notes
from vwvisit_request order by isnull(visit_start_date, '20991231')"""

    rows = db.safeselect(sql)
    
    requests = []
    if rows:
        for row in rows:
            request = row2dict(row)
            request['pets'] = [pet_request_html(pet_no) for pet_no in request['visit_pet_nos'].split(',')]
            requests.append(request)
        
    return render_template('visitrequests.html', requests=requests)

@app.route('/availability')
def availability():
    start_date_arg = request.args.get('start_date')
    end_date_arg = request.args.get('end_date')
    spec_no = request.args.get('spec_no')
    
    species = 'cat' if spec_no == 2 else 'dog'
        
    if start_date_arg:
        start_date = parse_date(start_date_arg)
    else:
        start_date = date.today()
        
    if end_date_arg:
        end_date = parse_date(end_date_arg)
    else:
        end_date = start_date + timedelta(days=7)
    
    date_range = f"{start_date:%d/%m/%Y} - {end_date:%d/%m/%Y}"
    return render_template('availability_new.html', date_range=date_range, species=species)

@app.route('/weight_report')
def weight_report():
    weight_data = get_weight_report_data()
    weights = weight_data['weights']
    most_weights = weight_data['most_weights']

    return render_template('weightreport.html', weights=weights, most_weights=most_weights)

@app.route('/check_availability')
def check_availability():
    form = CheckAvailabilityForm()
    return render_template('check_availability.html', form=form)

@app.route('/visits')
def show_visits():
    return render_template('visits.html')

@app.route('/show_visit/<int:visit_no>', methods=['GET', 'POST'])
def show_visit(visit_no):
    cust_no = db.safeselect_value('select visit_cust_no from vwvisit where visit_no = ?', visit_no)
    return visit_edit(cust_no, visit_no)

@app.route('/create_visit/<int:cust_no>', methods=['GET', 'POST'])
def create_visit(cust_no):
    return visit_edit(cust_no, 0)

def visit_edit(cust_no, visit_no, d = None):
    form = VisitForm()
    
    new_visit_no = int(process_visit_form(form))
    
    visit_date = d
    if new_visit_no < 0:
        if form.visit_date.data:
            visit_date = form.visit_date.data
        url = url_for('home_page', date_arg=visit_date)
        return redirect(url)
        
    if new_visit_no:
        visit_no = new_visit_no

    if visit_no and not cust_no:
        cust_no = db.safeselect_value(f"select visit_cust_no from vwvisit where visit_no=?", visit_no)

    pets = None
    customer = None
    upcoming_bookings = None
    if cust_no:
        customer = Customer.fetch(cust_no)
        
        sql = 'select bk_no from vwupcoming_bookings where bk_cust_no = ? order by bk_start_date'
        upcoming_bookings_rows = db.safeselect(sql, cust_no)
        upcoming_bookings = [Booking.fetch(r.bk_no) for r in upcoming_bookings_rows] if upcoming_bookings_rows else []
        
        pet_nos = db.safeselect("""select pet_no
from pa..tblpet
join pa..tblbreed on breed_no = pet_breed_no
where pet_cust_no = ? and pet_deceased <> 'Y' and
pet_no not in (select distinct bi_pet_no from pa..tblbooking
join pa..tblbookingitem on bi_bk_no = bk_no
where bk_status in ('', 'V') and bk_start_date < getdate()
and bk_cust_no = ?)
""", cust_no, cust_no)
        if pet_nos:
            pets = [pet_button(r.pet_no) for r in pet_nos]
        
    form.cust_no.data = cust_no
    form.visit_date.data = visit_date
    
    if visit_no:
        sql = """select visit_no, visit_date, visit_time, visit_duration, visit_cust_no,
        visit_subject, visit_notes, visit_kt, visit_ct, visit_mg, visit_dca from vwvisit where visit_no = ?"""
        visit = db.safeselect_one(sql, visit_no)
        assert(visit)
        form.visit_no.data = visit_no
        form.visit_date.data = visit.visit_date
        form.visit_time.data = visit.visit_time
        form.duration.data = visit.visit_duration
        form.subject.data = visit.visit_subject
        form.kennel_tour.data = visit.visit_kt
        form.cattery_tour.data = visit.visit_ct
        form.meet_and_greet.data = visit.visit_mg
        form.daycare_assessment.data = visit.visit_dca
        form.notes.data = visit.visit_notes
    else:
        visit = None

    return render_template('visit.html', visit=visit, customer=customer, form=form, upcoming_bookings=upcoming_bookings, pets=pets)

@app.route('/punches')
@role_required('manager')
def punches():
    sql = """select punch_no, punch_emp_no, punch_emp_nickname, punch_emp_fullname, punch_date, punch_time, punch_actual_time,
punch_comment, punch_image, punch_wagepayment, punch_emp_order
from vwpunch order by punch_date, punch_emp_order, punch_time"""

    punch_recs = db.safeselect(sql)
    punches = {}
    
    if punch_recs:
        for punch in punch_recs:
            if punch.punch_date not in punches:
                punches[punch.punch_date] = {}
            if punch.punch_emp_nickname not in punches[punch.punch_date]:
                punches[punch.punch_date][punch.punch_emp_nickname] = []
            punches[punch.punch_date][punch.punch_emp_nickname].append({
                'time': punch.punch_time,
                'actual_time': punch.punch_actual_time,
                'comment': punch.punch_comment,
                'image': punch.punch_image
            })
    
    return render_template('pages/punches.html', punches=punches)

@app.route('/unconfirmed_report')
def unconfirmed_report():
    sql = """select bk_no, convert(date, bk_create_date) bk_create_date, convert(date, bk_start_date) bk_start_date,
convert(date, bk_end_date) bk_end_date, cust_no, cust_surname, cust_forename,
cust_email, cust_telno_home, cust_telno_mobile, string_agg(pet_name + ' (' + breed_desc + ')', ', ') pets,
isnull(m_c, 0) priors, convert(varchar(MAX), bk_memo) bk_memo
from
pa..tblbooking
join pa..tblbookingitem on bi_bk_no = bk_no
join pa..tblcustomer on cust_no = bk_cust_no
join pa..tblpet on pet_no = bi_pet_no
join pa..tblbreed on breed_no = pet_breed_no
left join (select bk_cust_no m_cust_no, count(*) m_c from pa..tblbooking where bk_Status in ('', 'V') and bk_start_date <= getdate() group by bk_cust_no) m on m_cust_no = bk_cust_no
where bk_status = ''
and bk_start_date > getdate() and datediff(day, bk_create_date, getdate()) >= 7 
group by cust_Email, cust_telno_home, cust_no, m_c, bk_no, convert(varchar(MAX), bk_memo), bk_Create_date, bk_start_date, bk_end_date, cust_Surname, cust_forename, cust_telno_mobile
order by bk_start_date
"""

    rows = db.safeselect(sql)
    return render_template('unconfirmed_report.html', rows=rows)

@app.route('/allocation_issue_report')
def allocation_issue_report():
    sql = """select iss_type, iss_bk_no, iss_run, iss_hours, convert(date, min(iss_date)) iss_min_date,
convert(date, max(iss_date)) iss_max_date, count(*) iss_count
from vwins_issue
join pa..tblbooking on bk_no = iss_bk_no
join pa..tblcustomer on cust_no = bk_cust_no
where iss_date >= getdate()
and not (iss_type = 'conflict' and iss_hours > 0.5)
and not (bk_day = 'Y' and iss_type <> 'conflict') and cust_surname <> 'Yehudai'
group by iss_type, iss_run, iss_hours, iss_bk_no
order by min(iss_date)"""

    rows = db.safeselect(sql)
    data = []
    if rows:
        for row in rows:
            d = row2dict(row)
            d['iss_min_date'] = d['iss_min_date'].strftime('%y-%m-%d')
            d['iss_max_date'] = d['iss_max_date'].strftime('%y-%m-%d')
            booking = Booking.fetch(int(row.iss_bk_no))
            pets = ', '.join([pet.description for pet in booking.pets
                            if pet.is_dog])
            d['pets'] = pets
            data.append(d)
    
    return render_template('pages/allocation_issue_report.html', data=data)

@app.route('/mark_request/<int:fd_no>/<string:status>')
def mark_request(fd_no, status):
    sql = "Execute pupdate_form_status ?, ?, ?, ?"
    
    db.safeexec(sql, fd_no, status, '', 'intranet')
    
    new_status = db.safeselect_value('select fd_status from tblformdata_6 where fd_no=?', fd_no)
    if new_status != status:
        flash('Status change failed', 'error')
    else:
        flash(f'Status changed to {status}', 'info')
    return redirect(url_for('show_requests'))

@app.route('/notice/<string:view_name>/<int:key_no>')
def notice(view_name, key_no):
    view = db.safeselect_value('select ev_view from tblemail_view where ev_name = ?', view_name)
    if not view:
        return 'View not found'
    html = get_notice_html(view, key_no, '', False)
    return Response(html, mimetype='text/html')

@app.route('/cashflow_report')
def cashflow_report():
    start_date = date.today()
    # end_date is the last day of the current month
    end_date = date(start_date.year, start_date.month, 1) + relativedelta(months=1) - timedelta(days=1)
    date_range = f"{start_date:%d/%m/%Y} - {end_date:%d/%m/%Y}"    
    return render_template('pages/cashflow_report.html', date_range=date_range)

@app.route('/upload_photo')
def upload_photo():
    return render_template('/pages/upload_photo.html')

@app.route('/upload', methods=['POST'])
@csrf.exempt
def upload_file():
    settings = get_settings()
    image_folder = settings['IMAGE_FOLDER']

    if 'photo' not in request.files:
        return redirect(request.url)
    file = request.files['photo']
    if file.filename == '':
        return redirect(request.url)
    if file:
        # Save the file to the server or process it as needed
        file.save(f"{image_folder}{file.filename}")
    return redirect('/')
    
@app.route('/note/<int:note_no>')
@app.route('/note')
def show_note(note_no: int = 0):
    sql = 'select pi_pet_no from vwpets_in where pi_date = ?'    
    pets = []
    rows = db.safeselect(sql, date.today())
    if rows:
        for row in rows:
            pet = Pet.fetch(row.pi_pet_no)
            pets.append(pet)
            
    sorted_pets = sorted(pets, key = lambda p: p.full_description)
    emps = get_employees(date.today())
    types = NoteType.get_all()
    
    note = Note.fetch(note_no) if note_no else None
    return render_template('/components/note.html', pets=sorted_pets, emps=emps, types=types, today=date.today(), now=datetime.now(), note=note)

@app.route('/pet_notes/<int:pet_no>')
def pet_notes(pet_no):
    pet = Pet.fetch(pet_no)
    notes = Note.notes_by_pet(pet)
    return render_template('/pages/pet_notes.html', pet=pet, notes=notes)

@app.route('/daily_notes')
@app.route('/daily_notes/<string:date_arg>')
def daily_notes(date_arg = None):
    if date_arg:
        the_date = parse_date(date_arg)
    else:
        the_date = date.today()

    notes = Note.notes_by_date(the_date)
    
#    return render_template('pages/daily_notes.html', the_date=the_date, notes=notes)
    return render_template('pages/note_table.html', the_date=the_date, notes=notes)

@app.route('/test')
def test():
    return render_template('pages/test.html')
@app.route('/daycare_candidates')
def show_daycare_candidates():
    show_all = int(request.values.get('show_all', 0))
    refresh = int(request.values.get('refresh', 0))
    
    if refresh:
        DaycareCandidate.reset()
    dccs = DaycareCandidate.get_all() if show_all else DaycareCandidate.get_active()
    return render_template('pages/daycare_candidates.html', dccs=dccs, show_all=show_all)

@app.route('/daycare_candidate/<int:dcc_no>')
def show_daycare_candidate(dcc_no: int):
    dcc = DaycareCandidate.fetch(dcc_no)
    statuses = DaycareCandidateStatus.get_all()
    return render_template('pages/daycare_candidate.html', dcc=dcc, statuses=statuses)

@app.route('/book_daycare')
def book_daycare():
    runs = []
    dogs = []

    pet_no = int(request.values.get('pet_no', 0))
    date_arg = request.values.get('date', '')
    
    date = None
    if date_arg:
        date = parse_date(date_arg)
        runs = get_daycare_runs(date)

    if not pet_no:
        dogs = get_daycare_dogs(date)
    
    if pet_no:
        pet = Pet.fetch(pet_no)
        dogs = [pet]    
    
    date_iso = date.isoformat() if date else ''
    return render_template('pages/book_daycare.html', dogs=dogs, date_iso=date_iso, runs=runs)

@app.route('/debug')
def debug_route():
    print(app.jinja_env.list_templates())
    return "Check your console."