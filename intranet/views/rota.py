from intranet import app
from intranet.forms import RotaConfirmForm
from intranet.utilities import date2weekstart
from flask_login import current_user
from flask import flash, render_template, request, jsonify
from datetime import date, timedelta
from utils import db, parse_date
from models.employee import Employee

def process_confirm(form):
    if (form.accept.data or form.decline.data) and form.validate() and current_user.is_authenticated:
        
        emp_no = form.employee.data if form.employee.data else current_user.id

        confirm = 1 if form.accept.data else 0
        
        sql = 'exec protaconfirm ?, ?, ?, ?'
        db.safeexec(sql, emp_no, form.weekstart.data, confirm, form.comment.data)
        
        sql = """select emp_nickname, rc_weekstart, rc_confirm, rc_shifts
from vwrotaconfirm join tblemployee on emp_no = rc_emp_no
where rc_weekstart = ? and emp_no = ?"""
        status = db.safeselect_one(sql, form.weekstart.data, emp_no)
        
        if not status:
            flash('Confirmation Failed', 'error')
            return
        
        msg = f"""{status.emp_nickname} {'Accepted' if status.rc_confirm else 'Declined'} {status.rc_shifts} shifts for week of {status.rc_weekstart.strftime('%d/%m')}"""
        print(msg)
        flash(msg, 'info')
        return True
        
    return False

def get_shift_confirm_status(start_date, id):
    if not id:
        return {'class_': '', 'msg': ''}
    
    sql = 'select rc_shifts, rc_confirm, rc_changed, rc_timestamp, rc_status from vwrotaconfirm where rc_emp_no=? and rc_weekstart=?'
    status_rec = db.safeselect_one(sql, id, start_date)
    
    if not status_rec:
        return {'class_': '', 'msg': 'Rota never reviewed'}

    action = 'accepted' if status_rec.rc_confirm else 'declined'
    
    msg = f'Rota has{"" if status_rec.rc_changed else " NOT"} changed since {action} on {status_rec.rc_timestamp.strftime("%d/%m at %H:%M")} '

    if status_rec.rc_changed:
        msg += f"<br>{action.capitalize()} shifts were {status_rec.rc_shifts}"
        
    return {'class_': status_rec.rc_status, 'msg': msg}


@app.route('/rota/_toggle_status')
def toggle_status():
    toggle_date = parse_date(request.values.get('toggle_date', ''))
    emp_no = int(request.values.get('emp_no', 0))
    status = request.values.get('status')
    
    sql = 'select ts_am + ts_pm status from vwtimesheet where ts_emp_no = ? and ts_date = ?'
    current_status = db.safeselect_value(sql, emp_no, toggle_date)
    
    if current_status in ('X ', ' X', 'XX'):
        return jsonify({
            'status': 'error',
            'msg': 'You may have already been assigned to work on this day. Please contact Crowbank.'
        })
    
    available = 0 if status == 'AA' else 1
    
    sql = 'exec ptoggle_availability ?, ?, ?, ?'
    db.safeexec(sql, emp_no, toggle_date, available, f'Changed by {current_user.id}')
    
    weekstart = date2weekstart(toggle_date)
    return personal_rota(weekstart, emp_no)

@app.route('/rota/_personal_rota')
def personal_rota_view():
    weekstart = parse_date(request.values.get('start_date', ''))
    emp_no = int(request.values.get('emp_no', 0))

    return personal_rota(weekstart, emp_no)

def personal_rota(weekstart, emp_no):
    if emp_no:
        sql = 'select ts_date, ts_am + ts_pm status from vwtimesheet_published where ts_emp_no = ? and ts_weekstart = ? order by ts_date'
        recs = db.safeselect(sql, emp_no, weekstart)
        rota_data = {rec.ts_date: rec.status for rec in recs}
        html = render_template('rota/_personal_rota.html', rota_data=rota_data)
        shift_confirm_status = get_shift_confirm_status(weekstart, emp_no)
        employee = Employee.get(emp_no)
    
    else:
        html = 'Error: Missing Employee Number'
        shift_confirm_status = None
        employee = None

    return jsonify({
            'status': 'success',
            'html': html,
            'shift_confirm_status': shift_confirm_status,
            'employee': {
                'emp_no': employee.no,
                'forename': employee.forename,
                'surname': employee.surname                
            } if employee else None
        })

@app.route('/rota', methods = ['GET', 'POST'])
@app.route('/rota/<string:date_arg>', methods = ['GET', 'POST'])
@app.route('/rota/<string:date_arg>/<int:emp_no>', methods = ['GET', 'POST'])
def rota_view(date_arg = None, emp_no = 0):
    """Display one person's weekly rota in a table, allowing unavailable/unavailable to be switched for unallocated days only.
Manager and above can select an employee, defaulting to themselves."""

    form = RotaConfirmForm()
    if date_arg:
        the_date = parse_date(date_arg)
    else:
        the_date = date.today()

    weekstart = date2weekstart(the_date)
    weekend = weekstart + timedelta(6)
    dates = [weekstart + timedelta(i) for i in range(7)]
    form.weekstart.data = weekstart.strftime('%y%m%d')
    
    sql = 'select emp_no, emp_forename, emp_surname from tblemployee where emp_iscurrent=1 and emp_rank_no not in (0, 8) order by emp_forename, emp_surname'
    recs = db.safeselect(sql)
    emps = [(emp.emp_no, f'{emp.emp_forename} {emp.emp_surname}') for emp in recs]
    emp_choices = [(0, ''), *emps]
    form.employee.choices = emp_choices

    if emp_no and not (current_user.is_authenticated and current_user.has_role('manager')):
        emp_no = 0
    if not emp_no:
        emp_no = current_user.id if current_user.is_authenticated else 0
    
    process_confirm(form)
    data = {
        'emp_no': emp_no,
        'start_date': weekstart,
        'end_date': weekend,
        'next_week': (weekstart + timedelta(7)).strftime('%d-%m-%y'),
        'prev_week': (weekstart - timedelta(7)).strftime('%d-%m-%y')
    }
    
    return render_template('rota/rota.html', start_date=weekstart, end_date=weekend, confirm_form=form,
                           dates=dates, data=data)
