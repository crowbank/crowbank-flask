from intranet import app, h2t
from utils import db, send_email, parse_date
from logic import send_customer_email, get_cashflow_report
from flask import render_template, url_for, jsonify, request
from flask_login import current_user
from datetime import datetime, timedelta, date
from decimal import Decimal
from intranet.utilities import  pet_button, row2dict, day_events_html, checkin_form_form, day_emp_html
from intranet.utilities import weekly_shifts_common, email_docs, get_employees
from intranet.runview import ro_display
from intranet.user import User
import css_inline
from dateutil.parser import parse
from models import Period, Note, NoteType, Pet
from models.availability import get_availablity_data, get_availability
from models.runtype import RunType
from models.note import NoteSeverity
from typing import cast, List, Optional, Dict

@app.route('/display_email/<int:hist_no>/<string:source>')
@app.route('/display_email/<int:hist_no>')
def display_email(hist_no, source='history'):
    if source == 'history':
        sql = 'select hist_msg from pa..tblhistory where hist_no = ?'
    else:
        sql = 'select mail_content from tblmailing join tblcustmailing on cm_mail_no = mail_no and cm_no = ?'
    html = db.safeselect_value(sql, hist_no)
    if html:
        return html
    return ''

@app.route('/_availability_vector')
def availability_vector():
    start_date = parse_date(request.values.get('start_date', ''))
    end_date = parse_date(request.values.get('end_date', ''))
    start_period = request.values.get('start_period', 'am')
    end_period = request.values.get('end_period', 'am')
    rts_desc = request.values.get('rts', 'any').lower().split(',')
    rts = list(map(RunType.get_by_code, rts_desc))
    assert(rts)
    rts_0 = rts[0]
    assert(rts_0)
    period = Period(start_date, start_period, end_date, end_period)

    availability = get_availablity_data(period, rts)
    overall_availability = get_availability(period, rts_0)
    run_type = list(rts)[0]
    assert(run_type)
    overall_availability = get_availability(period, run_type)
    
    date_count = period.date_count()
    html = render_template('components/_availability_vector.html', availability=availability, date_count=date_count,
                           overall_availability=overall_availability, slots=period.slots(), rts_desc=rts_desc)
    return jsonify(html=html)    

@app.route('/_availability_table')
def availabililty_table():
    start_date: date = parse_date(request.values.get('start_date', ''))
    end_date: date = parse_date(request.values.get('end_date', ''))
    daycare: bool = request.values.get('daycare', 'false') == 'true'
    standard: bool = request.values.get('standard', 'true') == 'true'
    double: bool = request.values.get('double', 'false') == 'true'
    deluxe: bool = request.values.get('deluxe', 'false') == 'true'
    home: bool = request.values.get('home', 'false') == 'true'
    cattery = request.values.get('cattery', 'false') == 'true'
    species: str = 'Cattery' if cattery else 'Kennels'

    rts_array = []
    if daycare:
        rts_array.append('daycare')
    if standard:
        rts_array.append('standard')
    if double:
        rts_array.append('double')
    if deluxe:
        rts_array.append('deluxe')
    if home:
        rts_array.append('home')
    if cattery:
        rts_array.append('cat')
        
    rts = ','.join(rts_array)

    sql = 'select a_date, a_period, a_code, a_taken, a_available, a_unallocated from dbo.fnavailability_table(?, ?, ?) order by a_date, a_period'
    avail = db.safeselect(sql, start_date, end_date, rts)
    
    html = render_template('components/_availability_table.html', avail=avail, species=species)
    return jsonify(html=html)

@app.route('/_email_docs')
def _email_docs():
    view: str = request.values.get('view', '')
    key_value: str = request.values.get('key_no', '')
    key_no: int = int(key_value) if key_value else 0
    email:str = request.values.get('address', '')
    reply_text: str = request.values.get('text', '')
    is_text: bool = request.values.get('is_text', 'false') == 'true'

    html = email_docs(view, key_no, email, reply_text, is_text)
            
    return jsonify(html=html)

@app.route('/_calendar')
def _calendar():
    start_date = request.values.get('start_date')
    end_date = request.values.get('end_date')
    
    assert(start_date)
    assert(end_date)
    
    dates = []
    sdate = parse_date(start_date)
    edate = parse_date(end_date)
    d = sdate
    while d < edate:
        dates.append(d.strftime("%d/%m/%y"))
        d = d + timedelta(1)
    dates.append(d.strftime("%d/%m/%y"))
    
    sql = 'select slot_no, slot_desc, left(slot_start, 5) slot_start, slot_class from tbltimeslot order by slot_desc'
    slots = db.safeselect(sql)

    events = {}
    
    for d in dates:
        events[d] = {}
        if slots:
            for slot in slots:
                events[d][slot.slot_no] = {}
                events[d][slot.slot_no]['class'] = slot.slot_class
                events[d][slot.slot_no]['events'] = []
            
    sql = """select visit_no, visit_date, visit_time, visit_desc, visit_slot,
visit_slot_no, visit_type, visit_tooltip, visit_bk_no, visit_first_slot, visit_short_desc
from vwvisitor where visit_date between ? and ?"""
    
    recs = db.safeselect(sql, start_date, end_date)
    if recs:
        for rec in recs:
            desc = rec.visit_short_desc
            if rec.visit_type in ('in', 'out', 'transport'):
                url = url_for('show_booking', bk_no=rec.visit_bk_no)
            else:
                url = url_for('show_visit', visit_no=rec.visit_no)
            
            event = { 'url': url, 'desc': desc, 'tooltip': rec.visit_tooltip }
            events[rec.visit_date.strftime("%d/%m/%y")][rec.visit_slot_no]['events'].append(event)

    if slots:
        for d in dates:
            for slot in slots:
                n = len(events[d][slot.slot_no]['events'])
                if n == 1:
                    events[d][slot.slot_no]['class'] = 'slot-busy'
                if n > 1:
                    events[d][slot.slot_no]['class'] = 'slot-full'
    
    html = render_template('components/_calendar.html', dates=dates, slots=slots, events=events)
    return jsonify(html=html)

@app.route('/_day_visits', methods=['POST', 'GET'])
def day_visits():
    date_arg = request.values.get('date', '')
    field = request.values.get('field', '')
    title = request.values.get('title', '')
    period = request.values.get('period', 'am')
    hide_field = request.values.get('hide_field', '')
    current_slot = request.values.get('current_slot', '')
    the_date: date = parse_date(date_arg) if date_arg else date.today()
    html = day_events_html(the_date, field, title, period, hide_field, current_slot)
    return jsonify(html=html)

@app.route('/_send_email')
def _send_email():
    key_no = int(request.values.get('key_no', ''))
    view = request.values.get('view', '')
    reply_text = request.values.get('text', '')
    address = request.values.get('address', '')
    key_type = request.values.get('key_type', '')
    
    hist_no = send_customer_email(key_no, view, reply_text, address, key_type)

    if hist_no: # success
        html = f"""Email sent successfully; <a target="_blank" href="/display_email/{hist_no}">Click</a> to view"""
    else:
        html = 'Email sending failed'
    return jsonify(html=html)

@app.route('/_checkin_report')
def _checkin_report():
    start_date: date = parse_date(request.values.get('start_date', ''))
    end_date: date = parse_date(request.values.get('end_date', ''))
    species = request.values.get('species', 'Dog')

    sql = """select ci_date, ci_bk_no, ci_cust_surname, ci_cust_no,
ci_rt_desc, ci_pet_nos, ci_pet_desc, ci_start_time, ci_end_date, ci_form,
ci_run_code, ci_kennel_notes, ci_end_time
from vwcheckin_report
where ci_date between ? and ? and ci_spec_desc = ?
order by ci_date, ci_start_time"""
    
    rows = db.safeselect(sql, start_date, end_date, species)
    
    ci = {}

    if rows:
        for row in rows:
            checkin = row2dict(row)
            checkin['pets'] = [pet_button(pet_no) for pet_no in row.ci_pet_nos.split(',')]
            if row.ci_date not in ci:
                ci[row.ci_date] = []
            ci[row.ci_date].append(checkin)
    
    html = render_template('components/_checkin_report.html', checkins=ci, species=species)
    
    return jsonify(html=html)

@app.route('/_checkout_report')
def _checkout_report():
    start_date: date = parse_date(request.values.get('start_date', ''))
    end_date: date = parse_date(request.values.get('end_date', ''))
    species: str = request.values.get('species', 'Dog')

    sql = """select co_date, co_bk_no, co_cust_surname, co_cust_no,
    co_pet_desc, co_end_time, co_balance
    from vwcheckout_report
    where co_date between ? and ? and co_spec_desc = ?
    order by co_date, co_end_time
    """
    
    checkouts = db.safeselect(sql, start_date, end_date, species)

    co = {}
    
    if checkouts:
        for c in checkouts:
            if c.co_date not in co:
                co[c.co_date] = []
            co[c.co_date].append(c)
    
    html = render_template('components/_checkout_report.html', checkouts=co, species=species)
    
    return jsonify(html=html)

@app.route('/_email_doc', methods=['POST'])
def _email_doc():
    email = request.values.get('email', '')
    body = ''
    if 'body' in request.args:
        body = request.values.get('body', '')
    subject = request.values.get('subject', '')
    bk_no = int(request.values.get('bk_no', '0'))
    deposit = Decimal(request.values.get('deposit', '0.0'))
        
    filename = request.values.get('filename', '')
    if not body:
        with open(f'/static/confirmations/{filename}.html') as f:
            body = f.readlines()
            body = '\n'.join(body)
    elif body:
        now = datetime.now()
        timestamp = now.strftime("%Y%m%d%H%M%S")
        filename = f"{bk_no}_{timestamp}.html"
        with open(filename, 'w') as f:
            f.write(body)
    
    if body:
        body_txt = h2t.handle(body)
        send_email(email, body, subject, body_txt)
        sql = 'Execute pinsert_confaction ?, ?, ?, ?, ?, ?, ?'
        db.safeexec(sql, 0, bk_no, '', subject, filename, deposit, email)
        html = f"""<p>Email with subject {subject} sent to {email} and saved as <a href="/static/confirmations/{filename}" target="_blank">{filename}</a></p>"""
    else:
        html = f"""<p style="color: red">Either body or filename must be specified</p>"""
    return html

@app.route('/_runview')
def runview_ajax():
    start_date: date = parse_date(request.values.get('start_date', ''))
    end_date: date = parse_date(request.values.get('end_date', ''))
    species: str = request.values.get('species', 'Dog')
        
    start_date = start_date.date() if isinstance(start_date, datetime) else start_date
    end_date = end_date.date() if isinstance(end_date, datetime) else end_date
    
    html = ro_display(start_date, end_date, species)
    return jsonify(html=html)    

@app.route('/_checkin_form')
def _checkin_form():
    bk_no = int(request.values.get('bk_no', '0'))
    pet_no = int(request.values.get('pet_no', '0'))
    form = checkin_form_form(bk_no, pet_no)
    if form:
        html = render_template('components/_checkin_form.html', form=form, attachments=True)
    else:
        html = 'Form not found'
    return jsonify(html=html)

@app.route('/_weekly_shifts/<string:date_arg>')
def weekly_shifts(date_arg = None):
    return weekly_shifts_common(date_arg)

@app.route('/_cashflow_report')
def _cashflow_report():
    start_date: date = parse_date(request.values.get('start_date', ''))
    end_date: date = parse_date(request.values.get('end_date', ''))
    cash_balance = Decimal(request.values.get('cash_balance', '0'))
    
    report = get_cashflow_report(start_date, end_date, cash_balance)
    html = render_template('components/_cashflow_report.html',
                           start_date=start_date, end_date=end_date, report=report)
    return jsonify(html=html)

@app.route('/_note_edit')
def note_edit():
    note_no = int(request.values.get('note_no', '0'))
    pet_no = int(request.values.get('pet_no', '0'))
    
    note = Note.fetch(note_no) if note_no else None
    if note:
        note_pet = note.pet
    elif pet_no:
        note_pet = Pet.fetch(pet_no)
    else:
        note_pet = None
        
    the_date = note.date if note else date.today()
    sorted_cats = []
    sorted_dogs = []
    
    snap = None
    if note:
        snap = note.current_snap
    elif not pet_no:
        dogs = []
        cats = []
        sql = 'select pi_pet_no from vwpets_in where pi_date = ?'    
        rows = db.safeselect(sql, date.today())
        if rows:
            for row in rows:
                pet = Pet.fetch(row.pi_pet_no)
                if pet.spec_no == 1:
                    dogs.append(pet)
                else:
                    cats.append(pet)
        
        sorted_dogs = sorted(dogs, key = lambda p: p.full_description)
        sorted_cats = sorted(cats, key = lambda p: p.full_description)
    
    emps = get_employees(the_date, True)
    
    sorted_emps = [note.reporter] if note else sorted(emps, key = lambda e: e.nickname)
    
    types: Dict[int, Optional[Dict]] = {}
    if note:
        types = {note.type.no: note.type.to_dict()}
    else:
        types = NoteType.get_all()
        types[0] = None
    
    cu = cast(User, current_user)
    default_emp_no = snap.recorder.no if snap else cu.id if cu.is_authenticated else 0
    
    return render_template('/components/note_modal.html', dogs=sorted_dogs, cats=sorted_cats,
                        emps=sorted_emps, types=types, editable=True,
                        default_emp_no=default_emp_no, snap=snap, note_pet=note_pet,
                        today=the_date, now=datetime.now(), note=note,
                        sevs=NoteSeverity.get_all())


@app.route('/_note_display/<int:note_no>/<int:ver_no>')
@app.route('/_note_display/<int:note_no>')
def note_display(note_no: int, ver_no: int = 0):
    note = Note.fetch(note_no)
    snap = note.get_snap(ver_no)
    return render_template('/components/note_display.html', editable=False, note=note, snap=snap, ver_no=ver_no, note_sev=snap.sev)
