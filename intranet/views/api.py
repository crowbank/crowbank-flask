from datetime import timedelta, datetime, date
from dateutil.relativedelta import relativedelta
from decimal import Decimal
from flask import jsonify, request, render_template, make_response, url_for, flash
from flask_login import login_required, current_user
from html2image import Html2Image
from typing import Optional, List, cast

from ..actions import process_markin, process_marknoshow, book_daycare
from .pages import home_page
from intranet import app, csrf
from intranet.user import User
from intranet.utilities import day_emp_html, get_revenue_wages, weekly_emp_summary, \
    day_emp, get_daycare_dogs, get_daycare_runs, get_dog_calendar, get_current_user_id
from logic import timesheet_status, add_payment
from logic.staffing import staffing_class
from models import Booking, Block, Customer, DaycareCandidate, Employee, Inventory, Note, NoteSeverity, NoteSnap, NoteType, Period, Pet, Run, Slot
from models.availability import get_availability, get_availablity_data, get_unallocated
from models.occupancy import OccupancyManager
from models.runoccupancy import RunOccupancyManager
from models.runtype import RunType
from utils import db, stripe_utils, get_settings, parse_date

@app.route('/_raw_dismiss')
def _raw_dismiss():
    pet_no_str = request.values.get('pet_no')
    assert(pet_no_str)
    pet_no = int(pet_no_str)
    sql = 'praw_dismiss ?'
    db.safeexec(sql, pet_no)
    return jsonify()

@app.route('/_day_emp')
def _day_emp():
    date_dmy = request.values.get('date')
    assert(date_dmy)
    emp_no = request.values.get('emp_no')
    status = request.values.get('status')
    required_str = request.values.get('required')
    assert(required_str)
    required = float(required_str)
    
    start_date = parse_date(date_dmy)

    if emp_no and status:
        db.safeexec('pupdate_emp_timesheet ?, ?, ?', emp_no, status, start_date)
    
    wd = start_date.weekday()  
    weekstart_date = start_date - timedelta(wd)

    wages, revenue, ratio, ratioClass, ratioDesc = get_revenue_wages(weekstart_date)
    emps = day_emp_html(start_date, False)

    emp_summary = weekly_emp_summary(weekstart_date)
    emp_summary_html = render_template('emp_summary.html', emps=emp_summary, total=wages)
    """Also need to obtain day staffing class (using staffing_class()) and actual employee count"""
    
    actual = day_emp(start_date)
    s_class = staffing_class(required, actual)
    
    
    return jsonify(
        emps=emps,
        actual=actual,
        revenue='{:,.2f}'.format(revenue),
        wages='{:,.2f}'.format(wages),
        ratio='{:,.1f}'.format(ratio),
        ratioClass=ratioClass,
        s_class=s_class,
        emp_summary_html=emp_summary_html
    )

@app.route('/_timesheet_publish')
@login_required
def _timesheet_publish():
    weekstart = request.values.get('weekstart')
    if not weekstart:
        return jsonify(data={'status': 'error', 'message': 'Must provide week start parameter'})
    
    weekstart = parse_date(weekstart)
    
    sql = 'ptimesheet_publish_week ?'
    db.safeexec(sql, weekstart)

    shot_name = f"{weekstart.strftime('%y-%m-%d')}.png" # (published {now.strftime('%y-%m-%d %H:%M')})"

    hti = Html2Image(output_path='intranet/static/shifts')
    url = url_for('weekly_shifts', date_arg=weekstart.strftime('%d-%m-%y'))
    paths = hti.screenshot(url=url, save_as=shot_name)
    
    return timesheet_status(weekstart, paths[0])

@app.route('/_timesheet_status')
@login_required
def _timesheet_status():
    weekstart = request.values.get('weekstart')
    if not weekstart:
        return jsonify(data={'status': 'error', 'message': 'Must provide week start parameter'})
    
    weekstart = parse_date(weekstart)
    return jsonify(timesheet_status(weekstart))

@app.route('/_vetdetail')
def vetdetail():
    vet_no = request.values.get('vet_no', 0, type=int)
    vet = db.safeselect_one(f"select * from vwvet where vet_no=?", vet_no)
    assert(vet)
    if vet.vet_time:
        minutes = int(vet.vet_time / 60.0)
        title = f'Tel: {vet.vet_telno_1}<br>{minutes} min away'
    else:
        title = f'Tel: {vet.vet_telno_1}'
        
    
    return jsonify(title=title)

@app.route('/_allocations')
def _allocations():
    runoccupancy_manager = RunOccupancyManager()
    
    start_date_str = request.values.get('start_date')
    assert(start_date_str)
    start_date = parse_date(start_date_str)
    if isinstance(start_date, datetime):
        start_date = start_date.date()

    end_date_str = request.values.get('end_date')
    assert(end_date_str)
    end_date = parse_date(end_date_str)
    if isinstance(end_date, datetime):
        end_date = end_date.date()
#    blocks = get_occupancy_blocks(start_date, end_date)

    force_str = request.values.get('force')
    force = force_str == 'true'
    blocks = runoccupancy_manager.get_blocks_by_date_range(start_date, end_date, force)
    pets = {}
    bookings = {}
    js_blocks = {}
    
    for (run_no, block_list) in blocks.items():
        run_code = Run.fetch(run_no).code
        js_blocks[run_code] = []
        prev_block = None
        for block in block_list:
            new_block = {
                'bk_no': block.booking.no,
                'pets': [pet.no for pet in block.pets],
                'start_date': block.start_slot.date.strftime('%Y%m%d'),
                'start_time': block.start_time,
                'start_slot': block.start_slot.slot,
                'end_date': block.end_slot.date.strftime('%Y%m%d'),
                'end_time': block.end_time,
                'end_slot': block.end_slot.slot,
                'time_span': 999
            }
            if prev_block and new_block['start_date'] <= prev_block['end_date']:
                if new_block['start_date'] == prev_block['end_date']:
                    start_time = datetime.strptime(new_block['start_time'] if new_block['start_time'] else '00:00', '%H:%M')
                    end_time = datetime.strptime(prev_block['end_time'] if prev_block['end_time'] else '23:59', '%H:%M')
                    time_span = (start_time - end_time).total_seconds() / 3600.0
                else:
                    time_span = -999
                new_block['time_span'] = time_span
            
            js_blocks[run_code].append(new_block)
            prev_block = new_block
            
            if not block.booking.no in bookings:
                bookings[block.booking.no] = block.booking.as_dict()
            for pet in block.pets:
                if not pet.no in pets:
                    pets[pet.no] = pet.as_dict()
    
    pets_data = jsonify(pets=pets)
    bookings_data = jsonify(bookings=bookings)
    blocks_data = jsonify(blocks=js_blocks)
    json_data = jsonify(pets=pets, bookings=bookings, blocks=js_blocks)
    return json_data

@app.route('/_move')
def _move():
    assert('ob_no' in request.values)
    ob_no = int(request.values['ob_no'])
    assert('bk_no' in request.values)
    bk_no = int(request.values['bk_no'])
    run_code = request.values.get('run_code')
    
    start_date = parse_date(request.values['start_date']) if 'start_date' in request.values else None
    start_slot = request.values.get('start_slot')
    
    assert('pet_no' in request.values)
    pet_no = int(request.values['pet_no'])
    
    run = Run.get_by_code(run_code)
    booking = Booking.fetch(bk_no)
    block = Block.get(ob_no)

    start = None
    if start_date and start_slot:
        start = Slot(start_date, start_slot)
    
    pet = None
    if pet_no:
        pet = Pet.get(pet_no)
        
    booking.get_blocks().move(block, run, start, pet)
    blocks = booking.get_blocks().as_json()
    return jsonify(blocks=blocks)
    
@app.route('/_change_request_status')
def _change_request_status():
    req_no = request.values.get('req_no')
    status = request.values.get('status')
    comment = request.values.get('comment')
    
    sql = "pupdate_form_status ?, ?, ?, '_change_request_status'"
    db.safeexec(sql, req_no, status, comment)
    
    data = {'status': 'success'}
    return jsonify(data=data)

@app.route('/api/booking')
def api_booking():
    bk_no = request.values.get('bk_no')
    cust_email = request.values.get('cust_email')

    exists = db.safeselect_value("""select cust_no
from pa..tblcustomer join pa..tblbooking on bk_cust_no = cust_no
where bk_no = ? and (cust_email = ? or cust_email2 = ?)""",
                                bk_no, cust_email, cust_email)
    
    if not exists:
        return jsonify(data={
            'status': 'fail',
            'msg': 'Failed to authenticate email'
        })
        
    rec = db.safeselect_one("""select bk_no, bk_start_date, bk_start_time, bk_end_date, bk_end_time,
bk_pets, bk_pet_names, bk_gross_amt, bk_paid_amt, bk_status, bk_deluxe, bk_amt_outstanding, bk_status_desc
from vwbooking
where bk_no = ?""", bk_no)
    
    assert(rec)
    booking = {
            'no': rec.bk_no,
            'start_date': rec.bk_start_date,
            'start_time': rec.bk_start_time,
            'end_date': rec.bk_end_date,
            'end_time': rec.bk_end_time,
            'pets_desc': rec.bk_pets,
            'pet_names': rec.bk_pet_names,
            'gross_amt': float(rec.bk_gross_amt),
            'paid_amt': float(rec.bk_paid_amt),
            'amt_outstanding': float(rec.bk_amt_outstanding),
            'status': rec.bk_status,
            'deluxe': rec.bk_deluxe,
            'status_desc': rec.bk_status_desc
        }
    
    pet_recs = db.safeselect("""select pet_no, pet_name, pet_deceased,  breed_desc, pet_dob, pet_sex, spec_desc, pet_vacc_status, billcat_desc, pet_diabetic
from vwpet join pa..tblbookingitem on bi_pet_no = pet_no where bi_bk_no = ?""", bk_no)
    
    pets = []
    
    if pet_recs:
        pets = [
            {
                'no': rec.pet_no,
                'name': rec.pet_name,
                'deceased': 1 if rec.pet_deceased == 'Y' else 0,
                'breed': rec.breed_desc,
                'dob': rec.pet_dob,
                'sex': rec.pet_sex,
                'spec': rec.spec_desc,
                'vacc_status': rec.pet_vacc_status,
                'billcat': rec.billcat_desc,
                'diabetic': rec.pet_diabetic
            } for rec in pet_recs
        ]
    
    data = {
        'status': 'success',
        'booking': booking,
        'pets': pets
    }
    return jsonify(data=data)
@app.route('/api/customer')
def api_customer():
    cust_no = request.values.get('cust_no')
    cust_email = request.values.get('cust_email')

    exists = db.safeselect_value("""select cust_no
from pa..tblcustomer
where cust_no = ? and (cust_email = ? or cust_email2 = ?)""",
                                cust_no, cust_email, cust_email)
    
    if not exists:
        return jsonify(data={
            'status': 'fail',
            'msg': 'Failed to authenticate email'
        })
    
    details_rec = db.safeselect_one("""select cust_surname, cust_forename, cust_addr1, cust_addr3, cust_postcode,
cust_email, cust_telno_mobile from pa..tblcustomer where cust_no = ?""", cust_no)
    
    
    if not details_rec:
        return jsonify(data={
            'status': 'fail',
            'msg': f'Failed to load customer number {cust_no}'
        })
        
    customer_rec = {
        'surname': details_rec.cust_surname,
        'forename': details_rec.cust_forename,
        'street': details_rec.cust_addr1,
        'city': details_rec.cust_addr3,
        'postcode': details_rec.cust_postcode,
        'email': details_rec.cust_email,
        'phone': details_rec.cust_telno_mobile
    }
    
    bookings_recs = db.safeselect("""select bk_no, bk_start_date, bk_start_time, bk_end_date, bk_end_time,
bk_pets, bk_pet_names, bk_gross_amt, bk_paid_amt, bk_status, bk_deluxe, bk_amt_outstanding, bk_status_desc
from vwbooking
where bk_cust_no = ?""", cust_no)
    
    bookings = []
    if bookings_recs:
        bookings = [
            {
                'no': rec.bk_no,
                'start_date': rec.bk_start_date,
                'start_time': rec.bk_start_time,
                'end_date': rec.bk_end_date,
                'end_time': rec.bk_end_time,
                'pets_desc': rec.bk_pets,
                'pet_names': rec.bk_pet_names,
                'gross_amt': float(rec.bk_gross_amt),
                'paid_amt': float(rec.bk_paid_amt),
                'amt_outstanding': float(rec.bk_amt_outstanding),
                'status': rec.bk_status,
                'deluxe': rec.bk_deluxe,
                'status_desc': rec.bk_status_desc
            } for rec in bookings_recs
        ]
    
    pet_recs = db.safeselect("""select pet_no, pet_name, pet_deceased,  breed_desc, pet_dob, pet_sex, spec_desc, pet_vacc_status, billcat_desc, pet_diabetic
from vwpet where cust_no = ?""", cust_no)
    
    pets = []
    
    if pet_recs:
        pets = [
            {
                'no': rec.pet_no,
                'name': rec.pet_name,
                'deceased': 1 if rec.pet_deceased == 'Y' else 0,
                'breed': rec.breed_desc,
                'dob': rec.pet_dob,
                'sex': rec.pet_sex,
                'spec': rec.spec_desc,
                'vacc_status': rec.vaccination_status,
                'billcat': rec.billcat,
                'diabetic': rec.pet_diabetic
            } for rec in pet_recs
        ]
    
    data = {
        'status': 'success',
        'customer': customer_rec,
        'bookings': bookings,
        'pets': pets
    }
    return jsonify(data=data)

@app.route('/_update_questionnaire')
def _update_questionnaire():
    ndf_no = request.values.get('ndf_no')
    notes = request.values.get('notes')
    visit = request.values.get('visit')
    warning = request.values.get('warning')
    
    sql = 'pupdate_questionnaire ?, ?, ?, ?'
    db.safeexec(sql, ndf_no, notes, visit, warning)
    return jsonify(html='Record updated')

@app.route('/_markin')
def markin():
    bk_no = int(request.values.get('bk_no', 0))
    assert('the_date' in request.values)
    mark_date = parse_date(request.values['the_date'])
    
    process_markin(bk_no, mark_date)
    return jsonify(status=f'Booking {bk_no} marked in')

@app.route('/_marknoshow')
def marknoshow():
    bk_no = int(request.values.get('bk_no', 0))
    assert('the_date' in request.values)
    mark_date = parse_date(request.values['the_date'])
    
    process_marknoshow(bk_no, mark_date)
    return jsonify(status=f'Booking {bk_no} marked as no show')

@app.route('/api/check_availability')
def _check_availability():
    types_str = request.values.get('types')
    if not types_str:
        return jsonify(data={'status': 'failed'})
    types = types_str.split(',')
    assert('start_date' in request.values and 'end_date' in request.values)
    start_date = parse_date(request.values['start_date'])
    start_time = request.values.get('start_time')
    end_date = parse_date(request.values['end_date'])
    end_time = request.values.get('end_time')

    rts = []
    if 'standard' in types:
        rts.append(RunType.get_by_code('standard'))
    if 'deluxe' in types:
        rts.append(RunType.get_by_code('deluxe'))
    if 'cattery' in types:
        rts.append(RunType.get_by_code('cat'))
    if 'standard' in types or 'deluxe' in types:
        rts.append(RunType.get_by_code('any'))
        
    period = Period(start_date, start_time, end_date, end_time)
    availabilities = map(lambda rt: get_availability(period, rt), rts)
    
    overall_availability = max(availabilities)
    return jsonify(data={
        'status': 'success',
        'types': types,
        'start_date': start_date,
        'start_time': start_time,
        'end_date': end_date,
        'end_time': end_time,
        'overall_availability': overall_availability
    })

@app.route('/_availability_new')
def _availability_new():
    assert('start_date' in request.values and 'end_date' in request.values)
    start_date = parse_date(request.values['start_date'])
    end_date = parse_date(request.values['end_date'])
    spec_no_str = request.values.get('spec_no')
    assert(spec_no_str)
    spec_no = int(spec_no_str)
    species = 'Kennels' if spec_no == 1 else 'Cattery'
    
    rt = RunType.get_by_spec(spec_no)
    assert(rt)
    column_count = rt.capacity
    

    rts_array = RunType.get_all_by_spec(spec_no)
    
    period = Period(start_date, 'am', end_date, 'pm')
    availability_data = get_availablity_data(period, rts_array)
    unallocated_data = get_unallocated(period, spec_no)
    
    html = render_template('components/_availability_new.html', data=availability_data, unallocated=unallocated_data, species=species, column_count=column_count)
    return jsonify(html=html)
    
@app.route('/_submit_weight')
def _submit_weight():
    assert('weight' in request.values)
    weight = float(request.values['weight'])
    assert('pet_no' in request.values)
    pet_no = int(request.values['pet_no'])
    sql = 'padd_weight ?, ?'
    db.safeexec(sql, pet_no, weight)
    
    sql = 'select pet_name, pet_weight, pw_date from vwpetweight where pet_no = ?'
    rec = db.safeselect_one(sql, pet_no)
    assert(rec)
    return jsonify(
        pet_name=rec.pet_name,
        weight = rec.pet_weight,
        date=rec.pw_date
    )

@app.route('/_add_daycare')
def _add_daycare():
    assert('bk_no' in request.values)
    bk_no = int(request.values['bk_no'])
    assert('date' in request.values)
    the_date = parse_date(request.values['date'])
    sql = 'padd_daycare_day ?, ?'
    db.safeexec(sql, bk_no, the_date)
    return jsonify()

@app.route('/api/ping')
def api_ping():
    return jsonify(data={
        'result': 'success'
    })

@app.route('/my-ip')
def api_test() -> str:
    ip_address = request.remote_addr
    if ip_address:
        return "Requester IP: " + ip_address
    return ''

@app.route('/_test', methods=['GET', 'POST'])
def test_post():
    return {'status': 'success'}

@app.route('/_body_score_table')
def body_score_table():
    sql = 'select bs_no, bs_desc, bs_long from tblbodyscore'
    body_scores = db.safeselect(sql)
    
    data = [{
        'score': bs.bs_no,
        'description': bs.bs_long
    } for bs in body_scores] if body_scores else []
    
    return jsonify(data=data)

@app.route('/_refresh_occupancy')
def refresh_occupancy():
    occupancy_manager = OccupancyManager()
    occupancy_manager.load()
    Inventory.refresh_all()
    return jsonify()

@app.route('/_punch', methods = ['GET', 'POST'])
def punch():
    employee = request.values.get('employee')
    print(f'employee: {employee}')
    if employee:
        employee = int(employee)
    punch_time = request.values.get('time')
    print(f'punch_time: {punch_time}')
    punch_comment = request.values.get('comment')
    punch_image = request.values.get('image')
    
    try:
        sql = 'exec ppunch ?, ?, ?, ?'
        db.safeexec(sql, employee, punch_time, punch_comment, punch_image)
        
        sql = 'select max(punch_no) from tblpunch'
        punch_no = db.safeselect_value(sql)
        
        sql = 'select punch_emp_no, punch_emp_fullname, punch_time, punch_comment from vwpunch where punch_no = ?'
        punch_rec = db.safeselect_one(sql, punch_no)
        if punch_rec and punch_rec.punch_emp_no == employee and (not punch_time or punch_rec.punch_time.strftime('%H:%M') == punch_time) and punch_rec.punch_comment == punch_comment:
            return jsonify({
                    'status': 'success',
                    'employee': punch_rec.punch_emp_fullname,
                    'time': punch_rec.punch_time.strftime('%H:%M') if punch_rec.punch_time else ''
                })
    except Exception as e:
        print('punch failed: ', e.args)
        return jsonify({'status': 'fail', 'message': e.args})
    
    return jsonify({'status': 'fail', 'message': 'Verification failed'})

@app.route('/_toggle_punch')
def _toggle_punch():
    do_punch = request.cookies.get('punch')
    if do_punch:
        do_punch = ''
    else:
        do_punch = 'yes'
    resp = make_response(home_page())
    resp.set_cookie('punch', do_punch, 60 * 60 * 24 * 365 * 3)
    return resp

@app.route('/_toggle_notes')
def _toggle_notes():
    do_notes = request.cookies.get('notes')
    if do_notes:
        do_notes = ''
    else:
        do_notes = 'yes'
    resp = make_response(home_page())
    resp.set_cookie('notes', do_notes, 60 * 60 * 24 * 365 * 3)
    return resp

@app.route('/api/webhook')
def webhook():
    return jsonify(data={
        'result': 'success'
    })

@app.route('/_payment_link', methods=['POST'])
@csrf.exempt
def _payment_link():
    data = request.get_json()
    if not data:
        return jsonify({'success': False})

    amount = Decimal(data.get('amount')) if 'amount' in data else None
    bk_no = int(data.get('bk_no')) if 'bk_no' in data else None
    
    booking = Booking.get(bk_no) if bk_no else None
    if not booking:
        return jsonify({'success': False})
    
    purpose = ''
    if amount == booking.amt_outstanding:
        purpose = 'full'
    elif booking.paid_amt == 0.0:
        purpose = 'deposit'
    else:
        purpose = 'partial'

    sql = f'select dbo.fnpayment_link(?, ?, ?)'
    url = db.safeselect_value(sql, bk_no, amount, purpose)
    
    if not url:
        return jsonify({'success': False})
    
    return jsonify({
        'success': True,
        'url': url
    })
    
    
@app.route('/_create_payment', methods=['POST'])
@csrf.exempt
def _create_payment():
    data = request.get_json()
    if not data:
        return jsonify({'client_secret': None})
    
    amount = data.get('amount')
    bk_no = int(data.get('bk_no', 0))
    cust_no = int(data.get('cust_no', 0))

    customer = Customer.fetch(cust_no)
    
    description = f'Payment for {customer.surname} (booking {bk_no})' if customer else f'Payment for booking {bk_no}'
    # Create a new payment.
    payment_intent = stripe_utils.create_payment_intent(amount, description, bk_no, cust_no)

    return jsonify({
        'client_secret': payment_intent.client_secret,
        'id': payment_intent.id
    })

@app.route('/_connection_token', methods=['POST'])
@csrf.exempt
def token():
  connection_token = stripe_utils.get_connection_token()
  return jsonify(secret=connection_token)

@app.route('/_delete_payment', methods=['POST'])
@csrf.exempt
def delete_payment():
    data = request.get_json()
    if not data:
        return jsonify({'status': 'error'})
    
    payment_intent_id = data.get('payment_intent_id')
    if stripe_utils.delete_payment(payment_intent_id):
        return jsonify({'status': 'sucess'})
    return jsonify({'status': 'error'})
        
@app.route('/_add_payment', methods=['POST'])
@csrf.exempt
def add_stripe_payment():
    try:
        data = request.get_json()
        if not data:
            return jsonify({'status': 'error'})
        
        amount = float(data.get('amount'))
        bk_no = data.get('bk_no')
        id = data.get('id')
        pay_type = 'Stripe'
        pay_date = date.today()
        
        add_payment(bk_no, amount, pay_type, pay_date, id)
        return jsonify({'status': 'success'})
    except Exception as e:
        return jsonify({'status': 'error', 'msg': str(e)})
    
@app.route('/_change_run', methods=['POST'])
@csrf.exempt
def _change_run():
    data = request.get_json()
    if not data:
        return jsonify({'success': False})

    bk_no = int(data.get('bk_no'))
    pets = data.get('pets')
    run = data.get('run')
    
    for pet_name in pets:
        sql = 'exec pchange_run ?, ?, ?'
        db.safeexec(sql, bk_no, pet_name, run)
        
    return jsonify({'success': True})

@app.route('/_approve_employee', methods=['POST'])
@csrf.exempt
def _approve_employee():
    data = request.get_json()
    assert(data)
    emp_no = int(data['emp_no'])
    date_arg: str = data['date']
    the_date: date = parse_date(date_arg)
    start: Optional[str] = data['start'] if 'start' in data and data['start'] else None
    end: Optional[str] = data['end'] if 'end' in data and data['end'] else None
    lunch = 1 if data['lunch'] else 0
    comment = data['comment']

    sql = 'exec pconfirm_employee_daily ?, ?, ?, ?, ?, ?'
    db.safeexec(sql, emp_no, the_date, start, end, lunch, comment)
    return jsonify({'success': True})


@app.route('/_refresh_employee_daily', methods=['GET'])
def _refresh_employee_daily():
    date_arg: str = request.values.get('date', '')
    the_date: date = parse_date(date_arg) if date_arg else date.today()

    try:
        sql = 'exec ppopulate_employee_daily ?'
        db.safeexec(sql, the_date)
    except Exception as e:
        return jsonify({'success': False, 'msg': str(e)})
    
    return jsonify({'success': True})

@app.route('/_refresh_booking', methods=['GET'])
def _refresh_booking():
    bk_no: int = int(request.values.get('bk_no', 0))
    
    Booking.reload(bk_no)
    return jsonify({'success': True})

@app.route('/_refresh_customer', methods=['GET'])
def _refresh_customer():
    cust_no: int = int(request.values.get('cust_no', 0))
    
    Customer.reload(cust_no)
    return jsonify({'success': True})

@app.route('/_create_stripe_customer', methods=['POST'])
def _create_stripe_customer():
    cust_no: int = int(request.values.get('cust_no', 0))
    
    if not cust_no:
        return jsonify({'success': False})
    
    customer = Customer.fetch(cust_no)
    id = stripe_utils.create_customer(customer)
    return jsonify({'success': bool(id), 'id': id})

@app.route('/_submit_note', methods=['POST'])
@csrf.exempt
def _submit_note():
    settings = get_settings()
    image_folder = settings['IMAGE_FOLDER']
    cu = cast(User, current_user)
    note_no = int(request.form.get('note_no', 0))
    comments = request.form.get('comments', '')
    uploaded_file = request.files.get('photo')
    severity = NoteSeverity(int(request.form.get('severity', 1)))
    recorder_no = cu.id if cu.is_authenticated else 0
    recorder = Employee.get(recorder_no)
    reporter = None
    file_path = ''
        
    if note_no:
        if not recorder_no:
            return jsonify({'success': False, 'msg': 'You must be logged in to change a note'})
        note = Note.fetch(note_no)
        file_path = note.current_snap.photo
    else:
        datestr = request.form.get('date')
        timestr = request.form.get('time')
        reporter_no = int(request.form.get('by', 0))
        pet_no = int(request.form.get('pet_no', 0))
        if not pet_no:
            pet_no = int(request.form.get('pet', 0))
        type_no = int(request.form.get('type_no', 0))
        reporter = Employee.fetch(reporter_no)
        pet = Pet.fetch(pet_no)    
        the_date = parse_date(datestr) if datestr else date.today()
        the_time = datetime.strptime(timestr[0:5], '%H:%M').time() if timestr else datetime.now().time()
        note_type = NoteType.fetch(type_no)
    
        note = Note(pet, the_date, the_time, note_type, datetime.now(), reporter)
        note.write_to_db()
        Pet.reload(pet_no)

    if uploaded_file and uploaded_file.filename:
        file_path = uploaded_file.filename
        uploaded_file.save(f"{image_folder}/{file_path}")

    if not recorder:
        recorder = reporter
        
    assert(recorder)
        
    notesnap = NoteSnap(note, severity, comments, recorder, file_path, datetime.now())
    notesnap.type_load(request.form)
    notesnap.write_to_db()
    
    flash('Note submitted', 'success')
    return jsonify({'success': True, 'msg': 'Note submitted'})

#    return jsonify({'success': True})
@app.route('/_update_dcc_status', methods=['POST'])
@csrf.exempt
def _update_dcc_status():
    status = request.values.get('status', 'open')
    dcc_no = int(request.values.get('dcc_no', 0))
    
    by = get_current_user_id()
    
    dcc = DaycareCandidate.fetch(dcc_no)
    dcc.update_status(status)
    
    sql = 'exec pupdate_daycare_candidate ?, ?, ?'
    db.safeexec(sql, dcc_no, by, status)
    
    return jsonify({'success': True})


@app.route('/_update_dcc_comment', methods=['POST'])
@csrf.exempt
def _update_dcc_comment():
    comment = request.values.get('comment', '')
    dcc_no = int(request.values.get('dcc_no', 0))
    
    by = get_current_user_id()
    
    dcc = DaycareCandidate.fetch(dcc_no)
    dcc.comment = comment
    
    sql = 'exec pupdate_daycare_candidate ?, ?, ?, ?'
    db.safeexec(sql, dcc_no, by, '', comment)
    
    return jsonify({'success': True})


@app.route('/_get_available_runs', methods=['GET'])
def _get_available_runs():
    the_date = request.values.get('date')
    if not the_date:
        return jsonify({'success': False})
    
    the_date = parse_date(the_date)
    runs = get_daycare_runs(the_date)
    
    runs_json = sorted([
        {
            'no': run.no,
            'code': run.code
        } for run in runs
    ], key=lambda run: run['code'])
    
    return jsonify({'success': True, 'runs': runs_json})
    
@app.route('/_get_daycare_dogs', methods=['GET'])
def _get_daycare_dogs():
    dogs = get_daycare_dogs()
    dogs_json = sorted([
        {
            'no': dog.no,
            'name': dog.name,
            'description': dog.description            
        } for dog in dogs
    ], key=lambda dog: dog['name'])
    
    return jsonify({'success': True, 'dogs': dogs_json})

@app.route('/_get_dog_calendar', methods=['GET'])
def _get_dog_calendar():
    pet_no = request.values.get('pet_no', 0)
    if not pet_no:
        return jsonify({'success': False})
    
    pet = Pet.fetch(pet_no)
    start_date = date.today()
    end_date = start_date + relativedelta(months=+2)
    cal = get_dog_calendar(pet, start_date, end_date)
    return jsonify({'success': True, 'cal': cal})

@app.route('/_book_daycare', methods=['POST'])
@csrf.exempt
def _book_daycare():
    the_date = request.values.get('date', '')
    if not the_date:
        return jsonify({'success': False})  
    
    the_date = parse_date(the_date)
    
    pet_no = int(request.values.get('dog', 0))
    pet = Pet.fetch(pet_no)
    run_no = int(request.values.get('run', 0))
    run = Run.fetch(run_no)
    comment = request.values.get('comment', '')
    duration = request.values.get('duration', 'full')
    
    bk_no, msg = book_daycare(the_date, pet, run, comment, duration)
    
    if bk_no:
        return jsonify({'success': True, 'msg': msg})
    else:
        return jsonify({'success': False, 'msg': msg})
    
