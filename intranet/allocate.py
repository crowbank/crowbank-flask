from utils import db
from flask import render_template, url_for
from json import dumps
from datetime import date
from models import Slot
from models.block import Block

# Run allocator functionality
#
# Primary functions:
# read_allocations(start_date, end_date)
#  returns allocations data structure of the form:
#    [
#      {
#           bk_no: bk_no,
#           start_date: bk_start_date,
#           end_date: bk_end_date,
#           start_time: bk_start_time,
#           end_time: bk_end_time,
#           status: bk_status,
#           cust_no: bk_cust_no,
#           cust_surname: bk_cust_surname,
#           pets:   [
#                       {
#                           pet_no: pet_no,
#                           name: pet_name,
#                           breed: pet_breed,
#                           photo: pet_photo (filename),
#                           allocations: [
#                                           {
#                                               date: ra_date,
#                                               run: ra_code
#                                           }
#                                       ]
#                       }, ...
#                   ]
#    
#      }, ...
# ]

def myconverter(o):
    if isinstance(o, date):
        return o.__str__()

def cell_card(cell):
    html = '<div class="cell-card-wrapper"><div class="pet-names">'
    for pet in cell.pets:
        html += f'<div class="pet-card">{pet.name} ({pet.breed.desc})</div>'
    html += '</div>'
    html += f"""<div class="booking-details">
<a href="{url_for('show_booking', bk_no=cell.booking.no)}">
{cell.booking.no}: {cell.booking.start_date.strftime('%d/%m')}-{cell.booking.end_date.strftime('%d/%m')}
</a></div>"""
    return html

def get_occupancy_blocks(start_date, end_date):
    sql = 'exec ppopulateoccupancyblock ?, ?'
    db.safeexec(sql, start_date ,end_date)
    
    sql = """select ob_no from vwoccupancyblock
where ob_start_date <= ? and ob_end_date >= ? and ob_status <> 'C' and ob_end_stamp is null
order by ob_start_date, ob_start_slot"""

    recs = db.safeselect(sql, end_date, start_date)
    blocks = []
    
    if recs:
        for rec in recs:
            blocks.append( Block.get(rec.ob_no) )
        
    return [block.as_dict() for block in blocks]

def read_allocations(start_date, end_date):
    allocations = {}
    pet_cards = {}
    
    sql = """select ra_date, ra_slot, ra_end_date, ra_end_slot, ra_bk_no, ra_pet_no, ra_bk_start_date, ra_bk_end_date, ra_bk_start_time,
    ra_bk_end_time, ra_bk_status, ra_cust_no,
    ra_cust_surname, ra_pet_name, ra_pet_sex, ra_breed_desc, ra_code, ra_pet_photo
    from vwrunalloc
    where ra_bk_start_date <= ? and ra_bk_end_date >= ?
    order by ra_date
    """
    rows = db.safeselect(sql, end_date, start_date)
    if rows:
        for row in rows:
            if row.ra_bk_no not in allocations:
                allocations[row.ra_bk_no] = {
                    'bk_no': row.ra_bk_no,
                    'start_date': row.ra_bk_start_date,
                    'start_time': row.ra_bk_start_time,
                    'start_slot': 'am' if row.ra_bk_start_time < '13:00' else 'pm',
                    'end_date': row.ra_bk_end_date,
                    'end_time': row.ra_bk_end_time,
                    'end_slot': 'am' if row.ra_bk_end_time < '13:00' else 'pm',
                    'status': row.ra_bk_status,
                    'cust_no': row.ra_cust_no,
                    'cust_surname': row.ra_cust_surname,
                    'pets': {}
                }
            if row.ra_pet_no not in allocations[row.ra_bk_no]['pets']:
                allocations[row.ra_bk_no]['pets'][row.ra_pet_no] = {
                    'pet_no': row.ra_pet_no,
                    'name': row.ra_pet_name,
                    'breed': row.ra_breed_desc,
                    'photo': row.ra_pet_photo,
                    'sex': row.ra_pet_sex,
                    'allocations': {}
                }
                pet_cards[row.ra_pet_no] = pet_card(row)
                
            if row.ra_date:
                allocations[row.ra_bk_no]['pets'][row.ra_pet_no]['allocations'][row.ra_date.strftime('%Y-%m-%d')] = {
                    'date': row.ra_date,
                    'slot': row.ra_slot,
                    'end_date': row.ra_end_date,
                    'end_slot': row.ra_end_slot,
                    'run': row.ra_code
                }
    
    jalloc = []
    for booking in allocations.values():
        booking['pets'] = list(booking['pets'].values())
        jalloc.append(booking)

    return dumps({'allocations': jalloc, 'pet_cards': pet_cards}, default=myconverter)

def view_allocs(start_date, end_date):
    data, pet_cards = read_allocations(start_date, end_date)
    date_range = f"{start_date.strftime('%d/%m/%Y')} - {end_date.strftime('%d/%m/%Y')}"
    
    return render_template('allocate.html', date_range=date_range, data=data, pet_cards=pet_cards)

def get_allocation_data(start_date, end_date):
    """Collect all relevant data for kennel allocation between dates.
That includes all kennel allocation, pet and booking data."""

    sql = "select ra_bk_no, ra_pet_nos, ra_date, ra_slot, ra_run_no from tblrunallocation where ra_date between ? and ?"
    recs = db.safeselect(sql, start_date, end_date)
    
    alloc = {}
    
    if recs:
        for rec in recs:
            slot = Slot(rec.ra_date, rec.ra_slot)
            if rec.ra_run_no not in alloc:
                alloc[rec.ra_run_no] = {}
                
            alloc[rec.ra_run_no][slot] = {
                'bk_no': rec.ra_bk_no,
                'pet_nos': [int(pet_no) for pet_no in rec.ra_pet_nos.split(',')]
            }
    
    sql = """select distinct pet_no, pet_name, breed_desc from pa..tblbookingitem
join pa..tblbooking on bk_no = bi_bk_no
join pa..tblpet on pet_no = bi_pet_no and pet_spec_no = 1
join pa..tblbreed on breed_no = pet_breed_no
where bk_status in ('', 'V') and bk_start_date <= ?
and bk_end_date >= ?"""

    pets = {}
    recs = db.safeselect(sql, end_date, start_date)
    
    pets = {
        rec.pet_no: {
            'name': rec.pet_name,
            'breed': rec.pet_breed
        } for rec in recs
    } if recs else {}

    sql = """select bk_no, bk_start_date, iif(bk_start_time < '13:00', 'am', 'pm') bk_start_slot,
bk_end_date, iif(bk_end_time < '13:00', 'am', 'pm') bk_end_slot,
string_agg(convert(varchar(10), bi_pet_no), ',') pet_nos
from pa..tblbooking
join pa..tblbookingitem on bi_bk_no = bk_no
where bk_status in ('', 'V')
and bk_start_date <= ?
and bk_end_date >= ?"""
    
    recs = db.safeselect(sql, end_date, start_date)
    bookings = {
        rec.bk_no: {
            'start_date': rec.bk_start_date,
            'end_date': rec.bk_end_date,
            'start_slot': rec.bk_start_slot,
            'end_slot': rec.bk_end_slot,
            'pet_nos': [int(pet_no) for pet_no in rec.pet_nos.split(',')]
        } for rec in recs
    } if recs else {}
    
    data = {
        'alloc': alloc,
        'pets': pets,
        'bookings': bookings
    }
    
    return data

