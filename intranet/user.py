from flask import current_app
from flask_security import current_user
from flask_security.core import UserMixin, RoleMixin
from flask_security.datastore import UserDatastore
from functools import wraps
from typing import cast

from models import Customer
from utils import db
from utils.security import pwd_context

    
class Role(RoleMixin):
    def __init__(self, id):
        self.id = id
        self.name = 'N/A'
        self.description = 'N/A'
        
    @classmethod
    def get(cls, id):
        sql = 'select role_name, role_desc from tblrole where role_no = ?'
        row = db.safeselect_one(sql, id)
        if not row:
            return None
        
        role = cls(id)
        role.name = row.role_name
        role.description = row.role_desc
        return role

class User(UserMixin):
    loaded = False
    roles = {}
    
    def __init__(self, id):
        self.id = id
        self.username = 'N/A'
        self.password = ''
        self.role = None
    
    @classmethod
    def load_roles(cls):
        if cls.loaded:
            return
        
        sql = 'select role_no, role_code from tblrole'
        rows = db.safeselect(sql)
        
        if rows:
            for row in rows:
                cls.roles[row.role_code.lower()] = row.role_no
            
        cls.loaded = True

    @classmethod
    def get_roles(cls):
        if not cls.loaded:
            cls.load_roles()
        return cls.roles
        
    def get_id(self):
        return self.id

    def check_pwd(self, password):
        return pwd_context.verify(password, self.password)
    
    def verify_password(self, password):
        return self.check_pwd(password)
    
    def has_role(self, role):
        if not User.roles:
            User.load_roles()
    
        role_desc = role.lower()
        if not role_desc in User.roles:
            return False
        
        role_no = User.roles[role_desc]
        
        return role_no <= self.role

class CustomerUser(User):
    def __init__(self, customer: Customer):
        self.customer = customer
        self.role = 2 # customer

    @property
    def id(self):
        return self.customer.no
    
    @property
    def password(self):
        return self.customer.password
    
    def get_id(self):
        return self.id
    
    def update_pwd(self, password):
        hash = pwd_context.hash(password)
        sql = 'update pa..tblcustomer set cust_custom6 = ? where cust_no = ?'
        db.safeexec(sql, hash, self.customer.no)

    @classmethod
    def get_by_email(cls, email):
        sql = 'select cust_no from pa..tblcustomer where cust_email = ?'
        row = db.safeselect_one(sql, email)
        if not row:
            return None
        
        return cls(Customer.fetch(row.cust_no))
            
        
class EmployeeUser(User):
    loaded = False
    roles = {}
    
    def __init__(self, id):
        super().__init__(id)
    
    @classmethod
    def get(cls, user_id):
        sql = """select emp_nickname, isnull(emp_password, '') emp_password, emp_role from tblemployee where emp_no = ? and emp_iscurrent = 1"""
        row = db.safeselect_one(sql, user_id)
        if not row:
            return None
        user = cls(user_id)
        user.username = row.emp_nickname
        user.password = row.emp_password
        user.role = row.emp_role
        return user
    
    @classmethod
    def get_by_name(cls, username):
        sql = 'select emp_no from tblemployee where emp_nickname = ? and emp_iscurrent = 1 and emp_password is not null'
        row = db.safeselect_one(sql, username)
        if not row:
            return None
        return cls.get(row.emp_no)

    @classmethod
    def get_by_email(cls, email):
        sql = 'select emp_no from tblemployee where emp_email = ? and emp_iscurrent = 1'
        row = db.safeselect_one(sql, email)
        if not row:
            return None
        return cls.get(row.emp_no)

    def update_pwd(self, password):
        hash = pwd_context.hash(password)
        sql = 'update tblemployee set emp_password = ? where emp_no = ?'
        db.safeexec(sql, hash, self.id)
        

def role_required(role):
    '''
    Built as a variation of login_required from flask_login.
    To use, place @role_required(role_name) in front of the view.
    It functions identically to @login_required.
    '''
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            cu = cast(UserMixin, current_user)

            if cu and cu.config.get('LOGIN_DISABLED'):
                return func(*args, **kwargs)
            elif not cu.is_authenticated or not cu.has_role(role):
                return current_app.login_manager.unauthorized()
            return func(*args, **kwargs)
        return wrapper
    return decorator


class CustomUserDatastore(UserDatastore):
    def __init__(self, user_model: type[User], role_model: type[Role]):
        super().__init__(user_model, role_model)
    
    def get_user(self, identifier):
        emp_user = EmployeeUser.get_by_email(identifier)
        if emp_user:
            return emp_user
        
        return CustomerUser.get_by_email(identifier)
    
    def get_user_by_email(self, email):
        return self.get_user(email)
    
    def find_user(self, **kwargs):
        return None
    
    def find_role(self, role):
        return Role.get(role)
    
