from flask import g
# from flask_login import LoginManager, UserMixin
from flask_security.core import UserMixin
from typing import Optional

from intranet import app
from intranet.user import User, CustomerUser, EmployeeUser
from models import Customer

# login_manager = LoginManager()
# login_manager.login_view = "login"

# @login_manager.user_loader
def load_user(user_id) -> Optional[UserMixin]:
    if g.user_type == 'customer':
        return CustomerUser(Customer.fetch(user_id))  # Assuming you have a method to get a customer

    return EmployeeUser.get(user_id)  # Assuming you have a method to get an employee

# @login_manager.request_loader
def load_user_from_request(request):
    if 'NGROK_USER' in request.environ:
        return request.environ['NGROK_USER']
    return None
