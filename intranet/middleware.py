from .user import User
import logging

def prepend_uri(prep, uri):
    if uri == '/':
        return prep
    return prep + uri

class CrowbankMiddleware:
    def __init__(self, app):
        self.app = app
        
    def __call__(self, environ, start_response):
        http_host = environ['HTTP_HOST']
        user_email = environ['HTTP_NGROK_AUTH_USER_EMAIL'] if 'HTTP_NGROK_AUTH_USER_EMAIL' in environ else None
        uri = environ['REQUEST_URI']
        path_info = environ['PATH_INFO']
        msg = f'{http_host} -> {uri} ({user_email})'
        logging.info(msg)
        user = None
#        print(msg)
        if uri[:7] != '/static':
            if user_email:
                user = User.get_by_email(user_email)
                environ['NGROK_USER'] = user

            if uri[:5] != '/rota' and user_email:
                if user and not user.has_role('admin'):
                    environ['REQUEST_URI'] = prepend_uri('/rota', uri)
                    environ['PATH_INFO'] = prepend_uri('/rota', path_info)

        return self.app(environ, start_response)