from utils import db, parse_date
from flask import flash
from intranet.utilities import start_log, end_log, get_current_user_id
from models.occupancy import OccupancyManager
from models import Booking, Customer, Employee, EmployeeStarter, Inventory, Pet, Run
from logic import send_customer_email, add_payment, check_dates, decline_booking
from datetime import date
from flask_wtf import FlaskForm
from intranet.forms import *
from typing import cast

occupancy_manager = OccupancyManager()

def flash_log_entries(log_entries):
    if log_entries:
        for log_entry in log_entries:
            flash(log_entry.msg, 'error')

def process_change_request_customer(form: ChangeRequestCustomerForm) -> bool:
    if not(form.change.data and form.validate()):
        return False
    fd_no = form.fd_no.data
    cust_no = form.cust_no.data
    
    sql = 'execute pupdate_form_customer ?, ?'
    db.safeexec(sql, fd_no, cust_no)
    new_cust_no = db.safeselect_value(f'select fd_cust_no from tblformdata_6 where fd_no = ?', fd_no )
    if new_cust_no != cust_no:
        flash('Customer change failed', 'error')
    else:
        flash(f'Customer number changed to {cust_no}', 'info')
    return True

def process_cancellation(cancel_form: CancelBookingForm) -> bool:
    if not(cancel_form.cancel.data and cancel_form.validate()):
        return False
    
    assert(cancel_form.booking.data)
    bk_no = int(cancel_form.booking.data)
    idiscount = cancel_form.discount.data
    discount = float(idiscount) / 100.0 if idiscount else 0.0
    voucher = 1 if cancel_form.voucher.data else 0
    lapsed = 1 if cancel_form.lapsed.data else 0
    sql = 'execute pcancel_booking ?, ?, ?, ?, ?, ?'
    db.safeexec(sql, bk_no, discount, None, 0, voucher, lapsed)
    flash(f'Booking #{bk_no} successfully cancelled', 'info')
    if not cancel_form.notice.data:
        sql = 'padd_email_override ?'
        db.safeexec(sql, bk_no)
        flash('No cancellation notice will be sent', 'info')
    Booking.reload(bk_no)
    occupancy_manager.load()
    Inventory.refresh_all()
    return True

def process_noshow(noshow_form: NoShowForm) -> bool:
    if not(noshow_form.noshow.data and noshow_form.validate()):
        return False
    
    assert(noshow_form.booking.data)
    bk_no = int(noshow_form.booking.data)
    idiscount = noshow_form.discount.data
    discount = float(idiscount) / 100.0 if idiscount else 0.0
    sql = 'execute pcancel_booking ?, ?, ?, ?, 0'
    db.safeexec(sql, bk_no, discount, None, 1)
    flash(f'Booking #{bk_no} successfully marked as No Show', 'info')
    Booking.reload(bk_no)
    occupancy_manager.load()
    return True

def process_uncancel(uncancel_form: UncancelForm) -> bool:
    if not(uncancel_form.uncancel.data and uncancel_form.validate()):
        return False
    
    assert(uncancel_form.booking.data)
    bk_no = int(uncancel_form.booking.data)
    sql = 'execute puncancel_booking ?'
    db.safeexec(sql, bk_no)
    flash(f'Booking #{bk_no} successfully Uncancelled', 'info')
    Booking.reload(bk_no)
    occupancy_manager.load()
    return True

def process_confirm(confirm_form: ConfirmForm) -> bool:
    if not(confirm_form.confirm.data and confirm_form.validate()):
        return False
    
    assert(confirm_form.booking.data)
    bk_no = int(confirm_form.booking.data)
    sql = 'execute pconfirm_booking ?'
    db.safeexec(sql, bk_no)
    flash(f'Booking #{bk_no} successfully confirmed', 'info')
    Booking.reload(bk_no)
    return True

def process_deposit2voucher(deposit2voucher_form: Deposit2VoucherForm) -> bool:
    if not(deposit2voucher_form.deposit2voucher.data and deposit2voucher_form.validate()):
        return False
    
    assert(deposit2voucher_form.booking.data)
    bk_no = int(deposit2voucher_form.booking.data)
    booking = Booking.fetch(bk_no)
    paid = booking.paid_amt

    if paid > 0:
        sql = 'execute pdeposit2voucher ?'
        db.safeexec(sql, bk_no)
        flash(f'Deposit for booking #{bk_no} successfully converted to voucher', 'info')
    else:
        flash(f'Booking #{bk_no} has no deposit', 'error')
    Booking.reload(bk_no)
    return True

def book_daycare(the_date: date, pet: Pet, run: Run, comment: str, duration: str):
    by = get_current_user_id()

    sql = 'pbook_daycare'
    bk_no, msg = db.safeexec_return_msg(sql, the_date, pet.no, run.no, duration, comment, by)
    if bk_no:
        Booking.reload(bk_no)
        occupancy_manager.load()
        return bk_no, msg
    else:
        return 0, msg

def process_book_request(form: BookRequestForm) -> bool:
    if not(form.submit.data and form.validate()):
        return False
    
    assert(form.req.data)
    fd_no = int(form.req.data)
    
    sql = """select fd_no, fd_period, fd_surname, fd_date_created, fd_status, fd_issues, fd_comments, fd_bk_no, fd_bk_status, fd_cust_no, fd_html_body, fd_email
from vwformdata_6 where fd_no = ?"""

    req = db.safeselect_one(sql, fd_no)

    deluxe = form.deluxe.data
    
    if not req:
        flash(f'Request #{fd_no} not found', 'error')
        return False
    
    if not req.fd_cust_no:
        flash(f'Request #{fd_no} has no valid customer', 'error')
        return False
    
    if req.fd_bk_no:
        msg = f'Request #{fd_no} is already booked (bk_no <a href="/booking/{req.fd_bk_no}">{req.fd_bk_no}</a>)'
        flash(msg, 'error')
        return False
    
    log_no = start_log()
    
    di = 1 if deluxe else 0
    
    sql = 'exec pcreate_booking_form ?, ?, ?, ?, ?'
    db.safeexec(sql, fd_no, 1, '', di, 'Intranet')

    log_entries = end_log(log_no)
    flash_log_entries(log_entries)
    
    bk_no = db.safeselect_value('select fd_bk_no from vwformdata_6 where fd_no = ?', fd_no)    
    
    if bk_no:
        ref = f'/booking/{ bk_no }'
        msg = f'<a href="{ref}" class="alert-link">Booking {bk_no}</a> created successfully.'
        flash(msg, 'info')
        Booking.get(bk_no)
        return True
    else:
        flash(f'Failed to book request {fd_no}', 'error')        
    return True

def process_delete_pet(form: PetSubform) -> int:
    assert(form.pet_no.data)
    pet_no = int(form.pet_no.data)
    assert(form.cust_no.data)
    cust_no = int(form.cust_no.data)
    sql = 'execute pdelete_pet ?'
    log_no = start_log()
    db.safeexec(sql, pet_no)
    log_entries = end_log(log_no)
    flash_log_entries(log_entries)
    
    still_pet_no = db.safeselect_value("select pet_no from pa..tblpet where pet_no=?", pet_no)
    if still_pet_no:
        msg = f'Deleting pet number {pet_no} failed'
        flash(msg, 'error')
        return pet_no
    else:
        msg = f'Deleted record for pet mumber {pet_no}'
        Customer.reload(cust_no)
        flash(msg, 'info')
    return -1
    
def process_pet_subform(form: PetSubform) -> int:
    cust_no = form.cust_no.data
    pet_no = 0
    new_pet = True
    if form.pet_no.data:
        pet_no = int(form.pet_no.data)
        if pet_no:
            new_pet = False
    origin = 'process_pet_form'
    deceased = ''
    if form.deceased.data:
        deceased = 'Y'
    
    daycare_approved = 1 if form.daycare_approved.data else 0
    
    meds = 0
    if form.meds.data:
        meds = 1
    
    feed = form.feeding.data
    kennel_notes = form.kennel_notes.data
    
    sql = 'execute pupdate_pet ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?'
    log_no = start_log()
    db.safeexec(sql, pet_no, cust_no, form.pet_name.data, form.species.data, form.breed.data,
        form.sex.data, form.neutered.data, form.dob.data, form.microchip.data, form.insurer.data,
        form.vet.data, form.vacc.data, form.kc.data, deceased, form.warning.data,
        feed, kennel_notes, meds, form.health.data, form.behaviour.data, form.inventory.data,
        form.body_score.data, form.diabetic.data, daycare_approved, 'crowbank', origin)
    
    log_entries = end_log(log_no)

    if not pet_no:
        pet_no = db.safeselect_value(f"select max(pet_no) from pa..tblpet where pet_cust_no=?", cust_no)

    if not pet_no:
        if log_entries:
            for log_entry in log_entries:
                flash(log_entry.msg, 'error')
        return 0

    if new_pet:
        msg = f'Pet {form.pet_name.data} created successfully'
    else:
        msg = f'Pet record for {form.pet_name.data} updated successfully'
    flash(msg, 'info')
    
    if form.weight.data:
        sql = 'execute padd_weight ?, ?, ?, ?'
        db.safeexec(sql, pet_no, form.weight.data, form.weight_date.data, form.weight_comment.data)
        msg = f'Pet weight for {form.pet_name.data} updated successfully'
    
    Pet.reload(pet_no)
    return pet_no

def process_pet_form(form: PetForm) -> int:
    if form.submit.data and form.validate():
        return process_pet_subform(cast(PetSubform, form.pet_form))
    if form.delete.data:
        return process_delete_pet(cast(PetSubform, form.pet_form))
    return False

def process_checkinreport_form(form: CheckinForm) -> bool:
    if form.submit.data and form.validate():
        return True
    return False

def reject_request(req_no: int, comment = '') -> int:
    sql = "exec pupdate_form_status ?, 'rejected', ?, 'reject_request'"
    db.safeexec(sql, req_no, comment)
    return -1

def process_booking_form(form: BookingForm) -> int:
    today = date.today()

    if not ((form.book.data or form.reject.data or form.amend.data)):
        return 0
    
    if not form.validate():
        if any(key != 'run' for key in form.errors.keys()):
            return 0
    
    if form.reject.data:
        assert(form.req_no.data)
        req_no = int(form.req_no.data)
        additional_text = form.additional_text.data if form.additional_text.data else ''
        return reject_request(req_no, additional_text)
    bk_no = 0
    if form.bk_no.data:
        status = ''
        bk_no = int(form.bk_no.data)
    elif form.book.data:
        status = 'B'
    else:
        status = 'S'
    
    pet_numbers = []

    for pet_field in form.pets.entries:
        if pet_field.pet_in.data:
            pet_numbers.append(pet_field.pet_no.data)

    pets = ','.join(pet_numbers)
    
    pet_objects = [Pet.fetch(pet_no) for pet_no in pet_numbers]

    start_time = form.start_time.data
    end_time = form.end_time.data
    slot = ''

    assert(form.cust_no.data)
    cust_no = int(form.cust_no.data)
    if form.deluxe.data:
        deluxe = 1
    else:
        deluxe = 0
        
    if form.separate.data:
        sharing = 0
    else:
        sharing = 1

    if form.daycare.data:
        daycare = 1
        slot = form.slot.data
        assert(form.date_range.data)
        start_date = parse_date(form.date_range.data)
        if not slot:
            slot = 'full'
        if slot == 'full':
            start_time = '08:00'
            end_time = '18:00'
        elif slot == 'am':
            start_time = '08:00'
            end_time = '12:30'
        else:
            start_time = '14:00'
            end_time = '18:00'

        bk_no = 0
        for pet in pet_objects:
            if pet.daycare_approved:
                assert(form.run.data)
                run_no = int(form.run.data)
                run = Run.fetch(run_no)
                comment = form.comment.data if form.comment.data else ''
                bk_no, _ = book_daycare(start_date, pet, run, comment, slot)
        
        return bk_no
        
    assert(form.date_range.data)
    if form.trial.data:
        trial = 1
        start_date = parse_date(form.date_range.data)
        end_date = start_date
        start_time = '14:00'
        end_time = '16:00'
    else:
        trial = 0
        rs = form.date_range.data.split(' - ')
        start_date = parse_date(rs[0])
        end_date = parse_date(rs[1])
    
    daycare = 0
        
    if form.transport.data:
        transport = 1
    else:
        transport = 0

    if form.retain_invoice.data:
        retain_invoice = 1
    else:
        retain_invoice = 0
                    
    comment = ''
    if form.comment.data:
        comment = form.comment.data.replace("'", "''")
    
    origin = 'process_booking_form'
    
    confirmation_text = form.additional_text.data
    
    has_dogs = any(map(lambda p: p.is_dog if p else False, pet_objects))
    has_cats = any(map(lambda p: p.is_cat if p else False, pet_objects))
    
    if not check_dates(start_date, end_date, has_dogs, has_cats):
        decline_booking('Some dates blocked in kennels')
        return 0
    
    log_no = start_log()
    rt_desc = form.runtype.data
    if rt_desc == 'default':
        rt_desc = None
    if bk_no:
        confirm = form.confirm_change.data
        if trial:
            sql = 'execute pamend_trial_booking ?, ?, ?, ?, ?, ?'
            db.safeexec(sql, bk_no, start_date, pets, comment, sharing, origin)
            if confirm:
                try:
                    hist_no = send_customer_email(bk_no, 'vwemail_confirm', form.additional_text.data, form.email.data)
                except:
                    hist_no = 0
                if hist_no:
                    flash(f'Amended confirmation sent. <a target="_blank" href="/display_email/{hist_no}">Click</a> to display', 'info')
                else:
                    flash('Sending amended confirmation failed', 'error')
        else:
            sql = 'execute pamend_booking ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?'
            db.safeexec(sql, bk_no, start_date, end_date, start_time, end_time, pets, status,
                comment, deluxe, sharing, transport, daycare, confirm, confirmation_text, retain_invoice, rt_desc, origin)
            if confirm:
                try:
                    hist_no = send_customer_email(bk_no, 'vwemail_confirm', form.additional_text.data, form.email.data)
                except:
                    hist_no = 0
                if hist_no:
                    flash(f'Amended confirmation sent. <a target="_blank" href="/display_email/{hist_no}">Click</a> to display', 'info')
                else:
                    flash('Sending amended confirmation failed', 'error')
    else:
        if trial:
            sql = 'execute pcreate_trial_booking ?, ?, ?, ?, ?, ?'
            db.safeexec(sql, cust_no, start_date, pets, comment, sharing, origin)
        else:
            sql = 'execute pcreate_online_booking ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?'
            db.safeexec(sql, cust_no, start_date, end_date, start_time, end_time, pets, status, 
                comment, deluxe, sharing, transport, daycare, confirmation_text, rt_desc, origin)
            bk_no = db.safeselect_value('select bk_no from pa..tblbooking where bk_cust_no=? and bk_start_date = ? and bk_create_date = ?',
                    cust_no, start_date, today)
            if form.additional_text.data:
                address = form.email.data
                if not address:
                    address = db.safeselect_value('select cust_email from pa..tblcustomer where cust_no=?', cust_no)
                hist_no = send_customer_email(bk_no, 'vwemail_confirm', form.additional_text.data, address)
                if hist_no:
                    flash(f'Confirmation sent. <a target="_blank" href="/display_email/{hist_no}">Click</a> to display', 'info')
                else:
                    flash('Sending confirmation failed', 'error')


    log_entries = end_log(log_no)

    if not bk_no:
        bk_no = db.safeselect_value('select bk_no from pa..tblbooking where bk_cust_no=? and bk_start_date = ? and bk_create_date = ?',
                                    cust_no, start_date, today)

    if not bk_no:
        flash_log_entries(log_entries)
        return 0
            
    occupancy_manager.load()
    Booking.reload(bk_no)
    
    return bk_no

def process_checkout(checkout_form: CheckoutForm) -> bool:
    if checkout_form.submit.data and checkout_form.validate():
        bk_no = checkout_form.bk_no.data
        
        sql = 'exec pcheckout ?, ?'
        db.safeexec(sql, bk_no, checkout_form.checkout_date.data)
        flash('All pets checked out', 'info')
        return True
    return False

def process_markin(bk_no: int, mark_date: date) -> None:
    sql = 'exec pcheckin_quick ?, ?'
    db.safeexec(sql, bk_no, mark_date)
    
def process_marknoshow(bk_no: int, mark_date: date) -> None:
    sql = 'exec pdaycare_noshow ?, ?'
    db.safeexec(sql, bk_no, mark_date)
    
def process_checkin(checkin_form: CheckinForm) -> bool:
    if checkin_form.submit.data and checkin_form.validate():
        assert(checkin_form.bk_no.data)
        bk_no = int(checkin_form.bk_no.data)
        for subform in checkin_form.pet_forms:
            process_pet_subform(subform)
        checkin_date = checkin_form.checkin_date.data
        assert(checkin_date)
        process_markin(bk_no, checkin_date)
        return True
    for subform in checkin_form.pet_forms:
        if subform.save.data:
            process_pet_subform(subform)
            return False
    return False

def process_payment(pay_form: PaymentForm) -> int:
    if (pay_form.payzone.data or pay_form.cash.data or pay_form.pbl.data or pay_form.voucher.data) and pay_form.validate():
        if pay_form.payzone.data:
            pay_type = 'Bank Xfer'
        elif pay_form.cash.data:
            pay_type = 'Cash'
        elif pay_form.voucher.data:
            pay_type = 'Voucher'
        else:
            pay_type = 'Stripe'
        
        if pay_form.amount.data:
            amount = float(pay_form.amount.data)
            voucher_balance = float(pay_form.voucher_balance.data) if pay_form.voucher_balance.data else 0.0
            if pay_type == 'Voucher' and amount > voucher_balance:
                amount = voucher_balance
            bk_no = pay_form.booking.data
            pay_date = pay_form.pay_date.data
            add_payment(bk_no, amount, pay_type, pay_date)
        return True
    return False

def process_vetbill_form(form: VetBillForm) -> bool:
    if not (form.submit.data and form.validate()):
        return False
    if form.amount.data:
        amount = float(form.amount.data)
        assert(form.bk_no.data)
        bk_no = int(form.bk_no.data)
        visit_date = form.date.data
        pet_no = form.pet.data
        visits = form.visits.data
        filename = f"{pet_no} {visit_date}.pdf"
        
        sql = 'execute padd_bill ?, ?, ?, ?'
        db.safeexec(sql, pet_no, visit_date, amount, visits)
        Booking.reload(bk_no)
        flash(f"Processed vet bill. Save file as {filename} in K:/vetbills folder", 'info')
    return True

def process_visit_form(form: VisitForm) -> int:
    if not ((form.submit.data or form.cancel.data or form.complete.data) and form.validate()):
        return 0
    
    visit_no = int(form.visit_no.data) if form.visit_no.data else 0
    new_visit = True if visit_no else False
    
    if form.cancel.data and form.visit_no.data:
        sql = 'execute pcancel_visit ?'
        db.safeexec(sql, visit_no)
        flash(f'Visit {visit_no} canceled', 'info')
        return -1

    if not form.visit_no.data:
        sql = 'set nocount on; declare @visit_no int; execute @visit_no = pcreate_visit ?, ?, ?, ?, ?, ?, ?, ?, ?, ?; select @visit_no visit_no'
        visit_no = db.safeselect_value(sql, form.cust_no.data, form.visit_date.data, form.visit_time.data, form.duration.data,
                form.notes.data, form.subject.data, form.kennel_tour.data, form.cattery_tour.data,
                form.meet_and_greet.data, form.daycare_assessment.data, commit=True)
        assert(visit_no)
        visit_no = int(visit_no)
        flash(f'New visit created', 'info')
    else:
        sql = 'execute pamend_visit ?, ?, ?, ?, ?, ?, ?, ?, ?, ?'
        db.safeexec(sql, visit_no, form.visit_date.data, form.visit_time.data, form.duration.data,
                form.subject.data, form.notes.data,
                form.kennel_tour.data, form.cattery_tour.data,
                form.meet_and_greet.data, form.cust_no.data)
        flash(f'Visit {form.visit_no.data} updated', 'info')
    if form.complete.data:
        sql = 'execute pcomplete_visit ?'
        db.safeexec(sql, visit_no)
        flash(f'Visit {visit_no} marked completed', 'info')
        return -1
    if new_visit:
        return visit_no
    return -1

def process_customer_form(form: CustomerForm) -> int:
    if not (form.submit.data and form.validate()):
        return 0
    cust_no = form.cust_no.data
    if cust_no:
        sql = 'execute pupdate_customer ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?'
        db.safeexec(sql, cust_no, form.cust_surname.data, form.cust_forename.data,
                    form.cust_email.data, form.cust_email2.data, form.cust_addr1.data,
                    form.cust_addr3.data, form.cust_postcode.data, form.cust_telno_home.data,
                    form.cust_telno_mobile.data, form.cust_telno_mobile2.data,
                    form.cust_notes.data, form.cust_deposit.data, form.cust_sms.data,
                    form.cust_fb_review.data, form.cust_approved.data, form.cust_banned.data)
        flash(f'Customer {cust_no} updated', 'info')
        Customer.reload(cust_no)
    else:
        sql = 'set nocount on; declare @cust_no int; execute @cust_no = pcreate_customer ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?; select @cust_no cust_no'
        cust_no = db.safeselect_value(sql, form.cust_surname.data, form.cust_forename.data,
                    form.cust_email.data, form.cust_email2.data, form.cust_addr1.data,
                    form.cust_addr3.data, form.cust_postcode.data, form.cust_telno_home.data,
                    form.cust_telno_mobile.data, form.cust_telno_mobile2.data,
                    form.cust_notes.data, form.cust_deposit.data, form.cust_sms.data,
                    form.cust_fb_review.data, form.cust_approved.data, commit=True)
        assert(cust_no)
        Customer.get(cust_no)
        flash(f'Customer {cust_no} created', 'info')
    return cust_no

def populate_employee_form(employee: Employee, form: EmployeeForm):
    form.emp_no.data = employee.no
    form.emp_nickname.data = employee.nickname
    form.emp_surname.data = employee.surname
    form.emp_forename.data = employee.forename
    form.emp_rank.data = str(employee.rank_no)
    form.emp_email.data = employee.email
    form.emp_dob.data = employee.dob
    form.emp_telno_mobile.data = employee.telno_mobile
    form.emp_addr1.data = employee.addr1
    form.emp_addr3.data = employee.addr3
    form.emp_postcode.data = employee.postcode
    form.emp_start_date.data = employee.start_date
    form.emp_end_date.data = employee.end_date
    form.emp_current_rate.data = employee.current_rate
    form.emp_iscurrent.data = employee.iscurrent
    form.emp_order.data = employee.order
    form.emp_facebook.data = employee.facebook
    form.emp_ni.data = employee.ni
    form.emp_wagecat.data = str(employee.wagecat_no)
    form.emp_contract_start_date.data = employee.contract_start_date
    form.emp_contract_end_date.data = employee.contract_end_date
    form.emp_code_no.data = employee.code_no
    form.emp_id.data = employee.id
    form.emp_short_code_no.data = employee.short_code_no
    form.emp_telno_home.data = employee.telno_home
    form.emp_telno_emergency.data = employee.telno_emergency
    form.emp_sex.data = employee.sex
    form.emp_ref.data = employee.ref
    form.emp_emergency_contact.data = employee.emergency_contact
    form.emp_sortcode.data = employee.sortcode
    form.emp_account_no.data = employee.account_no
    form.emp_role.data = str(employee.role)
    pass


def populate_employee_form_from_starter(starter: EmployeeStarter, form: EmployeeForm):
    form.emp_no.data = 0
    form.emp_nickname.data = starter.nickname
    form.emp_surname.data = starter.surname
    form.emp_forename.data = starter.forename
    form.emp_rank.data = '3' # Dog Walker
    form.emp_email.data = starter.email
    form.emp_dob.data = starter.dob
    form.emp_telno_mobile.data = starter.telno_mobile
    form.emp_addr1.data = starter.addr1
    form.emp_addr3.data = starter.addr3
    form.emp_postcode.data = starter.postcode
    form.emp_start_date.data = starter.start_date
    form.emp_current_rate.data = starter.min_wage
    form.emp_iscurrent.data = True
    form.emp_order.data = 70
    form.emp_ni.data = starter.ni
    form.emp_wagecat.default = '3' # ASSISTANT
    form.emp_telno_home.data = starter.telno_home
    form.emp_emergency_contact.data = starter.emergency_contact
    form.emp_telno_emergency.data = starter.telno_emergency
    form.emp_sex.data = starter.sex
    form.emp_sortcode.data = starter.sortcode
    form.emp_account_no.data = starter.account_no
    form.emp_role.data = '2' # EMPLOYEE


def process_employee_form(form: EmployeeForm) -> int:
    if not (form.submit.data and form.validate()):
        return 0
    
    emp_no = int(form.emp_no.data) if form.emp_no.data else 0
    if emp_no:
        sql = 'execute pupdate_employee ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?'
#             unsafe_sql = f"""execute pupdate_employee
# '{form.emp_no.data}', '{form.emp_nickname.data}', '{form.emp_surname.data}', '{form.emp_forename.data}',
# '{form.emp_rank.data}', '{form.emp_email.data}', '{form.emp_dob.data}', '{form.emp_telno_mobile.data}',
# '{form.emp_addr1.data}', '{form.emp_addr3.data}', '{form.emp_postcode.data}', '{form.emp_start_date.data}',
# '{form.emp_end_date.data}', '{form.emp_current_rate.data}', '{form.emp_iscurrent.data}', '{form.emp_role.data}',
# '{form.emp_order.data}', '{form.emp_facebook.data}', '{form.emp_ni.data}', '{form.emp_wagecat.data}',
# '{form.emp_contract_start_date.data}', '{form.emp_contract_end_date.data}', '{form.emp_code_no.data}',
# '{form.emp_shared.data}', '{form.emp_id.data}', '{form.emp_short_code_no.data}', '{form.emp_telno_home.data}',
# '{form.emp_telno_emergency.data}', '{form.emp_sex.data}',' {form.emp_ref.data}'"""
        db.safeexec(sql, form.emp_no.data, form.emp_nickname.data, form.emp_surname.data, form.emp_forename.data,
                    form.emp_rank.data, form.emp_email.data, form.emp_dob.data, form.emp_telno_mobile.data,
                    form.emp_addr1.data, form.emp_addr3.data, form.emp_postcode.data, form.emp_start_date.data,
                    form.emp_end_date.data, form.emp_current_rate.data, 1 if form.emp_iscurrent.data else 0,
                    form.emp_role.data, form.emp_order.data, form.emp_facebook.data, form.emp_ni.data,
                    form.emp_wagecat.data, form.emp_contract_start_date.data, form.emp_contract_end_date.data,
                    form.emp_code_no.data, form.emp_id.data, form.emp_short_code_no.data,
                    form.emp_telno_home.data, form.emp_telno_emergency.data, form.emp_sex.data, form.emp_ref.data,
                    form.emp_emergency_contact.data, form.emp_sortcode.data, form.emp_account_no.data)
        flash(f'Employee {emp_no} updated', 'info')
        Employee.reload_employee(emp_no)
    else:
        sql = 'execute pcreate_employee ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?'
        nickname = form.emp_nickname.data
        db.safeexec(sql, nickname, form.emp_surname.data, form.emp_forename.data,
                    form.emp_rank.data, form.emp_email.data, form.emp_dob.data, form.emp_telno_mobile.data,
                    form.emp_addr1.data, form.emp_addr3.data, form.emp_postcode.data, form.emp_start_date.data,
                    form.emp_current_rate.data, form.emp_iscurrent.data, 
                    form.emp_order.data, form.emp_facebook.data, form.emp_ni.data, form.emp_wagecat.data,
                    form.emp_telno_emergency.data, form.emp_sex.data, form.emp_role.data)
        emp_no = db.safeselect_value('select emp_no from tblemployee where emp_nickname = ?', nickname)
        if emp_no:
            flash(f'Employee {emp_no} created', 'info')
            Employee.get(emp_no)
        else:
            flash('Employee creation failed', 'error')
            return 0
    return emp_no
