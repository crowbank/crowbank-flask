from utils import db
from flask import render_template, url_for
from datetime import timedelta, date
from models import Pet, Run

d1 = timedelta(days=1)

# Collection of functions for creating and displaying the run view.
#
# Summary
#
# 1. Data is collected from vwrunoccupancy_summary view, which presents:
#  ro_date, ro_boundary, ro_bk_no, ro_rt_desc, ro_pet_nos, ro_pet_names, ro_pet_desc
# 
# ro_boundary is blank for ongoing stay, or can be 'trial', 'in', 'in pm', 'out' or 'out pm'
#
# 2. ro_create_units converts the data into blocks, each of which
#  represents a continuous occupancy record by the same pets/booking in the
#  same rt over a block of dates. That creates that following data structure:
#
#  ro_unit: { rt: [ (bk_no, pets, start_date, start_slot, end_date, end_slot), ... ], ... }
# 
# 3. ro_allocate_runs allocates occupancy blocks into numbered runs. The logic
#  is to allocate blocks based on the order they come in, with later-ending
#  blocks taking precedence. Each block is allocated to the next available
#  run number. The resulting data structure is:
#
# { rt:
#   [
#      [
#         ro_unit, ...
#      ], ...
#   ], ...
# }
#
# 4. ro_create_table creates a table suitable for display, with the following structure:
#
# { rt: 
#    [
#       [ cell, ...], ...
#    ], ...
# }
#
# with each cell representing date+(am/pm) as follows:
# cell: (border_type, status, bk_no, pet_desc)
#   border_type is one of: empty, first, last, trial, in
#   status is one of 'V', 'B' or 'O'
#
# 5. runview.html displays the table in a table. For that it also needs a time vector of
#    dates

def cell_count(start_date, start_slot, end_date, end_slot):
    days = (end_date - start_date).days
    cells = 2 * days + 1
    if start_slot < end_slot:
        cells += 1
    if start_slot > end_slot:
        cells -= 1
    return cells
    
def ro_create_units(rows):
    #  ro_create_units converts the data into blocks, each of which
#  represents a continuous occupancy record by the same pets/booking in the
#  same rt over a block of dates. That creates that following data structure:
#
#  ro_unit: { rt: [ (bk_no, pets, start_date, start_slot, end_date, end_slot), ... ], ... }

    ro_unit_dict = {}
    pet_dict = {}
    # a dictionary mapping rt+pet_nos -> (start_date, start_slot, end_date, end_slot)
    
    for row in rows:
        if not row.ro_pet_nos in pet_dict:
            bk_no = row.ro_bk_no
            pet_numbers = row.ro_pet_nos.split(',')
            pet_names = row.ro_pet_desc.split(',')
            pets = zip(pet_numbers, pet_names)
            pet2url = lambda p:'<a target="_" href="' + url_for('show_booking', bk_no=bk_no) + '">' + p[1] + '</a>'
            html = ', '.join(map(pet2url, pets))
            pet_dict[row.ro_pet_nos] = html
        else:
            pass
            
        key = (row.ro_rt_desc, row.ro_bk_no, row.ro_pet_nos)
        if key in ro_unit_dict:
            match_date = ro_unit_dict[key][-1]['end_date'] + d1
            if row.ro_date.date() == match_date:
                # extend by one day
                unit = ro_unit_dict[key][-1]
                unit['end_date'] = row.ro_date.date()
                unit['end_slot'] = 'am' if row.ro_boundary == 'out' else 'pm'    
        else:
            # create a new block
            unit = {
                'status': row.ro_bk_status,
                'start_date': row.ro_date.date(),
                'start_slot': 'pm' if row.ro_boundary == 'in pm' else 'am',
                'end_date': row.ro_date.date(),
                'end_slot': 'pm'
            }
            if key in ro_unit_dict:
                ro_unit_dict[key].append(unit)
            else:
                ro_unit_dict[key] = [unit]
    
    ro_units = {}
    for (key, units) in ro_unit_dict.items():
        rt = key[0]
        if not rt in ro_units:
            ro_units[rt] = []
        for unit in units:
            ro_units[rt].append( {
              'bk_no': key[1],
              'pet_nos': key[2],
              'status': unit['status'],
              'start_date': unit['start_date'],
              'start_slot': unit['start_slot'],
              'end_date': unit['end_date'],
              'end_slot': unit['end_slot']
            })

    return ro_units, pet_dict

def actual_ro_create_units(rows):
    #  ro_create_units converts the data into blocks, each of which
#  represents a continuous occupancy record by the same pets/booking in the
#  same run over a block of dates. That creates that following data structure:
#
#  ro_unit: { wing: [ {
#       'run_code': run_code,
#       'bookings': [
#           'bk_no': bk_no,
#           'pets': [pet_no_1, pet_no_2, ...],
#           'start_date': start_date,
#           'start_slot': start_slot,
#           'start_time': start_time,
#           'end_date': end_date,
#           'end_slot': end_slot,
#           'end_time': end_time
#       ] }, ... ], ... }

    ro_unit_dict = {}
    pet_dict = {}
    # a dictionary mapping rt+pet_nos -> (start_date, start_slot, end_date, end_slot)

    for wing in Run.runs_by_wing:
        ro_unit_dict[wing] = []
        for run in Run.runs_by_wing[wing]:
            ro_unit_dict[wing].append( {
                'run': run.code,
                'bookings': []
            })
    
    for row in rows:
        if not row.ro_pet_nos in pet_dict:
            bk_no = row.ro_bk_no
            pet_numbers = row.ro_pet_nos.split(',')
            for pet_no_str in pet_numbers:
                pet_no = int(pet_no_str)
                if not pet_no in pet_dict:
                    pet_dict[pet_no] = Pet.get(pet_no)

        run = row.ro_run_code
                    
        key = (row.ro_run_code, row.ro_bk_no, row.ro_pet_nos)
        if key in ro_unit_dict:
            match_date = ro_unit_dict[key][-1]['end_date'] + d1
            if row.ro_date.date() == match_date:
                # extend by one day
                unit = ro_unit_dict[key][-1]
                unit['end_date'] = row.ro_date.date()
                unit['end_slot'] = 'am' if row.ro_boundary == 'out' else 'pm'    
        else:
            # create a new block
            unit = {
                'status': row.ro_bk_status,
                'start_date': row.ro_date.date(),
                'start_slot': 'pm' if row.ro_boundary == 'in pm' else 'am',
                'end_date': row.ro_date.date(),
                'end_slot': 'pm'
            }
            if key in ro_unit_dict:
                ro_unit_dict[key].append(unit)
            else:
                ro_unit_dict[key] = [unit]
    
    ro_units = {}
    for (key, units) in ro_unit_dict.items():
        run_code = key[0]
        run = Run.fetch(run_code)
        rt = run.runtype
        if not rt in ro_units:
            ro_units[rt] = []
        for unit in units:
            ro_units[rt].append( {
              'bk_no': key[1],
              'pet_nos': key[2],
              'status': unit['status'],
              'start_date': unit['start_date'],
              'start_slot': unit['start_slot'],
              'end_date': unit['end_date'],
              'end_slot': unit['end_slot']
            })

    return ro_units, pet_dict

def ro_allocate_runs(units, rt_capacity):
#  ro_allocate_runs allocates occupancy blocks into numbered runs. The logic
#  is to allocate blocks based on the order they come in, with later-ending
#  blocks taking precedence. Each block is allocated to the next available
#  run number. The resulting data structure is:
#
# { rt:
#   [
#      [
#         ro_unit, ...
#      ], ...
#   ], ...
# }
#
    allocated_runs = {}
    for (rt, rt_units) in units.items():
        allocated_runs[rt] = []
        for details in rt_units:
            bk_no = details['bk_no']
            pets = details['pet_nos']
            found = False
            status = details['status']
            start_date = details['start_date']
            start_slot = details['start_slot']
            end_date = details['end_date']
            end_slot = details['end_slot']
            block = {
                        'bk_no': bk_no,
                        'pets': pets,
                        'status': status,
                        'start_date': start_date,
                        'start_slot': start_slot,
                        'end_date': end_date,
                        'end_slot': end_slot
                    }
            for run in allocated_runs[rt]:
                latest = run[-1]
                if latest['end_date'] < details['start_date'] or (
                        latest['end_date'] == details['start_date'] and latest['end_slot'] == 'am' and
                        details['start_slot'] == 'pm'):
                    run.append(block)
                    found = True
                    break
            if not found:
                allocated_runs[rt].append([block])
        if rt != 'Unalloc' and len(allocated_runs[rt]) < rt_capacity[rt]:
            for _ in range(rt_capacity[rt] - len(allocated_runs[rt])):
                allocated_runs[rt].append([])
    
    return allocated_runs

def ro_date_vector(start_date, end_date):
    date_vector = []
    d = start_date
    while d <= end_date:
        date_vector.append(d)
        d += d1
    return date_vector

def ro_create_table(allocated_runs, start_date: date, end_date: date, pet_dict):
# ro_create_table creates a table suitable for display, with the following structure:
#
# { rt: 
#    [
#       [ cell, ...], ...
#    ], ...
# }
#
# with each cell representing a range of columns which are either empty, or refer to a single occupancy unit
# cell: (status, columns, bk_no, pet_names, pet_desc)
#   status corresponds to the booking status, one of 'B', 'V' or 'O'

    ro_table = {}
    
    row_num = 0
    for (rt, runs) in allocated_runs.items():
        row_num += 1
        rt_table = []
        for run in runs:
            run_row = []
            prev_date = (start_date + timedelta(days=-1))
            prev_slot = 'pm'
            block_count = 0
            bk_end_date = date(1900, 1, 1)
            bk_end_slot = 'am'
            
            for block in run:
                block_count += 1
                bk_start_date = block['start_date']
                bk_start_slot = block['start_slot']
                bk_end_date = block['end_date']
                bk_end_slot = block['end_slot']
                bk_status = block['status']
                columns = cell_count(bk_start_date, bk_start_slot, bk_end_date, bk_end_slot)
                pets = block['pets']
                bk_no = block['bk_no']
                if bk_start_date > prev_date or bk_start_slot > prev_slot:
                    c = cell_count(prev_date, prev_slot, bk_start_date, bk_start_slot) - 2
                    if c:
                        run_row.append({ 'status': '',
                                        'columns': c
                                        })
                run_row.append( {
                    'status': bk_status,
                    'columns': columns,
                    'bk_no': bk_no,
                    'pets': pet_dict[pets],
                    'start_date': bk_start_date,
                    'end_date': bk_end_date
                })
                prev_date = bk_end_date
                prev_slot = bk_end_slot
            if bk_end_date < end_date or bk_end_slot < 'pm':
                run_row.append({ 'status': '',
                                'columns': cell_count(bk_end_date, bk_end_slot, end_date, 'pm')
                               })
            
            
            rt_table.append(run_row)
        ro_table[rt] = rt_table
    return ro_table

def actual_ro_display(start_date, end_date):
    """Display actual runs; kennels only"""
    sql = """select ro_date, ro_run_no, ro_boundary, ro_bk_status, ro_bk_no, ro_pet_nos, ro_pet_names, ro_pet_desc
from vwrunoccupancy_actual
where ro_date between ? and ?
order by ro_rt_desc, ro_date
"""
    rows = db.safeselect(sql, start_date, end_date)
    
    units, pet_dict = actual_ro_create_units(rows)
    pass

def ro_display(start_date: date, end_date: date, species: str):
    assert(end_date >= start_date)
    
    # a wrapper that displays all rts for species between dates
    sql = """select ro_date, ro_rt_desc, ro_boundary, ro_bk_status, ro_bk_no, ro_pet_nos, ro_pet_names, ro_pet_desc
from vwrunoccupancy_summary
where ro_date between ? and ? and ro_spec = ?
order by ro_rt_desc, ro_date
"""
    rows = db.safeselect(sql, start_date, end_date, species)
    
    sql = """select rt_desc, count(*) rt_c
from pa..tblruntype
join pa..tblrun on run_rt_no = rt_no and run_inactive = ''
where rt_no <> -1
group by rt_desc"""
    
    rtc = db.safeselect(sql)
    
    rt_capacity = {}
    
    if rtc:
        for rt in rtc:
            rt_capacity[rt.rt_desc] = rt.rt_c
        
    units, pet_dict = ro_create_units(rows)
    
    allocated_runs = ro_allocate_runs(units, rt_capacity)
    
    date_vector = ro_date_vector(start_date, end_date) 
       
    ro_table = ro_create_table(allocated_runs, start_date, end_date, pet_dict)
    
    total_columns = 2 * (end_date - start_date).days + 2

    return render_template('components/_runview.html', ro_table=ro_table, dates=date_vector, total_columns=total_columns)
