{{ record.email_subject }}

Checking Out of Crowbank
________________________


You are scheduled to collect {{ record.email_pets }} at about {{ record.email_end_time }}
on {{ record.email_end_date.strftime("%d/%m/%Y") }}.

{% if record.email_deposit_amount > 0 %}
The balance on your booking is £{{ "{:.2f}".format(record.email_deposit_amount) }}.
{% else %}
The booking has been paid in full - thank you!
{% endif %}

As part of our Covid procedures, please note the following rules:

1. Upon arrival, please ring the bell in reception, but then wait outside. Depending on the weather, you might find it more comfortable
   to wait in your car.

2. We will bring {{ record.email_pets }} and their belonging to you.

{% if record.email_deposit_amount > 0 %}
{% if record.email_cashonly %}
3. We are currently having difficulties with our card service, and ask that you bring cash with you.
{% else %}
3. We are able to accept cash payments
4. Alternatively, to pay the balance, paste the following to your browser: {{ record.email_paylink }}
{% endif %}
{% endif %}