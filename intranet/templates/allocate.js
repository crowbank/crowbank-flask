let period_start_date = ddmmyyyy2date('{{ start_date }}')
let period_end_date = ddmmyyyy2date('{{ end_date }}')
const slots = ['am', 'pm']
let active_date = new Date(period_start_date)
let active_date_str = date2yyyymmdd(active_date)
let bookings = null
let pets = null
let blocks = null
let allocations = null
const allocate_style = '{{ allocate_style }}'
const bod = new Date('1970-01-01 08:00').getTime()
let date_width = 100
let select_date_ratio = 2.0

function get_present_occupants(vector) {
    return vector.filter(block => active_date_str >= block.start_date && active_date_str <= block.end_date)
}

function get_future_occupants(vector) {
    return vector.filter(block => active_date_str < block.start_date)
}
function pet2html(pet) {
    if (!pet) {
        console.log('pet2html called without a pet')
        return
    }
    // html = `<span class="pet-card" id="${pet.no}" data-bs-toggle="tooltip" data-bs-html="true" class="pet-name" title="${pet.breed.desc}">
    // ${pet.name} (${pet.breed.desc})</span>`
    const html = `${pet.name} (${pet.breed.desc})`
    return html
}

function booking_href(bk_no) {
    return `${$SCRIPT_ROOT}/booking/${bk_no}`
}

function booking_anchor_wrap(bk_no, content) {
    return `<a href="${booking_href(bk_no)}" target="_blank">${content}</a>`
}

function booking2html(booking) {
    return booking_anchor_wrap(booking.no, booking.no)
}

function occupant2html(occupant) {
    if (!occupant) {
        return 'Error: null occupant'
    }
    if (!(occupant.bk_no in bookings)) {
        return `Error: ${occupant.bk_no} not in bookings`
    }

    const occ_pets = occupant.pets.map(pet_no => pets[pet_no])
    const pet_html_array = occ_pets.map(pet2html)
    const pet_html = pet_html_array.join(', ')

    html = pet_html
    return html
}

function occupant2tooltip(occupant) {
    const occupants = occupant2html(occupant)
    const start_date = date2wwwddmm(yyyymmdd2date(occupant.start_date))
    const end_date = date2wwwddmm(yyyymmdd2date(occupant.end_date))
    const start = occupant.start_time ? `${start_date} (${occupant.start_time})` : ''
    const end = occupant.end_time ? `${end_date} (${occupant.end_time})` : ''
    const tooltip = `${occupants}<br>${start} -<br>${end}  `
    return tooltip
}

function block2pet_names(block) {
    return block.pets.map(pet_no => pets[pet_no].name)
}

function occupant2html_table(block, id) {
    const pet_html = occupant2html(block)
    const pet_names = block2pet_names(block)
    const booking = bookings[block.bk_no]
    const booking_class = booking.daycare == 1 ? 'daycarebooking' : (booking.status.code == 'V' ? 'confirmedbooking' : 'bookingbooking')
    // html = `<span class="pet-card" id="${pet.no}" data-bs-toggle="tooltip" data-bs-html="true" class="pet-name" title="${pet.breed.desc}">
    // ${pet.name} (${pet.breed.desc})</span>`
    const tooltip = occupant2tooltip(block)
    const html = `<div data-bs-toggle="tooltip" data-bk_no="${block.bk_no}"
data-pets='${JSON.stringify(block.pets)}' data-bs-html="true" title="${tooltip}"
class="occupant-block ${booking_class}" id=${id}>${pet_html}</div>`
    return html
}

function datetime2html(date, time) {
    if (!time) {
        return ''
    }

    return `${date.substr(6, 2)}/${date.substr(4, 2)} ${time ? time : ''}`
}

function draw_occupant_table(container, block, id) {
    const html = occupant2html_table(block, id)
    const element = $(html)
    container.append(element)
    new bootstrap.Tooltip(element)
}

function draw_occupant(parent_container, tense, occupant = null) {
    const container = parent_container.find(`.run-${tense}`)
    const occupant_container = container.find('.occupant')
    const date_container = container.find('.dates')
    const booking_container = container.find('.booking')

    if (!container) {
        return
    }

    const containing_run = $(container.parent().parent())
    containing_run
        .removeClass('run-conflict')
        .removeClass('run-intranday')
        .removeClass('run-today')
        .removeClass('run-issue')

    if (!occupant) {
        booking_container.html('')
        occupant_container.html('')
        date_container.html('')
        return
    }

    occupant_container.html(occupant2html(occupant))

    
    const start_datetime = datetime2html(occupant.start_date, occupant.start_time)
    const end_datetime = datetime2html(occupant.end_date, occupant.end_time)
    date_container.html(`${start_datetime} -- ${end_datetime}`)

    const booking_html = booking2html(bookings[occupant.bk_no])
    booking_container.html(booking_html)

    if (tense == 'future' && occupant.time_span < 24) {
        const class_name = occupant.time_span < 0 ? 'conflict' : 'intraday'
        containing_run.addClass('run-issue').addClass(`run-${class_name}`)
        if (occupant.start_date == date2yyyymmdd(active_date)) {
            containing_run.addClass('run-today')
        }
    }
}

function block2unallocated_container(block) {
    const container = $('<div class="block-container"></div>')
    container.append($('<div class="booking"></div>'))
    container.append($('<div class="occupant"></div>'))
    container.append($('<div class="dates"></div>'))

    const booking_html = booking2html(bookings[block.bk_no])
    container.find('.booking').html(booking_html)
    container.find('.occupant').html(occupant2html(block))

    const date_html = `${datetime2html(block.start_date, block.start_time)} - ${datetime2html(block.end_date, block.end_time)}`
    container.find('.dates').html(date_html)

    return container
}

function draw_unallocated_tense(vector, tense) {
    const container = $(`.unallocateds #unalloc-${tense}`)
    container.html('')
    vector.forEach(block => {
      const block_container = block2unallocated_container(block)
      container.append(block_container)
    })
}

function draw_unallocateds(vector) {
    const present_unallocateds = get_present_occupants(vector)
    draw_unallocated_tense(present_unallocateds, 'present')

    const future_unallocateds = get_future_occupants(vector)
    draw_unallocated_tense(future_unallocateds, 'future')
}

function draw_run_table(run_code, run_vector) {
    if (!run_vector.length) {
        return
    }

    const container = $(`#${run_code} .run-container`)
    container.html('')
    run_vector.forEach((block, i) => {
        const id = `${run_code}_${i}`
        draw_occupant_table(container, block, id)
    })
}

function draw_table_dates() {
    const container = $('.date-row')
    $('.date-heading').remove()
    $('.date-boundary').remove()
    for(let date = new Date(period_start_date); date <= period_end_date; date.setDate(date.getDate() + 1)) {
        const date_str = date2wwwddmm(date)
        const date_id = date2yyyymmdd(date)
        container.append($(`<div class="date-heading" data-date="${date_id}">${date_str}</div>`))
        container.append($(`<div class="date-boundary" data-date="${date_id}"></div>`))
    }
    size_dates()
}

function draw_run(run_code, run_vector) {
    if (allocate_style == 'map') {
        return draw_run_map(run_code, run_vector)
    }
    return draw_run_table(run_code, run_vector)
}

function draw_run_map(run_code, run_vector) {
    if (!run_vector.length) {
        return
    }

    if (run_code == 'Unalloc') {
        draw_unallocateds(run_vector)
        return
    }

    const container = $(`#${run_code} .run-container`)
    draw_occupant(container, 'present')
    draw_occupant(container, 'future')
    const present_occupants = get_present_occupants(run_vector)
    if (present_occupants.length) {
        draw_occupant(container, 'present', present_occupants[0])
        if (present_occupants.length > 1) {
            draw_occupant(container, 'future', present_occupants[1])
            return
        }
    }

    const future_occupants = get_future_occupants(run_vector)
    if (!future_occupants.length) {
        return
    }

    draw_occupant(container, 'future', future_occupants[0])
}

function datetime2offset(date, time, is_start) {
    // Calculate the offset of a specific date/time
    // Offset depends on date relative to start/end dates, active_date 

    const days_from_start = (date - period_start_date) / 1000 / 3600 / 24
    const include_active = date > active_date ? 1 : 0
    const date_offset = date_width * (days_from_start + (select_date_ratio - 1.0) * include_active )
    const is_active = date2ddmmyyyy(date) == date2ddmmyyyy(active_date) ? 1 : 0
    let time_offset
    if (time) {
        const time_ms = (new Date(`1970-01-01 ${time}`)).getTime()
        time_offset = (time_ms - bod) / 1000 / 3600 / 10 * date_width *
            (is_active ? select_date_ratio : 1)
    } else {
        if (is_start) {
            time_offset = 0
        } else {
            time_offset = date_width * (is_active ? select_date_ratio : 1)
        }
    }
    return date_offset + time_offset
}

function get_max_offset() {
    return datetime2offset(period_end_date, '18:00')
}

function size_container() {
    const max_offset = get_max_offset()
    $('.run-container').each((i, d) => {
        $(d).css('width', `${max_offset}px`)
    })
}

function size_dates() {
    const height = $('.run-table').height()
    $('.date-heading').each((i, d) => {
        const date_id = $(d).data('date')
        const is_active = (active_date_str == date_id)
        const width = date_width * (is_active ? select_date_ratio : 1)
        const width_css = `${width}px`
        const date = yyyymmdd2date(date_id)
        const start_position = datetime2offset(date, '08:00') + 60
        $(d).css('width', width_css).css('left', `${start_position}px`)
    })
    $('.date-boundary').each((i, b) => {
        const date_id = $(b).data('date')
        const is_active = (active_date_str == date_id)
        const date = yyyymmdd2date(date_id)
        const start_position = datetime2offset(date, '08:00') + 60
        $(b).css('left', `${start_position}px`).css('height', `${height}px`)
    })
}    

function size_block(block_element, max_offset) {
    const id = $(block_element).attr('id')
    // id is of the form run-code:n with n being the index within the run vector for that run
    const [run_code, block_index] = id.split('_')
    const block = blocks[run_code][block_index]
    if (!block) {
        console.log('Block not found for: ', $(block_element))
        return
    }
    if (!'start_date' in block) {
        console.log('No Start Date: ', block)
        return
    }
    let start_offset = block.start_date ? datetime2offset(yyyymmdd2date(block.start_date), block.start_time, 1) : 0
    let end_offset = block.end_date ? datetime2offset(
        yyyymmdd2date(block.end_date), block.end_time, 0) : max_offset
    if (end_offset > max_offset) {
        end_offset = max_offset
    }
    if (start_offset < 0) {
        start_offset = 0
    }
    $(block_element).css('left', `${start_offset}px`).css('width', `${end_offset - start_offset}px`)
}

function remove_modal() {
    $('.modal').remove()
    $('.modal-backdrop').remove()
}

function size_blocks() {
    // Change size of all occupancy blocks as well as date labels
    // Called every time blocks are changed, as well as upon zoom in/out events
    // let date_width = 100
    // let select_date_ratio = 2.0

    const max_offset = get_max_offset()

    $('.occupant-block').each((i, block_element) => {
        size_block(block_element, max_offset)
    })
}

function move_run(bk_no, pet_nos, done) {
    // Set list of run names
    const runNames = ['SF1', 'SF2', 'SF3', 'SF4', 'SF5', 'SF6', 'SF7',
'SF8', 'DLX-F', 'SR1', 'SR2', 'SR3', 'SR4', 'SR5', 'SR6', 'SR7', 'SR8', 'DLX-R',
'NR1', 'NR2', 'NR3', 'NR4', 'NR5', 'NR6', 'NR7', 'NR8',
'NF1', 'NF2', 'NF3', 'NF4', 'NF5', 'NF6', 'NF7', 'NF8']

    pet_names = pet_nos.map(pet_no => pets[pet_no].name)

    // Create modal content
    let modalContent = `
    <div class="modal" tabindex="-1">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Move Run</h5>
            <button type="button" class="btn-close remove-modal"></button>
          </div>
          <div class="modal-body">
            <div class="d-flex justify-content-end">
                <button id="bookingButton" class="btn btn-info btn-sm float-right">Booking</button>
            </div>
            <div class="clearfix"></div>
            <ul id="petsList">${pet_names.map(pet => `<li style="list-style-type: none">
                <input data-name="${pet}" type="checkbox" checked> ${pet}</li>`).join('')}</ul>
            <label for="runSelector">New Run:</label>
            <select id="runSelector">${runNames.map(name => `<option>${name}</option>`).join('')}</select>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary remove-modal">Cancel</button>
            <button id="submitButton" type="button" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </div>
    </div>`

    // Append modal to body
    $('body').append(modalContent)

    // Show the modal
    $('.modal').modal('show')

    // Event handler for 'Booking' button
    $('#bookingButton').click(() => {
        window.open(`/booking/${bk_no}`, '_blank')
    })

    $('.remove-modal').click(function() {
        // Add your additional action here.
        remove_modal()
    })
  
    // Event handler for 'Submit' button
    $('#submitButton').click(() => {
        let selectedPets = []
        $('#petsList input:checked').each(function() {
            selectedPets.push($(this).data('name'))
        })

        let selectedRun = $('#runSelector').val()

        $.post({
            dataType: "json",
            contentType: 'application/json',
            url: '/_change_run',
            data: JSON.stringify({
                bk_no: bk_no,
                pets: selectedPets,
                run: selectedRun
            }),
            success: (data) => {
                if (data.success) {
                    $('.modal').remove()
                    $('.modal-backdrop').remove()
                    done()
                }
            }
        })

        // Hide the modal
        remove_modal()
    })
}

function draw_blocks() {
    for (const [run_code, run_vector] of Object.entries(blocks)) {
        draw_run(run_code, run_vector)
    }
    if (allocate_style == 'table') {
        size_blocks()
    }

    $('.occupant-block').click(function() {
        const bk_no = $(this).data('bk_no')  // Get the 'data-bk_no' attribute
        const pets = $(this).data('pets')  // Parse the 'data-pets' attribute
      
        move_run(bk_no, pets, () => {
            update_status('Run Changed')
            refresh_allocations(true)
        })
    })
}

function refresh_allocations(force = false) {
    let r = /(\d\d\/\d\d\/\d\d\d\d) - (\d\d\/\d\d\/\d\d\d\d)/
    let s = $('#date-range').val()
    let a = r.exec(s)

    period_start_date = ddmmyyyy2date(a[1])
    period_end_date = ddmmyyyy2date(a[2])
    console.log(period_start_date, period_end_date)

    active_date = new Date(period_start_date)
    active_date_str = date2yyyymmdd(active_date)
    reset_active()
    if (allocate_style == 'table') {
        size_container()
        draw_table_dates()
    }
    $('#unalloc-future').empty()

    const status_element = $('#status')
    status_element.html('Loading...')
    if(force) {
        $('.occupant-block').remove()
    }

    $.getJSON($SCRIPT_ROOT + '/_allocations', {
        start_date: a[1],
        end_date: a[2],
        force: force
    }, data => {
        status_element.html('Drawing...')
        bookings = data.bookings
        pets = data.pets
        blocks = data.blocks
        draw_blocks()
        status_element.html('Done')
    })
    .fail(function(jqXHR, textStatus, errorThrown) {
        update_status(textStatus, jqXHR.responseText)
    })
}

function reset_active() {
    const weekday = ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"]

    const day = weekday[active_date.getDay()];
    const date_str = `${day}, ${date2ddmmyyyy(active_date)}`
    size_blocks()
    size_dates()
    $('#active-date').text(date_str)

    $('.wing-header#north').text(`North Wing [${date_str}]`)
    $('.wing-header#south').text(`South Wing [${date_str}]`)
}

function complete_reset() {
    active_date_str = date2yyyymmdd(active_date)
    reset_active()
    draw_blocks()
}

$(document).ready(function() {
    $('#date-range').val(date2ddmmyyyy(period_start_date) + ' - ' + date2ddmmyyyy(period_end_date))
    reset_active()

    $('#active-date-box #fast-backward').click( () => {
        active_date = new Date(period_start_date)
        complete_reset()
    })
    
    $('#active-date-box #step-backward').click( () => {
        if (active_date > period_start_date) {
            active_date.setDate(active_date.getDate() - 1)
            complete_reset()
        }
    })
    
    $('#active-date-box #step-forward').click( () => {
        if (active_date < period_end_date) {
            active_date.setDate(active_date.getDate() + 1)
            complete_reset()
        }
    })
    
    $('#active-date-box #fast-forward').click( () => {
        active_date = new Date(period_end_date)
        complete_reset()
        }
    )

    $('#refresh').click(() => refresh_allocations(true))
    $('#date-range').change(() => refresh_allocations(true))
    refresh_allocations()
})