from urllib.parse import quote_plus
from datetime import timedelta, date
from markupsafe import Markup
from intranet import app

@app.template_filter('urlencode')
def urlencode_filter(s):
    if not s:
        return ''
    if type(s) == 'Markup':
        s = s.unescape()
    s = s.encode('utf8')
    s = quote_plus(s)
    return Markup(s)

@app.template_filter('next_day')
def next_day(the_date: date) -> date:
    return the_date + timedelta(days=1)

@app.template_filter('prev_day')
def prev_day(the_date: date) -> date:
    return the_date + timedelta(days=-1)

@app.template_filter('prev_week')
def prev_week(the_date: date) -> date:
    return the_date + timedelta(days=-7)

@app.template_filter('next_week')
def next_week(the_date: date) -> date:
    return the_date + timedelta(days=7)

@app.template_filter('remove_spaces')
def remove_spaces(value: str) -> str:
    return value.replace(' ', '')
