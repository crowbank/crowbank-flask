from flask_wtf import FlaskForm, RecaptchaField
from wtforms import (TextAreaField,
                     StringField,
                     SubmitField,
                     HiddenField,
                     BooleanField,
                     DecimalField,
                     DateField,
                     TimeField,
                     FormField,
                     FieldList,
                     IntegerField,
                     RadioField,
                     PasswordField,
                     SelectField,
                     FloatField,
                     FileField,
                     EmailField)

from wtforms.validators import (InputRequired,
                                DataRequired,
                                NumberRange,
                                Email,
                                EqualTo,
                                Optional)

def always_pass(form, field):
    # This validator does nothing, meaning it always passes validation
    pass

class BlankForm(FlaskForm):
    submit = SubmitField('Submit')
    
class EmailDocsForm(FlaskForm):
    view = SelectField('Notice Type')
    email_address = StringField('Email Address (leave empty for display only)')
    bk_no = StringField('Booking #')
    submit = SubmitField('Generate')
    
class PaymentForm(FlaskForm):
    booking = HiddenField('Booking')
    voucher_balance = HiddenField('VoucherBalance')
    amount = DecimalField('Amount', [InputRequired()])
    pay_date = DateField('Date', [InputRequired()], format='%Y-%m-%d')
    payzone = SubmitField('Bank Xfer')
    pbl = SubmitField('Stripe')
    cash = SubmitField('Cash')
    voucher = SubmitField('Voucher')
    
class SendConfirmationForm(FlaskForm):
    booking = HiddenField('Booking')
    email = StringField('Address', [Email()])
    force = BooleanField('Regenerate')
    cashonly = BooleanField('Cash Only')
    confirm = SubmitField('Confirmation')
    checkin = SubmitField('Checkin Form')
    checkout = SubmitField('Checkout Notice')
    outstanding = SubmitField('Outstanding')
    
class CancelBookingForm(FlaskForm):
    booking = HiddenField('Booking')
    voucher = BooleanField('To Voucher')
    notice = BooleanField('Send Notice')
    lapsed = BooleanField('Mark as Lapsed')
    refund = BooleanField('Refund Deposit')
    discount = IntegerField('Discount (%)', [NumberRange(min=0, max=100, message='Between 0 and 100%')])
    cancel = SubmitField('Cancel Booking')

class NoShowForm(FlaskForm):
    booking = HiddenField('Booking')
    discount = IntegerField('Discount (%)', [NumberRange(min=0, max=100, message='Between 0 and 100%')])
    notice = BooleanField('Send Cancellation Notice')
    noshow = SubmitField('Mark as No Show')

class UncancelForm(FlaskForm):
    booking = HiddenField('Booking')
    uncancel = SubmitField('Un-Cancel Booking')

class ConfirmForm(FlaskForm):
    booking = HiddenField('Booking')
    confirm = SubmitField('Confirm Booking')

class Deposit2VoucherForm(FlaskForm):
    booking = HiddenField('Booking')
    deposit2voucher = SubmitField('Deposit to Voucher')

class BookRequestForm(FlaskForm):
    req = HiddenField('Request')
    deluxe = BooleanField('Deluxe')
    submit = SubmitField('Create Booking')

class CheckoutForm(FlaskForm):
    bk_no = HiddenField('Booking')
    checkout_date = DateField('Checkout Date', [InputRequired()], format='%Y-%m-%d')
    submit = SubmitField('Checkout')

class PetBookForm(FlaskForm):
    """Checkin form."""
    pet_no = HiddenField('Pet Number')
    pet_name = HiddenField('Pet Name')
    pet_in = BooleanField('Book')

class SearchForm(FlaskForm):
    search = StringField("Search")
    go = SubmitField("Go")
    
class BookingForm(FlaskForm):
    rt_choices = [('default', 'Automatic'), ('standard', 'Standard'), ('double', 'Double'), ('deluxe', 'Deluxe'), ('home', 'Home')]
    cust_no = HiddenField('Customer')
    req_no = HiddenField('Request')
    email = HiddenField('Email')
    additional_text = TextAreaField('Email Note')
    bk_no = HiddenField('Booking')
    date_range = StringField('Dates', [InputRequired()])
    start_period = RadioField('Start Period', validators=[Optional()], choices=[('am', 'am'), ('pm', 'pm')])
    start_time = StringField('Start Time')
    end_period = RadioField('End Period', validators=[Optional()], choices=[('am', 'am'), ('pm', 'pm')])
    end_time = StringField('End Time')
    deluxe = BooleanField('Deluxe')
    trial = BooleanField('Trial')
    daycare = BooleanField('Day Care')
    runtype = SelectField('Run Type', validators=[Optional()], choices = rt_choices)
    
    slot = RadioField('Slot', validators=[Optional()], choices=[('full', 'Full Day'), ('am', 'Morning'), ('pm', 'Afternoon')], default='full')
    separate = BooleanField('Separate')
    transport = BooleanField('Transport')
    comment = TextAreaField('Comment')
    pets = FieldList(FormField(PetBookForm), min_entries = 1)
    confirm_change = BooleanField('Send New Confirmation')
    retain_invoice = BooleanField('Retain current charges')
    run = SelectField('Run', validators=[always_pass], choices = [])
    book = SubmitField('Book')
    reply = SubmitField('Reply')
    reject = SubmitField('Reject')
    amend = SubmitField('Amend')

class ChangeRequestCustomerForm(FlaskForm):
    fd_no = HiddenField('Request')
    cust_no = IntegerField('Customer #')   
    change = SubmitField('Change')
    
class RunCardForm(FlaskForm):
    bk_no = HiddenField('Booking')
    pet_no = HiddenField('PetNumber')
    food = TextAreaField('Food')
    health = TextAreaField('Health')
    comment = TextAreaField('Comment')
    meds = BooleanField('Meds')
    warning = TextAreaField('Warning')
    save = SubmitField('Save')
    print = SubmitField('Print')

class VetBillForm(FlaskForm):
    bk_no = HiddenField('Booking')
    amount = DecimalField('Amount', [InputRequired()])
    date = DateField('Date', [InputRequired()], format='%Y-%m-%d')
    visits = IntegerField('Visits', default=1)
    pet = SelectField('Pet', coerce=str)
    submit = SubmitField('Add Vet Bill')

class MedForm(FlaskForm):
    drug = StringField('Medication')
    dose = StringField('Dosage')
    comment = StringField('Comment')
    morning = BooleanField('Morning')
    breakfast = BooleanField('Breakfast')
    lunch = BooleanField('Lunch')
    dinner = BooleanField('Dinner')
    night = BooleanField('Night')
    acute = BooleanField('Acute')
    start_date = DateField('Start Date', format='%Y-%m-%d')
    end_date = DateField('End Date', format='%Y-%m-%d')

class PetSubform(FlaskForm):
    pet_no = HiddenField('PetNumber')
    cust_no = HiddenField('CustomerNumber')
    pet_name = StringField('Name', [InputRequired()])                                             # string
    species = RadioField('Species', [InputRequired()], coerce=int, choices=[(1, 'Dog'), (2, 'Cat')])    # integer?
    breed = SelectField('Breed', [InputRequired()], coerce=int)                                         # integer
    sex = RadioField('Sex', choices = [('M', 'Male'), ('F', 'Female')])  # string
    neutered = RadioField('Neutered', validators=[Optional()], choices = [('Y', 'Yes'), ('N', 'No')])
    dob = DateField('Date of Birth', format='%Y-%m-%d')
    microchip = StringField('Microchip')
    insurer = SelectField('Insurer', coerce=int)
    vet = SelectField('Vet', coerce=int)
    vacc = DateField('Vaccinations', validators=[Optional()], format='%Y-%m-%d')
    kc = DateField('KC', validators=[Optional()], format='%Y-%m-%d')
    body_score = SelectField('Body Score', coerce=int)
    weight_date = DateField('Weight Date', validators=[Optional()], format='%Y-%m-%d')
    weight = FloatField('Weight', validators=[Optional()])
    weight_comment = StringField('Comment')
    deceased = BooleanField('Deceased')
    warning = StringField('Warning')
    kennel_notes = TextAreaField('Kennel Notes')
    feeding = TextAreaField('Feeding')
    meds = BooleanField('On medication or supplaments')
    med_form = FieldList(FormField(MedForm), min_entries=1)
    health = TextAreaField('Health')
    behaviour = TextAreaField('Behaviour')
    inventory = TextAreaField('Inventory')
    diabetic = BooleanField('Diabetic')
    daycare_approved = BooleanField('Daycare Approved')
    save = SubmitField('Save')
    
class PetForm(FlaskForm):
    pet_form = FormField(PetSubform)
    submit = SubmitField('Submit')
    delete = SubmitField('Delete Pet')

class CheckinForm(FlaskForm):
    bk_no = HiddenField('Booking')
    checkin_date = DateField('Checkin Date', [InputRequired()], format='%Y-%m-%d')
    pet_forms = FieldList(FormField(PetSubform), min_entries=1)
    submit = SubmitField('Checkin')
    
class CheckinReportForm(FlaskForm):
    date_range = StringField('Date Range', [InputRequired()])
    dogs = BooleanField('Dogs')
    cats = BooleanField('Cats')
    submit = SubmitField('Go')
    
class VisitForm(FlaskForm):
    visit_no = HiddenField('VisitNumber')
    visit_date = DateField('Visit Date', [InputRequired()], format='%Y-%m-%d')
    visit_time = StringField('Visit Time')
    subject = StringField('Subject')
    duration = SelectField('Duration', choices=[(0.5, '30 min'), (1.0, '1 hour')], coerce=float)
    cust_no = IntegerField('Customer #', validators=[Optional()])
    kennel_tour = BooleanField('Kennel Tour')
    cattery_tour = BooleanField('Cattery Tour')
    meet_and_greet = BooleanField('Meet and Greet')
    daycare_assessment = BooleanField('Daycare Assessment')
    notes = TextAreaField('Notes')
    submit = SubmitField('Submit')
    complete = SubmitField('Completed')
    cancel = SubmitField('Cancel')
    
class ContactForm(FlaskForm):
    contact_no = HiddenField('No')
    name = StringField('Name')
    phone = StringField('Phone')
    email = EmailField('email')
    
class CustomerForm(FlaskForm):
    cust_no = HiddenField('CustomerNumber')
    cust_surname = StringField('Surname')
    cust_forename = StringField('Forename')
    cust_email = EmailField('Email')
    cust_email2 = EmailField('Email 2')
    cust_addr1 = StringField('Street')
    cust_addr3 = StringField('City')
    cust_postcode = StringField('Postcode')
    cust_telno_home = StringField('Home')
    cust_telno_mobile = StringField('Mobile')
    cust_telno_mobile2 = StringField('Mobile 2')
    cust_contact1 = FormField(ContactForm)
    cust_contact2 = FormField(ContactForm)
    cust_notes = TextAreaField('Notes')
    cust_deposit = BooleanField('Deposit')
    cust_sms = BooleanField('SMS')
    cust_fb_review = StringField('FB Review')
    cust_approved = BooleanField('Approved')
    cust_banned = BooleanField('Banned')
    submit = SubmitField('Submit')

class EmployeeForm(FlaskForm):
    emp_no = HiddenField('EmployeeNumber')
    emp_nickname = StringField('Nickname')
    emp_surname = StringField('Surname')
    emp_forename = StringField('Forename')
    emp_rank = SelectField('Rank', choices=[('7', 'Manager'), ('6', 'Senior Shift Leader'),
                                            ('1', 'Shift Leader'), ('2', 'Kennel Assistant'), ('3', 'Dog Walker'),
                                            ('5', 'Volunteer'), ('4', 'Other')])
    emp_email = EmailField('Email')
    emp_dob = DateField('Date of Birth', format='%Y-%m-%d')
    emp_telno_mobile = StringField('Mobile #')
    emp_addr1 = StringField('Street Address')
    emp_addr3 = StringField('City')
    emp_postcode = StringField('Postcode')
    
    emp_start_date = DateField('Start Date', format='%Y-%m-%d')
    emp_end_date = DateField('End Date', validators=[Optional()], format='%Y-%m-%d')
    
    # Optional field, defaults to min wage
    emp_current_rate = DecimalField('Hourly Rate', places=2, validators=[Optional()])
    
    emp_iscurrent = BooleanField('Current?')
    
    # Order as displayed on timesheet table
    emp_order = IntegerField('Order', validators=[Optional()])
    
    # Facebook Address
    emp_facebook = StringField('Facebook')
    emp_ni = StringField('NI')
    
    # Wage category, usually age-dependent
    emp_wagecat = SelectField('Wage Category',
                              choices=[('1', 'Manager'),
                                       ('2', 'Leader'),
                                       ('3', 'Assistant'),
                                       ('4', 'Walker'),
                                       ('5', 'Volunteer')])
    
    emp_contract_start_date = DateField('Contract Start Date', validators=[Optional()], format='%Y-%m-%d')
    emp_contract_end_date = DateField('Contract End Date', validators=[Optional()], format='%Y-%m-%d')
    
    # code associated with Salary Paid employee (401-* freeagent category)
    # set automatically with padd_employee_code
    emp_code_no = IntegerField('Code #', validators=[Optional()])
    
    # Freeagent user id for employee; set by padd_employee_code
    emp_id = IntegerField('Employee Id', validators=[Optional()])
    
    # Employee role, as a code from tblrole, with 3 for regular employees,
    # 4 for managers,
    # 5 for administrators and 6 for owners
    emp_role = SelectField('Employee Role')
    
    # code assocaited with Salary and Bonuses ( 902-* freeagent category)
    # set automatically with padd_employee_code    
    emp_short_code_no = IntegerField('Short Code#', validators=[Optional()])

    emp_telno_home = StringField('Home Phone')
    emp_telno_emergency = StringField('Emergency Phone')
    emp_sex = SelectField('Sex', choices = [('F', 'Female'), ('M', 'Male')])
    
    # reference number with accountant
    emp_ref = StringField('Emp Ref')
    
    emp_sortcode = StringField('Sort Code')
    emp_account_no = StringField('Account #')
    emp_emergency_contact = StringField('Emergency Contact')
    
    submit = SubmitField('Submit')
    
class LoginForm(FlaskForm):
    name = StringField('Username')
    password = PasswordField('Password')
    submit = SubmitField('Submit')

class PasswordForm(FlaskForm):
    current_pwd = PasswordField('Current Password')
    new_pwd = PasswordField('New Password', [
        DataRequired(), EqualTo('confirm', message='Passwords must match')
    ])
    confirm = PasswordField('Repeat Password')
    submit = SubmitField('Submit')
    
class CheckAvailabilityForm(FlaskForm):
    standard = BooleanField('Standard Kennels')
    deluxe = BooleanField('Deluxe Kennels')
    cattery = BooleanField('Cattery')
    start_date = DateField('Start Date')
    start_period = RadioField('Start Period', choices=[('am', 'am'), ('pm', 'pm')])
    end_date = DateField('End Date')
    end_period = RadioField('Start Period', choices=[('am', 'am'), ('pm', 'pm')])
    submit = SubmitField('Submit')
    
class PunchForm(FlaskForm):
    employee = SelectField('Employee')
    time = TimeField('Override Time [leave empty if not overriding current time]')
    comment = TextAreaField('Comment')
    submit = SubmitField('Submit')

class RotaConfirmForm(FlaskForm):
    weekstart = HiddenField('Weekstart')
    employee = SelectField('Employee', validators=[Optional()])
    comment = TextAreaField('Comments')
    accept = SubmitField('Accept')
    decline = SubmitField('Decline')

class NewEmployeeForm(FlaskForm):
    start_form_no = HiddenField('StartFormNo')
    nickname = StringField('Nickname')
    rank = SelectField('Rank', choices=[
        (1, 'Shift Leader'),
        (2, 'Kennel Assistant'),
        (3, 'Dog Walker'),
        (4, 'Other'),
        (5, 'Volunteer'),
        (6, 'Senior Shift Leader'),
        (7, 'Manager')
        ])
    order = IntegerField('Order')
    start_date = DateField('Start Date', format='%Y-%m-%d')
    rate = DecimalField('Hourly Rate', places=2)
    submit = SubmitField('Submit')

class NoteForm(FlaskForm):
    date = DateField('Date')
    time = TimeField('Time')
    entered_by = SelectField('Entered By', [InputRequired()], coerce=int)
    pet = SelectField('Pet', [InputRequired()], coerce=int)
    type = SelectField('Type', [InputRequired()], coerce=int)
    severity = SelectField('Severity', [InputRequired()], coerce=int)
    notes = TextAreaField('Notes')
    photo = FileField('Upload or take photo')
    submit = SubmitField('Submit')

