import logging
from flask import Flask, g, request, redirect
from flask_security.core import Security
from flask_wtf.csrf import CSRFProtect
from html2text import HTML2Text
from flask_cors import CORS

from intranet.middleware import CrowbankMiddleware
from .portal import portal
from .user import CustomUserDatastore, User, Role

log = logging.getLogger(__name__)

app = Flask(__name__)

app.secret_key = 'dasldkfj234 alkdsj'
app.config['SECURITY_RECOVERABLE'] = True  # Enable "forgot password" functionality
app.config['SECURITY_REGISTERABLE'] = True  # Enable "forgot password" functionality

app.register_blueprint(portal, url_prefix='/portal')

CORS(app)
csrf = CSRFProtect(app)

app.wsgi_app = CrowbankMiddleware(app.wsgi_app)

driver = 'ODBC+Driver+17+for+SQL+Server'
server = 'HP-SERVER\\SQLEXPRESS'
uid = 'PA'
pwd = 'petadmin'
database = 'crowbank'
h2t = HTML2Text()
h2t.ignore_links = True

user_datastore = CustomUserDatastore(User, Role)
security = Security(app, user_datastore)

import intranet.filters
@app.before_request
def before_request_func():
    from intranet.user import User
    from intranet.utilities import get_today_employees
    from intranet.forms import PunchForm

    host = app.config.get('CLI_HOST') or request.headers.get('Host', '')
    
    if 'rota.crowbank.co.uk' in host:
        g.user_type = 'employee'
    elif 'portal.crowbank.co.uk' in host:
        g.user_type = 'customer'
    else:
        g.user_type = 'unknown'
    
#    g.user_type = 'customer' # Hardwired for debugging purposes
    
    if g.user_type == 'customer':
        if not request.path.startswith('/portal') and not request.path.startswith('/static'):
            new_path = '/portal' + request.path
            # Use try-except to avoid infinite redirects
            try:
                return redirect(new_path, code=302)
            except:
                pass
    
    g.roles = User.get_roles()
    g.employees = get_today_employees()
    g.punch = request.cookies.get('punch')
    g.notes = request.cookies.get('notes')

    g.remote = 'HTTP_NGROK_AUTH_USER_EMAIL' in request.environ
    form = PunchForm()
    form.employee.choices = [(emp['id'], emp['name']) for emp in g.employees]

    g.punch_form = form
