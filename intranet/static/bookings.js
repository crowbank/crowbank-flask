/*
$(document).ready(function() {
    $("#past").bootstrapToggle('off');
    $("#present").bootstrapToggle('on');
    $("#future").bootstrapToggle('on');
    $("#confirmed").bootstrapToggle('on');
    $("#booked").bootstrapToggle('on');
    $("#hold").bootstrapToggle('on');
    $("#cancelled").bootstrapToggle('off');
    $("#standby").bootstrapToggle('on');
    filterBookings();
});
*/

function filterBookings() {
    var incPast = $("#past").prop('checked')
    var incPresent = $("#present").prop('checked')
    var incFuture = $("#future").prop('checked')
    var incConfirmed = $("#confirmed").prop('checked')
    var incBooked = $("#booked").prop('checked')
    var incHold = $("#hold").prop('checked')
    var incCancelled = $("#cancelled").prop('checked')
    var incStandby = $("#standby").prop('checked')
    var value = ''
    if ( $("#search").length ) {
        value = $("#search").val().toLowerCase()
    }

    $(".searched tr").filter(function() {
        $(this).toggle(
            $(this).text().toLowerCase().indexOf(value) > -1 &&

            ((incPast && $(this).hasClass('booking-past')) ||
            (incPresent && $(this).hasClass('booking-present')) ||
            (incFuture && $(this).hasClass('booking-future'))) &&

            ((incConfirmed && $(this).hasClass('booking-confirmed')) ||
            (incBooked && $(this).hasClass('booking-booked')) ||
            (incHold && $(this).hasClass('booking-hold')) ||
            (incCancelled && ($(this).hasClass('booking-cancelled') || $(this).hasClass('booking-no-show'))) ||
            (incStandby && $(this).hasClass('booking-standby')))
        )
    })
}

$(document).ready(function(){
    $("#search").on("keyup", filterBookings)
    $("#search").focus()
})

$(document).ready(function() {
    $(".search-toggle").change(filterBookings)
})

$(document).ready(function() {
    $('#bookings-table').DataTable({
        pageLength: 25
    })
})
