const message_stack = []
let current_message = 0

const datepickerOptions = {
  dateFormat: 'dd/mm/yyyy'
}

$(document).ready(() => {
  const url = 'https://crowbank.co.uk/wp-content/plugins/crowbank/last_transfer.php'
  $.getJSON(url, (data) => {
    const last_transfer = data.last_transfer
    const dlt = yyyymmddhhmmss2date(last_transfer)
    const now = new Date()
    const diff =(now.getTime() - dlt.getTime()) / 1000 / 60
    if (diff < 20) {
      dlt_class = 'recent'
      dlt_text = 'OK'
    } else if (diff < 60) {
      dlt_class = 'late'
      dlt_text= 'Late'
    } else {
      dlt_class = 'vlate'
      dlt_text = 'Very Late'
    }
    $('#last-transfer').removeClass('dlt-loading').html(dlt_text).addClass(`dlt-${dlt_class}`)
    })

    $('#open-note').on('click', prepare_note)
})        

function prepare_note() {
  $('#note-body').empty()
  $('#note-modal').modal('show')
}

$(document).on('click', '#add-pet-note', prepare_note)

$(document).on('click', '#close-note-btn', function() {
  $('#note-modal-dialog').modal('hide')
  $('#note-modal').modal('hide')
});

$(document).ready(function($) {
  $(".table tr[data-href]").click(function(event) {
    event.stopPropagation()
    const url = $(this).data("href")
    console.log('Relocating to ', url)
    setTimeout(function() {
      window.location.href = url;
    }, 100)
  })
})

$(document).ready(() => {
  const saved_messages = localStorage.getItem("messages")

  if (saved_messages) {
  // Parse saved_messages as an array of objects
    const parsed_messages = JSON.parse(saved_messages)

  // Append parsed_messages to message_stack

    message_stack.push(...parsed_messages)
  }
})

function save_messages() {
  const messages_json = JSON.stringify(message_stack)

// Store messages_json in localStorage under the key "messages"
  localStorage.setItem("messages", messages_json)
}

$(document).ready(() => {
  $('[data-toggle="tooltip"]').tooltip()
})

$(document).ready(() => {
	$('.confirm').click(ev => {
      const href = $(this).attr('confirm-href')
      const msg = $(this).attr('confirm-msg')
      bootbox.confirm(msg, result => {
        if (result) {
          location.assign(href)
        }
      })
    })
})

$(document).ready(() => {
  $("[select_default]").each(() => {
    $(this).val($(this).attr("select_default")).change()
  }
)
})

$(document).ready(() => {
  $('.clickable_table tr').click(() => {
      const href = $(this).find("a").attr("href")
      if(href) {
          window.location = href
      }
  })
})

function ConfirmDelete()
{
  const x = confirm("Are you sure you want to delete?")
  if (x) return true
  else return false
}

$(document).ready(() => {
  $('#transfer-date-to-kc').on('click', () => {
  $('#pet_form-kc').val($('#pet_form-vacc').val())
})
})

$( '#topheader .navbar-nav a' ).on( 'click', () => {
	$( '#topheader .navbar-nav' ).find( 'li.active' ).removeClass( 'active' )
	$( this ).parent( 'li' ).addClass( 'active' )
})

function do_daterange(single) {
  const daterange_input=$('.date-range')
  var startDate = $('#start_date').text()
  var endDate = $('#end_date').text()
  const today = new Date()
  if (startDate.length ==0) {
    startDate = today
  }
  if (endDate.length ==0) {
    endDate = startDate
  }

  daterange_input.daterangepicker({
    startDate: startDate,
    endDate: endDate,
    singleDatePicker: single,
    locale: {format: 'DD/MM/YYYY'},
    autoApply: true
  })
}

$(document).ready(() => {
  const singleDatePicker = $('#trial').length > 0 && $('#trial').prop("checked")
  do_daterange(singleDatePicker)
})

function populate_run_from_date(date_control, on_success=null) {
  const date = $('#' + date_control).val()

  $.getJSON($SCRIPT_ROOT + '/_get_available_runs', {
    date: date
  }, function(data) {
      $('#run').empty()

      $('#run').append($('<option>', {
      value: '',
      text: 'Select a Run'
      }))

      $.each(data.runs, function(index, run) {
          $('#run').append($('<option>', {
              value: run.no,
              text: run.code
          }))
      })

      if (on_success) {
        on_success()
      }
  })
}

$(document).ready(() => {
  if($('#trial').prop("checked")) {
    $('.show-on-trial').show()
    $('.hide-on-trial').hide()
  } else {
    $('.show-on-trial').hide()
    $('.hide-on-trial').show()
  }
  $('#trial').on('change', () => {
    if ($('#trial').is(':checked')) {
      $('.show-on-trial').show()
      $('.hide-on-trial').hide()
      do_daterange(true)
    } else {
      $('.show-on-trial').hide()
      $('.hide-on-trial').show()
      do_daterange(false)
    }
  })
})

$(document).ready(() => {
  $('.time-picker').timepicker({
    'minTime': '7:30am',
	  'maxTime': '6:30pm',
    'timeFormat': 'H:i'
  })
})

$(document).ready(() =>{
  const date_input=$('.datepicker') //our date input has the name "date"
  const container="body"
  const options = datepickerOptions

  // { dateFormat: 'dd/mm/yyyy',
  //   container: container,
  //   todayHighlight: true,
  //   autoclose: true,
  //   orientation: 'bottom left',
  //   uiLibrary: 'bootstrap4',
  //   maxViewMode: 2
  // }
  date_input.datepicker(options)
})

$(document).ready(() => {
  $('#calendar-search')
    .datepicker({format: 'dd-mm-yyyy'})
    .on('changeDate', (e) => {
      const date_arg = date2date_arg(e.date)
      const url = window.location.origin + '/home/' + date_arg
      window.location.href = url
     })
})

/* $(document).ready(function(){
  const date_input=$('.datepicker-past') //our date input has the name "date"
  const container="body"
  const today = new Date()
  const options={
    dateFormat: 'dd/mm/yyyy',
    maxDate: today
  }
  date_input.datepicker(options)
})

$(document).ready(function(){
    const date_input=$('.datepicker-future') //our date input has the name "date"
    const container="body"
    const options={
      dateFormat: 'dd/mm/yyyy',
      minDate: today
    }
    date_input.datepicker(options)
  })
 */
  $(document).ready(() => {
    $('.checkbox-indeterminate').prop('indeterminate', true)
  })

  $(document).ready(() => {
    $('.datepicker-start').datepicker()
    .on('changeDate', e => {
      $('.datepicker-end').datepicker('setStartDate', e.date)
    })
  })

  $(document).ready(() => {
    $('input[type=radio][name="pet_form-species"]').on('change', () => {
      const spec = $('input[name=pet_form-species]:checked').val()
      if (spec == 1) {
        $('.breed-group').show()
      } else {
        $('.breed-group').hide()
      }
    }
  )})

  function dateConvert(ds) {
    const parts = ds.split("/")

// month is 0-based, that's why we need dataParts[1] - 1
    const d = new Date(+parts[2], parts[1] - 1, +parts[0])
    return d
  }

  function myISO(d) {
    const y = d.getFullYear()
    const m = d.getMonth() + 1
    const da = d.getDate()

    return `${y}-${m}-${da}`
  }

  function change_availability_link() {
    const start_date = myISO(dateConvert($('#start_date').val()))
    const end_date = myISO(dateConvert($('#end_date').val()))
    const url = '../availability/' + $('#rts').text() + '/' + start_date + '/' + end_date
    $('#availability').attr('href', url)
  }

$(document).ready(() => {
  $('#start_date').on('change', change_availability_link)
  $('#end_date').on('change', change_availability_link)
})

function update_doc() {
  const n = $('#document option:selected').val()
  if (n > 0) {
    $('.doc-box').hide()
    $('#doc-html-' + n).show()
  }
}
$(document).ready(() => {
  $('#document').change(update_doc)
  update_doc() 
})

$(document).ready(() => {
  $('.meds-check').change(() => {
      const check_id = $(this).attr('id')
      const pet_no = check_id.substring(check_id.lastIndexOf('-')+1, check_id.length)
      const meds_id = '#meds-panel-' + pet_no
      if ($(this).is(':checked')) {
        $(meds_id).show()
      } else {
        $(meds_id).hide()
      }
    }
  )
})

function autoload_runs(date_control, on_success=null) {
  $('#' + date_control).change(function() {
    populate_run_from_date(date_control, on_success)
  })  
}

$(document).ready(() => {
  if($('#daycare').length > 0 && $('#daycare').prop("checked")) {
    $('.show-on-daycare').show()
    $('.hide-on-daycare').hide()
    do_daterange(true)
    autoload_runs('date_range')
  } else {
    $('.show-on-daycare').hide()
    $('.hide-on-daycare').show()
    do_daterange(false)
  }

  $('#daycare').change(function() {
    if($(this).prop("checked")) {
      $('.show-on-daycare').show()
      $('.hide-on-daycare').hide()
      do_daterange(true)
      autoload_runs('date_range')
    } else {
      $('.show-on-daycare').hide()
      $('.hide-on-daycare').show()
      do_daterange(false)
    }
  })
})

function printPageArea(areaID){
  const printContent = document.getElementById(areaID)
  const WinPrint = window.open('', '', 'width=900,height=650')
  WinPrint.document.write(printContent.innerHTML)
  WinPrint.document.close()
  WinPrint.focus()
  WinPrint.print()
  WinPrint.close()
}

function date_from_single_field(field_name) {
  return () => {
    return $('#' + field_name).val()
  }
}

function date_from_range_field(field_name, position) {
  return () => {
    const r = /(\d\d\/\d\d\/\d\d\d\d) - (\d\d\/\d\d\/\d\d\d\d)/
    const r1 = /(\d\d\/\d\d\/\d\d\d\d)/
    const s = $('#' + field_name).val()
    const a = r.exec(s)
    if (a == null) {
      const res = r1.exec(s)
      return res[0]
    } else {
      if (position == 'start') {
        return a[1]
      } else {
        return a[2]
      }
    }
  }
}

function string2date(s) {
  const patt = /(\d{2})\/(\d{2})\/(\d{4})/
  return new Date(s.replace(patt, '$3-$2-$1'))
}

var refresh_functions = {}

function fillTime(field, value, hide_field = '') {
  $('#' + field).val(value)
  if (hide_field) {
    $('#' + hide_field).hide(0)
  }
  refresh_functions[field]()
}

function attach_date_to_time_field(time_field, selector_name, options = {}) {
  $(document).ready(() => {
    const period_function = options.period_function || (() => 'all')
    const refresh_fields = options.refresh_fields || [time_field]
    const date_function = options.date_function || (() => (new Date()).toLocaleDateString())
    const day_count = options.date_count || 1
    const hide_field = options.hide_field || ''

    refresh_functions[time_field] = () => {
      if ($('#' + time_field).is(":visible")) {
        const date = date_function()
        const period = period_function()
        const current_slot = $('#' + time_field).val()
        $.getJSON($SCRIPT_ROOT + '/_day_visits', {
          date: date,
          day_count: day_count,
          field: time_field,
          title: 'Time on ' + date,
          period: period,
          hide_field: hide_field,
          current_slot: current_slot
        }, data => {
          $('#' + selector_name).html(data.html)
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
          update_status(textStatus, jqXHR.responseText)
        })
      }
    }

    $('#' + time_field).focus(() => {
      refresh_functions[time_field]()
      if (hide_field) {
        $('#' + hide_field).show()
      }
    })

    refresh_fields.forEach((value, index, array) => {
      $('#' + value).change(refresh_functions[time_field])
    })
  }) 
}

$(document).ready(() => {
  const a = $('#navbar-search')
  if (!a) return
  
  $(document).keydown(b => {
    if (b.ctrlKey && b.key === '/') {
      b.preventDefault()
      a.focus()
    }
  })
})

function refresh_availability() {
  if (!($('#date_range').length)) {
    return
  }
  
  const s_date = date_from_range_field('date_range', 'start')()
  const e_date = date_from_range_field('date_range', 'end')()

  if ($('#run').length) {
    $.getJSON($SCRIPT_ROOT + '/_get_daycare_dogs', {
      date: s_date
  }, function(data) {
      $('#run').empty()

        $('#run').append($('<option>', {
          value: '',
          text: 'Select a Run'
      }))

      $.each(data.runs, function(index, run) {
        $('#run').append($('<option>', {
          value: run.no,
          text: run.code
        }))
    })

  })
  }

  var rts = $('#rts').text()
  if($('#deluxe').prop('checked')) {
    rts = rts.replace('standard', 'deluxe').replace('double', 'deluxe')
  }
  const start_period = $("input[name='start_period']:checked").val()
  const end_period = $("input[name='end_period']:checked").val()
  $('#availability-vector').html('Loading...')
  $.getJSON($SCRIPT_ROOT + '/_availability_vector', {
          start_date: s_date,
          end_date: e_date,
          rts: rts,
          start_period: start_period,
          end_period: end_period
  }, data => {
      $('#availability-vector').html(data.html)

      $('#availability-detail-toggle').click((e) => {
        e.preventDefault()
        $('#availability-details').collapse('toggle')
    })
  })
  .fail((jqXHR, textStatus, errorThrown) => {
    update_status(textStatus, jqXHR.responseText)
  })
}

function trigger_date_range() {
  $(document).ready(() => {
    refresh_availability()
    $('#date_range').change(refresh_availability)})
    $('#refresh-availability').click(refresh_availability)
    $('#deluxe').change(refresh_availability)
}

$(document).ready( () => {
  const tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
  const tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
    return new bootstrap.Tooltip(tooltipTriggerEl)
  })
})

$(document).ready($ => {
  $(".table-row").click(e => {
      if (!$(e.target).hasClass('read-more')) {
      window.document.location = $(this).data("href")
  }
})
})

function stop_dropdowns() {
  /////// Prevent closing from click inside dropdown
    $('.dropdown-menu').click(event => {
      event.stopPropagation()
    })
  }

function multilevel() {
  $("ul.dropdown-menu [data-toggle='dropdown']").on("click", event => {
    event.preventDefault()
    event.stopPropagation()

    $(this).siblings().toggleClass("show")

    if (!$(this).next().hasClass('show')) {
      $(this).parents('.dropdown-menu').first().find('.show').removeClass("show")
    }

    $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', e => {
      $('.dropdown-submenu .show').removeClass("show")
    })

  })
}
$(document).ready(multilevel)

document.querySelectorAll('.dropdown-menu').forEach(element => {
  element.addEventListener('click', e => {
    e.stopPropagation()
  })
})

function date2ddmmyyyy(d) {
  const dd = String(d.getDate()).padStart(2, '0')
  const mm = String(d.getMonth() + 1).padStart(2, '0') //January is 0!
  const yyyy = d.getFullYear()
  
  const str_date = `${dd}/${mm}/${yyyy}`
  return str_date
}

function date2yyyymmdd(d) {
  const dd = String(d.getDate()).padStart(2, '0')
  const mm = String(d.getMonth() + 1).padStart(2, '0') //January is 0!
  const yyyy = d.getFullYear()
  
  const str_date = `${yyyy}${mm}${dd}`
  return str_date
}

function date2wwwddmm(d) {
  const weekdays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
  const weekday = weekdays[d.getDay()]
  const dd = String(d.getDate()).padStart(2, '0')
  const mm = String(d.getMonth() + 1).padStart(2, '0') //January is 0!

  const str_date = `${weekday} ${dd}/${mm}`
  return str_date
}

function date2date_arg(d) {
  const dd = String(d.getDate()).padStart(2, '0')
  const mm = String(d.getMonth() + 1).padStart(2, '0') //January is 0!
  const yyyy = d.getFullYear()
  
  const str_date = `${dd}-${mm}-${yyyy}`
  return str_date
}

function ddmmyyyy2date(s) {
  const r = /(\d\d?)\/(\d\d?)\/(\d\d\d\d)/
  const a = r.exec(s)

  const date = new Date(a[3], a[2]-1, a[1])
  return date
}

function yyyymmddhhmmss2date(s) {
  const r = /(\d\d\d\d)-(\d\d)-(\d\d) (\d\d):(\d\d):(\d\d)/
  const a = r.exec(s)

  const date = new Date(a[1], a[2]-1, a[3], a[4], a[5], a[6])
  return date
}

function yyyymmdd2date(s) {
  const r = /(\d\d\d\d)(\d\d)(\d\d)/
  const a = r.exec(s)

  const date = new Date(a[1], a[2]-1, a[3])
  return date
}

function get_lasttransfer() {
  const url = 'https://crowbank.co.uk/wp-content/plugins/crowbank/last_transfer.php'
  $.getJSON(url, (data) => {
    return(data.last_transfer);
  })
}

function reset_messages() {
  message_stack.length = 0
  current_message = 0
}

function get_message_list_status(start_pos, end_pos) {
  let status = ''
  for (let i = start_pos; i <= end_pos; i ++ ) {
    const this_status = message_stack[i].status_type
    if (!status || status == 'info' || (status == 'warning' && this_status == 'error')) {
      status = this_status
    }
  }
  return status
}

function next_message() {
  if (current_message < message_stack.length - 1) {
    current_message += 1
  }
  update_message()
}

function prev_message() {
  if (current_message > 0) {
    current_message -= 1
  }
  update_message()
}

function last_message() {
  if (message_stack.length) {
    current_message = message_stack.length - 1
  }
  update_message()
}

function update_message() {
  if (!message_stack) {
    return
  }

  const prev_status = get_message_list_status(0, current_message - 1)
  const next_status = get_message_list_status(current_message + 1, message_stack.length - 1)
  const message = message_stack[current_message]
  $('#status').removeClass().addClass(message.status_type).html(message.status_msg)
  $('#step-backward').removeClass('info').removeClass('warning').removeClass('error').addClass(prev_status)
  $('#step-forward').removeClass('info').removeClass('warning').removeClass('error').addClass(next_status)
}

function update_status(status_type, status_msg) {
  const message = {
    status_type: status_type,
    status_msg: status_msg
  }
  message_stack.push(message)
  last_message()
//  update_message(message_stack.length - 1)
}

$(document).ready(() => {
  $('#step-forward').click(next_message)
  $('#step-backward').click(prev_message)
})

$(document).ready(() => {
  $(".read-more").click(() => {
    $(this).siblings(".more-text").contents().unwrap()
    $(this).remove()
  })
})

function trim_more_text(maxLength) {
	$(".show-read-more").each(() => {
		var myStr = $(this).text()
		if($.trim(myStr).length > maxLength) {
			var newStr = myStr.substring(0, maxLength)
			var removedStr = myStr.substring(maxLength, $.trim(myStr).length)
			$(this).empty().html(newStr)
			$(this).append(' <a href="javascript:void(0);" class="read-more">read more...</a>')
			$(this).append('<span class="more-text">' + removedStr + '</span>')
		}
	})
	$(".read-more").click(e => {
    e.preventDefault()
		$(this).siblings(".more-text").contents().unwrap()
		$(this).remove()
	})
}

$(document).ready(() => {
  $('#refresh-occupancy').click(() => {
    $.getJSON($SCRIPT_ROOT + '/_refresh_occupancy', {
  }, () => {
    update_status('info', 'Occupancy Refreshed')
  })
  .fail((jqXHR, textStatus) => {
      update_status(textStatus, jqXHR.responseText);
  })
  })
})
/*
$(document).ready(function() {
  $('.more-button').click((e) => {
    e.preventDefault()
    e.stopPropagation()
    const label = e.target.parentElement.dataset.target
    console.log(label)
    $('#' + label).show()
  })
})
*/

function reset_punch() {
  $('#punch-form #employee').val('0')
  $('#punch-form #time').val('')
  $('#punch-form #comment').val('')
  $('#punch-status').html('')
  $('#punch-status').css('background-color', 'white')
}

function submit_punch(time, employee, comment, image) {
  const url = $SCRIPT_ROOT + '/_punch'
  $.ajaxSetup({
    beforeSend: (xhr, settings) => {
        const punch_csrf = $('#punch-form #csrf_token').val()
        xhr.setRequestHeader("X-CSRFToken", punch_csrf)
    }
  })

  $.post({
    dataType: "json",
    url: url,
    data: {
      employee: employee,
      time: time,
      comment: comment,
      image: image
    },
    success: (data) => {
      const status = data.status
      if (status == 'success') {
        const employee = data.employee
        const punch_time = data.time
        const status_update = punch_time && punch_time != '00:00' ? 
          `Punch for ${employee} recorded at ${punch_time}` : `Punch for ${employee} recorded now`
        $('#punch-feedback').html(status_update)
        $('#punch-status').css('background-color', 'green')
        reset_punch()
      } else {
        $('#punch-feedback').text(data.message)
        $('#punch-feedback').css('background-color', 'red')
      }
    }
  })
}

$(document).ready(() => {
  function close_punch_dialog() {
    const md = navigator.mediaDevices
    if (md && Webcam.loaded) {
      Webcam.reset()
    }
    $('#staff-punch-dialog').hide()
  }

  $('#staff-punch').click(() => {
    const today = new Date()
    const hour = today.getHours()
    const minutes = today.getMinutes()
    const time = (hour > 9 ? hour : '0' + hour) + ":" + (minutes > 9 ? minutes : '0' + minutes)
    reset_punch()
    $('#staff-punch-dialog').show()
    
    const md = navigator.mediaDevices
    if(md) {
      Webcam.set({
        width: 320,
        height: 240,
        image_format: 'jpeg',
        jpeg_quality: 90
        })
      Webcam.attach( '#my_camera' )  
    } else {
      $('#my_camera').html('<div class="p-5 text-center">Not Available</div>')
    }
  })
  $('#staff-punch-dismiss').click(close_punch_dialog)
  $('#punch-form').submit((event) => {
    event.preventDefault()
    const employee = $('#punch-form #employee').val()
    if (employee == '0') {
      return
    }
    const time = $('#punch-form #time').val()
    const comment = $('#punch-form #comment').val()
    const md = navigator.mediaDevices
  
  if (md) {
      Webcam.snap( data_uri => {
        submit_punch(time, employee, comment, data_uri)
      })
    } else {
      submit_punch(time, employee, comment, '')
    }
    $('#punch-form #employee').val('0')
  // close_punch_dialog()
  }
)})

async function discoverReaderHandler(terminal) {
  const config = { simulated: false }
  try {
    const discoverResult = await terminal.discoverReaders(config)
    if (discoverResult.error) {
      console.log('Failed to discover: ', discoverResult.error)
      update_status('error', 'Failed to discover: ' + discoverResult.error)
    } else if (discoverResult.discoveredReaders.length === 0) {
      console.log('No available readers.')
      update_status('error', 'No available readers.')
    } else {
      console.log('terminal.discoverReaders', discoverResult.discoveredReaders)
      return discoverResult.discoveredReaders
    }
  } catch (err) {
    console.log('Promise resulted in error: ', err)
  }
}

async function stripeConnect(terminal, success) {
  const discoveredReaders = await discoverReaderHandler(terminal)
  if (discoveredReaders && discoveredReaders.length > 0) {
    const selectedReader = discoveredReaders[0]
    connectReaderHandler(terminal, selectedReader, success)
  }
  // Handle the case where no readers were discovered
}

function connectReaderHandler(terminal, selectedReader, success) {
  // Just select the first reader here.
  terminal.connectReader(selectedReader).then(connectResult => {
    if (connectResult.error) {
      const msg = 'Failed to connect: ' + connectResult.error 
      console.log(msg)
      update_status('error', msg)
      return 0
    } else {
        const msg = 'Connected to reader: ' + connectResult.reader.label
        console.log(msg)
        update_status('info', msg)
        success()
        console.log('terminal.connectReader', connectResult)
        return 1
    }
  })
}

function get_terminal() {
  const terminal = StripeTerminal.create({
    onFetchConnectionToken: fetchConnectionToken,
    onUnexpectedReaderDisconnect: unexpectedDisconnect,
  })
  return terminal
}

function fetchConnectionToken() {
  // Do not cache or hardcode the ConnectionToken. The SDK manages the ConnectionToken's lifecycle.
  return fetch('/_connection_token', { method: "POST" })
    .then(response => {
      return response.json()
    })
    .then(data => {
      return data.secret
    })
}

function unexpectedDisconnect() {
  // In this function, your app should notify the user that the reader disconnected.
  // You can also include a way to attempt to reconnect to a reader.
  console.log("Disconnected from reader")
  update_status('error', "Disconnected from reader")
}
    
function fetchPaymentIntentClientSecret(amount, bk_no=0, cust_no=0) {
  const bodyContent = JSON.stringify({
    amount: amount,
    bk_no: bk_no,
    cust_no: cust_no
   })
  return fetch('/_create_payment', {
    method: "POST",
    headers: {
      'Content-Type': 'application/json'
    },
    body: bodyContent
  })
  .then(response => {
    return response.json()
  })
  .then(data => {
    return {
      'secret': data.client_secret,
      'id': data.id
    }
  })
}

function get_payment_link(bk_no, amount, process, fail = null) {
  const url = '/_payment_link'
  params = {
    url: url,
    dataType: 'json',
    contentType: 'application/json',
    data: JSON.stringify({
      'bk_no': bk_no,
      'amount': amount
    }),
    success: (data) => {
      const success = data.success
      if (!success) {
        if (fail) {
          fail()
        }
      }
      const url = data.url
      console.log(url)
      process(url)
    },
    error: (jqXHR, textStatus, errorThrown) => {
      console.log(textStatus)
      update_status('error', textStatus)
      if (fail) {
        fail()
      }
  }
  }
  localStorage.setItem('AddPaymentParams', params)
  $.post(params)

}

function record_stripe_payment(bk_no, amount, id, done_function) {
  const url = '/_add_payment'
  params = {
    url: url,
    dataType: 'json',
    contentType: 'application/json',
    data: JSON.stringify({
      'bk_no': bk_no,
      'amount': amount,
      'id': id
    }),
    success: () => {
      console.log('Success!')
      done_function()
    },
    error: (jqXHR, textStatus, errorThrown) => {
      console.log(textStatus)
      update_status('error', textStatus)
    }
  }
  localStorage.setItem('AddPaymentParams', params)
  $.post(params)
}

function deletePayment() {
  const payment_id = localStorage.getItem('payment_id')
  const url = '/_delete_payment'
  $.post({
    dataType: "json",
    url: url,
    contentType: 'application/json',
    data: JSON.stringify(
      {
        'payment_intent_id': payment_id
      })
  })
}

async function cancelPayment(terminal) {
  return terminal.cancelCollectPaymentMethod().then( () => {
      deletePayment()
    }
  )
}

function collectPayment(terminal, amount, bk_no = 0, cust_no = 0,
  on_success = null, on_intent_id = null) {
  const iamount = Math.floor(amount * 100)
  fetchPaymentIntentClientSecret(iamount, bk_no, cust_no).then((data) => {
    const client_secret = data.secret
    const payment_id = data.id
    if (on_intent_id) {
      on_intent_id(payment_id)
    }

    update_status('warning', 'Payment request sent to terminal')
    terminal.collectPaymentMethod(client_secret).then(result => {
      if (result.error) {
          if (result.error.code == 'canceled') {
            update_status('warning', 'Payment Cancelled')
            return
          }
          update_status('error', 'Error ' + result.error)
          console.log('Error', result.error)
          return
      } 

      console.log('terminal.collectPaymentMethod', result.paymentIntent);

      const intent = result.paymentIntent

      terminal.processPayment(intent).then(result => {
        if (result.error) {
            update_status('error', result.error)
            console.log(result.error)
            return
        } 
        if (!result.paymentIntent) {
          return
        }
        paymentIntentId = result.paymentIntent.id
        record_stripe_payment(bk_no, amount, paymentIntentId, () => {
          console.log('terminal.processPayment', result.paymentIntent)
          if (on_success) {
            on_success()
          } else {
            update_status('info', 'Payment successful')
            location.reload()
          }
        })
      })    
    })
  })
}

let oldDOM = null;

// Call this function at the beginning to save a copy of the DOM
function saveDOM() {
  oldDOM = $("*").map(function() {
    return $(this).prop('outerHTML')
  }).get()
}

// Call this function later to check for changes and log any new nodes
function checkForChanges() {
  $("*").each(function() {
    // Check if the current node exists in the old DOM
    const existsInOldDOM = oldDOM.includes($(this).prop('outerHTML'))

    if (!existsInOldDOM) {
      // Log the parent of the new node, or the node itself if it has no parent
      console.log($(this).parent().length ? $(this).parent() : $(this))
    }
  })
}

function displayImage(input) {
  const file = input.files[0]
  if (file) {
    const reader = new FileReader()
    reader.onload = function(e) {
      $("#uploadedImage").attr("src", e.target.result).show()
    }
    reader.readAsDataURL(file)
  }
}

$(document).ready(function() {
  // Populate 'Entered By' options
  // Function to display uploaded image
  $('body').on('change', '#note_photo', function() {
    const file = this.files[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = function(e) {
        $('#uploadedImage').attr('src', e.target.result).removeClass('d-none')
      }
      reader.readAsDataURL(file);
    }
  })

  $('body').on('click', '#close-image-modal', function() {
    $('#image-modal').modal('hide')
  })
})

  $(document).ready(function() {
    $(document).on('submit', '#note_form', function(e) {
      e.preventDefault()
      const formElement = e.target // Access the form element
      const formData = new FormData(formElement)
      const note_no = +formData.get('note_no')
      const note_pet_no = +formData.get('pet_no')

      if (!note_no) {
        const note_type = +formData.get('type_no')
        const note_pet = +formData.get('pet')

        let errors = false

        if (!note_type) {
          $('#note-type-error').show()
          errors = true
        }

        if (!note_pet && !note_pet_no) {
          $('#note-pet-error').show()
          errors = true
        }

        if (errors) {
          return
        }

        if (note_pet && !note_pet_no) {
          formData.set('pet_no', note_pet)
        }
      }


    // Make the AJAX request
    $.ajax({
      url: '/_submit_note',
      type: 'POST',
      data: formData,
      processData: false, // Important for file uploads
      contentType: false, // Important for file uploads
      success: function(response) {
        if (!response.success) {
          // Handle errors here
          // For example, response.errors can be used to highlight fields
          $('#note-error-message').text(response.msg)
          return
        }
        // Close the modal
        $('#close-note').click()
        // Refresh the note display, if it exists
        location.reload()
      },
      error: function(error) {
        // Handle any errors here
        // For example, show an error message
        $('#note-error-message').text(error.statusText)
      }
    })
  })
})

/*
$(document).ready(function() {
  $('.photo-thumbnail').click(function() {
    var photoSrc = $(this).data('photo')
    $('#full-size-photo').attr('src', photoSrc)
    console.log('Showing photo modal')
    $('#photo-modal').modal('show')
    console.log('Photo modal shown')
  })
})
*/
function getDescriptionByValue(value) {
  const noteSeverityMap = {
    1: 'A routine note, no action required',
    2: 'An unusual note, action may be required',
    3: 'A serious note, action required immediately'
  }

  if(noteSeverityMap.hasOwnProperty(value)) {
      return noteSeverityMap[value]
  }
  return "Unknown"
}

function fetch_note(div) {
  var noteNo = $(div).data("note-no")
  if (noteNo === undefined) {
    console.log("fetch_note: noteNo is undefined", div)
    return
  }
  const verNo = $(div).data("ver-no")
  const verCount = $(div).data("ver-count")

  var url
  if (!verNo) {
    url = `/_note_display/${noteNo}`
  } else {
    url = `/_note_display/${noteNo}/${verNo}`
  }
  
  $.get(url, function(data) {
    $(div).html(data)
  })
}

$(document).ready(function() {
  $('.kennel-note').each(function(index, element) {
    fetch_note(element)
  })
})

$(document).ready(function() {
  $('body').on('click', '#close-note-btn', function() {
    $('#note-modal').modal('hide')
  })
  $('body').on('click', '#close-note', function() {
    $('#note-modal').modal('hide')
  })
})

// Attach the click event to the closest static parent (e.g., `body` or a known static ancestor element)

function update_note_version(note_no, direction) {
  const container = $(`.kennel-note[data-note-no='${note_no}']`)
  const ver_no = $(container).data('ver-no')
  const new_ver_no = ver_no + direction
  $(container).data('ver-no', new_ver_no)
  fetch_note(container)
}

function showFullSizeImage(imageElement) {
  const src = $(imageElement).attr("src")

  // Update the src attribute for the image in the modal
  $("#fullSizeImage").attr("src", src)

  // Show the Bootstrap modal
  $("#image-modal").modal("show")
}

function reset_error(id) {
  $('#' + id).hide()
}
function CheckboxDropdown(el) {
    var _this = this
    this.isOpen = false
    this.areAllChecked = false
    this.$el = $(el)
    this.$label = this.$el.find('.dropdown-label')
    this.$checkAll = this.$el.find('[data-toggle="check-all"]').first()
    this.$inputs = this.$el.find('[type="checkbox"]')
    
    this.onCheckBox()
    
    this.$label.on('click', function(e) {
      e.preventDefault()
      _this.toggleOpen()
    })
    
    this.$checkAll.on('click', function(e) {
      e.preventDefault()
      _this.onCheckAll()
    })
    
    this.$inputs.on('change', function(e) {
      _this.onCheckBox()
    })
}
  
  CheckboxDropdown.prototype.onCheckBox = function() {
    this.updateStatus()
  }
  
  CheckboxDropdown.prototype.updateStatus = function() {
    var checked = this.$el.find(':checked')
    
    this.areAllChecked = false
    this.$checkAll.html('Check All')
    
    if(checked.length <= 0) {
      this.$label.html('Select Options')
    }
    else if(checked.length === 1) {
      this.$label.html(checked.parent('label').text())
    }
    else if(checked.length === this.$inputs.length) {
      this.$label.html('All Selected')
      this.areAllChecked = true
      this.$checkAll.html('Uncheck All')
    }
    else {
      this.$label.html(checked.length + ' Selected')
    }
  }
  
  CheckboxDropdown.prototype.onCheckAll = function(checkAll) {
    if(!this.areAllChecked || checkAll) {
      this.areAllChecked = true
      this.$checkAll.html('Uncheck All')
      this.$inputs.prop('checked', true)
    }
    else {
      this.areAllChecked = false
      this.$checkAll.html('Check All')
      this.$inputs.prop('checked', false)
    }
    
    this.updateStatus()
  }
  
  CheckboxDropdown.prototype.toggleOpen = function(forceOpen) {
    var _this = this;
    
    if(!this.isOpen || forceOpen) {
       this.isOpen = true;
       this.$el.addClass('on')
      $(document).on('click', function(e) {
        if(!$(e.target).closest('[data-control]').length) {
         _this.toggleOpen()
        }
      });
    }
    else {
      this.isOpen = false
      this.$el.removeClass('on')
      $(document).off('click')
    }
  }

  $(document).ready(() => {
    $(document).on("click", "#icon", function() {
        $("#icon-grid").removeClass("d-none").addClass("d-flex")
    })

  // Delegated event for updating the icon and hidden field when an item in the grid is clicked
})

  $(document).ready(() => {
  var checkboxesDropdowns = document.querySelectorAll('[data-control="checkbox-dropdown"]')
  for(var i = 0, length = checkboxesDropdowns.length; i < length; i++) {
    new CheckboxDropdown(checkboxesDropdowns[i])
  }
  })

  function inject_column_filter(table, column_no) {
    const column = table.column(column_no)
    var select = $('<select multiple="multiple"></select>')
      .appendTo($(column.header()));

// Get unique values from the column
    column.data().unique().sort().each(function(value, index) {
      select.append('<option value="' + value + '">' + value + '</option>')
    })

// Initialize jquery.multiselect plugin
    select.multiselect({
        selectedText: "# of # selected"
    })

// Event handler to filter rows
    select.on('change', function() {
      var selected_values = $(this).val()

      if(selected_values.length === 0) {
        column.search('').draw()
      } else {
    // Join array into a pipe-separated string for regex search
        var regex_search = selected_values.join('|')
    
    // Use regex search to filter the data
        column.search(regex_search, true, false).draw()
      }
    })
  }
  
function set_note_type(type_no, type_code) {
  const type_unit_desc = type_code.toLowerCase()

  const type_unit = $(`#${type_unit_desc}-unit`)
  $('.type-unit').removeClass("d-flex").addClass("d-none")

  if (type_unit.length) {
      type_unit.removeClass("d-none").addClass("d-flex")
  }
    // Update hidden field
  $("#type-no").val(type_no)

  // Update displayed icon
  $("#icon").attr("src", `/static/img/note_types/${type_code}.png`)

  
  // Hide the grid
  $("#icon-grid").removeClass("d-flex").addClass("d-none")

  $('#note_type_description').text(noteTypesArray[type_no].description)
}

$(document).on("click", ".icon-item", function() {
  const type_no = $(this).attr("data-type-no")
  const type_code = $(this).attr("data-type-code")

  set_note_type(type_no, type_code)
})    

function set_button_group_behavior(button_group_id, hidden_field_id) {
  $('#' + button_group_id + ' .btn').click(function() {
    $('#' + button_group_id + ' .btn').removeClass('active')
    $(this).addClass('active')
    
    var selected_value = $(this).data('value')
    $('#' + hidden_field_id).val(selected_value)
  })
}

$(document).ready(function() {
  $('body').on('click', '#color-picker button', function() {
      $('#color-picker button').removeClass('active')
      $(this).addClass('active')
      const sev = $(this).data('sev')
  
      // Update hidden field if you're using one
      $('#severity').val(sev)
      $('#severity-description').text(getDescriptionByValue(sev))      
  })
})

document.body.addEventListener('htmx:afterOnLoad', function() {
  $('[data-toggle="tooltip"]').tooltip()
  if ($('#selected_color').val()) {
    setSeverity($('#selected_color').val())
  }
  set_button_group_behavior('cat_dry_food', 'cat_dry_food_eaten')
  set_button_group_behavior('cat_wet_food', 'cat_wet_food_eaten')
})
