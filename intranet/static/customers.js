function filterCustomers() {
    var value = '';
    if ( $("#search").length ) {
        value = $("#search").val().toLowerCase();
    }

    $(".searched tr").filter(function() {
        $(this).toggle(
            $(this).text().toLowerCase().indexOf(value) > -1
    )});
}

$(document).ready(function(){
    $("#search").on("keyup", filterCustomers);
    $("#search").focus();
});

