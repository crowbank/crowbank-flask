import css_inline
from datetime import datetime, timedelta, date
from flask import render_template, flash, url_for
from flask_login import current_user
from os import path, scandir
from pyodbc import Row
import re
import requests
from typing import cast, Optional, Dict, List, Tuple
from wtforms.fields import Label

from intranet import app
from intranet.forms import EmployeeForm, PetSubform
from intranet.user import User
from logic import prepare_email, send_record_email
from models import Booking, Employee, Inventory, Pet, Run
from models.availability import get_summary
from utils import db, parse_date

def row2dict(row: Row) -> dict:
    return {
        field[0]: row[i] for i, field in enumerate(row.cursor_description)
    }

def get_booking_time(date: date, period: str, dir: str) -> str:
    # given a date, direction ('in' or 'out') and a period ('am' or 'pm'),
    # get the best time slot
    sql = 'select dbo.fnpick_time(?, ?, ?)'
    slot = db.safeselect_value(sql, date, period, dir)
    
    return slot if slot else ''

def day_events(date: date, period: str) -> Dict[str, Dict[str, list]]:
    slots = {}
    sql = "select slot_desc, convert(varchar(5), slot_start) slot_start, slot_class from tbltimeslot where slot_period = ? or ? = 'all' order by slot_desc"
    slot_records = db.safeselect(sql, period, period)
    if slot_records:
        for slot in slot_records:
            slots[slot.slot_desc] = {'class': 'slot-' + slot.slot_class, 'in': [], 'out': [], 'event': [], 'count': 0, 'start': slot.slot_start }
        
    sql = """select visit_no, visit_cust_no, visit_bk_no, visit_desc, visit_slot, visit_type, visit_tooltip,
    visit_first_slot, visit_name from vwvisitor where visit_date = ? and (visit_period = ? or ? = 'all')"""
    visits = db.safeselect(sql, date, period, period)
    
    if visits:
        for visit in visits:
                if visit.visit_type == 'in' or visit.visit_type == 'out':
                    type = visit.visit_type
                else:
                    type = 'event'
                if visit.visit_slot in slots:
                    slots[visit.visit_slot][type].append(visit)
                    slots[visit.visit_slot]['count'] += 1
                else:
                    pass
    
    for slot in slots.values():
        if slot['count'] == 1:
            slot['class'] += ' slot-busy'
        if slot['count'] > 1:
            slot['class'] += ' slot-full'
        
    return slots
    
def day_events_html(the_date: Optional[date], field='', title='', period='all',
                    hide_field='', current_slot='') -> str:
    if not the_date:
        the_date = date.today()

    visits = day_events(the_date, period)

    html = render_template('day_visits.html', visits=visits, field=field, title=title, current_slot=current_slot, hide_field=hide_field)
    return html
    
def is_date(string: str, fuzzy=False) -> bool:
    """
    Return whether the string can be interpreted as a date.

    :param string: str, string to check for date
    :param fuzzy: bool, ignore unknown tokens in string if True
    """
    try: 
        parse_date(string)
        return True

    except ValueError:
        return False
    
BILLCAT_CLASS = {
    'Cat': 'pet-cat',
    'Dog large': 'pet-dog-l',
    'Dog medium': 'pet-dog-m',
    'Dog small': 'pet-dog-s'
}

BILLCAT_ICON = {
    'Cat': 'cat',
    'Dog large': 'dog-large',
    'Dog medium': 'dog-medium',
    'Dog small': 'dog-small'
}

def start_log() -> int:
    log_no = db.safeselect_value('select max(log_no) from tbllog')
    assert(log_no)
    return log_no
    
def end_log(log_no: int) -> List[Row]:
    log_entries = db.safeselect(f'select log_sev_desc severity, log_msg msg from vwlog where log_no > ? and log_sev_no > 0', log_no)
    return log_entries if log_entries else []

class inventory_row():
    def __init__(self, row):
        self.bk_no = row.pi_bk_no
        self.rt = row.pi_rt
        self.species = row.pi_species
        self.surname = row.pi_cust_surname
        self.cust_no = row.pi_cust_no
        self.start_date = row.pi_start_date
        self.end_date = row.pi_end_date
        self.in_out = row.pi_in_out
        self.order = row.pi_order
        self.run_code = row.pi_run_code
        self.pets = map(lambda s: s.split(':'), row.pi_pets.split(','))

def pet_request_html(pet_no: int):
    sql = """select pet_no, pet_name, breed_desc, pet_custom4, ndf_no
from pa..tblpet
join pa..tblbreed on breed_no = pet_breed_no
left join tblquestionnaire on ndf_pet_no = pet_no and ndf_status <> 'duplicate'
where pet_no = ?
"""
    rec = db.safeselect_one(sql, pet_no)
    assert(rec)
    
    if rec.ndf_no:
        ref = f'/new-dog-questionnaire/{rec.ndf_no}'
    else:
        ref = f'/pet/{pet_no}'
        
    pet_visit_class = f'pet-{rec.pet_custom4}' if rec.pet_custom4 in ('received', 'must', 'may', 'no') else 'pet-none'
    
    html = f"""<a href="{ref}">
<button type="button" style="font-size: 0.75rem;" class="mb-1 pet btn {pet_visit_class}">
{rec.pet_name} ({rec.breed_desc})
    </button>
</a>"""
    return html

def pet_button(pet_no: int):
    pet = Pet.fetch(pet_no)
    assert(pet.breed)
    pet_class = BILLCAT_CLASS[pet.breed.bill_category]

    if pet.deceased:
        pet_sex = 'pet-deceased'
    else:
        pet_sex = 'pet-sex-' + pet.sex.lower()
    
    html = f"""<a href="/pet/{pet.no}">
    <button type="button" style="font-size: 0.75rem;" class="mb-1 pet {pet_sex} {pet_class} btn btn-outline-dark">
        {pet.name} ({pet.breed.full_description})
    </button></a>"""
    return html

def get_booking_row(bk_no):
    sql = """
select bk_no, bk_cust_no, bk_cust_surname, bk_cust_email, bk_start_datetime bk_start_date,
bk_end_datetime bk_end_date, bk_status_desc, bk_allocated, bk_pet_names, bk_weight_date, 
bk_gross_amt, bk_paid_amt, convert(float, bk_amt_outstanding) bk_amt_outstanding, bk_memo, hist_date, hist_destination,
hist_subject, hist_no, bk_paylink, bk_expiry, bk_deposit, bk_checked_in, bk_checked_out, bk_voucherbalance, bk_tooltip,
bk_unallocated, bk_day from vwbooking where bk_no = ?
"""

    return db.safeselect_one(sql, bk_no)

#    invoice_items = db.safeselect("""select inv_bk_no, inv_pet_name pet_name, inv_description description, inv_rate rate, inv_units units, inv_cost cost
# from vwinvoice where inv_bk_no = ?""", bk_no)
    
#    payments = db.safeselect("select pay_date date, pay_type type, pay_amount amount from pa..tblpayment where pay_bk_no = ?", bk_no)
#    invoice_items = db.safeselect("""select inv_bk_no, inv_pet_name pet_name, inv_description description, inv_rate rate, inv_units units, inv_cost cost
# from vwinvoice where inv_bk_no = ?""", bk_no)
    
#    payments = db.safeselect("select pay_date date, pay_type type, pay_amount amount from pa..tblpayment where pay_bk_no = ?", bk_no)
    
#    discount = db.safeselect_value("select bk_disc_total from pa..tblbooking where bk_no = ?", bk_no)
    
#    invoice = {
#        'bk_no': bk_no,
#        'invoice_items': invoice_items,
#        'payments': payments,
#        'discount': discount,
#        'total': booking.bk_gross_amt,
#        'paid_amt': booking.bk_paid_amt,
#        'amt_outstanding': booking.bk_amt_outstanding

def recent_health_reports(start_date = None, n = 7, max_skip = 30):
    # Return a dictionary mapping the most recent n dates for which a health report is available to the full filenames of those reports
    # For example, if start_date is 2020-09-23 and n is 3, the return might be:
    # { '2020-09-23': '2020/2020-09/2020-09-23.pdf', '2020-09-22': '2020/2020-09/2020-09-22.pdf', '2020-09-21': '2020/2020-09/2020-09-21.pdf'}
    
    if not start_date:
        start_date = datetime.date.today()
    
    files = {}
    skipped = 0
    i = 0
    
    root = app.root_path
    
    while len(files) <= n and skipped <= max_skip:
        the_date = start_date - i * timedelta(1)
        filename = the_date.strftime('%Y/%Y-%m/%Y-%m-%d.pdf')
        fullpath = path.join(root, 'static', 'health', filename)
        if path.isfile(fullpath):
            files[the_date] = filename
        else:
            skipped += 1
        i += 1
        
    return files

def date2words(a_date):
    # convert a date to a few words such as:
    # earlier today (if it matches system date)
    # yesterday
    # on XXX (day of the week)
    # or on XXX (date)
    today = date.today()
    days = (today - a_date.date()).days
    if days == 0:
        return 'Earlier today'
    if days == 1:
        return 'Yesterday'
    if days < 7:
        return f"On {a_date.date().strftime('%A')}"
    return f"On {a_date.date().strftime('%d/%m')}"

def get_availability_url(rts, from_date, to_date):
    url = f"../availability/{rts}"
    if from_date:
        url = url + f"/{from_date.strftime('%Y-%m-%d')}"
        if to_date:
            url = url + f"/{to_date.strftime('%Y-%m-%d')}"
    return url

def populate_pet_form_choices(form: PetSubform):
    vets = db.safeselect("select vet_no, left(vet_practice_name, 23) practice from pa..tblvet order by iif(vet_no = 0, '', vet_practice_name)")
    form.vet.choices = [(vet.vet_no, vet.practice) for vet in vets] if vets else [(0, '')]

    sql = """select 0 breed_no, '' breed_name
union all
select breed_no, left(breed_desc, 23)
from pa..tblbreed
where breed_spec_no = 1
order by breed_name"""

    breeds = db.safeselect(sql)
    form.breed.choices = [(breed.breed_no, breed.breed_name) for breed in breeds] if breeds else [(0, '')]
    
    insurers = db.safeselect("select pins_no, pins_name from pa..tblpetinsurer order by iif(pins_name = 'N/A', '', pins_name)")
    form.insurer.choices = [(ins.pins_no, ins.pins_name) for ins in insurers] if insurers else [(0, '')]

    sql = "select sv_value, sv_desc from pa..tblscorevalue where sv_st_no = 4"
    body_scores = db.safeselect(sql)
    body_score_choices = []
    if body_scores:
        body_score_choices = [(bs.sv_value, bs.sv_desc) for bs in body_scores]
    
    body_score_choices.append((0, 'N/A'))
    
    form.body_score.choices = body_score_choices

def populate_role_choices(form: EmployeeForm) -> None:
    roles = User.get_roles()
    form.emp_role.choices = [(str(role_value), role_code.capitalize()) for (role_code, role_value) in roles.items()]

def date_range(d1: Optional[date], d2: Optional[date]) -> str:
    if not d1:
        d1 = datetime.now()
    if not d2:
        d2 = d1
        
    d1s = d1.strftime('%d/%m/%Y')
    d2s = d2.strftime('%d/%m/%Y')
    return f'{d1s} - {d2s}'

def populate_pet_form(form, pet: Pet) -> None:
    customer = pet.customer
    form.cust_no.data = customer.no
    form.pet_no.data = pet.no
    form.weight_date.data = date.today()
    
    if pet:
        form.species.data = pet.spec_no
        form.pet_name.data = pet.name
        form.breed.data = pet.breed.no if pet.breed else 0
        form.sex.data = pet.sex
        form.neutered.data = 'Y' if pet.neutered else 'N'
        form.dob.data = pet.dob
        form.microchip.data = pet.microchip
        form.insurer.data = pet.pins_no
        form.warning.data = pet.warning
        form.vet.data = pet.vet.no if pet.vet else 0
        form.vacc.data = pet.vacc_given
        form.kc.data = pet.kc_given
        form.deceased.data = (pet.deceased == 'Y')
        form.diabetic.data = pet.is_diabetic
        form.body_score.data = pet.body_score
        form.daycare_approved.data = pet.daycare_approved
        
        if pet.is_dog:
            form.vacc.label = Label(field_id="vacc", text='Lepto')
        else:
            form.vacc.label = Label(field_id="vacc", text='Feline Set')
        
#        meds = pet_medic(pet.no) 
                
        petinfo = db.safeselect("select pi_type, pi_content, pi_date, pi_source from vwpetinfo_current where pi_pet_no = ?", pet.no)
        if petinfo:
            for info in petinfo:
                if info.pi_type == 'feeding':
                    form.feeding.data = info.pi_content
                    form.feeding.label = Label(field_id='feeding', text=f'Feeding [last updated: {info.pi_date.strftime("%d/%m/%Y %H:%M")}]')
                elif info.pi_type == 'kennel':
                    form.kennel_notes.data = info.pi_content
                    form.kennel_notes.label = Label(field_id='kennel_notes', text=f'Kennel Notes [last updated: {info.pi_date.strftime("%d/%m/%Y %H:%M")}]')
                elif info.pi_type == 'health':
                    form.health.data = info.pi_content
                    form.health.label = Label(field_id='health', text=f'Health [last updated: {info.pi_date.strftime("%d/%m/%Y %H:%M")}]')
                elif info.pi_type == 'behaviour':
                    form.behaviour.data = info.pi_content        
                    form.behaviour.label = Label(field_id='behaviour', text=f'Behaviour [last updated: {info.pi_date.strftime("%d/%m/%Y %H:%M")}]')
                elif info.pi_type == 'inventory':
                    form.inventory.data = info.pi_content        
                    form.inventory.label = Label(field_id='inventory', text=f'Inventory [last updated: {info.pi_date.strftime("%d/%m/%Y %H:%M")}]')
                elif info.pi_type == 'meds':
                    pass

def checkin_form_form(bk_no, pet_no):
    sql = """select cif_no, cif_bk_no, cif_pet_no, cif_pet_name, cif_created, cif_attachments, cif_vet_name,
cif_insurer, cif_food, cif_quantity,
cif_food_notes, cif_med_details, cif_health, cif_emergency_contact, cif_emergency_number,
cif_comment, cif_insured, cif_medications, cif_species, cif_cat_lifestyle, cif_friend, cif_destination
from vwcheckin_detail where cif_bk_no = ? and cif_pet_no = ?
"""
    form = db.safeselect_one(sql, bk_no, pet_no)
    return form

def questionnaire_fields(ndf_no):
    sql = """select ff_desc description, rglt_value value, ff_type type, ff_order [order]
from tblrg_lead_detail
join tblrg_lead on rgl_no = rglt_rgl_no and rgl_form_no = 23
left join tblform_field on ff_form_no = rgl_form_no and ff_field_no = rglt_field_no
where rgl_no = ?
order by ff_order
"""
    fields = db.safeselect(sql, ndf_no)
    return fields

def pet_documents(pet_no: int) -> Tuple[List[Dict], Optional[Dict]]:
    sql = "select pd_category, pd_content, pd_type, pd_date, pd_name, row_number() over (order by isnull(pd_date, '2099-12-31') desc) pd_n from vwpetdocument where pd_pet_no = ?"
    rows = db.safeselect(sql, pet_no)
    docs = []
    questionnaire = None
    if rows:
        for row in rows:
            doc = {'pd_category': row.pd_category,
                'pd_content': row.pd_content,
                'pd_type': row.pd_type,
                'pd_date': row.pd_date,
                'pd_name': row.pd_name,
                'pd_n': row.pd_n
                }
            if doc['pd_category'] == 'document':
                doc['pd_html'] = f'<iframe src="/static/{row.pd_type}/{ row.pd_content }#toolbar=0" width="100%" height="400px"></iframe>'
                doc['pd_link'] = f'/static/{row.pd_type}/{ row.pd_content }'
            elif doc['pd_category'] == 'checkin':
                if row.pd_type == 'form':
                    bk_no = int(row.pd_content)
                    form = checkin_form_form(bk_no, pet_no)
                    if not form:
                        continue
                    doc['pd_link'] = ''
                    doc['pd_html'] = render_template('components/_checkin_form.html', form=form, attachments=False)
                else:
                    doc['pd_html'] = f'<iframe src="/static/{row.pd_type}/{ row.pd_content }#toolbar=0" width="100%" height="400px"></iframe>'
                    doc['pd_link'] = f'/static/{row.pd_type}/{ row.pd_content }'
            elif doc['pd_category'] == 'questionnaire':
                ndf_no = int(doc['pd_content'])
                fields = questionnaire_fields(ndf_no)
                doc['pd_html'] = render_template('form_output.html', fields=fields)
                questionnaire = doc
            else:
                doc['pd_html'] = row.pd_content
                doc['pd_link'] = ''
            docs.append(doc)
    return docs, questionnaire

def pet_medic(pet_no) -> List[Row]:
    sql = """select pm_no, pm_morning, pm_breakfast, pm_lunch, pm_dinner, pm_night, pm_drug, pm_dose, pm_comment, pm_frequency, pm_acute, pm_start_date, pm_end_date
from vwpetmedic where pm_pet_no = ?"""
    meds = db.safeselect(sql, pet_no)
    if not meds:
        meds = []
    return meds

def leaf2html(x):
    latest_path = ''
    latest_name = ''
    if x.is_dir():
        prefix = f'<li id="{ x.name }">'
        prefix += f'<span class="caret">{ x.name }</span>'
        prefix += ' <ul class="nested">'
        html = ''
        for c in scandir(x.path):
            if not (re.match(r'\d\d\d\d-\d\d-\d\d\.pdf', c.name) or c.is_dir()):
                continue
            h, latest_path, latest_name = leaf2html(c)
            html += h
        if html:
            html = f'{prefix}{html} </ul></li>'
    else:
        path = x.path.replace('intranet', '').replace('\\', '/')
        name = x.name.replace('.pdf', '')
        latest_path = path
        latest_name = name
        html = f'<li class="file1" id="{name}" data-path="{ path }">{ name }</li>'
        
    return html, latest_path, latest_name

def restart_server():
    url = 'http://192.168.0.200:8585/api/restart?application=Intranet&password=26b43d6cf4a9c7b25a1ab38b6153987c'
    r = requests.get(url)
    code = r.status_code
    if code == 200:
        flash('Server restarting', 'info')
    else:
        flash('Server restart failed', 'error')    

def get_revenue_wages(start_date):
    end_date = start_date + timedelta(days=6)
    
    wages = db.safeselect_value('select convert(float, sum(ts_wages)) from vwtimesheet where ts_weekstart = ?', start_date)
    if not wages:
        wages = 0.0
    revenue = db.safeselect_value("select convert(float, sum(rv_revenue)) from vwrevenue where rv_date between ? and ?", start_date, end_date)
    
    ratioDesc = ''
    if not revenue or revenue <= 0.0:
        ratio = 0.0
        ratioClass = 'high-ratio'
    else:
        ratio = 100.0 * wages / revenue
        
        if ratio < 40:
            ratioClass = 'low-ratio'
            ratioDesc = 'Moderate'
        elif ratio < 45:
            ratioClass = 'med-ratio'
            ratioDesc = 'Borderline'
        else:
            ratioClass = 'high-ratio'
            ratioDesc = 'High'
        
    return wages, revenue, ratio, ratioClass, ratioDesc

def day_emp_html(start_date, published):
    emps = {}
    emps['working'] = ''
    emps['available'] = ''
    emps['unavailable'] = ''

    table_name = 'vwtimesheet_published' if published else 'vwtimesheet'
    
    recs = db.safeselect(f"""select ts_date, ts_emp_no, ts_emp_nickname, ts_status, ts_rank_class, ts_rank_no
from {table_name} where ts_date = ?""", start_date)
    
    if recs:
        for rec in recs:
            if rec.ts_status in (' X', 'XX', 'X '):
                status = 'working'
            elif rec.ts_status[0] == ' ':
                status = 'available'
            else:
                status = 'unavailable'
            emps[status] += emp_html(rec)
    return emps

def weekly_emp_summary(start_date):
    sql = f"""select emp_nickname ts_emp_nickname, sum(iif(ts_am = 'X', 0.5, 0) + iif(ts_pm = 'X', 0.5, 0)) ts_c,
sum(dbo.fnday_wage(emp_no, ts_am, ts_pm)) ts_wages,
rc_comments, rc_status
from tbltimesheet
join tblemployee on emp_no = ts_emp_no
join tbldate on date=ts_date
left join vwrotaconfirm on rc_weekstart = d_weekstart and rc_emp_no = emp_no
where emp_rank_no not in (0, 4) and d_weekstart = ? and (emp_end_date is null or ts_date <= emp_end_date)
group by emp_nickname, emp_no, emp_order, rc_confirm, rc_comments, rc_status, rc_shifts
order by emp_order
"""
    emps = db.safeselect(sql, start_date)
    return emps

def day_emp(d):
    t = { 'XX': 1, 'X ': 0.5, ' X': 0.5 }
    emp_count = 0.0
    
    recs = db.safeselect("""select ts_emp_no, ts_am, ts_pm, emp_rank_no
from tbltimesheet
join tblemployee on ts_emp_no = emp_no and (emp_end_date >= ts_date or emp_end_date is null)
where ts_date = ? and ts_emp_no <> 33 and emp_rank_no <> 0
""", d)
    if recs:
        for rec in recs:
            if rec.ts_am == 'X':
                emp_count += 0.5
            if rec.ts_pm == 'X' and rec.emp_rank_no != 4:
                emp_count += 0.5
        
        return emp_count
    return 0

def get_current_user_id() -> int:
    cu = cast(User, current_user)

    if cu.is_authenticated:
        return cu.id
    
    return 0

def emp_html(emp_rec):
    cu = cast(User, current_user)
    is_current_user = cu.is_authenticated and cu.id == emp_rec.ts_emp_no

    if cu.is_authenticated and cu.id == 2135 and emp_rec.ts_emp_no == 2118:
        is_current_user = True
    
    status = ''
    if emp_rec.ts_status == 'X ':
        status = ' (AM)'
    elif emp_rec.ts_status == ' X':
        status = ' (PM)'
    elif emp_rec.ts_status[0] == 'H':
        status = ' (Hol)'
    elif emp_rec.ts_status[0] == 'N':
        status = ' (No Show)'
    date_str = emp_rec.ts_date.strftime('%d-%m-%Y')
    
    html = f"""<div data-status="{emp_rec.ts_status}" data-emp_no="{emp_rec.ts_emp_no}" data-date="{date_str}" class="p-1 m-1 border border-solid emp-{ emp_rec.ts_rank_class } draggable emp-card { "current-emp" if is_current_user else "" }">{ emp_rec.ts_emp_nickname }{ status }</div>"""
    return html

def employee_html(emp_no, emp_nickname, start_date, status, rank, active):
    status_dict = {
        'XX': 'Work Full Day',
        'X ': 'Work AM',
        ' X': 'Work PM',
        '  ': 'Off',
        'UU': 'Unavailable',
        'HH': 'Holiday'
    }
    
    # short_date = the_date.strftime('%Y-%m-%d')
    url = url_for('show_employee', emp_no=emp_no)
    
    desc = emp_nickname
    date_mdy = start_date.strftime('%Y-%m-%d')

    qualifier = ''
    if status == 'X ':
        qualifier = ' (AM)'
    elif status == ' X':
        qualifier = ' (PM)'
    
    item = ''
    if not active:
        item = 'dropdown-item'
#    html = f"""<div class="btn-group border border-solid { item }" style="width: 100%">
    html = f"""<button type="button" class="btn dropdown-toggle { rank }_class border border-solid { item }" data-bs-toggle="dropdown" aria-expanded="false" style="width: 100%">
    { emp_nickname }{ qualifier }
</button>
<ul class="dropdown-menu">\n"""

    options = []
    for (code, desc) in status_dict.items():
        if status != code:
            options.append(f'<li><a class="dropdown-item emp-dropdown" data-date="{date_mdy}" data-emp="{emp_no}" data-status="{code}" href="#">{desc}</a></li>')
    
    html += '\n'.join(options)
    html += f"""<li><hr class="dropdown-divider"></li>
        <li><a class="dropdown-item" href="{ url }">Home Page</a></li>
    </ul>
"""
    return html

def get_lasttransfer():
    url = 'https://crowbank.co.uk/wp-content/plugins/crowbank/last_transfer.php'
    resp = requests.get(url)
    last_transfer_str = resp.json()['last_transfer']
    last_transfer = datetime.strptime(last_transfer_str, '%Y-%m-%d %H:%M:%S')
    return last_transfer

def get_weight_report_data():
    today = date.today()
    
    sql = """select pw_bk_no, pw_pet_no, pw_pet_name, pw_breed_desc,
pw_date, pw_weight, pw_notes, pw_bk_start_date, pw_bk_end_date
from vwpetweight2
where pw_current = 1
order by pw_bk_start_date, pw_pet_no, pw_date"""
    recs = db.safeselect(sql)
    
    weights = {}
    most_weights = 0
    if recs:
        for rec in recs:
            if rec.pw_pet_no not in weights:
                weights[rec.pw_pet_no] = {
                    'pet_no': rec.pw_pet_no,
                    'pet_name': rec.pw_pet_name,
                    'breed_desc': rec.pw_breed_desc,
                    'bk_no': rec.pw_bk_no,
                    'bk_start_date': rec.pw_bk_start_date,
                    'bk_end_date': rec.pw_bk_end_date,
                    'weights': []
                }
            if rec.pw_date and rec.pw_weight:
                weights[rec.pw_pet_no]['weights'].append({
                    'date': rec.pw_date,
                    'weight': float(rec.pw_weight),
                    'notes': rec.pw_notes
                })
                weights[rec.pw_pet_no]['last_weight_date'] = rec.pw_date
                if len(weights[rec.pw_pet_no]['weights']) > most_weights:
                    most_weights = len(weights[rec.pw_pet_no]['weights'])
    
    due = 0
    overdue = 0
    changes = []
    for weight_record in weights.values():
        booking_length = (weight_record['bk_end_date'] - weight_record['bk_start_date']).days
        if weight_record['weights']:
            weight_record['initial_weight'] = weight_record['weights'][0]['weight']
            weight_record['last_weight'] = weight_record['weights'][-1]['weight']
            weight_record['weight_change'] = (weight_record['last_weight'] - weight_record['initial_weight']) / weight_record['initial_weight']
            if abs(weight_record['weight_change']) > 0.1:
                changes.append(weight_record)
            start_date = weight_record['bk_start_date']
            end_date = weight_record['bk_end_date']
            last_weight_date = weight_record['last_weight_date']
            next_weight_date = min(end_date, last_weight_date + timedelta(7), start_date + timedelta(booking_length / 2))
            next_weight_date = max(next_weight_date, last_weight_date + timedelta(5))
            if (next_weight_date - last_weight_date).days < 2:
                next_weight_date = None
            weight_record['next_weight_date'] = next_weight_date
        else:
            weight_record['next_weight_date'] = weight_record['bk_start_date']
        status = ''
        
        if weight_record['next_weight_date']:
            if weight_record['next_weight_date'] < today:
                status = 'overdue'
                overdue += 1
            if weight_record['next_weight_date'] == today:
                status = 'due'
                due += 1
        weight_record['status'] = status

    return {'weights': weights, 'most_weights': most_weights, 'weight_status': {'due': due, 'overdue': overdue, 'changes': changes}}

def get_employees(the_date: date, include_owners: bool = False) -> List[Employee]:
    sql = 'select ed_emp_no from vwemp_daily where ed_date = ?'

    emps = []
    rows = db.safeselect(sql, the_date)
    if rows:
        for row in rows:
            emps.append(Employee.fetch(row.ed_emp_no))
    
    if include_owners:
        owner = Employee.get_by_nickname('Fiona')
        emps.append(owner)
        
    return emps

def get_employees_for_date(the_date = None):
    if not the_date:
        the_date = date.today()
        
    sql = """select ed_emp_no, ed_emp_nickname, ed_emp_fullname, ed_rank_desc, ed_am, ed_pm, ed_order,
    ed_rank_class, ed_confirmed ed_approved, ed_comment, isnull(ed_start_override, ed_start_punch) ed_start_punch,
    isnull(ed_end_override, ed_end_punch) ed_end_punch, ed_lunch, ed_start_status, ed_end_status
    from vwemp_daily
    where ed_date = ?"""

    return db.safeselect(sql, the_date)

def get_today_employees():
    emps = get_employees_for_date()
    
    emp_list = [{'id': emp.ed_emp_no, 'name': emp.ed_emp_fullname} for emp in emps] if emps else []
    return emp_list

def weekly_shifts_common(date_arg=None):
    if date_arg:
        the_date = parse_date(date_arg)
    else:
        the_date = date.today()

    wd = the_date.weekday()
    
    start_date = the_date - timedelta(wd)
    end_date = start_date + timedelta(6)
    
    dates = {(start_date + timedelta(i)).strftime("%y%m%d"): start_date + timedelta(i) for i in range(7)}
    recs = db.safeselect('select ts_date, ts_off from vwdayoff where ts_date between ? and ?',
                            start_date, end_date)

    days_off = {rec.ts_date.strftime("%y%m%d") : rec.ts_off for rec in recs} if recs else {}
    recs = db.safeselect("""
select ts_date, ts_emp_no, ts_emp_nickname, ts_rank_desc, ts_am, ts_pm, ts_rank_class from vwtimesheet_published
where (ts_am in ('X', 'W') or ts_pm in ('X', 'W')) and ts_date between ? and ?""",
        start_date, end_date)
    
    emps = {}
    for ds, d in dates.items():
        emps[ds] = day_emp_html(d, True)

    return render_template('components/weekly_shifts.html', dates=dates, days_off=days_off, emps=emps)

def weekly_common(date_arg):
    if date_arg:
        the_date = parse_date(date_arg)
    else:
        the_date = date.today()

    wd = the_date.weekday()
    
    start_date = the_date - timedelta(wd)
    end_date = start_date + timedelta(6)
    
    dates = dates_between_dates(start_date, end_date)
    
    date_dict = {d.strftime("%y%m%d"): d for d in dates}
    
    shifts_html = weekly_shifts_common(date_arg)

    kennel_inv = {d.strftime('%y%m%d') : Inventory.get(d, 'Dog') for d in dates}
    cattery_inv = {d.strftime('%y%m%d') : Inventory.get(d, 'Cat') for d in dates}
  
    recs = db.safeselect("""select visit_date, visit_no, visit_time, visit_name, visit_type from vwvisitor
where visit_date between ? and ? and visit_type not in ('in', 'out') and visit_first_slot=1 order by visit_date, visit_time""", start_date, end_date)

    visits = {}
    
    if recs:
        for rec in recs:
            d = rec.visit_date.strftime('%y%m%d')
            if d not in visits:
                visits[d] = []
            visits[d].append(rec)

    wages, revenue, ratio, ratioClass, ratioDesc = get_revenue_wages(start_date)

    return render_template('weekly.html', start_date=start_date, end_date=end_date, dates=date_dict, 
                           kennel_inv=kennel_inv, cattery_inv=cattery_inv, visits=visits, wages=wages,
                           revenue=revenue, ratio=ratio, ratioClass=ratioClass, ratioDesc=ratioDesc,
                           shifts_html=shifts_html)

def date2weekstart(the_date):
    wd = the_date.weekday()    
    weekstart = the_date - timedelta(wd)
    return weekstart

def refund_deposit(bk_no):
    booking = Booking.fetch(bk_no)
    payment = booking.invoice.payments[0]
    ref = payment.ref

def get_email_record(view: str, key_no: int):
    sql = f'select * from {view}_unfiltered where email_key = ?'
    record = db.safeselect_one(sql, key_no)

    return record
    
def get_notice_html(view: str, key_no: int, reply_text: str, is_text: bool):
    record = get_email_record(view, key_no)

    if not record:
        return ''
    html, text, _ = prepare_email(record, reply_text)
    if is_text == 'text':
        html = text
    else:
        # html = html.replace('/static', 'intranet/static')
        html = css_inline.inline(html)

    return html
    
def email_docs(view: str, key_no: int, email: str, reply_text: str, is_text: bool):
    if not key_no:
        sql = f'select top 1 email_key from {view} order by email_repeat desc'
        key_value = db.safeselect_value(sql)
        key_no = int(key_value) if key_value else 0

    if not key_no:
        html = '<span style="color: red;">Could not find a booking/request</span>'
    else:
        html = get_notice_html(view, key_no, reply_text, is_text)

        if not html:
            html = '<span style="color: red;">Could not load record</span>'
        elif email:
            record = get_email_record(view, key_no)
            send_record_email(record, email, None)
            html = '<span class="bg-success">Message sent</span>'
            
    return html

def get_daycare_runs(the_date: date) -> List[Run]:
    sql = """select run_no from pa..tblrun
where run_no not in (select ro_run_no from pa..tblrunoccupancy where ro_date = ?)
and run_inactive = '' and run_spec_no = 1 and run_group <> 'unalloc'
"""

    run_nos = db.safeselect(sql, the_date)
    runs = [Run.fetch(row.run_no) for row in run_nos]
    return runs

def get_daycare_dogs(the_date: Optional[date] = None) -> List[Pet]:
    sql = """select pet_no
from pa..tblpet
left join pa..tblrunoccupancy on ro_pet_no = pet_no and ro_date = ?
where pet_custom6 = 'Yes' and ro_pet_no is null"""

    pet_nos = db.safeselect(sql, the_date)
    return [Pet.fetch(row.pet_no) for row in pet_nos]

def dates_between_dates(start_date: date, end_date: date) -> List[date]:
    dates = [start_date]
    for d in range((end_date - start_date).days + 1):
        dates.append(start_date + timedelta(d))
    return dates

def date_status(d: date, bookings: List[Booking]) -> Dict[str, str]:
    status = ''
    msg = ''
    for b in bookings:
        if b.start_date <= d <= b.end_date:
            status = 'D' if b.is_daycare else b.status.code
            if b.is_daycare:
                msg = 'Daycare booked'
                break
            if b.status.code == "V":
                msg = 'Confirmed'
            else:
                msg = 'Unconfirmed'
            msg += f' booking {b.no}'
            break
        
    if not status:
        _, occupied_level = get_summary(d)
        status = str(occupied_level)
        if occupied_level == 2:
            msg = 'Limited Availability'
        if occupied_level == 3:
            msg = 'Fully Booked'

    return {'status': status, 'tooltip': msg}

def get_dog_calendar(pet: Pet, start_date: date, end_date: date) -> Dict[str, Dict[str, str]]:
    sql = """select bk_no from pa..tblbooking
join pa..tblbookingitem on bi_bk_no = bk_no
where bi_pet_no = ? and bk_status <> 'C' and bk_start_date < ? and bk_end_date > ?"""
    
    
    bk_nos = db.safeselect(sql, pet.no, end_date, start_date)
    bookings = [Booking.fetch(row.bk_no) for row in bk_nos] if bk_nos else []
    
    dates = dates_between_dates(start_date, end_date)
    annotated_dates = {str(d): date_status(d, bookings) for d in dates}
    return annotated_dates
