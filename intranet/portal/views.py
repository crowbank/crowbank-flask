from flask import flash, redirect, render_template, request, url_for
from flask_login import current_user, login_required, login_user

from . import portal
from .forms import LoginForm
from intranet.user import CustomerUser


@portal.route('/login_old', methods=['GET', 'POST'])
def login_old():
    # Here we use a class of some kind to represent and validate our
    # client-side form data. For example, WTForms is a library that will
    # handle this for us, and we use a custom LoginForm to validate.
    form = LoginForm()
    if form.validate_on_submit():
        # Login and validate the user.
        # user should be an instance of your `User` class
        user = CustomerUser.get_by_email(form.email.data)
        if user and user.check_pwd(form.password.data):
            login_user(user)

            flash('Logged in successfully.')

            next = request.args.get('next')

            return redirect(next or url_for('portal.home'))
        else:
            flash('Login Failed', 'error')
            
    return render_template('portal/login.html', form=form)

@portal.route('/')
@login_required
def home():
    return render_template('portal/home.html')

@portal.route('/bookings')
@login_required
def show_bookings():
    return render_template('portal/bookings.html')

@portal.route('/pets')
@login_required
def show_pets():
    return render_template('portal/pets.html')

@portal.route('/profile')
@login_required
def show_profile():
    return render_template('portal/profile.html')

@portal.route('/forgot_password')
def forgot_password():
    return render_template('portal/forgot_password.html')

@portal.route('/register')
def register():
    return render_template('portal/register.html')
