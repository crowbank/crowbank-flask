from flask_wtf import FlaskForm, RecaptchaField
from wtforms import (TextAreaField,
                     StringField,
                     SubmitField,
                     HiddenField,
                     BooleanField,
                     DecimalField,
                     DateField,
                     TimeField,
                     FormField,
                     FieldList,
                     IntegerField,
                     RadioField,
                     PasswordField,
                     SelectField,
                     FloatField,
                     FileField,
                     EmailField)

from wtforms.validators import (InputRequired,
                                DataRequired,
                                NumberRange,
                                Email,
                                EqualTo,
                                Optional)

class LoginForm(FlaskForm):
    email = StringField('Email Address')
    password = PasswordField('Password')
    login = SubmitField('Login')

