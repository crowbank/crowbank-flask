from utils import db
from .cached import CachedPreloaded
from pyodbc import Row
from typing import Optional

DOG_RUNTYPES = [
    'standard',
    'double',
    'deluxe'
]
class RunType(CachedPreloaded):
    _cache = {}
    _by_code = {}
    _loaded = False
    _sql = 'select rtx_rt_no, rtx_desc, rtx_spec_no, rtx_capacity, rtx_reserve, rtx_amber_level, rtx_red_level from tblruntype_extra'

    def __init__(self, row: Row):
        self.no = row.rtx_rt_no
        self.desc = row.rtx_desc.lower()
        self.spec_no = row.rtx_spec_no
        self.reserve = row.rtx_reserve
        self.capacity = row.rtx_capacity
        self.amber_level = row.rtx_amber_level
        self.red_level = row.rtx_red_level
        
        RunType._by_code[self.desc] = self
            
    @classmethod
    def get_all(cls):
        return list(cls._cache.values())
        
    @classmethod
    def get_by_code(cls, code: str) -> 'Optional[RunType]':
        if not cls._loaded:
            cls._load_from_database()
        
        code = code.lower()
        
        if code in cls._by_code:
            return cls._by_code[code]
        
        return None
    
    @classmethod
    def get_by_spec(cls, spec_no: int):
        desc = 'any' if spec_no == 1 else 'cat'
        return cls.get_by_code(desc)

    @classmethod
    def get_all_by_spec(cls, spec_no: int):
        spec_no = int(spec_no)
        if spec_no == 2:
            return [cls.get_by_code('cat')]
        rts = list(map(cls.get_by_code, DOG_RUNTYPES))
        return rts
    
    def __str__(self):
        return f'Run Type {self.desc}'

    def __repr__(self):
        return self.desc.lower()