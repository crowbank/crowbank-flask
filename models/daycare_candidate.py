from models import Customer, Pet, Employee
from models.cached import CachedPreloaded
from datetime import datetime, date
from typing import Optional
from pyodbc import Row
from utils import db, parse_date

class DaycareCandidateHistory():
    def __init__(self, by: int, when: datetime, comment: str, status: str):
        self.by: Optional[Employee] = Employee.fetch(by) if by else None
        self.when: datetime = when
        self.comment: str = comment
        self.status: str = status

class DaycareCandidateStatus():
    STATIC_STATES = [
        (1, 'open', 'Not yet reviewed', True),
        (2, 'green', 'Pre-assessed as likely to be OK', True),
        (3, 'amber', 'Pre-assessed as a possible candidate', True),
        (4, 'red', 'Pre-assessed as unlikely to work', True),
        (5, 'rejected', 'Rejected', False),
        (6, 'scheduled', 'An assessment has been scheduled', True),
        (7, 'approved', 'Approved', False),
        (8, 'duplicate', 'Duplicate', False),
        (9, 'withdrawn', 'Withdrawn', False),
        (10, 'waiting', 'Waiting for customer response', True)
    ]
    _cache = {}
    _loaded = False
    
    def __init__(self, id: int, code: str, description: str, active: bool):
        self.id: int = id
        self.code: str = code
        self.description: str = description
        self.active: bool = active
    
    def __str__(self):
        return self.code
    
    @classmethod
    def _load_from_static(cls):
        for state in cls.STATIC_STATES:
            cls._cache[state[1]] = cls(*state)
        cls._loaded = True
        
    @classmethod
    def get_all(cls):
        return cls._cache.values()
    
    @classmethod
    def get(cls, code: str) -> Optional['DaycareCandidateStatus']:
        if not cls._loaded:
            cls._load_from_static()
            
        return cls._cache[code] if code in cls._cache else None
    
    @classmethod
    def fetch(cls, code: str) -> 'DaycareCandidateStatus':
        obj = cls.get(code)
        assert(obj)
        return obj

class DaycareCandidate(CachedPreloaded):
    _sql = """select dcc_no, dcc_create_date, dcc_cust_no, dcc_pet_no, dcc_status, dcc_comment, dcc_source
from tbldaycare_candidate where dcc_cust_no is not null and dcc_pet_no is not null"""
    _cache = {}
    _loaded = False
    
    def __init__(self, row: Row):
        self.no: int = row.dcc_no
        self.create_date: datetime = row.dcc_create_date
        self.cust_no: int = row.dcc_cust_no
        self.pet_no: int = row.dcc_pet_no
        self.comment: str = row.dcc_comment
        self.customer: Customer = Customer.fetch(self.cust_no)
        self.pet: Pet = Pet.fetch(self.pet_no)
        self.status: DaycareCandidateStatus = DaycareCandidateStatus.fetch(row.dcc_status)
        self.source = row.dcc_source
        self.fields = {}
        self.history = []
        
        self.load_fields()
    
    def read_history(self):
        self.history = []
        
        sql = """select dchh_by, dchh_when, dchh_comment, dchh_status
from tbldaycare_candidate_history where dcch_dcc_no = ?"""
        rows = db.safeselect(sql, self.no)
        if not rows:
            return
        
        for row in rows:
            hist = DaycareCandidateHistory(row.dcch_by, row.dcch_created, row.dcch_comment, row.dcch_status)
            self.history.append(hist)
                    
    def load_fields(self):
        sql = """select ff_desc, ff_field_no, ff_type, rglt_value
from tblform_field
join tblrg_lead_detail on rglt_field_no = ff_field_no
where ff_form_no = 29 and rglt_rgl_no = ?"""

        if self.source != 'crowbank.co.uk':
            sql = """select ff_desc, ff_field_no, ff_type, fei_value rglt_value
from tblform_field
join tblform_entry_item on fei_field_no = ff_field_no and fei_form_no = ff_form_no
where fei_fe_no = ?"""
            
        rows = db.safeselect(sql, self.no)
        if not rows:
            return
        weekdays = []
        for row in rows:
            str_value = row.rglt_value
            field_no = row.ff_field_no
            
            if field_no[:2] == '4.':
                weekdays.append(str_value[:2])
                continue
            
            if row.ff_type in ('checkbox', 'pulldown', 'radio', 'string', 'text'):
                self.fields[row.ff_desc.lower()] = str_value
                continue
            if row.ff_type == 'date':
                self.fields[row.ff_desc.lower()] = parse_date(str_value[:10])
                continue
            if row.ff_type in ('bk_no', 'cust_no', 'pet_no'):
                self.fields[row.ff_desc.lower()] = int(str_value)                
        
        if weekdays:
            self.fields['weekdays'] = ','.join(weekdays)
        
    def __getattr__(self, name):
        if name.lower() in self.fields:
            return self.fields[name.lower()]
        
        return None
    
    def update_status(self, status: str):
        dcc_status = DaycareCandidateStatus.fetch(status)
        self.status = dcc_status
    
    @classmethod
    def get_active(cls):
        if not cls._loaded:
            cls._load_from_database()
            
        return [candidate for candidate in cls._cache.values() if candidate.status.active]