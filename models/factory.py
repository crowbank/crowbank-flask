from utils import db
from abc import ABC, abstractmethod
from typing import Optional, TypeVar, Dict, Type, Union
from pyodbc import Row
from decimal import Decimal
from customer import Customer, Pet
from employee import Employee
from booking import Booking

KeyType = Union[int, str]

class Factory(ABC):
    def __init__(self):
        self._cache: Dict = {}
        self._sql: str = ''

    def get(self, key: KeyType):
        if key in self._cache:
            return self._cache[key]

        obj = self._get(key)
        if obj is not None:
            self._cache[key] = obj
            return obj

        return None

    def _get(self, key: KeyType):
        row = db.safeselect_one(self._sql, key)
        if not row:
            return None

        return self._create(row)
    
    @abstractmethod
    def _create(self, row: Row):
        pass

    def reset(self):
        self._cache = {}

class PreloadFactory(Factory):
    def __init__(self):
        super().__init__()
        self._loaded: bool = False

    def get(self, key: KeyType):
        if not self._loaded:
            self._load()

        return self._cache[key] if key in self._cache else None

    def _load(self):
        rows = db.safeselect(self._sql)
        if not rows:
            return
        
        for row in rows:
            obj = self._create(row)
            key = row[0]
            self._cache[key] = obj

        self._loaded = True
            
    def reset(self):
        super().reset()
        self._loaded = False

class PetFactory(Factory):
    def __init__(self):
        super().__init__()
        self._sql = """select pet_no, pet_cust_no, pet_name, pet_sex, pet_breed_no, pet_dob, pet_microchip,
spec_desc, spec_no, pet_warning, pet_deceased, pet_neutered, pet_vet_no, pet_pins_no,
pet_custom3, sv_value, pet_weight
from pa..tblpet
join pa..tblspecies on spec_no = pet_spec_no
left join pa..tblpetscore on ps_pet_no = pet_no
left join pa..tblscorevalue on sv_no = ps_sv_no and sv_st_no = 4
where pet_no = ?""" 

        
class CustomerFactory(Factory):
    def __init__(self):
        super().__init__()
        self._sql = """select cust_no, cust_title, cust_surname, cust_forename, cust_email,
cust_email2, cust_addr1, cust_addr3, cust_postcode, cust_telno_home,
cust_telno_mobile, cust_telno_mobile2, cust_notes,
iif(cust_custom1 = 'No', 0, 1) cust_deposit,
iif(cust_custom2 = 'No', 0, 1) cust_sms,
cust_custom3 cust_fb_review, cust_custom4 cust_approved,
cust_custom5 cust_banned
from pa..tblcustomer
where cust_no = ?"""

    def _create(self, row: Row):
        customer = Customer(row)
    
        sql = 'select v_amount from vwvoucher_balance where v_cust_no = ?'
        balance = db.safeselect_value(sql, customer.no)
        customer.voucher_balance = Decimal(balance) if balance else Decimal(0.0)
        
        self.add_pets(customer)

        return customer
    
    def add_pets(self, customer):
        sql = 'select pet_no from pa..tblpet where pet_cust_no = ?'
        rows = db.safeselect(sql, customer.no)
        if not rows:
            return
        
        for row in rows:
            pet = PetFactory.get(row.pet_no)
            customer.add_pet(pet)
    