from abc import ABC, abstractmethod
from pyodbc import Row
import sys, os
import time
from typing import Optional, TypeVar, Type, Union

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

from utils import db


T = TypeVar('T', bound='Cached')
P = TypeVar('P', bound='CachedPreloaded')
KeyType = Union[int, str]

def standardize_key(key: KeyType) -> KeyType:
    if isinstance(key, int):
        return key
    
    try:
        key = int(key)
        return key
    except ValueError:
        return key.lower()

class CachedPreloaded(ABC):
    _cache = {}
    _loaded = False
    _sql = ''
    
    @classmethod
    def reset(cls):
        cls._cache = {}
        cls._loaded = False

    @classmethod
    def reload(cls, key: KeyType):
        pass
    
    @abstractmethod
    def __init__(self, row: Row):
        pass

    @classmethod
    def get_all(cls):
        if not cls._loaded:
            cls._load_from_database()
            
        return cls._cache.values()
    
    @classmethod
    def get(cls: Type[P], key: KeyType) -> Optional[P]:
        key = standardize_key(key)
        
        if not cls._loaded:
            cls._load_from_database()
        return cls._cache[key] if key in cls._cache else None

    @classmethod
    def fetch(cls: Type[P], key: KeyType) -> P:
        obj = cls.get(key)
        assert(obj)
        return obj
    
    @classmethod
    def _load_from_database(cls) -> None:
        rows = db.safeselect(cls._sql)
        if not rows:
            return
        
        for row in rows:
            obj = None
#            try:
            obj = cls(row)
 #           except Exception as e:
  #              pass
            key = row[0].lower() if isinstance(row[0], str) else row[0]
            if obj:
                cls._cache[key] = obj
        cls._loaded = True

class Cached(ABC):
    _cache = {}
    _loaded = False
    _sql = ''
    _TTL = 3600 # default Time-To-Live is 1 hour

    @classmethod
    def reset(cls):
        cls._cache = {}

    @abstractmethod
    def __init__(self):
        pass
    
    @classmethod
    def reload(cls: Type[T], key: KeyType) -> None:
        key = standardize_key(key)
        row = cls.row_from_key(key)

        if not key in cls._cache:
            return
        
        _, obj = cls._cache[key]

        if not row:
            return
        
        obj.load_from_row(row)
        
        cls._cache[key] = (time.time(), obj)

    @classmethod
    def get(cls: Type[T], key: KeyType) -> Optional[T]:
        key = standardize_key(key)
        
        if key in cls._cache:
            timestamp, obj = cls._cache[key]
            if time.time() - timestamp < cls._TTL:
                return obj

        obj = cls._read_from_database(key)
        if obj is not None:
            cls._cache[key] = (time.time(), obj)
        
        return obj

    @classmethod
    def add(cls: Type[T], key: KeyType, obj: T) -> None:
        assert(key not in cls._cache)
        cls._cache[key] = (time.time(), obj)
        
    @classmethod
    def fetch(cls: Type[T], key: KeyType) -> T:
        obj = cls.get(key)
        assert(obj)
        return obj
    
    @abstractmethod
    def load_from_row(self, row: Row):
        pass
    
    @classmethod
    def row_from_key(cls: Type[T], key: KeyType) -> Optional[Row]:
        key = standardize_key(key)
        
        row = db.safeselect_one(cls._sql, key)
        
        return row
        
    @classmethod
    def _read_from_database(cls: Type[T], key: KeyType) -> Optional[T]:
        row = cls.row_from_key(key)
        if not row:
            return None
        
        obj = cls()
        obj.load_from_row(row)
        
        return obj

class Species(CachedPreloaded):
    _sql = 'Select spec_no, spec_desc from pa..tblspecies'
    _loaded = False
    
    def __init__(self, row: Row):
        self.no = row.spec_no
        self.desc = row.spec_desc
        
if __name__ == '__main__':
    spec = Species.get(1)
    assert(spec)
    print(spec.desc)
