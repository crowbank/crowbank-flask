from utils import db
from datetime import timedelta
from datetime import datetime
import json
from employee import Employee

# This module has two separate use cases:
# First, it models published timesheet data, used in /daily, /weekly, /rota, etc.
#   for this use, published data is used in read-only mode
# Second, it is used by /timesheet to prepare and publish timesheet data

class TimesheetMove():
    def __init__(self, date, emp, status):
        self.date = date
        self.emp = emp
        self.status = status
        
class Timesheet():
    def __init__(self, weekstart):
        self.weekstart = weekstart
        self.weekend = weekstart + timedelta(days=6)
        self.dates = {}
        self.emps = {}
        self.last_publish = None
        self.loaded = False
        self.no = None
        
    def load(self):
        """Load data from database"""
        
        sql = "select rs_no from tblrotaset where rs_weekstart = ? and rs_published is null"
        self.no = db.safeselect_value(sql, self.weekstart)
        
        sql = "select rd_emp_no, rd_date, rd_am, rd_pm from tblrotaday where rd_rs_no = ?"
        recs = db.safeselect(sql, self.no)
        
        for rec in recs:
            date = rec.rd_date
            emp = Employee.fetch(rec.rd_emp_no)
            if date not in self.dates:
                self.dates[date] = {}
            if emp.no not in self.emps:
                self.emps[emp.no] = {}
                
            record = {
                'employee': emp,
                'date': date,
                'am': rec.rd_am,
                'pm': rec.rd_pm                
            }
            
            self.dates[date][rec.ts_emp_no] = record
            self.emps[emp.no]  = record
            
        self.loaded = True
     
    def get_date(self, date):
        if not self.loaded:
            self.load()
        if date in self.dates:
            return self.dates[date]
        return None
    
    def get_emp(self, emp_no):
        if not self.loaded:
            self.load()
        if emp_no in self.emps:
            return self.emps[emp_no]
        return None
    
    def as_json(self):
        return json.dumps({
            date: {
                'date': date,
                'data': {
                    emp_no: {
                        'am': self.dates[date][emp_no]['am'],
                        'pm': self.dates[date][emp_no]['pm']
                    } for emp_no in self.dates[date]
                }
            }
            for date in self.dates
        })
        
class Timesheet_Draft(Timesheet):
    def __init__(self, weekstart):
        super().__init__(weekstart)
        self.move_stack = []
        self.latest_move = None

    def apply_move(self, date, emp_no, status):
        assert(date in self.dates)
        assert(emp_no in self.dates[date])
        self.dates[date][emp_no]['am'] = status[0]        
        self.dates[date][emp_no]['pm'] = status[1]
        
    def move(self, date, emp_no, new_status):
        if not self.loaded:
            self.load()
        
        if not date in self.dates or not emp_no in self.dates[date]:
            return

        old_status = self.dates[date][emp_no]['am'] + self.dates[date][emp_no]['pm']
        emp = Employee.get(emp_no)
        move_rec = {
            'date': date,
            'employee': emp,
            'old': old_status,
            'new': new_status,
            'prev': self.latest_move,
            'next': None
        }
        
        self.move_stack.append(move_rec)
        self.latest_move['next'] = move_rec
        self.latest_move = move_rec
        
        sql = f"""exec protamove ?, ?, ?"""
        db.safeexec(sql, date, emp_no, new_status)
        self.apply_move(date, emp_no, new_status)

    def undo(self):
        if not self.loaded:
            self.load()

        if not self.move_stack:
            return
        
        last_move = self.latest_move
        assert(last_move)
        sql = 'exec protaundo ?'
        db.safeexec(sql, self.weekstart)
        
        self.apply_move(last_move['date'], last_move['employee'].no, last_move['old'])
        self.latest_move = last_move['prev']
    
    def redo(self):
        sql = 'exec protaredo ?'
        db.safeexec(sql, self.weekstart)
        
        if not self.latest_move:
            return
        if not self.latest_move['next']:
            return
        
        self.latest_move = self.latest_move['next']
        self.apply_move(self.latest_move)
    
    def publish(self):
        if not self.loaded:
            return
        if not self.move_stack:
            return
        
    def revert(self):
        # Revert to most recently published state
        pass