from utils import db
from typing import List, Optional, Dict, TypeVar, Type, Union
from datetime import date, datetime, time
from pyodbc import Row
from .cached import Cached, CachedPreloaded
from .customer import Pet
from .employee import Employee

T = TypeVar('T', bound='NoteSeverity')

class NoteSeverity:
    _instances: Dict[int, 'NoteSeverity'] = {}

    def __new__(cls: Type[T], value: int, code: str = "", description: str = "") -> T:
        if value in cls._instances:
            return cls._instances[value]
        instance = super(NoteSeverity, cls).__new__(cls)
        instance.value = value
        instance.code = code
        instance.description = description
        cls._instances[value] = instance
        return instance

    GREEN: Union['NoteSeverity', None] = None
    AMBER: Union['NoteSeverity', None] = None
    RED: Union['NoteSeverity', None] = None

    def to_dict(self):
        return {
            'value': self.value,
            'code': self.code,
            'description': self.description
        }
        
    @classmethod
    def initialize_class_vars(cls):
        cls.GREEN = NoteSeverity(1, "GREEN", "A routine note, no action required")
        cls.AMBER = NoteSeverity(2, "AMBER", "An unusual note, action may be required")
        cls.RED = NoteSeverity(3, "RED", "A serious note, action required immediately")
        
    @classmethod
    def get_all(cls):
        return [cls.GREEN.to_dict(), cls.AMBER.to_dict(), cls.RED.to_dict()]

# Initialize class variables
NoteSeverity.initialize_class_vars()



def load_cat_note(form) -> Dict[str, str]:
    note_dict = {
        'cat_dry_food_eaten': form.get('cat_dry_food_eaten', 'N/A'),
        'cat_wet_food_eaten': form.get('cat_wet_food_eaten', 'N/A'),
        'cat_poo': form.get('cat_poo', '0'),
        'cat_pee': form.get('cat_pee', '0'),
        'cat_wet_given': form.get('cat_wet_given', ''),
        'cat_dry_given': form.get('cat_dry_given', '0')
    }
    return note_dict

def load_poop_note(form) -> Dict[str, str]:
    note_dict = {
        'poop_score': form.get('poop-score', '0'),
        'poop_blood': form.get('poop-blood', '0'),
        'poop_mucus': form.get('poop-mucus', '0')
    }
    return note_dict

class NoteType(CachedPreloaded):
    _sql = 'select nt_no, nt_code, nt_description from tblnotetype'
    
    loading_function_dict = {
        'CAT': load_cat_note,
        'POOP': load_poop_note
    }
    
    _cache = {}
    _loaded = False
    _cache_by_code = {}

    def __init__(self, row: Row):
        self.no: int = row.nt_no
        self.code: str = row.nt_code
        self.description: str = row.nt_description
        self.loading_function = NoteType.loading_function_dict.get(self.code, None)
        
    @classmethod
    def get_all(cls):
        if not cls._loaded:
            cls._load_from_database()

            for note_type in cls._cache.values():
                cls._cache_by_code[note_type.code] = note_type

            cls._cache_by_code['CAT'].loading_function = load_cat_note
            cls._cache_by_code['POOP'].loading_function = load_poop_note
        
        sorted_types = sorted(cls._cache.values(), key=lambda t: t.code)
        return {nt.no: nt.to_dict() for nt in sorted_types}

    def to_dict(self):
        return {
            'no': self.no,
            'code': self.code,
            'description': self.description
        }
        
class NoteSnap():
    _sql = """select sn_no, sn_sev_no, sn_obs, sn_comment,
sn_recorder, sn_photo, sn_created
from tblnotesnapshot where sn_note_no = ? order by sn_created"""

    def __init__(self, note: 'Note', sev: NoteSeverity,
                 comment: str, recorder: Employee,
                 photo: str, created: datetime):
        self.no = 0
        self.note = note
        self.sev = sev
        self.comment = comment
        self.recorder = recorder
        self.photo = photo
        self.created = created
        self.type_specific = {}
        note.push_snap(self)
    
    def type_load(self, form):
        if self.note.type.loading_function:
            self.type_specific = self.note.type.loading_function(form)
            
    def write_to_db(self):
        if self.no:
            return
        
        sql = """insert into tblnotesnapshot (sn_note_no, sn_sev_no, sn_comment, sn_recorder, sn_photo, sn_created)
values (?, ?, ?, ?, ?, ?)"""

        db.safeexec(sql, self.note.no, self.sev.value, self.comment,
                    self.recorder.no, self.photo, self.created)
        
        sql = 'select max(sn_no) from tblnotesnapshot where sn_note_no = ?'
        no = db.safeselect_value(sql, self.note.no)
        assert(no)
        self.no = int(no)
        assert(self.no)
        
        if self.type_specific:
            sql = """insert into tblnotedetail (nd_nt_no, nd_sn_no, nd_field, nd_value)
values (?, ?, ?, ?)"""
            for field, value in self.type_specific.items():
                db.safeexec(sql, self.note.type.no, self.no, field, value)
    
    @classmethod
    def create_from_row(cls, note: 'Note', row: Row):
        no = row.sn_no
        sev_no = row.sn_sev_no
        sev = NoteSeverity(sev_no)
        comment = row.sn_comment
        recorder = Employee.fetch(row.sn_recorder)
        photo = row.sn_photo if row.sn_photo else ''
        created = row.sn_created
        snap = cls(note, sev, comment, recorder, photo, created)
        snap.no = no
        
        sql = 'select nd_field, nd_value from tblnotedetail where nd_sn_no = ?'
        rows = db.safeselect(sql, no)
        if rows:
            for d_row in rows:
                snap.type_specific[d_row.nd_field] = d_row.nd_value
        
        return snap
    
    @classmethod
    def get_note_snaps(cls, note: 'Note'):
        rows = db.safeselect(cls._sql, note.no)
        return [cls.create_from_row(note, row) for row in rows] if rows else []
  
class Note(Cached):
    _sql = """select note_no, note_date, note_pet_no,
note_type_no, note_created, note_time, note_reporter
from tblnote
where note_no = ?
order by note_created desc"""
    _cache = {}
    
    @classmethod
    def notes_by_date(cls, date: date) -> List['Note']:
        sql = 'select note_no from tblnote where note_date = ?'
        rows = db.safeselect(sql, date)
        return [cls.fetch(row.note_no) for row in rows] if rows else []
    
    @classmethod
    def get_daily_count(cls, date: date) -> int:
        sql = 'select count(*) from tblnote where note_date = ?'
        c = db.safeselect_value(sql, date)
        return int(c) if c else 0
    
    @classmethod
    def notes_by_pet(cls, pet: Pet) -> List['Note']:
        sql = 'select note_no from tblnote where note_pet_no = ?'
        rows = db.safeselect(sql, pet.no)
        return [cls.fetch(row.note_no) for row in rows] if rows else []
    
    @classmethod
    def _read_from_database(cls, key: int) -> Optional['Note']:
        row = cls.row_from_key(key)
        if not row:
            return None
        
        pet: Pet = Pet.fetch(row.note_pet_no)
        the_date: date = row.note_date
        the_time: time = row.note_time
        type: NoteType = NoteType.fetch(row.note_type_no)
        created: datetime = row.note_created
        reporter = Employee.fetch(row.note_reporter)
        
        note = cls(pet, the_date, the_time, type, created, reporter)
        note.no = row.note_no
        
        note.snaps = NoteSnap.get_note_snaps(note)
        
        return note

    def __init__(self, pet: Pet, the_date: date, the_time: time, type: NoteType, created: datetime, reporter: Employee):
        self.no = 0
        self.pet: Pet = pet
        self.date: date = the_date
        self.time: time = the_time
        self.type: NoteType = type
        self.created: datetime = created
        self.reporter: Employee = reporter
        self.snaps = []
        
    def write_to_db(self):
        if self.no:
            return
        
        sql = """insert into tblnote (note_date, note_time, note_pet_no, note_type_no, note_created, note_reporter) values
(?, ?, ?, ?, ?, ?)"""
        db.safeexec(sql, self.date, self.time, self.pet.no, self.type.no, self.created, self.reporter.no)
        sql = 'select max(note_no) from tblnote where note_date = ? and note_pet_no = ? and note_type_no = ?'
        no = db.safeselect_value(sql, self.date, self.pet.no, self.type.no)
        assert(no)
        self.no = int(no)
        assert(self.no)
        Note.add(self.no, self)
        
    def load_from_row(self, row: Row):
        self.no = row.note_no
        self.pet = Pet.fetch(row.note_pet_no)
        self.date = row.note_date
        self.type = NoteType.fetch(row.note_type_no)
        self.created = row.note_created
        self.reporter = Employee.fetch(row.note_reporter)
        
        self.snaps = NoteSnap.get_note_snaps(self)

    def push_snap(self, snap: NoteSnap):
        self.snaps.append(snap)
    
    def get_snap(self, snap_no: int) -> NoteSnap:
        return self.snaps[snap_no]

    @property
    def ver_count(self) -> int:
        return len(self.snaps)
    
    @property
    def current_snap(self) -> NoteSnap:
        return self.snaps[-1]
    