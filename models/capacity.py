from flask import flash
from dataclasses import dataclass
from datetime import date
from .runtype import RunType
from utils import db, SingletonMeta

@dataclass
class CapacityRecord():
    start_date: date
    end_date: date
    limit: int
    rt: RunType
    
# Availablity Manager
class CapacityManager(metaclass=SingletonMeta):
    """Read and use capacity records from tblcapacity
    First matching record is used
    Capacity here only refers to overall number of dog kennels are are happy to fill
    for a given period."""

    def __init__(self):
        self.capacity_records = []
        self.default_capacity = 0
        self.loaded = False
    
    def load(self):
        sql = """select cap_start_date, cap_end_date, cap_limit, cap_rt_no from tblcapacity"""
        rows = db.safeselect(sql)
        
        if rows:
            for row in rows:
                rt = RunType.get(row.cap_rt_no)
                assert(rt)
                self.capacity_records.append(CapacityRecord(start_date=row.cap_start_date, end_date=row.cap_end_date, limit=row.cap_limit, rt=rt))
            
        all_run = RunType.get_by_code('Any')
        assert(all_run)
        self.default_capacity = all_run.capacity
        self.loaded = True        
        
    def get(self, cap_date: date, rt: RunType):
        if not self.loaded:
            self.load()
            
        for cap in self.capacity_records:
            if cap.rt.no != rt.no:
                continue
            if cap.start_date and cap.start_date > cap_date:
                continue
            if cap.end_date and cap.end_date < cap_date:
                continue
            return cap.limit
        return rt.capacity