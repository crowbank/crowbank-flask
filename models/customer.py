from utils import db
from pyodbc import Row
from typing import Dict, Optional, List
from datetime import date, timedelta
from decimal import Decimal
from .cached import Cached, CachedPreloaded
from .breed import Breed
from .vet import Vet

def custom2bool(custom: str) -> bool:
    return custom == 'Yes'

class Customer(Cached):
    _sql = """select cust_no, cust_title, cust_forename, cust_surname, cust_addr1,
cust_addr3, cust_postcode, cust_telno_home, cust_telno_mobile,
cust_telno_mobile2, cust_email, cust_email2,
cust_notes, cust_custom1 cust_deposit, cust_custom2 cust_sms,
cust_custom3 cust_fb_review, cust_custom4 cust_approved,
cust_custom5 cust_banned, cust_custom6
from pa..tblcustomer where cust_no = ?"""
    _cache = {}

    @classmethod
    def get(cls, cust_no):
        customer = super().get(cust_no)
        if customer:
            customer.add_pets()
            
        return customer
    
    def __init__(self):
        self.bookings = {}
        self.pets: List[Pet] = []
        self.voucher_balance = Decimal(0.0)       
        self.no = 0
        self.title = ''
        self.forename = ''
        self.surname = ''
        self.telno_mobile = ''
        self.email = ''
        self.email2 = ''
        self.addr1 = ''
        self.addr3 = ''
        self.postcode = ''
        self.telno_home = ''
        self.telno_mobile = ''
        self.telno_mobile2 = ''
        self.fb_review = ''
        self.notes = ''
        self.password = ''
        self.deposit = True
        self.approved = False
        self.banned = False
        self.sms = False

    def load_from_row(self, row: Row):
        self.no = row.cust_no
        self.title = row.cust_title
        self.forename = row.cust_forename
        self.surname = row.cust_surname
        self.telno_mobile = row.cust_telno_mobile
        self.email = row.cust_email
        self.email2 = row.cust_email2
        self.addr1 = row.cust_addr1
        self.addr3 = row.cust_addr3
        self.postcode = row.cust_postcode
        self.telno_home = row.cust_telno_home
        self.telno_mobile = row.cust_telno_mobile
        self.telno_mobile2 = row.cust_telno_mobile2
        self.notes = row.cust_notes
        self.password = row.cust_custom6
        self.sms = custom2bool(row.cust_sms)
        self.fb_review = row.cust_fb_review
        self.approved = custom2bool(row.cust_approved)
        self.banned = custom2bool(row.cust_banned)
        self.deposit = custom2bool(row.cust_deposit)    
        
    def add_pet(self, pet: 'Pet') -> None:
        if pet not in self.pets:
            self.pets.append(pet)

    def add_pets(self) -> None:
        sql = 'select pet_no from pa..tblpet where pet_cust_no = ?'
        rows = db.safeselect(sql, self.no)
        if rows:
            for row in rows:
                self.add_pet(Pet.fetch(row.pet_no))
        self._pets_loaded = True
    
    def add_booking(self, booking: 'Booking') -> None:
        self.bookings[booking.no] = booking
        
    def as_dict(self) -> Dict:
        return {
            'no': self.no,
            'forename': self.forename,
            'surname': self.surname,
            'telno_mobile': self.telno_mobile,
            'email': self.email,
            'email2': self.email2,
            'pets': [pet.no for pet in self.pets],
            'bookings': [booking.no for booking in self.bookings]            
        }
        
    @property
    def pet_descriptions(self) -> str:
        if not self.pets:
            return ''
        if len(self.pets) == 1:
            return self.pets[0].description
        return ', '.join(map(lambda p: p.description, self.pets[0:-1])) + \
            ' and ' + self.pets[-1].description
    
    @property
    def display_name(self) -> str:
        result = f"{self.title + ' ' if self.title else ''}{self.forename + ' ' if self.forename else ''}{self.surname if self.surname else ''}"
        return result.strip()
    
    @property
    def pet_numbers(self) -> List[int]:
        return list(map(lambda p: p.no, self.pets))
    
    @property
    def name(self) -> str:
        return f'{self.forename} {self.surname}'
    
    @property
    def has_dogs(self) -> bool:
        return any([pet.is_dog for pet in self.pets])
    
    @property
    def has_cats(self) -> bool:
        return any([pet.is_cat for pet in self.pets])

    def repopulate_pets(self):
        pet_nos = [pet.no for pet in self.pets]
        self.pets = [Pet.fetch(pet_no) for pet_no in pet_nos]

class VaccinationType(CachedPreloaded):
    KC = 'Kennel cough'
    Lepto = 'Lepto/DHP'
    Cat = 'Feline set'
    _cache = {}
    _sql = """select inn_no, inn_desc from pa..tblinnoculation"""
    _loaded = False
    
    def __init__(self, row: Row):
        self.no = row.inn_no
        self.description = row.inn_desc
        
    def __repr__(self) -> str:
        return f'VaccinationType({self.no}, {self.description})'
    
class Vaccination():
    _sql = """select pi_pet_no, pi_inn_no, pi_innoculation_date from pa..tblpetinnoculation
join pa..tblinnoculation on inn_no = pi_inn_no
where pi_pet_no = ? and pi_innoculation_date is not null"""

    def __init__(self, pet: 'Pet', row: Row):
        self.pet: 'Pet' = pet
        self.type: VaccinationType = VaccinationType.fetch(row.pi_inn_no)
        inc_date = row.pi_innoculation_date
        if inc_date:
            self.date: date = inc_date.date()
        else:
            pass
        self.expiry = self.date + timedelta(days=365)
        self.expired = self.expiry < date.today()

    @classmethod
    def get_vaccinations(cls, pet: 'Pet') -> List['Vaccination']:
        rows = db.safeselect(cls._sql, pet.no)
        return [cls(pet, row) for row in rows] if rows else []
    
    
class PetDocument():
    def __init__(self, pet: 'Pet', row: Row):
        self.pet = pet
        self.description = row.pd_desc
        self.path = row.pd_path
        
    @classmethod
    def get_documents(cls, pet: 'Pet') -> List['PetDocument']:
        sql = """select pd_desc, pd_path from pa..tblpetdocument where pd_pet_no = ?"""
        rows = db.safeselect(sql, pet.no)
        return [cls(pet, row) for row in rows] if rows else []

        
class Pet(Cached):
    _sql = """select pet_no, pet_cust_no, pet_name, pet_sex, pet_breed_no, pet_dob, pet_microchip,
spec_desc, spec_no, pet_warning, pet_deceased, pet_neutered, pet_vet_no, pet_pins_no,
pet_custom3, sv_value, pet_weight, pet_custom6
from pa..tblpet
join pa..tblspecies on spec_no = pet_spec_no
left join pa..tblpetscore on ps_pet_no = pet_no
left join pa..tblscorevalue on sv_no = ps_sv_no and sv_st_no = 4
where pet_no = ?""" 
    _cache = {}
    
    @classmethod
    def _read_from_database(cls, key: int) -> Optional['Pet']:
        row = cls.row_from_key(key)
        if not row:
            return None
        
        cust_no = row.pet_cust_no
        obj = cls(cust_no)
        obj.load_from_row(row)
        
        return obj

    def __init__(self, cust_no: int):
        self.no = 0
        self.name = ''
        self.cust_no = cust_no
        self.sex = ''
        self.dob = date.today()
        self.breed_no = 259
        self.breed: Breed = Breed.fetch(self.breed_no)
        self.species = ''
        self.spec_no = 1
        self.vacc_status = ''
        self.deceased = False
        self.warning = ''
        self.microchip = ''
        self.pins_no = 0
        self.vet: Optional[Vet] = None
        self.neutered: Optional[bool] = None
        self.bookings: List = []
        self._customer = None
        self.check = True
        self.breed = Breed.fetch(0)
        self.documents: list[PetDocument] = []
        self.vaccinations: list[Vaccination] = []
        self.vacc_date: Optional[date] = None
        self.kc_date: Optional[date] = None
        self.is_diabetic = False
        self.body_score = 0
        self.weight = 0.0
        self._bookings_loaded = False
        self.daycare_approved = False
        
    def load_from_row(self, row: Row):
        self.no = row.pet_no
        self.name = row.pet_name
        self.cust_no = row.pet_cust_no
        self.sex = row.pet_sex
        self.dob = row.pet_dob
        self.breed_no = row.pet_breed_no
        self.breed = Breed.fetch(self.breed_no)
        self.species = row.spec_desc
        self.spec_no = row.spec_no
        self.deceased = row.pet_deceased == 'Y'
        self.warning = row.pet_warning
        self.microchip = row.pet_microchip
        self.pins_no = row.pet_pins_no
        self.vet = Vet.get(row.pet_vet_no)
        self.is_diabetic = row.pet_custom3 == 'Yes'
        self.body_score = row.sv_value if row.sv_value else 0
        self.weight = row.pet_weight
        self.daycare_approved = row.pet_custom6 == 'Yes'
        if row.pet_neutered:
            self.neutered = row.pet_neutered == 'Y'
            
        self.documents = PetDocument.get_documents(self)
        self.vaccinations = Vaccination.get_vaccinations(self)
        if self.is_cat:
            self.vacc_date = self.vaccinations[0].date if self.vaccinations else None
        else:
            self.vacc_date = next((v.date for v in self.vaccinations if v.type == VaccinationType.Lepto), None)
            self.kc_date = next((v.date for v in self.vaccinations if v.type == VaccinationType.KC), None)
            
        self.note_count = self.get_note_count()
    
    def get_note_count(self) -> int:
        sql = 'select count(*) from tblnote where note_pet_no = ?'
        c = db.safeselect_value(sql, self.no)
        return c if c else 0        
    
    @property
    def vacc_req(self) -> bool:
        return self.vaccination_status == 'Valid'

    @property
    def vaccination_status(self) -> str:
        if not self.vaccinations:
            return 'None'
        if self.is_cat:
            return 'Expired' if self.vaccinations[0].expired else 'Valid'

        vaccine_types = set([v.type.description for v in self.vaccinations if not v.expired])
        if VaccinationType.KC in vaccine_types and VaccinationType.Lepto in vaccine_types and all(not v.expired for v in self.vaccinations):
                return 'Valid'
        
        return 'Expired'
    
    @property
    def is_dog(self) -> bool:
        return self.species == 'Dog'
        
    @property
    def is_cat(self) -> bool:
        return self.species == 'Cat'
        
    @property
    def customer(self) -> Customer:
        if not self._customer:
            self._customer = Customer.fetch(self.cust_no)
        return self._customer
    
    def as_dict(self) -> Dict:
        return {
            'no': self.no,
            'name': self.name,
            'cust_no': self.cust_no,
            'sex': self.sex,
            'breed_no': self.breed_no,
            'breed': self.breed.as_dict() if self.breed else None,
            'warning': self.warning
        }

    @property
    def description(self) -> str:
        breed_desc = self.breed.description if self.is_dog else 'Cat'
        return f'{self.name} ({breed_desc})'
    
    @property
    def full_description(self) -> str:
        breed_desc = self.breed.description if self.is_dog else 'Cat'
        return f'{self.name} {self.customer.surname} ({breed_desc})'
    
    @property
    def vacc_given(self) -> Optional[date]:
        return self.vaccinations[0].date if self.vaccinations else None
    
    @property
    def kc_given(self) -> Optional[date]:
        if not self.vaccinations or self.is_cat:
            return None
        
        if not VaccinationType.KC in [v.type.description for v in self.vaccinations]:
            return None
        
        return [v.date for v in self.vaccinations if v.type.description == VaccinationType.KC][0]
    
    @property
    def questionnaire_no(self) -> int:
        sql = """select max(ndf_no)
from tblquestionnaire
where ndf_pet_no = ? and ndf_status <> 'duplicate'"""
        return db.safeselect_value(sql, self.no) or 0    
                    
    def add_booking(self, booking: 'Booking'):
        if booking.no in map(lambda b: b.no, self.bookings):
            return
        self.bookings.append(booking)

    def __repr__(self) -> str:
        return f'Pet({self.no}, {self.name})'
    
    @property
    def button_html(self) -> str:
        if self.deceased:
            pet_sex = 'pet-deceased'
        else:
            pet_sex = 'pet-sex-' + self.sex.lower()
    
        html = f"""<a href="/pet/{self.no}">
<button type="button" style="font-size: 0.75rem;" class="mb-1 pet {pet_sex} btn btn-outline-dark">
            {self.name} ({self.breed.full_description})
        </button></a>"""
        return html
