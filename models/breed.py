from .cached import CachedPreloaded
from pyodbc import Row
from typing import Optional

class Breed(CachedPreloaded):
    _sql = """select breed_no, bs_desc, billcat_desc, breed_desc
from pa..tblbreed
join tblbreedshortname on bs_breed_no = breed_no
join pa..tblbillcategory on billcat_no = breed_billcat_no"""
    _loaded = False
    _cache = {}
    
    def as_dict(self):
        return {
            'no': self.no,
            'desc': self.description,
            'full': self.full_description
        }
        
    def __init__(self, row: Optional[Row]):
        if not row:
            self.no = 0
            self.description = 'N/A'
            self.full_description = 'N/A'
            self.bill_category = 'N/A'
            return
        
        self.no = row.breed_no
        self.description = row.bs_desc
        self.full_description = row.breed_desc
        self.bill_category = row.billcat_desc

    @classmethod
    def fetch(cls, no: int) -> 'Breed':
        if no:
            return super().fetch(no)
        
        return Breed(None)