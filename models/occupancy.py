from dataclasses import dataclass
from datetime import date
from flask import flash

from .slot import Slot
from .runtype import RunType
from utils import db, SingletonMeta

class OccupancyException(Exception):
    pass
@dataclass
class SlotRT():
    slot: Slot
    rt: RunType

    def __hash__(self):
        return hash(f'{self.slot}-{self.rt}')
       
class OccupancyManager(metaclass=SingletonMeta):
    def __init__(self):
        self.occupancy = {}
        self.unallocated = {}
        self.loaded = False
        self.first_date = date(2099, 12, 31)
        self.last_date = date(1970, 1, 1)
    
    def load(self):
        old_occupancy = self.occupancy
        self.occupancy = {}
        self.closed = {}
        self.unallocated = {}
        try:
            sql = """select rc_date, rc_rt_no, rc_rt_desc, rc_count
from vwrunclosed where rc_date >= convert(date, getdate())"""
            rows = db.safeselect(sql)
            if rows:
                for row in rows:
                    rt = RunType.fetch(row.rc_rt_no)
                    self.closed[row.rc_date, rt] = row.rc_count
                
            sql = """select ra_date, ra_rt_no, ra_rt_desc, ra_slot, ra_count
from vwrunavailability3 where ra_date >= convert(date, getdate())"""
            rows = db.safeselect(sql)
            if rows:
                for row in rows:
                    rt = RunType.fetch(row.ra_rt_no)
                    self.occupancy[SlotRT(slot=Slot(row.ra_date, row.ra_slot), rt=rt)] = row.ra_count
                    if row.ra_date < self.first_date:
                        self.first_date = row.ra_date
                    if row.ra_date > self.last_date:
                        self.last_date = row.ra_date
    
            sql = """select ua_date, ua_slot, ua_dogs, ua_cats
from vwunallocated2
where ua_date >= convert(date, getdate())"""
            rows = db.safeselect(sql)
            if rows:
                for row in rows:
                    self.unallocated[Slot(row.ua_date, row.ua_slot)] = [row.ua_dogs, row.ua_cats]
            self.loaded = True
        except Exception as e:
            flash('Occupancy update failed')
            self.occupancy = old_occupancy

    def get(self, slot: Slot, rt: RunType) -> int:
        if not self.loaded:
            self.load()
        slot_rt = SlotRT(slot, rt)
        if slot_rt in self.occupancy:
            return self.occupancy[slot_rt]
        return -1
        # raise OccupancyException(f'No data for {rt} on {slot}')

    def get_closed(self, closed_date: date, rt: RunType) -> int:
        if not self.loaded:
            self.load()
        if (closed_date, rt) in self.closed:
            return self.closed[closed_date, rt]
        return 0
        
    def get_unalloc(self, slot: Slot, spec_no: int) -> int:
        spec_no = int(spec_no)
        if not self.loaded:
            self.load()
        if not slot in self.unallocated:
            return 0
        return self.unallocated[slot][spec_no - 1]
        
    def __call__(self, slot: Slot, rt: RunType) -> int:
        return self.get(slot, rt)
