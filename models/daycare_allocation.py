from utils import db

class DaycareAllocation():
    allocations = []
    loaded = False
    
    def load():
        sql = """select dca_start_date, dca_end_date, dca_mo, dca_tu, dca_we, dca_th, dca_fr, dca_sa, dca_su from tbldaycare_allocation"""
        rows = db.safeselect(sql)
        
        for row in rows:
            DaycareAllocation.allocations.append(DaycareAllocation(row))
        DaycareAllocation.loaded = True
        
    def get(a_date):
        if not DaycareAllocation.loaded:
            DaycareAllocation.load()
            
        for allocation in DaycareAllocation.allocations:
            if allocation.is_match(a_date):
                return allocation.get_allocation(a_date.weekday())
        return -1

    def __call__(a_date):
        return DaycareAllocation.get(a_date)
    
    def __init__(self, row):
        self.start_date = row.dca_start_date
        self.end_date = row.dca_end_date
        self.allocations = [row.dca_mo, row.dca_tu, row.dca_we, row.dca_th, row.dca_fr, row.dca_sa, row.dca_su]
    
    def is_match(self, a_date):
        if self.start_date and a_date < self.start_date:
            return False
        if self.end_date and a_date > self.end_date:
            return False
        return True
    
    def get_allocation(self, weekday):
        return self.allocations[weekday]
        
            
