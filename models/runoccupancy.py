from .slot import Slot
from .run import Run
from utils import db, SingletonMeta
from flask import flash
from dataclasses import dataclass
from datetime import date
import time
from models import Pet, Booking

@dataclass
class OccupancyCell():
    booking: Booking
    pets: list[Pet]
    
    def as_dict(self):
        return {
            'booking': self.booking.as_dict(),
            'pets': [pet.as_dict() for pet in self.pets]
        }

    def pet_nos(self):
        pet_no_list = [pet.no for pet in self.pets]
        pet_no_list.sort()
        return ','.join([str(pet_no) for pet_no in pet_no_list])
    
    def __repr__(self):
        return f'{self.booking.no}:{self.pet_nos()}'

class OccupancyBlock():
    def __init__(self, cell, slot, run):
        self.cell = cell
        self.booking = cell.booking
        self.pets = cell.pets
        self.start_slot = slot
        self.end_slot = slot
        self.run = run
        self.end_time = None
        if slot.date == cell.booking.start_date:
            self.start_time = cell.booking.start_time
        else:
            self.start_time = None
        
    def add_slot(self, slot):
        self.end_slot = slot
        if slot.date == self.cell.booking.end_date:
            self.end_time = self.cell.booking.end_time
        
    def __repr__(self):
        return f'{self.start_slot}:{self.run.code}:{self.cell.__repr__()}'
        
        
class RunOccupancyManager(metaclass=SingletonMeta):
    def __init__(self):
        self.occupancy = {}
        self.blocks = {}
    
    def load(self, from_date: date, to_date: date):
        et1 = time.time()
        ct1 = time.process_time()
        self.occupancy = {}
        self.blocks = {}
               
        try:
            sql = """select ro_date, ro_slot, ro_bk_no, string_agg(convert(varchar(10), ro_pet_no), ',') ro_pet_nos,
ro_run_no
from vwrunoccupancy_slot
join pa..tblbooking on bk_no = ro_bk_no
where ro_date between ? and ? and
bk_status in ('', 'V')
group by ro_date, ro_slot, ro_bk_no, ro_run_no
order by ro_run_no, ro_date, ro_slot, ro_bk_no"""

            rows = db.safeselect(sql, from_date, to_date)
            current_block = None
            if rows:
                for row in rows:
                    slot = Slot(row.ro_date, row.ro_slot)
                    bk_no = row.ro_bk_no
                    rec = { 'bk_no': row.ro_bk_no,
                        'pets': [int(pet_no) for pet_no in row.ro_pet_nos.split(',')]
                        }

                    run_no = row.ro_run_no
                    run = Run.get(run_no)
                    booking = Booking.get(rec['bk_no'])
                    cell = OccupancyCell(booking=booking,
                        pets=[Pet.get(pet_no) for pet_no in rec['pets']]
                    )

                    if not run_no in self.blocks:
                        self.blocks[run_no] = []

                    if not current_block or current_block.run.no != run_no or \
                        current_block.booking.no != bk_no or not current_block.end_slot.isPreceding(slot):
                        current_block = OccupancyBlock(cell, slot, run)
                        self.blocks[run_no].append(current_block)
                    current_block.add_slot(slot)
            
            et2 = time.time()
            ct2 = time.process_time()
            
            print(f'Total time elapsed {et2 - et1}')
            print(f'CPU time elapsed {ct2 - ct1}')

        except Exception as e:
            flash('Occupancy update failed')
    
    def get_blocks_by_date_range(self, start_date: date, end_date: date, force=False):
        self.load(start_date, end_date)

        blocks = {}
        for run_no, _ in self.blocks.items():
            blocks[run_no] = list(filter(
                lambda b: b.start_slot.date <= end_date and b.end_slot.date >= start_date,
                self.blocks[run_no]))
            
        return blocks
