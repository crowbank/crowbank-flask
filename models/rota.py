from datetime import timedelta, date, datetime
from utils import db
from .employee import Employee
from typing import List, Optional, Dict, Tuple
from decimal import Decimal

class RotaItem:
    def __init__(self, no: int, status: str, timestamp: datetime, published, label: str):
        self.no = no
        self.status = status
        self.timestamp = timestamp
        self.published = published
        self.label = label

class RotaSlot:
    def __init__(self, the_date: date, emp_no: int):
        self.date: date = the_date
        self.emp_no: int = emp_no
        self.employee: Employee =  Employee.fetch(emp_no)
        self.items: List[RotaItem] = []
        self.current_status: str = ''
        self.published_status: str = ''
        self.current_no: int = 0
        self.published_no: int = 0
        self.shifts: Dict[date, Dict[int, str]] = {}
        
    def add_item(self, no: int, status: str, timestamp: datetime, published: bool, label: str):
        item = RotaItem(no, status, timestamp, published, label)
        self.items.append(item)
        self.current_no = no
        self.current_status = status
        if item.published:
            self.published_no = no
            self.published_status = status
            
    def get_status(self, selector: str = 'published'):
        if selector == 'published':
            return self.published_status
        
        if selector == 'current':
            return self.current_status
        
        """Determine if selector is int - take that to be an item no
            If selector is a label, look for the label"""
        if isinstance(selector, int):
            status = ''
            for item in self.items:
                if item.no > selector:
                    return status
                status = item.status
            return status
        
        return ''
    
    def publish(self):
        self.items[-1].published = 1
        self.published_no = self.current_no
        self.published_status = self.current_status
        
    def revert(self):
        self.current_no = self.published_no
        self.current_status = self.published_status
        self.items = list(filter(lambda ri: ri.no > self.published_no, self.items))
            
    def wage(self, the_date: date) -> Decimal:
        code_hours = {
            'XX': 8.75,
            'X ': 4.5,
            ' X': 4.25
        }
        
        if not the_date in self.shifts:
            return Decimal(0.0)
        
        if not self.employee.no in self.shifts[the_date]:
            return Decimal(0.0)
        
        code = self.shifts[the_date][self.employee.no]
        if not code in code_hours:
            return Decimal(0.0)
        
        hours = code_hours[code]
        if self.employee.hourly:
            return Decimal(hours) * self.employee.current_rate
        
        days = 1.0 if code == 'XX' else 0.5
        return Decimal(days / 240.0) * self.employee.current_rate


class Rota:
    rotas = {}
    status_desc = {
        'X ': ' (AM)',
        ' X': ' (PM)',
        'XX': ''
    }
    
    @staticmethod 
    def get(date: date) -> Optional['Rota']:
        weekstart = date - timedelta(date.weekday())
        if weekstart in Rota.rotas:
            return Rota.rotas[weekstart]
    
    def __init__(self, weekstart: date):
        self.weekstart = weekstart
        self.loaded = False
        self.shifts: Dict[date, Dict[int, RotaSlot]] = {}
        self.current = 0
        self.published = 0
        self.labels = {}

    def load(self):
        """Load rota data from database"""
        sql = 'select ri_no, ri_date, ri_emp_no, ri_status, ri_label, ri_timestamp, ri_published from tblrotaitem where ri_weekstart = ? and ri_active = 1 order by ri_no'
        recs = db.safeselect(sql, self.weekstart)
        if recs:
            for rec in recs:
                if not rec.ri_date in self.shifts:
                    self.shifts[rec.ri_date] = {}
                if not rec.ri_emp_no in self.shifts[rec.ri_date]:
                    self.shifts[rec.ri_date][rec.ri_emp_no] = RotaSlot(rec.ri_date, rec.ri_emp_no)
                    
                self.shifts[rec.ri_date][rec.ri_emp_no].add_item(rec.ri_no, rec.ri_status, rec.ri_timestamp, rec.ri_published, rec.ri_label)            
                if rec.ri_label:
                    self.labels[rec.ri_label] = rec.ri_no
        self.loaded = True

    def publish(self):
        sql = 'update tblrotaitem_orig set ri_published=1 where ri_no=?'
        for date in self.shifts:
            for rotaslot in self.shifts[date].values():
                rotaslot.publish()
                db.safeexec(sql, rotaslot.current_no)
                
    def revert(self):
        sql = 'update tblrotaitem_orig set ri_active=0 where ri_no > ? and ri_date = ? and ri_emp_no = ?'
        for date in self.shifts:
            for rotaslot in self.shifts[date].values():
                rotaslot.revert()
                db.safeexec(sql, rotaslot.published_no, date, rotaslot.emp_no)
                
    def get_employees_for_date(self, date: date) -> List[Dict[str, str]]:
        emps = []
        if not date in self.shifts:
             return emps
         
        for rotaslot in self.shifts[date].values():
            status = rotaslot.get_status()
            if status in ('X ', ' X', 'XX'):
                emps.append({
                    'employee': f'{rotaslot.employee.nickname}{Rota.status_desc[status]}',
                    'class': rotaslot.employee.rank_class,
                    'emp_no': rotaslot.employee.no}
                )
        
        return emps

    def day_workers(self, the_date: date) -> Dict[int, RotaSlot]:
        if not self.loaded:
            self.load()
        if not the_date in self.shifts:
            return {}
        
        return self.shifts[the_date]
    
    def all_shifts(self, published: bool) -> List[Tuple[Employee, date, str]]:
        all_shifts = []
        for (date, date_shifts) in self.shifts.items():
            for rotaslot in date_shifts.values():
                all_shifts.append((rotaslot.employee, date, rotaslot.get_status('published' if published else 'current')))
                
        return all_shifts
    
    def emp_summary(self):
        emps = {}
        for date in self.shifts.values():
            for (emp_no, rotaslot) in date.items():
                if emp_no not in emps:
                    emps[emp_no] = {
                        'employee': rotaslot.employee,
                        'wages': 0.0,
                        
                    }
        
        
    def total_wage(self) -> Decimal:
        if not self.loaded:
            self.load()
            
        total_wage = Decimal(0.0)
        for day in self.shifts:
            for rotaslot in self.shifts[day].values():
                total_wage += rotaslot.wage(day)
                
        return total_wage
