from utils import db
from .employee import Employee
from datetime import date, datetime, time, timedelta
from collections import defaultdict
from typing import Optional, Dict

class Punch():
    _sql = "select * from tblpunch where punch_no = ?"
    punches = {}
    
    def __init__(self, row):
        self.no: int = row.punch_no
        self.emp_no: int = row.punch_emp_no
        self.date: date = row.punch_date
        self.enteredtime: str = row.punch_time
        self.actualtime: str = row.punch_actualtime
        self.comment: str  = row.punch_comment
        self.image: str  = row.punch_image
        self.notes: str = row.punch_notes
        self.employee: Employee
        self.employee = Employee.fetch(self.emp_no)
        
        Punch.punches[self.no] = self
    
    @property
    def time(self) -> time:
        time_str = self.actualtime if self.enteredtime == '00:00:00.0000000' else self.enteredtime
        return datetime.strptime(time_str, '%H:%M:%S.%f').time()
    
    @property
    def inout(self) -> str:
        if self.time < time(10, 0, 0):
            return 'in'
        
        if self.time > time(16, 0, 0):
            return 'out'
        
        return 'unknown'

    @property
    def slot(self) -> str:
        if self.time < time(13, 0, 0):
            return 'am'
        
        return 'pm'
    
    @property
    def type(self):
        inout = self.inout
        if inout == 'unknown':
            return 'unknown'
        
        return f'{inout}_{self.slot}'
    
    @classmethod
    def get_by_dates(cls, start_date: date, end_date: date):
        sql = "select * from tblpunch where punch_date between ? and ?"
        rows = db.safeselect(sql, start_date, end_date)
        
        if not rows:
            return []
        
        all_punches = []
        for row in rows:
            punch = cls(row)
            all_punches.append(punch)
            
        return all_punches
    
class PunchDay():
    DEFAULT_TIMES = {
        'in_am': time(8, 0, 0),
        'in_pm': time(13, 15, 0),
        'out_am': time(12, 30, 0),
        'out_pm': time(17, 30, 0)
    }
    
    def __init__(self, employee: Employee, date: date, am: bool, pm: bool):
        self._sorted = False
        self.date = date
        self.punches = []
        self.employee = employee
        self.in1: Optional[time] = None
        self.in2: Optional[time] = None
        self.out1: Optional[time] = None
        self.out2: Optional[time] = None
        self.am = am
        self.pm = pm
        
    def add_punch(self, punch: Punch):
        self.punches.append(punch)
        
    def sort(self):
        punches_by_type = defaultdict(list)
        punch_by_type: Dict[str, Punch] = defaultdict(None)
        
        for punch in self.punches:
            if punch.type == 'unknown':
                continue
            punches_by_type[punch.type].append(punch)
            
        for punch_type, punch_list in punches_by_type.items():
            sorted_punch_list = sorted(punch_list, key=lambda p: p.time)
            
            punch_by_type[punch_type] = sorted_punch_list[0] if punch_type in ('in_am', 'in_pm') else sorted_punch_list[-1]

        if self.am:        
            self.in1 = punch_by_type['in_am'].time if 'in_am' in punch_by_type else PunchDay.DEFAULT_TIMES['in_am']
            self.out1 = punch_by_type['out_am'].time if 'out_am' in punch_by_type else PunchDay.DEFAULT_TIMES['out_am']
        else:
            assert 'in_am' not in punch_by_type
            assert 'out_am' not in punch_by_type
            
        if self.pm:
            self.in2 = punch_by_type['in_pm'].time if 'in_pm' in punch_by_type else PunchDay.DEFAULT_TIMES['in_pm']
            self.out2 = punch_by_type['out_pm'].time if 'out_pm' in punch_by_type else PunchDay.DEFAULT_TIMES['out_pm']
        else:
            assert 'in_pm' not in punch_by_type
            assert 'out_pm' not in punch_by_type

        self._sorted = True

        
    @property
    def hours(self) -> float:
        if not self._sorted:
            self.sort()
            
        hours = 0.0
        if self.am:
            assert self.in1
            assert self.out1
            
            today = date.today()
            
            in_datetime = datetime.combine(today, self.in1)
            out_datetime = datetime.combine(today, self.out1)
            hours += (out_datetime - in_datetime).total_seconds() / 3600.0

        if self.pm:
            assert self.in2
            assert self.out2
            
            today = date.today()
            
            in_datetime = datetime.combine(today, self.in2)
            out_datetime = datetime.combine(today, self.out2)
            hours += (out_datetime - in_datetime).total_seconds() / 3600.0
            
        return hours
    
