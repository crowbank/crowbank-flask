from .slot import Slot
from datetime import timedelta

class Period():
    def __init__(self, start_date, start_slot, end_date, end_slot):
        self.start = Slot(start_date, start_slot)
        self.end = Slot(end_date, end_slot)
        
    def dates(self):
        a_date = self.start.date
        while a_date <= self.end.date:
            yield a_date
            a_date += timedelta(days=1)
            
    def slots(self):
        slot = self.start
        while slot <= self.end:
            yield slot
            slot = slot.next()
            
    def date_count(self):
        return (self.end.date - self.start.date).days + (1 if self.end.slot == 'pm' else 0)
        
        