# from intranet import app
# from datetime import date
# from flask import render_template
# from . import *

# def test_allocations(env):        
#     init_models(env)
#     bk_no = 38212
#     env.safeexec('delete from tbloccupancyblock where ob_bk_no = ?', bk_no)
#     step = 0
#     steps = []
#     html = ''
#     booking = Booking.get(bk_no)
#     blocks = booking.get_blocks()
#     blocks.debug = True
    
#     def test_move(block, run, start_slot=None, pet=None):
#         nonlocal html
#         nonlocal step
#         nonlocal steps
#         nonlocal blocks
        
#         step_desc = f"Move {str(block)} to run {run.code}"
#         if start_slot:
#             step_desc += f" starting {str(start_slot)}"
#         if pet:
#             step_desc += f" moving only {pet.name}"
#         step += 1
        
#         blocks.move(block, run, start_slot, pet)
#         steps.append(
#             {
#                 'step': step,
#                 'step_desc': step_desc,
#                 'blocks': [{
#                     'no': b.no,
#                     'id': hex(id(b)),
#                     'run': b.run.code,
#                     'start_slot': str(b.start_slot) if b.start_slot else 'None',
#                     'end_slot': str(b.end_slot) if b.end_slot else 'None',
#                     'pets': '<br>'.join([p.name for p in b.pets]) if b.pets else 'None',
#                     'deleted': b.deleted,
#                     'dirty': b.dirty
#                 } for b in  blocks.blocks]
#             }
#         )        

#     #  move(block, run, start_slot=None, pet=None)
#     run = Run.get_by_code('DLX-F')    
    
#     block_to_move = blocks.blocks[-1]
#     test_move(block_to_move, run)
    
#     run = Run.get_by_code('DLX-R')
#     pet = list(filter(lambda pet: pet.name == 'Pickle', booking.pets))[0]
#     block_to_move = blocks.blocks[-1]
#     test_move(block_to_move, run, None, pet)

#     pet = list(filter(lambda pet: pet.name == 'Bert', booking.pets))[0]
#     block_to_move = list(filter(lambda b: b.run.code == 'DLX-F', blocks.active_blocks()))[0]
#     test_move(block_to_move, run, None, pet)
    
#     block_to_move = list(filter(lambda b: b.run.code == 'DLX-F', blocks.active_blocks()))[0]
#     run = Run.get_by_code('SF6')
#     start_slot = Slot(date(2022, 4, 27), 'am')
#     test_move(block_to_move, run, start_slot)
    
#     block_to_move = list(filter(lambda b: b.run.code == 'SF6', blocks.active_blocks()))[0]
#     run = Run.get_by_code('DLX-R')
#     pet = list(filter(lambda pet: pet.name == 'Blue', booking.pets))[0]
#     start_slot = Slot(date(2022, 4, 29), 'pm')
#     test_move(block_to_move, run, start_slot, pet)

#     return render_template('test.html', steps=steps)