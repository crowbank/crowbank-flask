from .capacity import CapacityManager
from .cached import Cached
from .breed import Breed
from .customer import Customer, Pet
from .vet import Vet


from .employee import Employee, EmployeeStarter
from .slot import Slot
from .run import Run
from .block import Block
# from .blocklist import BlockList
from .booking import Booking
# from .test import test_allocations
from .availability import OccupancyManager
from .period import Period
from .rota import Rota
from .note import Note, NoteType, NoteSnap, NoteSeverity
from .daycare_candidate import DaycareCandidate
from .inventory import Inventory

occupancy_manager = OccupancyManager()
capacity_manager = CapacityManager()

def init_models():
    Breed.reset()
    Pet.reset()
    Run.reset()
#    Block.reset()
    Booking.reset()
    Employee.reset()
    occupancy_manager.load()
    capacity_manager.load()
    
