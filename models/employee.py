from utils import db, parse_date
from datetime import date, datetime
from .cached import CachedPreloaded
from typing import Optional, Dict
from pyodbc import Row
from decimal import Decimal

def calculate_minimum_wage(dob: date, wage_table: list) -> Decimal:
    # Calculate the person's age
    today = datetime.today()
    age = today.year - dob.year - ((today.month, today.day) < (dob.month, dob.day))

    # Sort the wage table by age in descending order
    wage_table.sort(key=lambda x: x[0] if x[0] is not None else -1, reverse=True)

    # Find the appropriate wage for the person's age
    for age_limit, wage in wage_table:
        if age_limit is None or age >= age_limit:
            return wage

    # If no wage is found, return None
    return Decimal(0)

def read_min_wage() -> list:
    wage_table = []
    sql = """select mw_age, mw_rate from tblminwage
where mw_date = (select max(mw_date) from tblminwage)"""

    rows = db.safeselect(sql)
    assert(rows)
    
    for row in rows:
        wage_table.append((row.mw_age, Decimal(row.mw_rate)))
        
    return wage_table

def dob2wage(dob: date) -> Decimal:
    wage_table = read_min_wage()
    wage = calculate_minimum_wage(dob, wage_table)
    return wage 

def secure_nickname(starter: 'EmployeeStarter') -> None:
    nickname = starter.nickname
    existing_employee = Employee.get_by_nickname(nickname)
    if not existing_employee:
        return
    
    if not existing_employee.iscurrent:
        sql = 'exec pchange_nickname ?'
        db.safeexec(sql, existing_employee.no)
    pass

class EmployeeStarter():
    def __init__(self, sf_no):
        self.properties: Dict[str, str] = {}
        sql = 'select * from vwemployee_starter where sf_no = ?'
        row = db.safeselect_one(sql, sf_no)
        
        assert(row)
        self.no = row.sf_no
        self.forename: str = row.sf_forename
        self.surname: str = row.sf_surname
        self.name = f'{self.forename} {self.surname}'
        self.date_created: date = row.sf_date_created
        
        sql = 'select esd_field_label, esd_field_value from vwemployee_starter_detail where esd_no = ?'
        rows = db.safeselect(sql, sf_no)
        if rows:
            for row in rows:
                self.properties[row.esd_field_label] = row.esd_field_value
                
        self.dob: Optional[date] = parse_date(self.properties['Date of Birth']) if 'Date of Birth' in self.properties else None
        self.min_wage: Decimal = dob2wage(self.dob) if self.dob else Decimal(0)
        
        self.nickname = self.forename
        self.email: str = self.properties['Email'] if 'Email' in self.properties else ''
        self.telno_mobile: str = self.properties['Mobile Phone'] if 'Mobile Phone' in self.properties else ''
        self.addr1: str = self.properties['Street'] if 'Street' in self.properties else ''
        self.addr3: str = self.properties['Town'] if 'Town' in self.properties else ''
        self.postcode: str = self.properties['Postcode'] if 'Postcode' in self.properties else ''
        self.start_date: date = date.today()
        self.ni: str = self.properties['National Insurance Number'] if 'National Insurance Number' in self.properties else ''
        self.telno_home: str = self.properties['Phone Number'] if 'Phone Number' in self.properties else ''
        self.emergency_contact: str = self.properties['Emergency Contact'] if 'Emergency Contact' in self.properties else ''
        self.telno_emergency: str = self.properties['Emergency Phone'] if 'Emergency Phone' in self.properties else ''
        self.sex: str = self.properties['Sex'] if 'Sex' in self.properties else 'F'
        self.sortcode: str = self.properties['Sort Code'] if 'Sort Code' in self.properties else ''
        self.account_no: str = self.properties['Account #'] if 'Account #' in self.properties else ''
        
        sql = 'select count(*) from tblemployee where emp_nickname = ?'
        nickname_used = db.safeselect_value(sql, self.nickname)
        if not nickname_used:
            return
        
        sql = 'select count(*) from tblemployee where emp_nickname = ? and emp_iscurrent = 1'
        is_current = db.safeselect_value(sql, self.nickname)
        
        if is_current:
            self.emp_nickname = self.forename + self.surname[0]
        else:
            sql = 'update tblemployee set emp_nickname = emp_forename + substring(emp_surname, 1) where emp_nickname = ?'
            db.safeexec(sql, self.nickname)
                 
        
    @classmethod
    def get_active(cls):
        sql = 'select sf_no from vwemployee_starter where sf_active = 1'
        rows = db.safeselect(sql)
        return [cls(row.sf_no) for row in rows] if rows else []
                
class Employee(CachedPreloaded):
    employees_by_nickname = {}
    _sql = """select emp_no, emp_nickname, emp_forename, emp_surname, emp_start_date, emp_end_date,
emp_iscurrent, emp_current_rate, emp_telno_mobile, emp_telno_home, emp_telno_emergency,
emp_email, emp_addr1, emp_addr3, emp_postcode, emp_sex, emp_emergency_contact,
emp_ishourly, isnull(er_class, '') er_class, emp_rank_no, emp_dob, emp_order, emp_facebook, emp_ni,
emp_wagecat_no, emp_contract_start_date, emp_contract_end_date, emp_code_no, emp_id, emp_short_code_no,
emp_ref, emp_sortcode, emp_account_no, emp_role
from tblemployee
left join tblemployeerank on er_no = emp_rank_no"""
    _cache = {}
    _loaded = False
        
    @classmethod
    def reload_employee(cls, emp_no: int):
        sql = f'{cls._sql} where emp_no = ?'
        row = db.safeselect_one(sql, emp_no)
        emp = cls(row)
        cls._cache[emp_no] = emp
    
    @classmethod
    def get_by_nickname(cls, nickname: str) -> Optional['Employee']:
        if not cls._loaded:
            cls._load_from_database()
            
        if nickname in cls.employees_by_nickname:
            return cls.employees_by_nickname[nickname]
        
        return None
    
    @classmethod
    def reload(cls):
        cls._loaded = False
        
    def populate_blank(self):
        self.no = 0
        
        self.nickname = ''
        self.surname = ''
        self.forename = ''
        self.rank_no = 0
        self.email = ''
        self.dob = date(1900, 1, 1)
        self.telno_mobile = ''
        self.addr1 = ''
        self.addr3 = ''
        self.postcode = ''
        self.start_date = date(2014, 1, 1)
        self.end_date = None
        self.current_rate = Decimal(0.0)
        self.iscurrent = False
        self.order = 0
        self.facebook = ''
        self.ni = ''
        self.wagecat_no = 0
        self.contract_start_date = None
        self.contract_end_date = None
        self.code_no = 0
        self.id = 0
        self.short_code_no = 0
        self.telno_home = ''
        self.telno_emergency = ''
        self.sex = ''
        self.ref = ''
        self.emergency_contact = ''
        self.sortcode = ''
        self.account_no = ''
        self.role = 0
        self.order = 999
        self.facebook = ''
        self.wagecat_no = 0

    def __init__(self, row: Optional[Row]): 
        if not row:
            self.populate_blank()
            return
        
        self.no = row.emp_no
        self.nickname = row.emp_nickname
        self.forename = row.emp_forename
        self.surname = row.emp_surname
        self.start_date = row.emp_start_date
        self.end_date = row.emp_end_date
        self.iscurrent = (row.emp_iscurrent == 1)
        self.current_rate: Decimal = row.emp_current_rate
        self.hourly = row.emp_ishourly
        self.telno_mobile = row.emp_telno_mobile
        self.telno_home = row.emp_telno_home
        self.telno_emergency = row.emp_telno_emergency
        self.email = row.emp_email
        self.addr1 = row.emp_addr1
        self.addr3 = row.emp_addr3
        self.postcode = row.emp_postcode
        self.rank_class = row.er_class
        self.rank_no = row.emp_rank_no
        self.dob = row.emp_dob
        self.order = row.emp_order
        self.facebook = row.emp_facebook
        self.ni = row.emp_ni
        self.wagecat_no = row.emp_wagecat_no
        self.contract_start_date = row.emp_contract_start_date
        self.contract_end_date = row.emp_contract_end_date
        self.code_no = row.emp_code_no
        self.id = row.emp_id
        self.short_code_no = row.emp_short_code_no
        self.sex = row.emp_sex
        self.ref = row.emp_ref
        self.emergency_contact = row.emp_emergency_contact
        self.sortcode = row.emp_sortcode
        self.account_no = row.emp_account_no
        self.role = row.emp_role
        
        Employee.employees_by_nickname[self.nickname] = self
        
    def full_name(self):
        return f'{self.forename} {self.surname}'
    
    def address(self):
        return f"""{self.addr1}
{self.addr3}
{self.postcode}"""

    def __str__(self):
        return self.nickname
    
    def __repr__(self):
        return f'{self.full_name()} (emp #{self.no})'
    
    