from pyodbc import Row
from decimal import Decimal
from utils import db
from abc import ABC, abstractmethod
from datetime import date, timedelta
from typing import Optional, List
from utils.stripe_utils import get_payouts_intransit

class Payment():
    def __init__(self, date: date, amount: Decimal, description: str) -> None:
        self.date = date
        self.amount = amount
        self.description = description

def get_monthend(date: date) -> date:
    if date.month == 12:
        next_month = date.replace(year=date.year+1, month=1, day=1)
    else:
        next_month = date.replace(month=date.month+1, day=1)

# Subtract one day from the first day of next month
    return next_month - timedelta(days=1)

def add_month(the_date: date) -> date:
    return the_date.replace(month=the_date.month+1) if the_date.month < 12 else the_date.replace(year=the_date.year+1, month=1)
    
class Cashflow(ABC):  
    def payments(self, start_date: Optional[date] = None, end_date: Optional[date] = None):
        if not start_date:
            start_date = date.today()
            
        if not end_date:
            end_date = get_monthend(start_date)

        return self._payments(start_date, end_date)
    
    @abstractmethod
    def _payments(self, start_date: date, end_date: date) -> list:
        return []

class CashflowComposite(Cashflow):
    def __init__(self):
        self.children = []
        
    def add_flow(self, cashflow: Cashflow) -> None:
        self.children.append(cashflow)
        
    def _payments(self, start_date: date, end_date: date) -> list:
        payments = []
        for child in self.children:
            payments.extend(child.payments(start_date, end_date))
            
        return payments

class CashflowMonthly(Cashflow):
    def __init__(self) -> None:
        super().__init__()
        self.frequency = 'Monthly'
        self.day_of_month = -1
        
    def _dates(self, start_date: date, end_date: date) -> List[date]:
        dates = []
        
        the_date = get_monthend(start_date) if self.day_of_month == -1 else start_date.replace(day=self.day_of_month)
        
        while the_date >= start_date and the_date <= end_date:
            dates.append(the_date)
            the_date = add_month(the_date)
        
        return dates
        
class CashflowWages(CashflowMonthly):
    ASSUMED_FRACTION = Decimal(0.40)
    
    def __init__(self):
        self.description = 'Wages'
    
    def payment(self, the_date: date) -> Optional[Decimal]:
        if the_date < date.today():
            sql = 'select sum(ew_gross) from tblemployeewage where ew_date = ?'
            row = db.safeselect_one(sql, the_date)
            if row:
                return row[0]
            return None
        
        sql = 'select min(date) first_date, max(date) last_date from tbldate where d_wagepayment = ?'
        row = db.safeselect_one(sql, the_date)
        if not row:
            return None
        
        first_date, last_date = row
        sql = 'select sum(rv_revenue) revenue from vwrevenue where rv_date between ? and ?'
        revenue = db.safeselect_value(sql, first_date, last_date)
        if not revenue:
            revenue = Decimal(0.0)
        
        sql = 'select sum(ts_wages) from vwtimesheet_published where ts_date between ? and ?'
        published_wages = db.safeselect_value(sql, first_date, last_date)
        if not published_wages:
            published_wages = Decimal(0.0)
        
        wages = max(published_wages, CashflowWages.ASSUMED_FRACTION * revenue)
        
        return wages
        
    
    def _payments(self, start_date: date, end_date: date) -> List[Payment]:
        payments = []
        for d in self._dates(start_date, end_date):
            amount = self.payment(d)
            if amount:
                payments.append(Payment(d, amount, f"Wages for {d.strftime('%B %Y')}"))
        return payments

class CashflowStripe(Cashflow):
    def __init__(self) -> None:
        super().__init__()
        
    def _payments(self, start_date: date, end_date: date) -> List[Payment]:
        payouts = get_payouts_intransit()
        
        return [Payment(p['arrival_date'], p['amount'], f"Stripe payout {p['id']}") for p in payouts]
        
        
class CashflowRevenue(Cashflow):
    def __init__(self) -> None:
        super().__init__()
        self.description = 'Revenue'
    
    def _payments(self, start_date: date, end_date: date) -> List[Payment]:
        payments = []
        sql = 'select rv_date, rv_revenue from vwrevenue where rv_date between ? and ?'
        return payments
