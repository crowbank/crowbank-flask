from datetime import date
from typing import Tuple, Optional, Dict, Any

from .slot import Slot
from .period import Period
from .runtype import RunType
from .capacity import CapacityManager
from .occupancy import OccupancyManager

capacity_manager = CapacityManager()
occupancy_manager = OccupancyManager()

def get_summary(date: date) -> Tuple[int, int]:
    """Return a summary of availability for a particular date"""
    any_rt = RunType.get_by_code('any')
    unallocated_rt = RunType.get_by_code('unallocated')
    daycare_rt = RunType.get_by_code('daycare')
    
    assert(any_rt)
    assert(unallocated_rt)
    assert(daycare_rt)
    
    am_slot = Slot(date, 'am')
    pm_slot = Slot(date, 'pm')
    
    am_occupancy = occupancy_manager(am_slot, any_rt) + occupancy_manager(am_slot, unallocated_rt)
    pm_occupancy = occupancy_manager(pm_slot, any_rt) + occupancy_manager(pm_slot, unallocated_rt)
    
    max_occupancy = max(am_occupancy, pm_occupancy)
    
    level = 1
    if max_occupancy >= daycare_rt.amber_level:
        level = 2
    if max_occupancy >= daycare_rt.red_level:
        level = 3
        
    return max_occupancy, level
    

def get_availability_stats(slot: Slot, rt: RunType):
    rt_occupied = occupancy_manager(slot, rt)
    rt_closed = occupancy_manager.get_closed(slot.date, rt)
    run_capacity = rt.capacity - rt_closed
    
    is_cat = (rt.desc == 'cat')
    
    if rt_occupied >= run_capacity:
        return rt_occupied, rt.capacity, rt_closed, 2
    
    if is_cat:
        any_rt = rt
        any_occupied = rt_occupied
        any_closed = 0
    else:
        any_rt = RunType.get_by_code('any')
        assert(any_rt)
        any_occupied = occupancy_manager(slot, any_rt)
        any_closed = occupancy_manager.get_closed(slot.date, any_rt)
    
    run_limit = capacity_manager.get(slot.date, rt)
    any_limit = capacity_manager.get(slot.date, any_rt)
    
    if rt_occupied - rt_closed >= run_limit or any_occupied - any_closed > any_limit:
        availability_level = 1
    else:
        availability_level = 0
        
    return rt_occupied, rt.capacity, rt_closed, availability_level

   
def get_availablity_data(period: Optional[Period] = None, rts = None, trans = False) -> Dict[str, Any]:
    """Return a vector of availability-related data for each slot within a period, and for each of one or more rts"""
    availability_vector = []
    
    rts_to_use = rts if rts else RunType.get_all()
    lrts = list(rts_to_use)
    
    if not period:
        period = Period(occupancy_manager.first_date, 'am', occupancy_manager.last_date, 'pm')
        
    for rt in rts_to_use:
        rt_data = []
        for slot in period.slots():
            occupied, capacity, closed, availability_level = get_availability_stats(slot, rt)
            rt_data.append({
                'capacity': capacity,
                'closed': closed,
                'occupied': occupied,
                'availability': availability_level
            })
        availability_vector.append(rt_data)
    return {'availability': availability_vector,
            'slots': list(period.slots()),
            'rts': lrts}

def get_unallocated(period: Period, spec_no: int) -> Dict[str, Any]:
    unallocated = []
    for slot in period.slots():
        unallocated.append(occupancy_manager.get_unalloc(slot, spec_no))
    return {
        'unallocated': unallocated,
        'slots': period.slots()
    }

def get_availability_table(period: Period, rts, spec_no: int) -> Dict[str, Any]:
    table_data = []
    
    for slot in period.slots():
        total_capacity = 0
        total_occupied = 0
        total_closed = 0
        for rt in rts:
            occupied, capacity, closed, _ = get_availability_stats(slot, rt)
            total_occupied += occupied
            total_capacity += capacity
            total_closed += closed
        table_data.append(
            {
                'occupied': total_occupied,
                'capacity': total_capacity,
                'closed': total_closed,
                'ua': occupancy_manager.get_unalloc(slot, spec_no)
            }
        )
    return {'availability': table_data, 'slots': period.slots(), 'rts': rts }

def get_availability(period: Period, rt: RunType) -> int:
    """Calculate overall availability (lowest availability of any slot) for a period and particular rt"""
    availability = 0
    for slot in period.slots():
        _, _, _, availability_level = get_availability_stats(slot, rt)
        if availability_level > availability:
            availability = availability_level
            
    return availability

def get_spec_availability(slot: Slot, spec_no: int) -> int:
    rt = RunType.get_by_spec(spec_no)
    assert(rt)
    _, _, _, availability = get_availability_stats(slot, rt)
    return availability
