from datetime import date
from pyodbc import Row
from typing import Dict, Tuple

from utils import db

class Inventory():
    _sql = """select inv_date, inv_spec, inv_morning morning, inv_am am, inv_noon noon, inv_pm pm, inv_evening evening,
inv_am_ins am_ins, inv_am_outs am_outs, inv_pm_ins pm_ins, inv_pm_outs pm_outs, inv_am_day am_day,
inv_pm_day pm_day
from vwinventory_quick
where inv_date = ? and inv_spec = ?"""

    _cache: Dict[Tuple[date, str], 'Inventory'] = {}
    
    def __init__(self, row: Row):
        self.date = row.inv_date
        self.spec = row.inv_spec
        self.morning = row.morning
        self.am = row.am
        self.noon = row.noon
        self.pm = row.pm
        self.evening = row.evening
        self.am_ins = row.am_ins
        self.am_outs = row.am_outs
        self.pm_ins = row.pm_ins
        self.pm_outs = row.pm_outs
        self.am_day = row.am_day
        self.pm_day = row.pm_day
    
    @classmethod
    def ins(cls, d: date) -> int:
        return cls.get(d, 'Dog').am_ins + cls.get(d, 'Dog').pm_ins + cls.get(d, 'Cat').am_ins + cls.get(d, 'Cat').pm_ins
    
    @classmethod
    def outs(cls, d: date) -> int:
        return cls.get(d, 'Dog').am_outs + cls.get(d, 'Dog').pm_outs + cls.get(d, 'Cat').am_outs + cls.get(d, 'Cat').pm_outs
    
    @classmethod
    def get(cls, the_date: date, spec: str) -> 'Inventory':
        if (the_date, spec) in cls._cache:
            return cls._cache[the_date, spec]
        
        row = db.safeselect_one(cls._sql, the_date, spec)
        assert(row)
        obj = Inventory(row)
        
        cls._cache[the_date, spec] = obj
        return obj
    
    @classmethod
    def refresh(cls, the_date: date, spec: str):
        if the_date in cls._cache:
            del cls._cache[the_date]
        
        cls.get(the_date, spec)
        
    @classmethod
    def refresh_all(cls):
        cls._cache = {}
        
        
    