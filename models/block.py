from utils import db
from .run import Run
from .customer import Pet
from .slot import Slot

class Block:
    blocks = {}
     
    @staticmethod
    def reset():
        Block.blocks = {}
        
    @staticmethod
    def get(ob_no):
        ob_no = int(ob_no)
        
        if ob_no not in Block.blocks:
            sql = """select ob_no, ob_bk_no, ob_run_no, ob_start_date, ob_end_date,
ob_start_slot, ob_end_slot, ob_pets, ob_create_stamp, ob_end_stamp
from vwoccupancyblock where ob_no = ?"""
            row = db.safeselect_one(sql, ob_no)
            Block.blocks[ob_no] = Block.new_from_row(row)
            
        return Block.blocks[ob_no]
    
    def __init__(self, booking):
        self.bk_no = booking.no
        self.booking = booking
        self.deleted = False
        self.dirty = True
        self.no = None
        self.run = Run.unallocated()
        self.pets = self.booking.pets
        self.start_slot = self.booking.start_slot
        self.end_slot = None
        self.start_stamp = self.booking.create_date
        self.end_stamp = None
        
    def __lt__(self, other):
        if self.booking.no < other.booking.no:
            return True
        if self.booking.no > other.booking.no:
            return False
        if self.start_slot < other.start_slot:
            return True
        if self.start_slot > other.start_slot:
            return False
        if self.run.code < other.run.code:
            return True
        return False
        
            
    @staticmethod
    def new_from_row(row):
        block = Block(row.ob_bk_no)
        block.no = row.ob_no
        block.start_slot = Slot(row.ob_start_date, row.ob_start_slot) if row.ob_start_date else block.booking.start_slot
        block.end_slot = Slot(row.ob_end_date, row.ob_end_slot) if row.ob_end_date else block.booking.end_slot
        block.create_stamp = row.ob_create_stamp
        block.end_stamp = row.ob_end_stamp    
        block.run = Run.get(row.ob_run_no if row.ob_run_no else -1)
        if row.ob_pets:
            pet_nos = sorted(list(map(int, row.ob_pets.split(','))))
        else:
            pet_nos = sorted([pet.no for pet in block.booking.pets])
        block.pets = [Pet.get(pet_no) for pet_no in pet_nos]
        block.dirty = False
        
        return block

    def pet_signature(self):
        names = list(map(lambda pet: pet.name, self.pets))
        sorted_names = sorted(names)
        return ','.join(sorted_names)
    
    def signature(self):
        return f"{self.bk_no}:{self.pet_signature()}:{self.run.code}:{self.start_slot}:{self.end_slot}"
    
    def __str__(self):
        str_self = self.signature()
        if self.no:
            str_self = f"({self.no}) {str_self}"
        if self.deleted:
            str_self += ':D'
        if self.dirty:
            str_self += ':X'
        str_self += f" [{hex(id(self))}]"
        return str_self
    
    def add_pet(self, pet):
        self.pets.append(pet)
    
    def add_pets(self, pets):
        for pet in pets:
            self.add_pet(pet)
            
    def remove_pet(self, pet):
        self.pets.remove(pet)
        
    def set_pet(self, pet):
        self.pets = set([pet])
        
    def write_to_db(self, stamp):
        if self.deleted or self.dirty or not self.blocks:
            if self.no:
                sql = 'update tbloccupancyblock set ob_end_stamp = ? where ob_no = ?'
                db.safeexec(sql, stamp, self.no)
            if self.deleted:
                return
        
        if not self.dirty:
            return

        self.start_stamp = stamp
        
        sql = """set nocount on;
declare @id int;
insert into tbloccupancyblock (ob_bk_no, ob_run_no, ob_status, ob_pets, ob_start_date,
ob_end_date, ob_start_slot, ob_end_slot, ob_create_stamp)
values (?, ?, ?, ?, ?, ?, ?, ?, ?);
select @id = @@identity;
select @id id"""
        self.no = db.safeselect_value(sql, self.bk_no, self.run.no if self.run else -1,
                            'U' if not self.run or self.run.no == -1 else 'A',
                            ','.join([str(pet.no) for pet in self.pets]),
                            self.start_slot.date, self.end_slot.date if self.end_slot else None,
                            self.start_slot.slot, self.end_slot.slot if self.end_slot else None,
                            self.start_stamp, commit=True)
        
        
    def as_dict(self):
        d = {
            'ob_no': self.no,
            'ob_bk_no': self.bk_no,
            'ob_start_date': self.start_slot.date,
            'ob_start_slot': self.start_slot.slot,
            'ob_end_date': self.end_slot.date,
            'ob_end_slot': self.end_slot.slot,
            'ob_pet_nos': self.pet_signature(),
            'ob_run': self.run.code,
            'ob_pets': {
                pet.no: {
                    'pet_no': pet.no,
                    'pet_name': pet.name,
                    'pet_breed': pet.breed.desc
                } for pet in self.pets
            }
        }
        return d
