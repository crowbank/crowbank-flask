import stripe
from typing import Optional
from utils import db, stripe_utils

class StripePayment():
    def __init__(self, pi: stripe.PaymentIntent, fee: Optional[float]):
        self.pi = pi
        self.id = pi.stripe_id
        self.fee = fee
        
    def save(self):
        sql = """insert into tblstripepayment
(sp_stripe_id, sp_description, sp_amount, sp_amount_refunded, sp_currency, sp_status,
sp_receipt_email, sp_created_at, sp_updated_at, sp_customer_id, sp_customer_email,
sp_latest_charge, sp_fee)
values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"""

        db.execute(sql, self.pi.id, self.pi.description, self.pi.amount / 100.0,
                   )
        
        
    def collect_fee(self):
        self.fee = stripe_utils.get_fees(self.id)