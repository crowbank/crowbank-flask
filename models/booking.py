from .slot import Slot
from .customer import Customer, Pet
from .blocklist import BlockList
from utils import db
from typing import List, Optional, Dict
from datetime import date, timedelta, datetime
from pyodbc import Row
from .cached import Cached, CachedPreloaded
from datetime import timedelta, date
from decimal import Decimal
from urllib.parse import quote_plus as encode


class Payment:
    def __init__(self, booking: 'Booking', amount: Decimal, type: str, date: date, ref: str):
        self.booking = booking
        self.amount = amount
        self.type = type
        self.date = date
        self.ref = ref
        
class InvoiceItem:
    def __init__(self, booking: 'Booking', pet: Optional[Pet], description: str, rate: Decimal, units: int, cost: Decimal):
        self.booking = booking
        self.pet = pet
        self.description = description
        self.rate = rate
        self.units = units
        self.cost = cost
    
    
class Invoice:
    def __init__(self, booking: 'Booking'):
        self.booking = booking
        self.items: List[InvoiceItem] = []
        self.payments: List[Payment] = []
        self.discount: Decimal = Decimal(0.0)
        self.total: Decimal = Decimal(0.0)
        self.paid: Decimal = Decimal(0.0)
        self.outstanding: Decimal = Decimal(0.0)
        
    def load(self):
        bk_no = self.booking.no
        
        sql = """select inv_bk_no, inv_pet_no, inv_description description,
inv_rate rate, inv_units units, inv_cost cost
from vwinvoice where inv_bk_no = ?"""
        invoice_rows = db.safeselect(sql, bk_no)
        if invoice_rows:
            for row in invoice_rows:
                pet = Pet.get(row.inv_pet_no) if row.inv_pet_no else None
                item = InvoiceItem(self.booking, pet, row.description, row.rate, row.units, row.cost)
                self.items.append(item)
        
        paid = Decimal(0.0)       
        payment_rows = db.safeselect("select pay_date date, pay_type type, pay_amount amount, pay_ref ref from pa..tblpayment where pay_bk_no = ?", bk_no)
        if payment_rows:
            for row in payment_rows:
                payment = Payment(self.booking, row.amount, row.type, row.date.date(), row.ref)
                self.payments.append(payment)
                paid += payment.amount
    
        row = db.safeselect_one("""select bk_disc_total, bk_gross_amt, bk_paid_amt, bk_amt_outstanding
                                from pa..tblbooking where bk_no = ?""", bk_no)
        assert(row)
        self.discount = Decimal(row.bk_disc_total) if row.bk_disc_total else Decimal(0.0)
        self.outstanding = Decimal(row.bk_amt_outstanding) if row.bk_amt_outstanding else Decimal(0.0)
        self.total = Decimal(row.bk_gross_amt) if row.bk_gross_amt else Decimal(0.0)
        self.paid = Decimal(row.bk_paid_amt) if row.bk_paid_amt else Decimal(0.0)
        
        if self.paid != self.total - self.outstanding:
            assert(False)
        assert(self.paid == paid)

class BookingHistory():
    def __init__(self, bk_no: int) -> None:
        self.bk_no = bk_no
        self.no = 0
        self.destination = self.subject = self.msg = ''
        self.date = None
        
        sql = 'select hist_no, hist_date, hist_destination, hist_subject, hist_msg from vwhistory where hist_bk_no = ?'
        row = db.safeselect_one(sql, bk_no)
        if row:
            self.no = row.hist_no
            self.destination = row.hist_destination
            self.subject = row.hist_subject
            self.msg = row.hist_msg
            self.date: Optional[date] = row.hist_date
            

class BookingStatus(CachedPreloaded):
    _cache = {}
    _loaded = False
    _sql = 'select bs_code, bs_desc, bs_allocated from tblbookingstatus'
    
    def __init__(self, row: Row):
        self.code = row.bs_code
        self.description = row.bs_desc
        self.allocated = row.bs_allocated
        
    def as_dict(self):
        return {
            'code': self.code,
            'desc': self.description,
            'allocated': self.allocated
        }

class Booking(Cached):
    from datetime import date

    _sql = """select bk_no, bk_cust_no, convert(date, bk_start_date) bk_start_date,
convert(date, bk_end_date) bk_end_date, bk_start_time, bk_end_time,
bk_start_date + bk_start_time bk_start_datetime,
bk_end_date + bk_end_time bk_end_datetime,
bk_status, convert(date, bk_create_date) bk_create_date,
bk_paid_amt, bk_gross_amt, bk_amt_outstanding, bk_status, bk_pickup_no,
bk_memo, be_expiry
from pa..tblbooking
left join tblbookingexpiry on be_bk_no = bk_no
where bk_no = ?"""
    _cache = {}
    
    def populate_pets(self):
        self.pets = []
        sql = 'select bi_pet_no pet_no from pa..tblbookingitem where bi_bk_no = ?'
        pet_rows = db.safeselect(sql, self.no)
        
        if not pet_rows:
            print(f'No pets for booking {self.no}')
            assert(pet_rows)
        
        pet_nos = [r.pet_no for r in pet_rows]
        
        for pet_no in pet_nos:
            pet = Pet.fetch(pet_no)
            self.pets.append(pet)
            pet.add_booking(self)

    def populate_flags(self):
        sql = 'select * from vwbooking_flag where bk_no = ?'
        flags_row = db.safeselect_one(sql, self.no)
        
        assert(flags_row)
        
        self.is_deluxe = bool(flags_row.is_deluxe)
        self.is_trial = bool(flags_row.is_trial)
        self.is_transport = bool(flags_row.is_transport)
        self.is_separate = bool(flags_row.is_separate)
        self.is_daycare = bool(flags_row.is_daycare)
        self.is_amending = bool(flags_row.is_amending)
        self.dc_weekdays = flags_row.dc_weekdays
        self.dc_slot = flags_row.dc_slot
        
        sql = 'select count(*) from pa..tblrunoccupancy where ro_run_no = -1 and ro_bk_no = ?'
        unallocated_days = db.safeselect_value(sql, self.no)
        self.is_unallocated = bool(unallocated_days)
        
        self.populate_history()

    def populate_history(self):
        self.history = BookingHistory(self.no)

    def populate_blank(self):
        self.no = 0
                
        self.start_date = date(2014, 1, 1)
        self.end_date = date(2014, 1, 1)
        self.start_time = '00:00'
        self.end_time = '00:00'
        self.create_date = date(2014, 1, 1)
        self.cust_no = 0
        
        self.customer = Customer.fetch(0)
        
        self.start_slot = Slot(self.start_date, 'am')
        self.end_slot = Slot(self.end_date, 'am')
        
        self.paid_amt = Decimal(0.0)
        self.gross_amt = Decimal(0.0)
        self.amt_outstanding = Decimal(0.0)
        self.status = BookingStatus.fetch('')
        self.pickup = 0
        self.notes = ''
        self.paid_amt = Decimal(0.0)
        self.expiry: Optional[date] = None

        assert(self.status)

    @classmethod
    def _read_from_database(cls, key: int) -> Optional['Booking']:
        row = cls.row_from_key(key)
        if not row:
            return None

        cust_no = row.bk_cust_no        
        obj = cls(cust_no)
        obj.load_from_row(row)
        
        return obj

    def __init__(self, cust_no: int):
        self.cust_no = cust_no
        self.customer = Customer.fetch(cust_no)
        self.pets = []
        self._check_status_loaded = False
        self._checked_in = False
        self._checked_out = False
        self.status: BookingStatus = BookingStatus.fetch('')
        
        self.invoice = Invoice(self)
        
    def load_from_row(self, row: Row):
            
        self.no: int = row.bk_no
                
        self.start_date: date = row.bk_start_date
        self.end_date: date = row.bk_end_date
        self.start_time: str = row.bk_start_time
        self.end_time: str = row.bk_end_time
        self.start_period = 'am' if self.start_time < '13:00' else 'pm'
        self.end_period = 'am' if self.end_time < '13:00' else 'pm'
        self.start_datetime: datetime = row.bk_start_datetime
        self.end_datetime: datetime = row.bk_end_datetime
        self.create_date: date = row.bk_create_date
        self.cust_no: int = row.bk_cust_no
        
        self.customer: Customer = Customer.fetch(self.cust_no)
        self.customer.add_booking(self)
        
        self.start_slot: Slot = Slot(self.start_date, 'am' if self.start_time < '13:00' else 'pm')
        self.end_slot: Slot = Slot(self.end_date, 'am' if self.end_time < '13:00' else 'pm')
        
        self.paid_amt: Decimal = Decimal(row.bk_paid_amt)
        self.gross_amt: Decimal = Decimal(row.bk_gross_amt)
        self.amt_outstanding: Decimal = Decimal(row.bk_amt_outstanding)
        self.expiry: Optional[date] = row.be_expiry
        
        status = BookingStatus.fetch(row.bk_status)
        self.status = status

        self.pickup: int = row.bk_pickup_no
        self.notes: str = row.bk_memo
        
        self.populate_pets()
        self.populate_flags()
        self.invoice = Invoice(self)
        self.invoice.load()

    def load_check_status(self):
        sql = 'select min(bi_checkin_time) min_checkin, min(bi_checkout_time) min_checkout from pa..tblbookingitem where bi_bk_no = ?'
        row = db.safeselect_one(sql, self.no)
        if row:
            if row.min_checkin:
                self._checked_in = True
            if row.min_checkout:
                self._checked_out = True
        self._check_status_loaded = True
    
    @property
    def checked_in(self) -> bool:
        if not self._check_status_loaded:
            self.load_check_status()
        return self._checked_in
    
    @property
    def checked_out(self) -> bool:
        if not self._check_status_loaded:
            self.load_check_status()
        return self._checked_out
    
    @property
    def tense(self) -> str:
        today = date.today()
        if self.end_date < today:
            return 'past'
        
        if self.start_date > today:
            return 'future'
        
        return 'present'

    @property
    def deposit(self) -> Decimal:
        deposit = Decimal(50.0) if self.has_dogs else Decimal(30.0)
        if deposit > self.gross_amt / Decimal(2.0):
            deposit = self.gross_amt / Decimal(2.0)

        return deposit
    
    @classmethod
    def get_by_start_date(cls, s_date: date) -> List['Booking']:
        bookings = []
        sql = """select bk_no from pa..tblbooking where bk_start_date = ? and bk_status in ('', 'V')"""
        rows = db.safeselect(sql, s_date)
        if rows:
            for row in rows:
                bookings.append(cls.get(row.bk_no))
            
        return bookings            
    
    def weight_date(self) -> Optional[date]:
        days = (self.end_date - self.start_date).days
        if days < 7:
            return None
        
        days_to_add = days / 2 if days < 14 else 7
        return self.start_date + timedelta(days=days_to_add)
    
    @property
    def pet_names(self) -> str:
        if len(self.pets) == 1:
            return self.pets[0].name
        return ', '.join(map(lambda p: p.name, self.pets[0:-1])) + \
            ' and ' + self.pets[-1].name
            
    @property
    def pet_names_short(self) -> str:
        if len(self.pets) == 1:
            return self.pets[0].name
        return ', '.join(map(lambda p: p.name, self.pets))
            
    @property
    def pet_descriptions(self) -> str:
        if len(self.pets) == 1:
            return self.pets[0].description
        return ', '.join(map(lambda p: p.description, self.pets[0:-1])) + \
            ' and ' + self.pets[-1].description
            
    @property
    def pet_descriptions_short(self) -> str:
        if len(self.pets) == 1:
            return self.pets[0].description
        return ', '.join(map(lambda p: p.description, self.pets))
            

    @property
    def has_dogs(self) -> bool:
        return any(pet.is_dog for pet in self.pets)
    
    @property
    def has_cats(self) -> bool:
        return any(pet.is_cat for pet in self.pets)
    
    @property
    def kennel_type(self) -> str:
        if not self.has_dogs:
            return ''
        return 'Deluxe' if self.is_deluxe else 'Standard'
    
    def as_dict(self) -> Dict:
        return {
            'no': self.no,
            'start_date': self.start_date,
            'end_date': self.end_date,
            'start_time': self.start_time,
            'end_time': self.end_time,
            'create_date': self.create_date,
            'start_slot': self.start_slot.slot,
            'end_slot': self.end_slot.slot,
            'pets': [pet.no for pet in self.pets],
            'daycare': 1 if self.is_daycare else 0,
            'status': self.status.as_dict()      
        }
        
    def get_blocks(self) -> BlockList:
        if not self._blocks:
            self._blocks = BlockList(self)
        return self._blocks

    def refund_status(self):
        """Work out refund status, which can be either 'none', 'yes', or 'no'
        'none' is returned when no payments have been made
        'yes' is returned when the first payment was made less than a week ago
        'no' is returned otherwise
        
        Use to determine whether to display the 'Refund Deposit' checkmark, and whether it should be checked by default"""

        if self.paid_amt == 0.0:
            return 'none'
        
        payments = self.invoice.payments
        assert(payments)
        
        first_payment = payments[0]
        if first_payment.type.lower() != 'stripe':
            return 'none'
        
        first_date = first_payment.date
        
        today = date.today()
        time_diff: timedelta = today - first_date
        days_ago = (today - first_date).days
        
        if days_ago < 7:
            return 'yes'
        
        return 'no'

    @property
    def datetimes(self) -> str:
        """23/06/23 (11:00) - 25/06/23 (14:00)"""
        return f'{self.start_date.strftime("%d/%m/%y")} ({self.start_time}) - {self.end_date.strftime("%d/%m/%y")} ({self.end_time})'
    
    def payment_link(self, amount: Decimal | float, purpose: Optional[str]) -> str:
        valid_to = (date.today() + timedelta(7)).strftime('%d/%m/%Y')
        start_date = self.start_date.strftime('%d/%m/%y')
        end_date = self.end_date.strftime('%d/%m/%y')
        if purpose == 'full' or amount == self.amt_outstanding:
            description = 'Payment of outstanding amount'
        elif purpose == 'deposit' or (amount == self.deposit and self.status.code == ''):
            description = 'Deposit'
        else:
            description = 'Partial payment'
        
        args = f"""bk_no={self.no}&cust_no={self.customer.no}&amount={amount}&desc={encode(description)}&forename={encode(self.customer.forename)}&surname={encode(self.customer.surname)}&email={encode(self.customer.email)}&pets={encode(self.pet_names)}&start_date={encode(start_date)}&end_date={encode(end_date)}&start_time={encode(self.start_time)}&end_time={encode(self.end_time)}&valid={encode(valid_to)}"""
        url = f'https://crowbank.co.uk/deposit?{args}'
#        url = urllib.parse.quote(url, safe=':/?&=')
        
        return url
    
    @property
    def deposit_link(self) -> str:
        return self.payment_link(self.deposit, 'Deposit')
    
    
    from datetime import date

    @property
    def dates(self) -> str:
        current_year = date.today().year

        if self.start_date == self.end_date:
            if self.start_date.year != current_year:
                return f"{self.start_date.day}/{self.start_date.month}/{self.start_date.year - 2000}"
            else:
                return f"{self.start_date.day}/{self.start_date.month}"

        elif self.start_date.year == self.end_date.year:
            if self.start_date.month == self.end_date.month:
                if self.start_date.year != current_year:
                    return f"{self.start_date.day} - {self.end_date.day}/{self.start_date.month}/{self.start_date.year - 2000}"
                else:
                    return f"{self.start_date.day} - {self.end_date.day}/{self.start_date.month}"
            else:
                if self.start_date.year != current_year:
                    return f"{self.start_date.day}/{self.start_date.month}/{self.start_date.year - 2000} - {self.end_date.day}/{self.end_date.month}/{self.end_date.year - 2000}"
                else:
                    return f"{self.start_date.day}/{self.start_date.month} - {self.end_date.day}/{self.end_date.month}"

        else:
            return f"{self.start_date.day}/{self.start_date.month}/{self.start_date.year - 2000} - {self.end_date.day}/{self.end_date.month}/{self.end_date.year - 2000}"

    def __repr__(self):
        return f'Booking({self.no})'
    
    def repopulate_pets(self):
        pet_nos = [pet.no for pet in self.pets] if self.pets else []
        self.pets = [Pet.fetch(pet_no) for pet_no in pet_nos]
        
def load_pet_bookings(pet: Pet):
    if pet._bookings_loaded:
        return
    
    sql = 'select bi_bk_no from pa..tblbookingitem where bi_pet_no = ?'
    rows = db.safeselect(sql, pet.no)
    if rows:
        for row in rows:
            booking = Booking.fetch(row.bi_bk_no)
            pet.add_booking(booking)
            
    pet._bookings_loaded = True
