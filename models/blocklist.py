from copy import copy
from datetime import datetime
from .block import Block
from utils import db

class BlockList:
    def __init__(self, booking):
        self.booking = booking
        self.debug = False
        
        self._read()
            
    def _read(self):
        self.blocks = []

        sql = 'select ob_no from tbloccupancyblock where ob_bk_no = ? and ob_end_stamp is null'
        rows = db.safeselect(sql, self.booking.no)
        
        if rows:
            for row in rows:
                self.blocks.append(Block.get(row.ob_no))
            return
        
        block = Block(self.booking.no)
        block.write_to_db(self.booking.create_date)
        self.blocks = [block]
    
    def __iter__(self):
        return iter(self.blocks)
    
    def __next__(self):
        return next(self.blocks)
    
    def _move_or_split(self, block, run, start_slot=None, pet=None):
        assert(not pet or pet in block.pets)
        block = self.duplicate(block, True)
        
        if start_slot:
            old_block = block
            block = self.duplicate(block, False)
            block.start_slot = start_slot
            old_block.end_slot = start_slot.prev()
        
        if pet:
            old_block = block
            block = self.duplicate(block, False)
            old_block.remove_pet(pet)
            block.set_pet(pet)
        
        block.run = run
            
    def duplicate(self, block, delete):
        new_block = Block(block.bk_no)
        new_block.start_slot = block.start_slot
        new_block.end_slot = block.end_slot
        new_block.run = block.run
        new_block.pets = copy(block.pets)
        
        block.deleted = delete
        
        self.blocks.append(new_block)
        
        return new_block
    
    def _stitch(self):
        # identify overlapping blocks with the same run
        # either coming one after the other, with the same pets,
        # or overlapping in time, with different pets
        # either case, amend and remove blocks as necessary
        blocks_by_run = {}
        for block in self.blocks:
            if block.deleted:
                continue
            if block.run.no not in blocks_by_run:
                blocks_by_run[block.run.no] = []
            blocks_by_run[block.run.no].append(block)
        
        for blocks in blocks_by_run.values():
            self._stitch_run(blocks)
            
    def _stitch_run(self, blocks):
        if len(blocks) == 1:
            return

        for b1 in blocks:
            for b2 in blocks:
                if b1 == b2:
                    continue
                
                if b1.end_slot.next() == b2.start_slot and b1.pet_signature() == b2.pet_signature():
                    b2.deleted = True
                    blocks.remove(b2)
                    b1.end_slot = b2.end_slot
                    return self._stitch_run(blocks)
                
                if b1.start_slot > b2.end_slot or b2.start_slot > b1.end_slot:
                    continue
                
                if b1.start_slot < b2.start_slot:
                    self.duplicate(b1, False).end_slot = b2.start_slot.prev()
                    b1.start_slot = b2.start_slot
                if b2.start_slot < b1.start_slot:
                    self.duplicate(b2, False).end_slot = b1.start_slot.prev()
                if b1.end_slot > b2.end_slot:
                    self.duplicate(b1, False).start_slot = b2.end_slot.next()
                    b1.end_slot = b2.end_slot
                if b2.end_slot > b1.end_slot:
                    self.duplicate(b2, False).start_slot = b1.end_slot.next()
                b2.deleted = True
                blocks.remove(b2)
                b1.add_pets(b2.pets)
                b1.dirty = True
                return self._stitch_run(blocks)
    
    def _flush(self):
        stamp = datetime.now()
        for block in self.blocks:
            block.write_to_db(stamp)

        self._read()
        
    def as_json(self):
        j = [block.as_dict() for block in self.blocks]
        return j
        
    def move(self, block, run, start_slot=None, pet=None):
        assert(block.bk_no == self.booking.no)
        assert(block in self.blocks)
        
        if start_slot == block.start_slot:
            start_slot = None
        if pet and block.pets == set([pet]):
            pet = None
    
        self._move_or_split(block, run, start_slot, pet)
        self._stitch()
        self._flush()
        
    def signature(self):
        return ','.join(map(str, sorted(filter(lambda b: not b.end_stamp, self.blocks))))

    def active_blocks(self):
        return list(filter(lambda b: not b.deleted, self.blocks))
