from datetime import timedelta, datetime

class Slot:
    def __init__(self, slot_date, slot_slot):
        if isinstance(slot_date, datetime):
            slot_date = slot_date.date()
        self.date = slot_date
        self.slot = slot_slot

    def as_dict(self):
        return {
            'date': self.date,
            'slot': self.slot
        }
    @staticmethod
    def range(start_date, end_date):
        """yield all the slots between start_date (am) and end_date (pm)"""
        slot = Slot(start_date, 'am')
        end_slot = Slot(end_date, 'pm')
        while slot <= end_slot:
            yield(slot)
            slot = slot.next()
            
    def next(self):
        if self.slot == 'am':
            next_slot = 'pm'
            next_date = self.date
        else:
            next_slot = 'am'
            next_date = self.date + timedelta(1)
            
        return Slot(next_date, next_slot)
                
    def prev(self):
        if self.slot == 'am':
            next_slot = 'pm'
            next_date = self.date + timedelta(-1)
        else:
            next_slot = 'am'
            next_date = self.date
            
        return Slot(next_date, next_slot)
                
    def inc(self):
        if self.slot == 'am':
            self.slot = 'pm'
            return
        self.slot = 'am'
        self.date = self.date + timedelta(1)
        
    def dec(self):
        if self.slot == 'pm':
            self.slot = 'am'
            return
        self.slot = 'pm'
        self.date = self.date - timedelta(1)
        
    def isPreceding(self, other):
        if self.date == other.date and self.slot == 'am' and other.slot == 'pm':
            return True
        if other.date == self.date + timedelta(1) and self.slot == 'pm' and other.slot == 'am':
            return True
        return False
    
    def isSucceeding(self, other):
        return other.isPreceding(self)
    
    def __lt__(self, other):
        return other and (self.date < other.date or (self.date == other.date and self.slot < other.slot))
    
    def __le__(self, other):
        try:
            result = other and (self.date < other.date or (self.date == other.date and self.slot <= other.slot))
        except:
            print(other, other.date, self.date)
        return result
    
    def __eq__(self, other):
        return other and (self.date == other.date and self.slot == other.slot)
    
    def __ne__(self, other):
        return other and (self.date != other.date or self.slot != other.slot)
    
    def __gt__(self, other):
        return other and (self.date > other.date or (self.date == other.date and self.slot > other.slot))
    
    def __ge__(self, other):
        return other and (self.date > other.date or (self.date == other.date and self.slot >= other.slot))
    
    def __str__(self):
        return f"{self.date.strftime('%d/%m/%y')} ({self.slot})"

    def __repr__(self):
        return f"{self.date.strftime('%Y%m%d')}:{self.slot}"

    def __hash__(self):
        return hash(f'{self.date}-{self.slot}')