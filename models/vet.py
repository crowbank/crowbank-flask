from .cached import CachedPreloaded
from pyodbc import Row

class Vet(CachedPreloaded):
    _sql = """select vet_no, vet_practice_name, vet_addr1, vet_addr3,
vet_postcode, vet_telno_1, vet_email, vet_website
from pa..tblvet"""
    _cache = {}
    _loaded = False
    
    def __init__(self, row: Row):
        self.no = row.vet_no
        self.practice_name = row.vet_practice_name
        self.addr1 = row.vet_addr1
        self.addr3 = row.vet_addr3
        self.postcode = row.vet_postcode
        self.telno = row.vet_telno_1
        self.email = row.vet_email
        self.website = row.vet_website
