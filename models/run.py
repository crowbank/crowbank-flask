from utils import db
from .cached import CachedPreloaded
from typing import Optional
from pyodbc import Row
from .runtype import RunType

class Run(CachedPreloaded):
    _sql = """select run_no, rx_short run_code, rx_wing, rx_wing_seq, run_rt_no
from pa..tblrun
join tblrunextra on rx_run_no = run_no
where run_spec_no in (1, -1)
order by rx_wing, rx_wing_seq"""
    _loaded = False
    _cache = {}

    runs_by_code = {}
    runs_by_wing = {}
    loaded = False
            
    @classmethod
    def unallocated(cls):
        return cls.get(-1)
    
    @classmethod
    def _load_from_database(cls):
        # Initialize wings
        sql = "select rx_wing, count(*) c from tblrunextra group by rx_wing"
        rows = db.safeselect(sql)
        
        if rows:
            for row in rows:
                cls.runs_by_wing[row.rx_wing] = [None] * row.c

        super()._load_from_database()
    
    @classmethod
    def get_by_code(cls, code):
        if not cls._loaded:
            cls._load_from_database()

        return Run.runs_by_code[code] if code in Run.runs_by_code else None
    
    def __init__(self, row: Optional[Row]):
        if row:
            self.no: int = row.run_no
            self.code: str = row.run_code
            self.wing: str = row.rx_wing
            self.seq: int = row.rx_wing_seq
            self.runtype: RunType = RunType.fetch(row.run_rt_no)
        else:
            self.no = -1
            self.code = 'UNALLOC'
            self.wing = 'unalloc'
            self.seq = 0
            
        Run.runs_by_code[self.code] = self
        Run.runs_by_wing[self.wing][self.seq - 1] = self
        
