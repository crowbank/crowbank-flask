from werkzeug.middleware.profiler import ProfilerMiddleware
from intranet import app

if __name__ == '__main__':
    import os
    import sys
    name = os.environ['COMPUTERNAME']
    
    app.config['PROFILE'] = True
    app.wsgi_app = ProfilerMiddleware(app.wsgi_app, restrictions=[30])
    app.run(debug=True)