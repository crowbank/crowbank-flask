from waitress import serve
from intranet import app
from models import init_models
# from flask_debugtoolbar import DebugToolbarExtension

if __name__ == '__main__':
    import os
    import sys
    name = os.environ['COMPUTERNAME']

    import intranet.views
    
    init_models()
    if name == 'HP-SERVER' and len(sys.argv) == 1:
        app.debug = True
        serve(app, host='0.0.0.0', port=8080)
    else:
        app.debug = True
#        toolbar = DebugToolbarExtension(app)
#        app.config['DEBUG_TB_INTERCEPT_REDIRECTS'] = False
        app.run(debug=True, port=5000)

