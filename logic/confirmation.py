from os import path
from utils import db, log, send_email, get_settings
import jinja2
import css_inline
from html2text import html2text
from models import Booking, Pet

def process_template(template_name, record, text='', pets=[]):  
    settings = get_settings()
    template_folder = settings['TEMPLATE_FOLDER']
    jinja_loader = jinja2.FileSystemLoader(template_folder)
    jinja_env = jinja2.Environment(loader=jinja_loader)

    template = jinja_env.get_template(template_name)
    body = template.render(record=record, pets=pets, as_email=True, text=text)
    
    folder = template_folder.replace('templates', 'static')
    body = body.replace('/static', folder)
    
    if template_name[-4:] == 'html':
        body = css_inline.inline(body)

    return body

def prepare_email(record, additional_text):
    # similar to send_email_of_record, but with no interactive options,
    # and the html content is written to the database
    
    # no checking of repeats is done here - it is presumed to have been done by caller
    
    # function returns html text (or null if failed)

    fields = [c[0] for c in record.cursor_description]
    
    pets = []
    if record.email_pet_view:
        pets_nos = db.safeselect(f'select pet_no from {record.email_pet_view} where key_no = ?', record.email_key)
        if pets_nos:
            pets = [Pet.get(r.pet_no) for r in pets_nos]
            
    if 'email_booking' in fields or 'email_invoice' in fields:
        booking = Booking.fetch(record.email_bk_no)

        if 'email_booking' in fields:
            record.email_booking = booking

        if 'email_invoice' in fields:
            record.email_invoice = booking.invoice

    if 'email_conf_text' in fields and not additional_text:
        additional_text = record.email_conf_text
    
    html = process_template(f'{record.email_template}.html', record, additional_text, pets)
    text = html2text(html)
    
    subject = record.email_subject

    return html, text, subject

def send_record_email(record, email, additional_text):
    # similar to send_email_of_record, but with no interactive options,
    # and the html content is written to the database
    
    # no checking of repeats is done here - it is presumed to have been done by caller
    
    # function returns html text (or null if failed)

    if not email and not record.email_address:
        log.warning(f'No email address found for #{record.email_key}')
        return ''
    
    html, text, subject = prepare_email(record, additional_text)

    try:
        send_email(email, html, subject, text)
        msg = f"Email {subject} sent to {email}"
        sql = 'precord_email ?, ?, ?, ?, ?'
        hist_no = db.safeexec_return(sql, record.email_key, record.email_key_type, email, subject, html)
        log.info(msg)
        return hist_no
    except BaseException as err:
        log.error(f'Error while sending email: {err}')
        return 0
    

def send_customer_email(key_no, view, reply_text, address, key_type = 'B'):
    if not key_type:
        key_type = 'B'
    sql = f'select * from {view}_unfiltered where email_key = ? and email_key_type = ?'
    record = db.safeselect_one(sql, key_no, key_type)
    if record:
        return send_record_email(record, address, reply_text)

    if key_type == 'B':
        key_type_desc = 'booking'
    else:
        key_type_desc = 'request'
    log.error(f'Unable to find {view} record for {key_type_desc} {key_no}')
    return 0

