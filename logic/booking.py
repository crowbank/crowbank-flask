from datetime import date
from utils import db
from flask import flash

# Logic associated with bookings

def check_dates(start_date: date, end_date: date, dogs: bool, cats: bool) -> bool:
    sql = 'select count(*) from tblblocked_date where b_date between ? and ? and b_spec_no = ?'
    if dogs:
        c = db.safeselect_value(sql, start_date, end_date, 1)
        if c:
            return False
    
    if cats:
        c = db.safeselect_value(sql, start_date, end_date, 2)
        if c:
            return False
    return True

def decline_booking(reason: str):
    flash(reason, 'error')
    
