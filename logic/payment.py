from flask import flash
from utils import db
from utils.stripe_utils import get_fees
from models import Booking

def add_payment(bk_no, amount, pay_type, pay_date, stripe_id=None):
    fees = get_fees(stripe_id) if stripe_id else 0.0
        
    flash(f'Processing payment of &pound;{"{:.2f}".format(amount)} (type {pay_type}) for booking #{bk_no}', 'info')
    sql = 'execute ppayment ?, ?, ?, ?, ?, ?'
    ref = stripe_id[-24:] if stripe_id else ''
    db.safeexec(sql, bk_no, amount, pay_type, pay_date, ref, fees)
    Booking.reload(bk_no)