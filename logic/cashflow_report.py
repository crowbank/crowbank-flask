from utils import db
from datetime import date, timedelta
from decimal import Decimal, ROUND_DOWN
import pandas as pd
from dateutil.relativedelta import relativedelta
from typing import Optional, List, Dict, Tuple

WAGE_FRACTION = Decimal(0.4)
BASE_VAT = date(2022, 12, 31)
data = {'Item': [], 'Amount': [], 'Direction': []}
precision = Decimal('1')
cashflow_by_date = {}

# Create a function that calculates stripe payout date given
# a payment date. The rule is that the payout date is exactly
# three business days after the payment date

def calculate_payout_date(payment_date):
    # Add three days to the payment date
    payout_date = payment_date + timedelta(days=3)
    
    # Check if the payout date is on a weekend
    if payout_date.weekday() == 5: # Saturday
        payout_date += timedelta(days=2)
    elif payout_date.weekday() == 6: # Sunday
        payout_date += timedelta(days=1)
    
    return payout_date

def latest_payment_date(payout_date):
    # Subtract three days from the payout date
    payment_date = payout_date - timedelta(days=3)
    
    # Check if the payment date is on a weekend
    if payment_date.weekday() == 5: # Saturday
        payment_date -= timedelta(days=1)
    elif payment_date.weekday() == 6: # Sunday
        payment_date -= timedelta(days=2)
    
    return payment_date

class CashflowItem():
    def __init__(self, description: str, cashflow_date: date, amount: Decimal, direction: str):
        self.description = description
        self.cashflow_date = cashflow_date
        self.amount = amount.quantize(precision, rounding=ROUND_DOWN)
        self.direction = direction
        
    @property
    def signed_amount(self) -> Decimal:
        return self.amount * (Decimal(1) if self.direction == 'Credit' else Decimal(-1))

class Cashflow():
    def __init__(self, description: str, items: Optional[List[CashflowItem]], direction: str):
        self.items = items if items else []
        self.description = description
        self.direction = direction
        
    def add_item(self, item: CashflowItem):
        self.items.append(item)
        
    @property
    def total(self):
        return sum([item.amount for item in self.items])
    
    @property
    def signed_total(self):
        return sum([item.signed_amount for item in self.items])
    
    def range_total(self, start_date: date, end_date: date) -> Decimal:
        total = sum([item.amount for item in self.items if start_date <= item.cashflow_date <= end_date])
        return total if total else Decimal(0)
        
class CashflowReport():
    def __init__(self, start_date: date, end_date: date):
        self.start_date = start_date
        self.end_date = end_date
        self.cashflows: List[Cashflow] = []
        self.balance_by_date: Dict[date, Decimal] = {}
        self.total = Decimal(0)
    
    def add_cashflow(self, cashflow: Cashflow):
        self.cashflows.append(cashflow)
        self.total += cashflow.signed_total
        
        for item in cashflow.items:
            self.balance_by_date[item.cashflow_date] = self.balance_by_date.get(item.cashflow_date, Decimal(0)) + item.amount
            
    @property
    def as_df(self) -> pd.DataFrame:
        data = {'Item': [], 'Amount': [], 'Direction': []}
        for cf in self.cashflows:
            data['Item'].append(cf.description)
            data['Amount'].append(cf.signed_total)
            data['Direction'].append(cf.direction)
            
        return pd.DataFrame(data)
    
def get_current_balance(start_date: date, end_date: date, cash_balance: Decimal) -> Tuple[Cashflow, date]:
    sql = 'select sum(bt_amount) balance, max(bt_bank_date) last_date from tblbanktransaction where bt_acct_no = 1 and bt_bank_date <= ?'
    row = db.safeselect_one(sql, start_date)
    assert(row)
    barclays_balance = row.balance
    last_date = row.last_date
    
    if not barclays_balance:
        barclays_balance = Decimal(0)
    
    barclays = CashflowItem('Barclays Balance', start_date, barclays_balance, 'Credit')
    cash = CashflowItem('Cash Balance', start_date, cash_balance, 'Credit')
    balances = Cashflow('Balances', [barclays, cash], 'Credit')
    return balances, last_date
    
def get_stripe_payouts(start_date: date) -> Cashflow:
    payouts = Cashflow('Stripe Payouts', [], 'Credit')
    
    sql = "select sp_arrival_date, sp_amount from tblstripepayout where sp_bt_no is null"
    rows = db.safeselect(sql)
    if rows:
        for row in rows:
            payout_date = row[0]
            amount = row[1]
            payouts.add_item(CashflowItem('Stripe in transit', payout_date, amount, 'Credit'))
            
    sql = 'select sb_amount from vwstripebalance'
    stripe_balance = db.safeselect_value(sql)
    if not stripe_balance:
        stripe_balance = Decimal(0)
    
    payout_date = calculate_payout_date(start_date)
    payouts.add_item(CashflowItem('Stripe Balance', payout_date, stripe_balance, 'Credit'))    
    
    return payouts

def get_expected_bookings(start_date: date, end_date: date) -> Cashflow:
    expected = Cashflow('Expected Bookings', [], 'Credit')
    
    sql = 'select convert(date, bk_start_date) bk_start_date, sum(bk_amt_outstanding) from pa..tblbooking where bk_start_date between ? and ? group by bk_start_date'
    # calculate latest payment date
    last_date = latest_payment_date(end_date)
    rows = db.safeselect(sql, start_date, last_date)
    if rows:
        for row in rows:
            payment_date = calculate_payout_date(row[0])
            expected.add_item(CashflowItem('Expected Booking', payment_date, row[1], 'Credit'))
    return expected

def get_wages(start_date: date, end_date: date) -> Cashflow:
    wages = Cashflow('Wages', [], 'Debit')
    sql = 'select d_wagepayment, min(date) d_from, max(date) d_to from tbldate where d_wagepayment between ? and ? group by d_wagepayment'
    rows = db.safeselect(sql, start_date, end_date)
    wage_periods = [(row.d_wagepayment, row.d_from, row.d_to) for row in rows] if rows else []

    if wage_periods:
        for (pay_date, from_date, to_date) in wage_periods:
            sql = 'select sum(ts_wages) from vwtimesheet_published where ts_date between ? and ?'
            published_wages = db.safeselect_value(sql, from_date, to_date)

            sql = 'select sum(rv_revenue) from vwrevenue where rv_date between ? and ?'
            revenue = db.safeselect_value(sql, from_date, to_date)
            if not revenue:
                revenue = Decimal(0.0)
            anticipated_wages = WAGE_FRACTION * revenue

            wage_amount = max(published_wages, anticipated_wages)
            wages.add_item(CashflowItem('Wages', pay_date, wage_amount, 'Debit'))
    return wages
    
def get_repeat_payments(start_date: date, end_date: date) -> Cashflow:
    repeats = Cashflow('Repeat Payments', [], 'Debit')
    sql = 'select cfm_desc, cfm_pattern from tblcashflow_monthly where cfm_active = 1 and cfm_frequency = 1'
    rows = db.safeselect(sql)
    cashflows = [{'description': row.cfm_desc, 'pattern': row.cfm_pattern} for row in rows] if rows else []

    sql = 'select bt_bank_date, -bt_amount amount from tblbanktransaction where bt_bank_date between ? and ? and bt_desc like ?'
    for cs in cashflows:
        rows = db.safeselect(sql, start_date + timedelta(-35), start_date + timedelta(-1), cs['pattern'])
        if not rows:
            continue

        if len(rows) > 1:
            print(f"{cs['description']}: multiple instances")
            continue

        the_date, amount = rows[0].bt_bank_date, rows[0].amount
        next_payment = the_date
        while True:
            next_payment = next_payment + timedelta(30)
            if next_payment <= end_date:
                repeats.add_item(CashflowItem(cs['description'], next_payment, amount, 'Debit'))
            else:
                break
    
    return repeats

def vat_payment_date(date):
    new_date = date + relativedelta(months=+2)
    return new_date.replace(day=7)

def vat_payment_amount(start_date: date, end_date: date) -> Decimal:
    sql = 'select sum(be_vat) from tblbankentry where be_date between ? and ?'
    vat_amount = db.safeselect_value(sql, start_date, end_date)
    if not vat_amount:
        vat_amount = Decimal(0.0)
    return vat_amount
    
    
def get_vat(start_date: date, end_date: date, expected_bookings: Cashflow) -> Cashflow:
    vat_dates = []
    vat_date = BASE_VAT
    vat_flow = Cashflow('VAT', [], 'Debit')
    while True:
        vat_payment = vat_payment_date(vat_date)
        if vat_payment > end_date:
            break
        vat_start = vat_date + relativedelta(months=-2)
        vat_start = vat_start.replace(day=1)
        if vat_payment >= start_date and vat_payment <= end_date:
            vat_dates.append((vat_start, vat_date, vat_payment))
        vat_date = vat_date + relativedelta(months=+3)

    for (vat_start, vat_end, vat_payment) in vat_dates:
        vat_amount = vat_payment_amount(vat_start, vat_end)
        expected_vat = expected_bookings.range_total(vat_start, vat_end) / Decimal(6.0)
        vat_flow.add_item(CashflowItem('VAT', vat_payment, vat_amount + expected_vat, 'Debit'))

    if vat_dates:
        vat_end = vat_dates[-1][1] + relativedelta(days=+1)
    else:
        vat_end = start_date

    latest_date = latest_payment_date(end_date)
    expected_vat = expected_bookings.range_total(vat_end, latest_date) / Decimal(6.0)
    vat_setaside = vat_payment_amount(vat_end, latest_date) + expected_vat
    
    vat_flow.add_item(CashflowItem('VAT Setaside', latest_date, vat_setaside, 'Debit'))
    
    return vat_flow

def get_cashflow_report(start_date: date, end_date: date, cash_balance: Decimal):
    report = CashflowReport(start_date, end_date)
    balance, _ = get_current_balance(start_date, end_date, cash_balance)
    report.add_cashflow(balance)
    report.add_cashflow(get_stripe_payouts(start_date))
    expected_bookings = get_expected_bookings(start_date, end_date)
    report.add_cashflow(expected_bookings)
    report.add_cashflow(get_wages(start_date, end_date))
    report.add_cashflow(get_repeat_payments(start_date, end_date))
    report.add_cashflow(get_vat(start_date, end_date, expected_bookings))

    return report
