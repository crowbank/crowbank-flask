from utils import db

def required_staffing(kennel_morning, ins, day_off):
    staffing_coeff = {
        'dogs': 0.112,
        'ins': 0.067,
        'holiday': 0.532
    }
    
    return staffing_coeff['dogs'] * kennel_morning + staffing_coeff['ins'] * ins + staffing_coeff['holiday'] * day_off

def staffing_class(required, actual):
    diff = actual - required
    s_class = ''
    if diff >= 1.0:
        s_class = 'under-xx'
    elif diff >= 0.5:
        s_class = 'under-x'
    elif diff >= 0.2:
        s_class = 'under'
    elif diff <= -1.0:
        s_class = 'over-xx'
    elif diff <= -0.5: 
        s_class = 'over-x'
    elif diff <= -0.2:
        s_class = 'over'
    return s_class

def timesheet_status(weekstart, path=''):
    sql = 'select tsh_latest, tsh_published, tsh_status, tsh_status_no from vwtimesheet_status where tsh_weekstart = ?'
    
    row = db.safeselect_one(sql, weekstart)
    
    latest = None
    published = None
    tsh_status = 'Never published'
    code = 2
    
    # 0 - Never Published
    # 1 - Clean
    # 2 - Unpublished Changes
    
    if row:
        latest = row.tsh_latest
        published = row.tsh_published
        tsh_status = row.tsh_status
        code = row.tsh_status_no
    else:
        sql = """select max(tss_createtime) from tbltimesheet_history
join tbltimesheet_set on tss_no = tsh_tss_no
where tsh_weekstart = ?"""
        latest = db.safeselect_value(sql, weekstart)

    return_dict = {
        'status': 'success',
        'latest': latest,
        'published': published,
        'timesheet_status': tsh_status,
        'code': code
    }
    
    if path:
        return_dict['path'] = path
    return return_dict
