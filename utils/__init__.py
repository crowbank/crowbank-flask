from .logger import get_logger, get_formatter, set_logger
from .db import DB, DatabaseHandler, TableStructure
from .mail import send_email, BufferingSMTPHandler
from .settings import get_settings
from .singleton import SingletonMeta
from .gf import get_gravity_forms_entries, get_gravity_forms_forms
from os import environ
from datetime import datetime, date

env = environ['ENVIRONMENT'] if 'ENVIRONMENT' in environ else 'prod'
log = get_logger(env)
db = DB()

db_handler = DatabaseHandler()
mail_handler = BufferingSMTPHandler()

set_logger(log, env, db_handler=db_handler, mail_handler=mail_handler)

def parse_date(s: str) -> date:
    formats = ['%d-%m', '%d-%m-%Y', '%d-%m-%y', '%Y-%m-%d',
               '%y-%m-%d', '%d/%m/%Y', '%d/%m/%y', '%d/%m',
               '%d-%m/%y']
    
    for fmt in formats:
        try:
            if len(fmt) < 6:
                parsed_date = datetime.strptime(s, fmt)
                return parsed_date.date().replace(year=datetime.now().year)
            parsed_date = datetime.strptime(s, fmt)
            return parsed_date.date()
        except ValueError:
            pass
    
    raise ValueError(f"Unrecognized date format - {s}")

