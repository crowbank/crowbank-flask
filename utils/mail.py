import re
import sys
import os
from datetime import datetime
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formatdate
from email import message_from_string
import logging
import logging.handlers
import imaplib
import smtplib
from .settings import get_settings

log = logging.getLogger(__name__)

def get_gmail():
    filename = 'C:/Crowbank/gmail.txt'
    f = open(filename, 'r')
    password = f.readline()
    account = 'crowbank.partners@gmail.com'
    server = 'imap.gmail.com'
    port = 993
    
    gmail = imaplib.IMAP4_SSL(server)
    try:
        gmail.login(account, password)
    except imaplib.IMAP4.error:
        print ("LOGIN FAILED!!! ")
        sys.exit(1)

smtp_server = None
        
def get_smtp_server():
    global smtp_server
    
    settings = get_settings()
    
    host = settings.gmail_host
    user = settings.gmail_user
    pwd = settings.gmail_pwd

    if smtp_server:
        if smtp_server.noop()[0] == 250:
            return smtp_server
        smtp_server.connect(host)
    else:
        smtp_server = smtplib.SMTP_SSL(host, 465, timeout=120)
    smtp_server.ehlo()
    try:
        smtp_server.login(user, pwd)
    except smtplib.SMTPException as e:
        print(e)
        if log:
            log.error(e)
        return None

    return smtp_server


class EmailClient():
    def __init__(self):
        filename = 'C:/Crowbank/gmail.txt'
        f = open(filename, 'r')
        password = f.readline()
        account = 'crowbank.partners@gmail.com'
        server = 'imap.gmail.com'
        port = 993
        
        self.gmail = imaplib.IMAP4_SSL(server)
        try:
            self.gmail.login(account, password)
        except imaplib.IMAP4.error:
            print ("GMAIL LOGIN FAILED!!! ")
            sys.exit(1)
    
    def set_folder(self, folder, readonly=True):
        rv, _ = self.gmail.select(folder, readonly)

        if rv != 'OK':
            print (f'Failed to open folder {folder}')
            sys.exit(1)
        
        self.folder = folder

    def mark_processed(self, msg):
        old_folder = self.folder
        new_folder = self.folder + '-processed'

        rv, data = self.gmail.uid('store', msg['uid'], '+X-GM-LABELS', new_folder)
            
        if rv != 'OK':
            m = msg['uid']
            print (f'Failed to add processed label to {m} in {self.folder}')
            sys.exit(1)
        
        self.set_folder(new_folder, False)
        rv, data = self.gmail.search(None, '(X-GM-MSGID "%s")' % msg['msgid'])

        if rv != 'OK':
            print (f'Failed to find message in {new_folder}')
            sys.exit(1)

        new_id = data[0]
        rv, data = self.gmail.store(new_id, '-X-GM-LABELS', old_folder)
        if rv != 'OK':
            print (f'Failed to remove label {self.folder} from {id}')
            sys.exit(1)
        
        self.set_folder(old_folder, False)
    
    def remove_label(self, msg, label):
        self.set_folder(label, False)
        rv, data = self.gmail.search(None, '(X-GM-MSGID "%s")' % msg['msgid'])
        
        if rv != 'OK':
            print (f'Failed to find message in {label}')
            sys.exit(1)
        
        new_id = data[0]
        rv, data = self.gmail.store(new_id, '-X-GM-LABELS', label)

        if rv != 'OK':
            print (f'Failed to remove label {self.folder} from {id}')

    def fetch_messages(self, data):
        ids = data[0].split()
        for id in ids:
            _, data = self.gmail.uid('fetch', id, '(X-GM-MSGID RFC822)')
            msgid = re.findall(r"X-GM-MSGID (\d+)", data[0][0].decode('UTF-8'))[0]
            msg = message_from_string(data[0][1].decode('UTF-8'))
            payload = msg.get_payload()
            subject = str(msg).split("Subject: ", 1)[1].split("\nTo:", 1)[0]
            if subject[:2] == 'Re':
                continue
            if type(payload) != type(''):
                continue
            yield { 'uid': id, 'msgid': msgid, 'date': msg['Date'], 'subject': msg['subject'],
                   'body': msg.get_payload(), 'lines': payload.split('\r\n') }
        
    def fetch_unread(self):
        _, data = self.gmail.uid('search', None, ('UNSEEN'))
        for msg in self.fetch_messages(data):
            yield msg

    def fetch_since(self, since_date):
        _, data = self.gmail.uid('search', None, ('SINCE %s' % since_date.strftime('%d-%b-%Y')))
        for msg in self.fetch_messages(data):
            yield msg

    def fetch_all(self):
        _, data = self.gmail.uid('search', None, ('ALL'))
        for msg in self.fetch_messages(data):
            yield msg
    
def send_email(send_to, send_body, send_subject, alt_body):
    settings = get_settings()
    msg = MIMEMultipart('alternative')

    if 'ENV' in os.environ and os.environ['ENV'] == 'dev':
        send_subject += ' (dev)'
        send_to = settings.email_bcc

    msg['Subject'] = send_subject
    msg['From'] = settings.email_user
    msg['To'] = send_to
    msg['Date'] = formatdate(localtime=True)
    timestamp = datetime.now().strftime("%Y%m%d%H%M%S")
    msg['Message-Id'] = f'{timestamp}@crowbank.co.uk'
    msg.add_header('Reply-To', settings.email_user)

    part1 = MIMEText(alt_body, 'plain')
    part2 = MIMEText(send_body, 'html')

    msg.attach(part1)
    msg.attach(part2)

    try:
        server = get_smtp_server()
        server.sendmail(settings.gmail_user, [send_to], msg.as_string())
    except smtplib.SMTPServerDisconnected:
        smtp_server.connect()
        server.sendmail(settings.gmail_user, [send_to], msg.as_string())

class BufferingSMTPHandler(logging.handlers.BufferingHandler):
    def __init__(self):
        logging.handlers.BufferingHandler.__init__(self, 1000)
        self.subject = 'Python Log'
        self.settings = get_settings()
        self.setFormatter(logging.Formatter(
            "%(asctime)s %(levelname)-7s %(message)s"))

    def flush(self):
        if len(self.buffer) > 0:
            try:
                body = ''
                alt_body = ''
                for record in self.buffer:
                    s = self.format(record)
                    body += s + "<br/>"
                    alt_body += s + '\n'

                send_email(self.settings.toaddrs, body, self.subject, alt_body, True)
            except Exception:
                self.handleError(None)  # no particular record
            self.buffer = []

