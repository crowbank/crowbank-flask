import logging
import logging.handlers
from os.path import join
from sqlite3 import DatabaseError

from utils.db import DatabaseHandler
from utils.mail import BufferingSMTPHandler
from .settings import get_settings

def get_formatter():
    return logging.Formatter('%(asctime)s  [%(levelname)-5s] %(message)s')

def get_logger(log_env = 'prod', log_file = ''):
    logger = logging.getLogger(__name__)
    settings = get_settings()
    
    if not log_file:
        log_file = __name__
    log_dir = settings['LOG_DIR']
    file_handler = logging.handlers.TimedRotatingFileHandler(
        join(log_dir, log_file), when='W0')

    debug = (log_env in ('qa', 'dev'))

    stream_handler = logging.StreamHandler()

    formatter = get_formatter()
    file_handler.setFormatter(formatter)
    stream_handler.setFormatter(formatter)

    logger.addHandler(file_handler)
    logger.addHandler(stream_handler)

    if debug:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)

    return logger        

def set_logger(logger: logging.Logger, env: str, db_handler: DatabaseHandler,
               mail_handler: BufferingSMTPHandler):
    """_summary_

    Args:
        logger (logging.RootLogger): _description_
        db_handler (DatabaseHandler): _description_
        mail_handler (BufferingSMTPHandler): _description_
    """
    if env in ('qa', 'dev'):
        mail_handler.setLevel(logging.INFO)
    else:
        logger.setLevel(logging.INFO)
        mail_handler.setLevel(logging.WARNING)
    
    formatter = get_formatter()    
    mail_handler.setFormatter(formatter)

#    logger.smtp_handler = mail_handler

    logger.addHandler(db_handler)
    logger.addHandler(mail_handler)
