import pyodbc

from utils import db, log
from utils.settings import get_settings

REMOTE_SERVERS = {}

class Table():
  loaded = False
  tables = {}
  
  def __init__(self, row):
    self.name = row.dt_table
    self.direction = row.dt_direction
    self.field_count = row.dt_field_count
    self.remote_db = row.dt_remote_db
    self.pk_name = row.dt_pk_name
    self.force = row.dt_force
  
  @staticmethod
  def load():
    sql = """SELECT dt_direction, dt_table, dt_field_count, dt_remote_db, dt_pk_name, dt_force
FROM tbldatatransfer
WHERE dt_active = 1"""

    tablerows = db.safeselect(sql)
    if tablerows:
      for tablerow in tablerows:
        new_table = Table(tablerow)
        Table.tables[new_table.name] = new_table
        
  @staticmethod
  def get(table_name):
    if not Table.loaded:
      Table.load()
    if not table_name in Table.tables:
      return None
    return Table.tables[table_name]

  @staticmethod
  def all_names():
    if not Table.loaded:
      Table.load()
    return Table.tables.keys()
  
  def __str__(self):
    return self.name

def sync_table(remote_server, remote_connection, remote_cursor, local_connection,
               local_cursor, table, force):
  log.debug(f"syncing {table} (force={force})")
  remote_db = remote_server[table.remote_db]
  
  if table.direction == 'l2r':
    source_cursor = local_cursor
    dest_cursor = remote_cursor
    
    source_table = table.name
    remote_db = remote_server[table.remote_db]
    
    dest_table = f'{remote_db}.{table.name}'
    
    dest_connection = remote_connection
  else:
    source_cursor = remote_cursor
    dest_cursor = local_cursor
    
    source_table = f'{remote_db}.{table.name}'
    dest_table = table.name

    dest_connection = local_connection
  
  dest_connection.autocommit = False

  where_clause = ''
  try:
    if force or table.force:
      dest_cursor.execute(f"DELETE FROM {dest_table}")
      max_id = 0
    else:
      sql = f'select max({table.pk_name}) from {dest_table}'
      dest_cursor.execute(sql)
      row = dest_cursor.fetchone()
      max_id = row[0]
      if max_id:
        where_clause = f'{table.pk_name} > ?'
      
    if max_id:
      source_cursor.execute(f"SELECT * FROM {source_table} where {where_clause}", max_id)
    else:
      source_cursor.execute(f"SELECT * FROM {source_table}")
      
    data = source_cursor.fetchall()
    rowcount = len(data)
    if not rowcount:
      return
    
    placeholders = ', '.join(['?' for _ in range(table.field_count)])
    sql = f"INSERT INTO {dest_table} VALUES ({placeholders})"
    dest_cursor.executemany(sql, data)
    dest_connection.commit()
  
  except pyodbc.Error as e:
    log.error(str(e))
    dest_connection.rollback()
    
def sync_tables(rdb_name, tables, force=False):
  settings = get_settings()
  REMOTE_SERVERS = settings.remote_servers
  
  assert(rdb_name in REMOTE_SERVERS)
  remote_server = REMOTE_SERVERS[rdb_name]

  url = remote_server['url']
  default_db = remote_server['pa']
  user = remote_server['user']
  pwd = remote_server['pwd']
  
  local_cursor, local_connection = db.get_cursor('mysql')
  if not local_cursor or not local_connection:
    log.error('Unable to establish local connection')
    return
  
  connection_string = f"""DRIVER={{MySQL ODBC 8.0 Unicode Driver}};SERVER={url};DATABASE={default_db};USER={user};PASSWORD={pwd};"""

# Establish the connection
  remote_connection = pyodbc.connect(connection_string)
  remote_cursor = remote_connection.cursor()
  if not remote_cursor:
    log.error('Unable to open remote cursor')
    return

# Retrieve table information
  for table_name in tables:
    table = Table.get(table_name)
    if table:
      sync_table(remote_server, remote_connection, remote_cursor, local_connection,
                  local_cursor, table, force)
    # Fetch data from the source database table
  remote_cursor.close()
  local_cursor.close()
  remote_connection.close()
  local_connection.close()

def sync_all_tables(rdb):
  table_names = Table.all_names()
  sync_tables(rdb, table_names)
  