import requests

# Basic functions for retrieving form data and form entries

def get_gravity_forms_forms(api_url, api_key, api_secret):
    """Request and return all forms and fields"""
    url = f'{api_url}/forms'
    
    response = requests.get(url, auth=(api_key, api_secret))
    forms = response.json()
    
    fields = []
    form_values = []
    
    for id, val in forms.items():
        response = requests.get(f'{url}/{id}', auth=(api_key, api_secret))
        status_code = response.status_code
        
        if status_code < 200 or status_code > 299:
            return
        
        form_data = response.json()
        form_values.append([
            id,
            form_data['title'],
            form_data['description'],
            val['entries']
        ])
    
        form_fields = form_data['fields']
        for f in form_fields:
            fields.append([
                f['id'],
                id,
                f['label'] if 'label' in f else '',
                f['type']
            ])

    return form_values, fields     

def get_gravity_forms_entries(api_url, api_key, api_secret, min_id=0, page_size=100):
    # Create request headers
    headers = {
        "Content-Type": "application/json",
    }

    # Define query parameters for pagination
    params = {
        "paging[page_size]": page_size,
        "sorting[key]": "id",
        "sorting[direction]": "ASC",
        "sorting[is_numeric]": "true"
    }

    if min_id > 0:
        params["search"] = '{"field_filters": [{"key":"id","value":' + f'{min_id}' + ',"operator":">"}]}'

    # Send the request to the API
    response = requests.get(f"{api_url}/entries", auth=(api_key, api_secret), headers=headers, params=params)

   # Check if the request was successful
    if response.status_code == 200:
        entries = response.json()
        
        if 'entries' not in entries:
            print(f"Error: returned JSON has no entries element")
            return
        
        entries = entries['entries']
        # If there are no more entries, break the loop
        if not entries:
            return

        return entries
    else:
        print(f"Error: {response.status_code}")
        return
