import pyodbc
import logging.handlers
import logging
from utils.singleton import SingletonMeta
import time
import datetime
from utils.settings import get_settings
from typing import List, Tuple

class TableStructure:
    def __init__(self, table_name):
        self.table_name = table_name
        self.columns = []

    def get_columns(self, db):
        sql = f"select column_name, data_type, is_nullable, character_maximum_length from information_schema.columns where table_name = '{self.table_name}'"
        
        rows = db.safeselect(sql)
        for row in rows:
            self.columns.append(self.row_to_dict(row))

    @staticmethod
    def row_to_dict(row):
        return {
            "column_name": row[0].lower(),
            "data_type": row[1],
            "is_nullable": True if row[2] == "YES" else False,
            "character_maximum_length": row[3]
        }

log = logging.getLogger(__name__)

def row_to_dict(row:pyodbc.Row) -> dict:
    result = {}
    for column in row.cursor_description:
        column_name = column[0]
        result[column_name] = getattr(row, column_name)
    return result

def my_connect():
    # connect to remote mysql database
    settings = get_settings()
    
    server = settings.remote_server
    username = settings.remote_user
    password = settings.remote_pwd
    database = settings.remote_pa_db
    
    connection_string = f"DRIVER={{MySQL ODBC 8.0 Unicode Driver}};SERVER={server};DATABASE={database};USER={username};PASSWORD={password};"

# Establish the connection
    connection = pyodbc.connect(connection_string)
    
    return connection

class DB(metaclass=SingletonMeta):
    def __init__(self):
        self.settings = get_settings()
        
        self.sql_debug = False
        self.db_module = 'pyodbc'
        self.profile_sql = False
    
    def __getattr__(self, attr):
        if attr in self.settings:
            return self.settings[attr]

        uattr = attr.upper()
        if uattr in self.settings:
            return self.settings[uattr]

        raise AttributeError

    def close_cursor(self, cur, **kw):
        if 'commit' in kw and kw['commit']:
            cur.commit()
        cur.close()

    def get_cursor(self, database=None) -> Tuple[pyodbc.Cursor, pyodbc.Connection]:
        if not database:
            database = 'crowbank'
        try:
            conn = pyodbc.connect(
f"""DRIVER={{ODBC Driver 17 for SQL Server}};SERVER={self.db_server};DATABASE={database};UID={self.db_user};PWD={self.db_pwd};MultipleActiveResultSets=True""" )
            
        except Exception:
            print(f"Failed to connect with db_server={self.db_server}, db_database={self.db_database}, db_user={self.db_user}, db_pwd={self.db_pwd}")
            assert(False)

        
        cur = conn.cursor()
        return cur, conn

    def execute_many(self, sql, values):
        cur, _ = self.get_cursor()
        assert(cur)
        cur.executemany(sql, values)
        return cur
    # Note that execute_many does NOT auto-commit
    
    def execute_base(self, sql, *args) -> Tuple[pyodbc.Cursor, pyodbc.Connection]:
        cur, conn = self.get_cursor()
        assert(cur)
        start = time.time()
        sql_now = datetime.datetime.now()
        try:
            cur.execute(sql, args)
        except Exception as e:
            log.error(f'Error executing {sql}: {e}')    
            assert(False)
            
        end = time.time()
        if self.profile_sql:
            elapsed = end - start
            self.sql_log.append((sql_now, sql, [*args], elapsed))
            if sql not in self.sql_log_summary:
                self.sql_log_summary[sql] = [0, 0.0]
            self.sql_log_summary[sql][0] += 1
            self.sql_log_summary[sql][1] += elapsed
        return cur, conn
    
    def start_sql_log(self):
        self.profile_sql = True
        self.sql_log = []
        self.sql_log_summary = {}
        
    def end_sql_log(self):
        self.profile_sql = False
        return self.sql_log, self.sql_log_summary
    
    def safeexec(self, sql, *args):
        cur, conn = self.execute_base(sql, *args)
        if cur:
            cur.commit()
        if conn:
            conn.close()

    def safeexec_return(self, sql, *args):
        nsql = f'set nocount on; declare @ret int; execute @ret = {sql}; select @ret ret_no'
        ret = self.safeselect_value(nsql, *args, commit=True)
        return ret
    
    
    def safeexec_return_msg(self, proc_name, *args):
        cur, _ = self.get_cursor()
        
        params_placeholder = ', '.join(['?'] * len(args))
        sql_call = f"{{CALL {proc_name} ({params_placeholder})}}"
        
        assert(cur)
        try:
            cur.execute(sql_call, *args)
            if cur.description:
                row = cur.fetchone()
                assert(row)
                cur.commit()
                return row[0], row[1]
            else:
                return 0, 'No return value'
        except:
            log.error(f'Error executing {sql_call}')
            return 0, 'Exception raised'
        finally:
            cur.close()
            
    
    
    def safeselect(self, sql, *args, **kw) -> List[pyodbc.Row]:
        cur, conn = self.execute_base(sql, *args)
        res = []
        if cur:
            try:
                res =  cur.fetchall()
            except Exception as e:
                log.error(f'Error fetching from {sql}: {e}')
            self.close_cursor(cur, **kw)
        if conn:
            conn.close()
        return res
    
    def safeselect_one(self, sql, *args, **kw):
        cur, conn = self.execute_base(sql, *args)
        res = None
        assert(cur)
        try:
            res = cur.fetchone()
        except Exception as e:
            log.error(f'Error fetching from {sql}: {e}')    
        if cur:
            self.close_cursor(cur, **kw)
        if conn:
            conn.close()
        return res
        
    def safeselect_value(self, sql, *args, **kw):
        cur, conn = self.execute_base(sql, *args)
        row = None
        assert(cur)
        try:
            row = cur.fetchone()
        except Exception as e:
            log.error(f'Error fetching from {sql}: {e}')

        if cur:
            self.close_cursor(cur, **kw)
        if conn:
            conn.close()
        if row:
            return row[0]
        else:
            return None
        
    def check_exists(self, sql):
        sql_wrapper = f"if exists ({sql}) select 1 else select 0"
        res = self.safeselect_value(sql_wrapper)
        return(res == 1)

    def get_server(self):
        sql = 'select @@servername'
        server = self.safeselect_one(sql)
        return server

class DatabaseHandler(logging.Handler):
    def __init__(self):
        logging.Handler.__init__(self)

    def emit(self, record):
        db = DB()
        msg = self.format(record)
        levelname = record.levelname
        filename = record.filename
        lineno = record.lineno
        sql = 'Execute plog ?, ?, ?, ?, ?, ?, ?'

        db.safeexec(sql, msg, levelname, 'python', 0, '', filename, lineno)

if __name__ == '__main__':
    db = DB()
    print(db.safeselect_value('select count(*) from pa..tblbooking'))
    