import stripe
from datetime import datetime, timedelta
import sys, os
from typing import Optional, List
from models import Customer

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

from utils.settings import get_settings

def get_fees(id) -> float:
  payment_intent = stripe.PaymentIntent.retrieve(id)
  if not payment_intent:
    return 0.0
  
  ch_id = payment_intent.latest_charge
  if not ch_id:
    return 0.0
  
  charge = stripe.Charge.retrieve(ch_id)
  if not charge:
    return 0.0
  
  bt_id = charge.balance_transaction
  if not bt_id:
    return 0.0
  
  bt = stripe.BalanceTransaction.retrieve(bt_id)
  if not bt:
    return 0.0
  
  fee: float = bt.fee / 100.0  
  return fee

def stripe_initialize():
  settings = get_settings()
  stripe_key: str = settings.stripe_key
  stripe.api_key = stripe_key

def create_payment_intent(amount, description:str='', bk_no:int=0, cust_no:int=0):
  intent = stripe.PaymentIntent.create(
    amount=amount,
    currency='gbp',
    payment_method_types=[
      'card_present',
    ],
    description=description,
    capture_method='automatic',
    metadata={
      'bk_no': bk_no,
      'cust_no': cust_no
    }
  )
  return intent

def get_connection_token():
  connection_token = stripe.terminal.ConnectionToken.create()
  return connection_token.secret

def delete_payment(payment_intent_id):
  try:
    intent = stripe.PaymentIntent.retrieve(payment_intent_id)
    if intent:
      intent.cancel()
      return 1
  except Exception as e:
    return 0
  return 0

def get_payouts_intransit() -> List[stripe.Payout]:
  payout_list = []
  payouts = stripe.Payout.list(status='in_transit', limit=1000)
  
  for payout in payouts:
    payout_list.append(payout)
    
  return payout_list

def get_payouts() -> List[stripe.Payout]:
  payout_list = []
  payouts = stripe.Payout.list(limit=1000)
  
  for payout in payouts:
    payout_list.append(payout)
    
  return payout_list
  
def get_customers() -> List[stripe.Customer]:
  customer_list = []
  customers = stripe.Customer.list(limit=1000)
  
  for customer in customers:
    customer_list.append(customer)
    
  return customer_list

def create_customer(customer: Customer) -> str:
  """Attempt to create a new Stripe customer to associate with petadmin customer"""
  stripe_customer = stripe.Customer.create(
    name=customer.display_name,
    email=customer.email,
    address={
      'line1': customer.addr1,
      'city': customer.addr3,
      'postal_code': customer.postcode,
      'country': 'United Kingdom'
    },
    phone=customer.telno_mobile,
    metadata={
      'cust_no': customer.no
    }
  )
  
  if stripe_customer:
    return stripe_customer.id
  
  return ''

def get_refunds(start_date:Optional[datetime]=None) -> List[stripe.Refund]:
  if start_date:
    start_timestamp = int(start_date.timestamp())
    refunds = stripe.Refund.list(created={'gte': start_timestamp})
  else:
    refunds = stripe.Refund.list()
  
  return refunds.data

def refund2payment(refund: stripe.Refund) -> Optional[stripe.PaymentIntent]:
  charge = stripe.Charge.retrieve(refund.charge)
  if charge:
    pi = stripe.PaymentIntent.retrieve(charge.payment_intent)
    return pi
  
  return None

def get_balance() -> stripe.Balance:
  balance = stripe.Balance.retrieve()
  return balance

def get_receipt(p: stripe.PaymentIntent) -> str:
  try:
    if not p.latest_charge:
      return ''
    
    charge_id = p.latest_charge
    if not charge_id:
      return ''
    
    charge = stripe.Charge.retrieve(charge_id)
    if not charge:
      return ''
    
    receipt_number = charge.receipt_number
    return receipt_number
  except Exception as e:
    return ''

def get_payments(start_date:Optional[datetime]=None) -> List[stripe.PaymentIntent]:

  all_payments = []
  if start_date:
# Convert to Unix timestamp
    start_timestamp = int(start_date.timestamp())

# Initialize an empty list to hold successful payments

# Get all PaymentIntents
    payment_intents = stripe.PaymentIntent.list(created={'gte': start_timestamp})
  else:
    payment_intents = stripe.PaymentIntent.list()

  for pi in payment_intents.auto_paging_iter():
      # Check if the PaymentIntent was successful
      all_payments.append(pi)

# Now, successful_payments contains all successful payments between start_date and end_date
  return all_payments

def delete_incomplete_payments(hours=24):
    # Calculate the cutoff time (24 hours ago)
    cutoff_time = datetime.now() - timedelta(hours=hours)

    # Get all incomplete payments created before the cutoff time
    query = f"""created<{int(cutoff_time.timestamp())} and status:'requires_payment_method'"""
    print(query)
    
    payments = stripe.PaymentIntent.search(query=query)

    # Delete each incomplete payment
    for payment in payments.auto_paging_iter():
        try:
            payment.cancel()
            print(f"Payment deleted: {payment.id}")
        except stripe.error.StripeError as e:
            # Handle any errors
            print(f"Error deleting payment {payment.id}: {e}")

def refund_payment(pi: str) -> bool:
  refund = stripe.Refund.create({
    'payment_intent': pi
  })
  status = refund.status
  return status == 'succeeded'
  

if __name__ == '__main__':
  stripe_initialize()
  start_date = datetime(2023, 1, 1) # Adjust dates as needed
  payments = get_payments(start_date)
  
  for payment in payments:
    print(payment)
