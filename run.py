import argparse
from flask import g
import os
import sys
from waitress import serve

from intranet import app, portal
from models import init_models
# from flask_debugtoolbar import DebugToolbarExtension

name = os.environ['COMPUTERNAME']

import intranet.views
import intranet.portal.views
from utils.stripe_utils import stripe_initialize

init_models()
stripe_initialize()

if __name__ == "__main__":
    if name == 'HP-SERVER' and len(sys.argv) == 1:
        app.debug = True
        serve(app, host='0.0.0.0', port=8080, threads=10)
    else:
        parser = argparse.ArgumentParser()
        parser.add_argument("-host", type=str, action="store", help="Host string in request header")
        parser.add_argument("-email", type=str, action="store", help="User email")

        args = parser.parse_args()
        
        app.config['CLI_HOST'] = args.host
        app.config['CLI_EMAIL'] = args.email
    
        app.debug = True

    #        toolbar = DebugToolbarExtension(app)
    #        app.config['DEBUG_TB_INTERCEPT_REDIRECTS'] = False
        app.run(debug=True, port=5000)
