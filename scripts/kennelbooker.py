import pandas as pd
import argparse
from datetime import datetime
import os, sys

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))


from utils import db  # Importing your database wrapper

# Function to convert date
def convert_date(dates_str: str) -> str:
    first_date_str = dates_str.split(' - ')[0]
    first_date = datetime.strptime(first_date_str, '%d/%m/%Y').strftime('%Y-%m-%d')
    return first_date

def convert_date2(date_str: str) -> str:
    date = datetime.strptime(date_str, '%d/%m/%Y').strftime('%Y-%m-%d')
    return date

# Function to handle database operations
def process_file(file_path: str):
    # Read CSV file
    df = pd.read_csv(file_path)

    # Transform date
    df['Dates'] = df['Dates'].apply(convert_date)
    df['Created'] = df['Created'].apply(convert_date2)
    # Fetch existing kbb_no values from the database
    existing_kbb_no = set(row.kbb_no for row in db.safeselect("SELECT kbb_no FROM tblkb_booking"))

    # SQL queries
    update_query = """
    UPDATE tblkb_booking
    SET kbb_customer = ?, kbb_pets = ?, kbb_date = ?, kbb_status = ?, 
        kbb_amt_due = ?, kbb_created = ?, kbb_customer_id = ?, kbb_modified = ?
    WHERE kbb_no = ?"""

    insert_query = """
    INSERT INTO tblkb_booking (kbb_customer, kbb_pets, kbb_date, kbb_status, 
                               kbb_amt_due, kbb_created, kbb_customer_id, kbb_added, kbb_no)
    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)"""

    now = datetime.now()
    # Loop through DataFrame and decide between INSERT and UPDATE
    for _, row in df.iterrows():
        args = (row['Customer'], row['Pets'], row['Dates'], row['Status'], row['Amt Due'], 
                row['Created'], row['Customer Id'], now, row['Reference'])
        if row['Reference'] in existing_kbb_no:
            # Update existing record
            db.safeexec(update_query, *args)
        else:
            # Insert new record
            db.safeexec(insert_query, *args)

# Main function to read file path from command line
def main():
    parser = argparse.ArgumentParser(description='Process a CSV file to update a SQL Server table.')
    parser.add_argument('file_path', action="store", help='Path to the CSV file')
    args = parser.parse_args()
    file_path = args.file_path
    process_file(file_path)

if __name__ == "__main__":
    main()
