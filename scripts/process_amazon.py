import pandas as pd
import argparse
import os
import sys

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

from utils import db, TableStructure

def value_to_sql(value):
    # If the value is a string, wrap it in single quotes and escape any existing single quotes
    if isinstance(value, str):
        escaped_value = value.replace("'", "''")
        return "'" + escaped_value + "'"
    # If the value is NaN (from pandas), convert it to NULL for SQL
    elif pd.isna(value):
        return 'NULL'
    else:
        return str(value)

def upload_file(csv_file: str, table_name: str):
    """Upload CSV file to local SQL Server accessed through db object after matching structure"""
    df = pd.read_csv(csv_file)
    # Convert column names to lowercase for case-insensitive matching
    df.columns = [col.lower() for col in df.columns]

    table = TableStructure(table_name)
    table.get_columns(db)
    
    # Clean data based on table structure
    for col in table.columns:
        col_name = col["column_name"]
        if col_name in df.columns:
            if col["data_type"] == "float":
                df[col_name].fillna(0.0, inplace=True)
            elif col["data_type"] == "varchar":
                max_length = col["character_maximum_length"]
                df[col_name] = df[col_name].str.slice(0, max_length)
                if df[col_name].str.len().max() > max_length:
                    print(f"Column {col_name} should be made at least {df[col_name].str.len().max()} long")
            if df[col_name].isnull().any() and not col["is_nullable"]:
                print(f"Column {col_name} should allow nulls")

    sql = f'truncate table {table_name}'
    db.safeexec(sql)
    
    for _, row in df.iterrows():
        values = ', '.join([value_to_sql(v) for v in row])
        sql = f"INSERT INTO {table_name} VALUES ({values})"
        db.safeexec(sql)
#        print(sql)  # This will print the fully-populated SQL statement
#        placeholders = ', '.join(['?'] * len(row))
#        sql = f"INSERT INTO {table_name} VALUES ({placeholders})"
#        params = [*row.tolist()]
        
#        db.safeexec(sql, *row.tolist())

def upload_file_old(csv_file: str, table_name: str):
    """Upload CSV file to local SQL Server accessed through db object"""
    df = pd.read_csv(csv_file)
    table = TableStructure(table_name)
    table.get_columns(db)
    
    sql = f'truncate table {table_name}'
    db.safeexec(sql)
    
    for _, row in df.iterrows():
        placeholders = ', '.join(['?'] * len(row))
        sql = f"INSERT INTO {table_name} VALUES ({placeholders})"
        db.safeexec(sql, *row.tolist())

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("file", type=str, action="store", help="CSV file to upload")

    args = parser.parse_args()
    
    filename = args.file
    
    upload_file(filename, 'tblamazonitem_staging')
    
    sql = 'select count(*) from tblamazonitem_staging'
    c = db.safeselect_value(sql)
    print(f'Number of rows in staging table: {c}')
    
    if c:
        sql = 'exec pimport_amazon_items'
        db.safeexec(sql)
        
    
if __name__ == '__main__':
    main()