import sys
import os
from pprint import pprint

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

from utils import db

pet_no = 32651
run_no = 56
sql = 'pbook_daycare(?, ?, ?)'
bk_no, msg = db.safeexec_return_msg(sql, pet_no, run_no, '2021-01-01')
print(f'bk_no={bk_no}, msg={msg}')

