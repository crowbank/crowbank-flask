import sys
import re
import os
from datetime import datetime
import logging

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

from utils import db, stripe_utils
log = logging.getLogger(__name__)

def read_customers():
    stripe_utils.stripe_initialize()
    
    customers = stripe_utils.get_customers()

    sql = 'select sc_id from tblstripecustomer'
    rows = db.safeselect(sql)
    
    ids = [row.sc_id for row in rows] if rows else []
    
    for customer in customers:
        cust_id = customer.stripe_id
        cust_name = customer.name
        cust_address = customer.address
        cust_email = customer.email
        cust_phone = customer.phone
        cust_balance = customer.balance
        
        if cust_id in ids:
            continue
    
        address = f'{cust_address.line1}, {cust_address.city}, {cust_address.postal_code}' if cust_address else ''
        sql = """insert into tblstripecustomer
(sc_id, sc_address, sc_email, sc_name, sc_phone, sc_balance)
values (?, ?, ?, ?, ?, ?)"""

        db.safeexec(sql, cust_id, address, cust_email, cust_name,
                    cust_phone, cust_balance)

    sql = 'exec pupdate_stripecustomer'
    db.safeexec(sql)

def read_payouts():
    stripe_utils.stripe_initialize()
    
    payouts = stripe_utils.get_payouts()
    for payout in payouts:
        created = datetime.fromtimestamp(payout.created)
        arrival_date = datetime.fromtimestamp(payout.arrival_date)
        amount = payout.amount / 100.0
        balance_transaction = payout.balance_transaction
        id  = payout.id
        status = payout.status
        
        sql = 'select count(*) from tblstripepayout where sp_id = ?'
        exists = db.safeselect_value(sql, id)
        
        if exists:
            sql = 'update tblstripepayout set sp_amount = ?, sp_balance_transaction = ?, sp_created = ?, sp_arrival_date = ?, sp_status = ? where sp_id = ?'
            db.safeexec(sql, amount, balance_transaction, created, arrival_date, status, id)
        else:    
            sql = 'insert into tblstripepayout (sp_id, sp_amount, sp_balance_transaction, sp_created, sp_arrival_date, sp_status) values (?, ?, ?, ?, ?, ?)'
            db.safeexec(sql, id, amount, balance_transaction, created, arrival_date, status)

    sql = 'exec pprocess_payouts'
    db.safeexec(sql)
    
    
def read_refunds(force = 0):
    stripe_utils.stripe_initialize()

    start_date = datetime(2023, 1, 1)    
    if force:
        sql = 'truncate table tblstriperefund'
        db.safeexec(sql)
        ids = []
    else:
        sql = 'select sr_id from tblstriperefund'
        rows = db.safeselect(sql)
        ids = [row.sr_id for row in rows] if rows else []
    
        sql = 'select max(sr_created_at) from tblstriperefund'
        start_date = db.safeselect_value(sql)
            
        if not start_date:
            start_date = datetime(2023, 1, 1)

    refunds = stripe_utils.get_refunds(start_date)
    for refund in refunds:
        if refund.id in ids and not force:
            continue
        
        pi = stripe_utils.refund2payment(refund)
        
        bk_no = 0
        cust_no = 0
        pi_id = ''
        if pi:
            pi_id = pi.id
            bk_no = pi.metadata.get('bk_no')
            cust_no = pi.metadata.get('cust_no')
        
        
        insert_query = """INSERT INTO tblstriperefund (sr_id, sr_amount, sr_created_at, sr_status, 
sr_charge_id, sr_reason, sr_bk_no, sr_cust_no, sr_payment_id)
VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)"""
        db.safeexec(insert_query, refund.id, refund.amount / 100.0, datetime.fromtimestamp(refund.created),
                    refund.status, refund.charge, refund.reason, bk_no, cust_no, pi_id)

def update_balance():
    stripe_utils.stripe_initialize()
    balance = stripe_utils.get_balance()
    sql = 'insert into tblstripebalance (sb_asof, sb_available, sb_amount) values (?, ?, ?)'
    db.safeexec(sql, datetime.now(), balance.instant_available[0].amount / 100.0, balance.pending[0].amount / 100.0)

def process_payment(payment):
        sp_stripe_id = payment.id
        sp_amount = payment.amount / 100 # Convert from cents to dollars
        sp_currency = payment.currency
        sp_status = payment.status
        sp_receipt_email = payment.receipt_email
        sp_created_at = datetime.fromtimestamp(payment.created)
        sp_updated_at = datetime.now() # Current timestamp
        sp_customer_id = payment.customer
        sp_description = payment.description
        sp_bk_no = payment.metadata.get('bk_no')
        sp_cust_no = payment.metadata.get('cust_no')    
        
        sp_receipt = stripe_utils.get_receipt(payment)

        sp_fees = stripe_utils.get_fees(payment.id)
        
        # Assuming the customer email is available in payment['charges']['data'][0]['billing_details']['email']
        # If it's not always available, you'll need to adjust this code accordingly.
#        sp_customer_email = payment['charges']['data'][0]['billing_details']['email'] if payment_intent['charges']['data'] else None
        sp_customer_email = ''
        if payment.customer:
            pass

        sp_source = ''

        if not sp_description:
            sp_description = ''
            print(f"No description for {sp_stripe_id}")
        if sp_description.startswith('Crowbank Ke'):
            sp_pay_no = 0
            sp_source = 'kennel booker'
        elif sp_description.startswith('Entry ID:'):
            sp_source = 'deposit'
            if not sp_bk_no:
                entry_id = re.findall(r'\d+', sp_description)[0]
                sql = 'select rglt_value from tblrg_lead_detail where rglt_rgl_no = ? and rglt_field_no = 1'
                bk_no_str = db.safeselect_value(sql, entry_id)
                sp_bk_no = int(bk_no_str) if bk_no_str else 0
        elif sp_description.startswith('Payment for'):
            if not sp_bk_no:
                sp_source = 'terminal'
                sp_bk_no = re.findall(r'\d+', sp_description)[0]

        sp_pay_no = 0
        if sp_bk_no:
            # Find payment number
            sql = "select pay_no from pa..tblpayment where pay_bk_no = ? and pay_amount = ? and pay_date = ? and (pay_ref = '' or pay_ref = ?)"
            pay_no_str = db.safeselect_value(sql, sp_bk_no, sp_amount, sp_created_at.date(), sp_stripe_id[-24:])
            if pay_no_str:
                sp_pay_no = int(pay_no_str)
            
        sp_method = payment.payment_method_types[0] if payment.payment_method_types else ''
        
        # Prepare the INSERT statement
        insert_query = """INSERT INTO tblstripepayment 
    (sp_id, sp_amount, sp_currency, sp_status, sp_receipt_email, sp_created_at,
    sp_updated_at, sp_customer_id, sp_customer_email, sp_description,
    sp_fees, sp_bk_no, sp_cust_no, sp_source, sp_pay_no, sp_receipt, sp_method) 
    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"""

        # Execute the INSERT statement
        db.safeexec(insert_query, sp_stripe_id, sp_amount, sp_currency, sp_status,
                    sp_receipt_email, sp_created_at, sp_updated_at, sp_customer_id,
                    sp_customer_email, sp_description, sp_fees, sp_bk_no, sp_cust_no, sp_source, sp_pay_no, sp_receipt, sp_method)


def read_payments(force = 0):
    stripe_utils.stripe_initialize()
    start_date = datetime(2023, 1, 1)

    ids = []
    if force:
        sql = 'truncate table tblstripepayment'
        db.safeexec(sql)
    else:
        sql = 'select sp_id from tblstripepayment'
        rows = db.safeselect(sql)
        ids = [row.sp_id for row in rows] if rows else []

        sql = 'select max(sp_created_at) from tblstripepayment'
        start_date = db.safeselect_value(sql)

        if not start_date:
            start_date = datetime(2023, 1, 1)

    payments = stripe_utils.get_payments(start_date)
# Get the PaymentIntent objects from Stripe

    for payment in payments:
        # Check if this PaymentIntent is already in the database
        if payment.id in ids and not force:
            continue

            # This PaymentIntent is not in the database, so insert it
            # ... rest of the code as before ...

        # Extract the necessary fields from the PaymentIntent
        process_payment(payment)
                
    sql = 'exec pprocess_stripe_payments'
    db.safeexec(sql)        

if __name__ == '__main__':
    read_payments()
    read_customers()
    read_payouts()
    read_refunds()
    update_balance()
