import requests
from PIL import Image
from shutil import copy
import sys, os

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

from utils import db, log

base = 'K:/Attachments'

def img2pdf(cif_no):
    sql = "select cifa_no, cifa_file from tblcheckin_form_attachment where cifa_downloaded = 1 and cifa_cif_no = ? and cifa_ext in ('jpeg', 'jpg', 'png')"
    rows = db.safeselect(sql, cif_no)
    images = []
    file = f'{base}/c{cif_no}.pdf'
    if rows:
        for row in rows:
            images.append(Image.open(row.cifa_file))
        img = images[0]
        try:
            if len(images) > 1:
                img.save(file, save_all=True, append_images=images[1:])
            else:
                img.save(file, save_all=True)
            log.info(f'{file} saved')
        except Exception as e:
            log.error(f'Failed saving pdf for cif_no={cif_no}')
            file = 'N/A'
    else:
        sql = "select cifa_file from tblcheckin_form_attachment where cifa_downloaded = 1 and cifa_cif_no = ? and cifa_ext = 'pdf'"
        rows = db.safeselect(sql, cif_no)
        if len(rows) == 1:
            copy(rows[0].cifa_file, file)
            log.info(f'{file} copied')
        else:
            log.error(f'Failed saving pdf for cif_no={cif_no} - multiple pdf files')
            file = 'N/A'

    sql = 'update tblcheckin_form set cif_attachments_pdf = ? where cif_no = ?'
    db.safeexec(sql, file, cif_no)

def img2pdf_all():
    sql = 'select cif_no from tblcheckin_form where cif_attachments > 0 and cif_attachments_pdf is null'
    rows = db.safeselect(sql)
    
    if rows:
        log.info(f'Converting {len(rows)} attachment sets to pdf')
        for row in rows:
            cif_no = row.cif_no
            try:
                img2pdf(cif_no)
            except Exception as e:
                log.error(f'Failed to convert {cif_no} to pdf')        

def download_attachments():
    sql = "select cifa_no, cifa_cif_no, cifa_path from tblcheckin_form_attachment where cifa_downloaded = 0 and cifa_path <> ''"
    rows = db.safeselect(sql)
    
    sql = 'update tblcheckin_form_attachment set cifa_downloaded = ?, cifa_file = ?, cifa_ext = ? where cifa_no = ?'

    forms = {}
    count = len(rows)
    fails = 0

    if count:
        log.info(f'Downloading attachments for {count} forms')

        for row in rows:
            no = row.cifa_no
            url = row.cifa_path
            ext = url.split('.')[-1]    
            outfile = f'{base}/{no}.{ext}'
            try:
                r = requests.get(url)
                open(outfile, 'wb').write(r.content)
                db.safeexec(sql, 1, outfile, ext, no)
                if row.cifa_cif_no not in forms:
                    forms[row.cifa_cif_no] = []
                forms[row.cifa_cif_no].append(f'{no}.{ext}')
                log.info(f'Downloded {url}')
            except Exception as e:
                fails += 1
                log.warning(f'Failed to download {url}')
                db.safeexec(sql, -1, None, None, no)

    return count - fails

def process_forms():   
    log.info(f'Running process_checkin_forms.py')
    
    db.safeexec('pimport_checkin_forms')
    download_attachments()
    
    img2pdf_all()
    
if __name__ == '__main__':
    process_forms()