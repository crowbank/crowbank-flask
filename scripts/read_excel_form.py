import sys
import os
import argparse
import pandas as pd

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

from utils import db


# Sample function to demonstrate how to upload Excel data to SQL Server
def upload_excel_to_sql_server(excel_path: str, form_no: int,
                               date_created_column: str, force: bool=False):
    # Load Excel data into a DataFrame
    df = pd.read_excel(excel_path)
   
   # Check if form fields are already defined
   
    n = db.safeselect_value("""select count(*) from tblform_field where ff_form_no = ?""", form_no)

    if force or not n:
        if n:
            db.safeexec("""delete from tblform_field where ff_form_no = ?""", form_no)
        # Populate tblform_field
        field_no = 1  # Starting field number
        for col, dtype in zip(df.columns, df.dtypes):
            # Determine the field type
            if "int" in str(dtype):
                field_type = "int"
            elif "float" in str(dtype):
                field_type = "float"
            elif "datetime" in str(dtype):
                field_type = "date"
            else:
                field_type = "string"
            
            # Insert into tblform_field
            db.safeexec("""
                INSERT INTO tblform_field (ff_form_no, ff_order, ff_field_no, ff_desc, ff_type)
                VALUES (?, ?, ?, ?, ?)
            """, form_no, 0, field_no, col, field_type)
            field_no += 1
    
    # Populate tblrg_lead and tblrg_lead_detail
    for _, row in df.iterrows():
        # Insert into tblform_entry
        created = row[date_created_column]
        db.safeexec("""INSERT INTO tblform_entry (fe_form_no, fe_date_created)
VALUES (?, ?)""", form_no, created)
        
        fe_no = db.safeselect_value("""select fe_no
from tblform_entry
where fe_form_no = ? and fe_date_created = ?""", form_no, created)
        # Insert into tblrg_lead_detail
        field_no = 1  # Reset field number
        for col, value in row.items():
            db.safeexec("""
INSERT INTO tblform_entry_item (fei_fe_no, fei_form_no, fei_field_no, fei_value)
VALUES (?, ?, ?, ?)
            """, fe_no, form_no, field_no, str(value))
            field_no += 1
    

# Note: The function is not run here due to absence of SQL Server access in this environment.
# You can run this function in your own environment after replacing the placeholders in the connection string and Excel path.

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('path', help='File to upload', action='store')
    parser.add_argument('form_no', help='Form number to use', action='store')
    parser.add_argument('date_col', help='Column to use for date created', action='store')
    parser.add_argument('-force', action='store_true', help='Force send email even if sent before')

    args = parser.parse_args()

    path = args.path
    form_no = args.form_no
    date_col = args.date_col
    force = args.force
    # Upload Excel data to SQL Server
    upload_excel_to_sql_server(excel_path=path, form_no=form_no,
                               date_created_column=date_col, force=force)

if __name__ == '__main__':
    main()