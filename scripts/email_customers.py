import sys
import os
from typing import Dict, Optional, List
from pyodbc import Row
from pprint import pprint

import argparse

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

from utils import db, log
from logic import send_record_email
from utils.db import row_to_dict

class EmailView():
    loaded: bool = False
    views: Dict[str, 'EmailView'] = {}
    views_by_view: Dict[str, 'EmailView'] = {}
    
    def __init__(self, row):
        self.no: int = row.ev_no
        self.view: str = row.ev_view
        self.name: str = row.ev_name
        self.desc: str = row.ev_desc
        self.active: bool = row.ev_active
        self.type: str = row.ev_type
        self.auto: Optional[str] = row.ev_auto
        
    @classmethod
    def load(cls):
        sql: str = "select ev_no, ev_view, ev_name, ev_desc, ev_active, ev_type, ev_auto from tblemail_view"
        view_rows: Optional[List[Row]] = db.safeselect(sql)
        if not view_rows:
            log.error('Unable to load any views')
            return
        
        for row in view_rows:
            new_view = cls(row)
            cls.views[new_view.name] = new_view
            cls.views_by_view[new_view.view] = new_view
            
        cls.loaded = True
        
    @classmethod
    def get(cls, name: str) -> Optional['EmailView']:
        if not cls.loaded:
            cls.load()
            
        if name in cls.views:
            return cls.views[name]
        
        if name in cls.views_by_view:
            return cls.views_by_view[name]
        
        log.warning(f'Unable to find email view {name}')
        return None
    
    def __str__(self):
        return self.name
    
    @classmethod
    def get_by_schedule(cls, schedule) -> List['EmailView']:
        if not cls.loaded:
            cls.load()
            
        schedule_views = [view for view in cls.views.values() if view.auto == schedule]
        return schedule_views
    
    @classmethod
    def get_all(cls) -> List['EmailView']:
        return list(cls.views.values())        
            
    
# This script is used to initiate two types of automatic emails to customers:
# First, triggered by an event such as new booking, booking cancellation or deposit payment made
# Second, a few days ahead of a collection.

# The business logic for which emails to send is incorporated into a database view
# The body of the emails is generated using Jinja2 out off templates
# An email sent successfully is recorded in the tblhistory table.
#
# This functionality partially replicates that of confirmation.py. 
# By keeping the logic in the database, and the content in a template, this script is kept light.

def send_emails(sql:str, review:bool=False, display:bool=False, max_display:int=3, force:bool=False, debug:bool=False):
    # Use db_view, the name of a database view, to generate emails.
    # The view is assumed to have the following mandatory fields:
    # 1. email_address
    # 2. email_subject
    # 3. email_template
    # 4. email_bk_no
    # 5. email_deposit_amount
    # 6. email_repeat [will be 1 if a similar email has already been sent]
    #
    # Any additional fields are transferred to the template in an object called record.
    
    # Note that confirmation.py's logic of checking against prior emails must also be done by the database view

    records = db.safeselect(sql)
    if not records:
        return
    d = 0

    if debug:
        dicts = [row_to_dict(record) for record in records]
        pprint(dicts)
        return
    
    for record in records:
        log.info('Working in sending to %s', record.email_address)
        hist_no = send_record_email(record, record.email_address, '')
        if hist_no:
            d += 1
        else:
            return
        if max_display and d >= max_display and (review or display):
            break
        else:
            continue
            
def send_scheduled_emails():
    pass

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-scheduled', help='Run scheduled views', action='store')
    parser.add_argument('-view', nargs="*", help='Database View', action='store')
    parser.add_argument('-sql', help='SQL to run', action='store')
    parser.add_argument('-where', help='Where clause on SQL', action='store')
    parser.add_argument('-review', action='store_true', help='Review email before sending')
    parser.add_argument('-display', action='store_true', help='Display email without sending')
    parser.add_argument('-debug', action='store_true', help='Debug by only printing email rows')
    parser.add_argument('-booking', nargs="*", action='append', help='Only generate for booking #')
    parser.add_argument('-max', action='store', default=3, help='Max number of notices to display (default: 3)')
    parser.add_argument('-force', action='store_true', help='Force send email even if sent before')

    args = parser.parse_args()
   
    log.info('Starting')
    
    max_notices:int = int(args.max)
    if args.sql:
        log.info('Running sql = %s', args.sql)
        send_emails(args.sql, args.review, args.display, max_notices, args.force)
    else:              
        auto = args.scheduled.lower() if args.scheduled else ''
        
        views = []
        if auto:
            views = EmailView.get_by_schedule(auto)

        if args.view:
            for v in args.view:
                v = v.lower()
                if v == 'all':
                    views = EmailView.get_all()
                    continue
                
                view = EmailView.get(v)
                if view:
                    views.append(view)
                else:
                    log.warning(f'Unknown view {v}')

        bookings = None
        if args.booking:
            bookings = ', '.join([y for x in args.booking for y in x])
        
        for view in views:
            act_view = f'{view.view}_unfiltered' if (bookings or args.sql or args.where) else view.view
            sql = f"select * from {act_view} where email_address <> ''"
            if args.where:
                sql += f' and ({args.where})'
            if bookings:
                sql += f' and email_key in ({bookings})'
                
            if not args.force:
                sql += ' and email_repeat = 0 and email_override = 0'

            log.info('Running sql = %s', sql)
            send_emails(sql, args.review, args.display, max_notices, args.force, args.debug)

if __name__ == '__main__':
    main()