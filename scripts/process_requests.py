import re
import logging
import sys, os

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

from utils import db, log, mail

def process_requests():
    log.info('Starting process_requests')
    g = mail.EmailClient()
    g.set_folder('Requests', False)
    p1 = re.compile('Booking Request for .* \((\d+)\)') 
    p2 = re.compile('Check Availability for .* \((\d+)') 
       
    log.info('process_requests retrieved gmail')

    for msg in g.fetch_all():
        subject = msg['subject']
        log.info(f'process_requests processing {subject}')
        m = p1.search(subject)
        m2 = p2.search(subject)
        form_no = 0
        if m:
            form_no = int(m.group(1))
        elif m2:
            form_no = int(m2.group(1))
        
        if form_no:
            sql = f"select count(*) c from tblformdata_6 where fd_no = {form_no}"
            c = db.safeselect_value(sql)
            if c == 0:
                process = False
                log.info(f'process_requests skipping {form_no}')
                break
        else:
            g.mark_processed(msg)
            print(f"Processed {subject}")
            continue
        
        html = msg['body'].replace("'", "''")
        
        sql = f"update tblformdata_6 set fd_html = ? where fd_no = ?"
        db.safeexec(sql, html, form_no)
        g.mark_processed(msg)
        log.info(f"Processed form {form_no}")

            
if __name__ == '__main__':
    process_requests()
