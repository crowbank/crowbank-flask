import re
import datetime

import sys, os

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

from utils import db, log, mail

def process_worldpay():
    g = mail.EmailClient()
    g.set_folder('Worldpay', False)
    p1 = re.compile('cart ID: PBL-(\d+)(-(\d+))?')
    pv = re.compile('cart ID: PBL-V(\d+)')
    p2 = re.compile('Sale value:\s+GBP (\d+\.\d\d)')
    p3 = re.compile('Generated at:\s*(\d\d/.../\d\d\d\d)')
    pr = re.compile('Purchase transaction ID: (\d+)')
    
    for msg in g.fetch_all():
        bk_no = 0
        v_no = 0
        ref = ''
        amount = 0.0
        for line in msg['lines']:
            m = p1.search(line)
            if m:
                bk_no = int(m.group(1))
                if m.group(3):
                    n = int(m.group(3))
                else:
                    n = 1
                continue
            
            m = pv.search(line)
            if m:
                v_no = int(m.group(1))
                continue
            
            m = p2.search(line)
            if m:
                amount = float(m.group(1))
                continue
            
            m = p3.search(line)
            if m:
                d = m.group(1)
                continue
            
            m = pr.search(line)
            if m:
                ref = m.group(1)
        
        if not amount:
            log.error(f'Could not get payment amount for booking {bk_no}')
            continue
        
        if bk_no:
            bk_status = db.safeselect_value('select bk_status from vwbooking where bk_no = ?', bk_no).upper()
            if bk_status == 'Z':
                log.warning(f'Attempted payment into an expired booking {bk_no}')
            sql = 'exec pattempt_payment ?, ?, ?, ?, ?'
            db.safeexec(sql, bk_no, amount, d, n, ref)
            log.info(f'Processing payment of {amount} for booking {bk_no}')
        elif v_no:
            sql = 'exec pprocess_voucher ?, ?'
            db.safeexec(sql, v_no, amount)
            log.info(f'Processing payment of {amount} for voucher {v_no}')
        else:
            log.error(f'Could not find booking number')
            continue
            
        g.mark_processed(msg)

            
if __name__ == '__main__':
    process_worldpay()
