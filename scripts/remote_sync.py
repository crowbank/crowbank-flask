import argparse
import sys, os
import process_checkin_forms

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

from utils import db, log
from utils.dbsync import sync_tables, sync_all_tables

def sync_full(rdb):
  # Perform a full sync. Steps are:
  # Read crwbnk_gf_* tables
  # Process with pimport_gf_entry
  # Import checkin attachments
  # Run pmaster to prepare mysql export tables
  # Transfer my_availability to remote
  # Update lasttransfer

  log.info('Starting full sync')  
  table_names = ['crwbnk_gf_entry', 'crwbnk_gf_entry_meta', 'crwbnk_gf_entry_notes']
  sync_tables(rdb, table_names)
  log.info('gf_entry tables imported')
  sql = 'exec pimport_gf_entry'
  db.safeexec(sql)
  process_checkin_forms.process_forms()
  log.info('gf_entry tables processed')
  
  sql = 'exec pmaster'
  db.safeexec(sql)
  log.info('pmaster executed')
  table_names = ['my_availability', 'my_employee', 'my_lasttransfer']
  sync_tables(rdb, table_names)
  log.info('Availability exported')
  log.info('Full sync completed')
  
def main():
  parser = argparse.ArgumentParser()
  parser.add_argument("rdb", help="Remote site, e.g. siteground or hostpresto")
  parser.add_argument("-full", action="store_true", help="Perform a full sync, including running local procs")
  parser.add_argument("-table", nargs="*", type=str, action="append", help="Table names; default to all active tables")
  parser.add_argument("-force", action="store_true", help="Read all entries. Default will only read new entries")
  
  args = parser.parse_args()

  if args.full:
    sync_full(args.rdb)
    return
    
  if args.table:
    sync_tables(args.rdb, args.table[0], args.force)
  else:
    sync_all_tables(args.rdb)
  
if __name__ == '__main__':
  main()
