#!/usr/bin/python
from requests.models import encode_multipart_formdata
import re
from datetime import datetime, date, timedelta
import argparse
import logging
from os import getenv, path
import sys
from utils import parse_date

SCRIPT_DIR = path.dirname(path.abspath(__file__))
sys.path.append(path.dirname(SCRIPT_DIR))

from utils import db, get_settings
from models import Booking

settings = get_settings()

log = logging.getLogger(__name__)

log.info('Running sms')

client = TextmagicRestClient(settings['TEXTMAGIC_USERNAME'], settings['TEXTMAGIC_TOKEN'])

def pet_name_combine(pets):
    names = list(map(lambda p: p.name, pets))
    if len(names) == 1:
        return names[0]
    
    comb = ', '.join(names[:-1])
    comb += ' and ' + names[-1]
    return comb

def send_sms(booking, msg, customer, test):
    
    if msg:
        pass
    else:
        if booking.start_date == date.today() + timedelta(days=1):
            date_string = 'tomorrow'
        else:
            date_string = 'on ' + booking.start_date.strftime("%A %d/%m/%Y")
        
        if (booking.pickup != -1):
            vacc_msg = 'You do not need to prepare vaccination cards this time.\n'
        else:
            vacc_msg = 'You do not need to bring vaccination cards this time.\n'
        
        unconfirmed_pets = []
        for pet in booking.pets:
            if pet.vacc_status == 'Valid':
                continue
            
            unconfirmed_pets.append(pet)
    
        if unconfirmed_pets:
            if len(unconfirmed_pets) > 1:
                card_msg = 'vaccination cards'
                card_pronoun = 'them'
            else:
                card_msg = 'a vaccination card'
                card_pronoun = 'it'
                
            if (booking.pickup != -1):
                vacc_msg = f'You need to have {card_msg} for {pet_name_combine(unconfirmed_pets)} ready'
            else:
                vacc_msg = f'You need to bring {card_msg} for {pet_name_combine(unconfirmed_pets)}'
            vacc_msg = vacc_msg + ' unless you already uploaded ' + card_pronoun
        
        collection_time = f'at {booking.start_time}'
            
        if (booking.pickup != -1):
            msg = f"We are due to pick up {booking.pet_names} for Crowbank {date_string} {collection_time}.\n"
        else:
            msg = f"You are due to bring in {booking.pet_names} to Crowbank {date_string}.\n"
            msg += f"Your checkin time is {collection_time}.\n"
        
        outstanding = "{:.2f}".format(booking.amt_outstanding)
        msg += f'\nThe £{outstanding} balance on the booking is now due. We accept both cash and card payments.'
        if (booking.pickup != -1):
            msg += f'\nPlease have it ready.\n'
        else:
            msg += f'\nPlease bring it with you when you check in.\n'
        
        msg += vacc_msg + '\n'
        msg += "Call us at 01236 729454 or reply to this message with any changes"
    
    phone = customer.telno_mobile.replace(' ', '')
    phone = re.sub('^0', '44', phone)
    
    if test:
        msg += f'\n[Testing - would have been sent to {phone}]'
        client.messages.create(phones="447590507946", text=msg)
        log_msg = f'Test Sent: {msg}\nTo: {phone}'

    else:
        log_msg = f'Sent: {msg}\nTo: {phone}'
        client.messages.create(phones=phone, text=msg)
    
    log.info(log_msg)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-booking', nargs='*', action='store', type=int, help='Booking number(s)')
    parser.add_argument('-date', action='store', help='The date for which messages are to be sent [YYYYMMDD]')
    parser.add_argument('-test', action='store_true', help='Send all messages to Eran')
    parser.add_argument('-msg', action='store', help='Alternative message to send')
    parser.add_argument('-customer', action='store', help='Customer number')

    args = parser.parse_args()

    if args.date:
        start_date = parse_date(args.date)
    else:
        start_date = date.today() + timedelta(days=1)
    
    test = args.test
    
    if args.booking:
        bookings = []
        for bk_no in args.booking:
            bookings.append(Booking.get(bk_no))
    else:
        bookings = Booking.get_by_start_date(start_date)
    
    if args.msg:
        msg = args.msg
    else:
        msg = ''
    
    if args.customer:
        customer = args.customer
    else:
        customer = 0
    
    for booking in bookings:
        customer = booking.customer
        if customer.telno_mobile and not customer.nosms:
            send_sms(booking, msg, customer, test)
        else:
            if customer.nosms:
                log.warning('Skipping booking %d - customer marked as no sms' % booking.no)
            else:
                log.warning('Skipping booking %d - no mobile number' % booking.no)


if __name__ == '__main__':
    main()
