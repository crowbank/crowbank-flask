import sys
import os
from datetime import date
from decimal import Decimal

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

from logic import get_cashflow_report


def main():
    start_date = date.today()
    end_date = date(2023, 8, 7)
    cash_balance = Decimal(3000.0)
    report = get_cashflow_report(start_date, end_date, cash_balance)
    
    print(f"Cashflow Report from {start_date.strftime('%d/%m/%y')} to {end_date.strftime('%d/%m/%y')}")
    print('Credits')
    format_string = "{:<20} {:>10,.2f}"
    total_credit = Decimal(0.0)
    for cf in report.cashflows:
        if cf.direction == 'Credit':
            print(format_string.format(cf.description, cf.total))
            total_credit += cf.total
    print(format_string.format("Total Credits", total_credit))

    print('Debits')
    total_debit = Decimal(0.0)
    for cf in report.cashflows:
        if cf.direction == 'Debit':
            print(format_string.format(cf.description, cf.total))
            total_debit += cf.total
    print(format_string.format("Total Debits", total_debit))
    print()
    print(format_string.format("Balance", total_credit - total_debit))    
    
if __name__ == '__main__':
    main()