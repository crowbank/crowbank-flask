# Gravity Forms Interface
import requests
import urllib.parse
import sys

import sys
import os

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

from utils import db, get_gravity_forms_forms, get_gravity_forms_entries

def get_credentials():
    KEY = 'ck_22d69be63618ef6c41796e64386788736e0fac03'
    SECRET = 'cs_23f03fecc2fe4d4cb8973b2bf46e1fee694143de'
    URL = 'crowbank.co.uk'

    return {
        'base_url': URL,
        'key': KEY,
        'secret': SECRET
    }
    
def get_forms():
    """Read all forms and fields, writing to database"""
    cred = get_credentials()
    base_url = cred['base_url']

    api_url = f'https://{base_url}//wp-json/gf/v2'
    api_key = cred['key']
    api_secret = cred['secret']

    form_values, fields = get_gravity_forms_forms(api_url, api_key, api_secret)
    
    sql = 'truncate table tblgf_form'
    db.safeexec(sql)
        
    sql = 'insert into tblgf_form values (?, ?, ?, ?)'    
    db.execute_many(sql, form_values)
    
    sql = 'truncate table tblgf_field'
    db.safeexec(sql)
        
    sql = 'insert into tblgf_field values (?, ?, ?, ?)'    
    db.execute_many(sql, fields)
    
def load_forms():
    forms = {}
    
    sql = 'select form_id, form_title from tblgf_form'
    rows = db.safeselect(sql)
    
    for row in rows:
        id = row.form_id
        forms[id] = {
            'title': row.form_title,
            'fields': {}
        }
        
    sql = 'select field_id, field_form_id, field_label, field_type from tblgf_field'
    rows = db.safeselect(sql)
    
    for row in rows:
        forms[row.field_form_id]['fields'][row.field_id] = {
            'label': row.field_label,
            'type': row.field_type
        }
        
    return forms

def process_entry(forms, entry):
    form_id = int(entry['form_id'])
    id = entry['id']
    created = entry['date_created']
    
    if form_id not in forms:
        return None, None
    
    form = forms[form_id]
    fields = form['fields'].keys()

    clean_entry = [id, form_id, created]
    values = []
    
    for key in entry.keys():
        if key in fields and entry[key]:
            value = entry[key]
            if isinstance(value, list):
                value = ','.join(value)
            values.append([id, form_id, key, value])                

    return clean_entry, values

def write_entries_and_values(entries, values):
    cur, _ = db.get_cursor()

    sql = 'insert into tblgf_entry values (?, ?, ?)'
    cur.executemany(sql, entries)
        
    sql = 'insert into tblgf_value values (?, ?, ?, ?)'
    cur.executemany(sql, values)
    cur.commit()

def get_entries_and_values(page_size):
    forms = load_forms()
    cred = get_credentials()
    base_url = cred['base_url']

    api_url = f'https://{base_url}//wp-json/gf/v2'
    api_key = cred['key']
    api_secret = cred['secret']

    sql = 'select max(entry_id) from tblgf_entry'
    min_id = db.safeselect_value(sql)
            
    entries = get_gravity_forms_entries(api_url, api_key, api_secret, min_id, page_size)
    
    clean_entries = []
    values = []

    if not entries:
        print('All Done')
        return None, None
        
    for entry in entries:
        clean_entry, entry_values = process_entry(forms, entry)
        if not clean_entry:
            continue
        clean_entries.append(clean_entry)
        values.extend(entry_values)
        
    return clean_entries, values


if __name__ == '__main__':
#    get_forms()

    page_size = 10
    if len(sys.argv) >= 2:
        page_size = int(sys.argv[1])
    
    
    while True:
        entries, values = get_entries_and_values(page_size)
        if not entries:
            break
        write_entries_and_values(entries, values)
        sql = 'select max(entry_id) from tblgf_entry'
        max_id = db.safeselect_value(sql)
        print(max_id)
    
