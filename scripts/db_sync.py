import MySQLdb as mysql
import sys, os

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

from utils import db

def main():

    # Connect to local SQL Server
    local_cursor, _ = db.get_cursor('mysql')

    # Connect to remote MySQL Server
    mydb = mysql.connect(
        host="cp165172.hpdns.net",
        user="crowbank_petadmin",
        password="Crowbank454",
        database="crowbank_wix"
    )
    remote_cursor = mydb.cursor()

    # Retrieve table information
    sql = "select dt_direction, dt_source_table, dt_dest_table, dt_field_count from tbldatatransfer where dt_active = 1" 
    tables = db.safeselect(sql)

    for table in tables:
        dt_direction, dt_source_table, dt_dest_table, bt_field_count = table

        # Fetch data from the source database table
        source_cursor = local_cursor if dt_direction == 'l2r' else remote_cursor
        source_cursor.execute(f"SELECT * FROM {dt_source_table}")
        data = source_cursor.fetchall()

        # Truncate the destination table and insert the new data
        dest_cursor = remote_cursor if dt_direction == 'l2r' else local_cursor

        dest_cursor.execute(f"TRUNCATE TABLE {dt_dest_table}")
        placeholders = ', '.join(['?' for _ in range(bt_field_count)])
        for record in data:
            sql = f"INSERT INTO {dt_dest_table} VALUES ({placeholders})"
            tup = tuple(record)
            dest_cursor.execute(sql, *tup)

    # Commit changes and close connections
    mydb.commit()
    mydb.close()
    local_cursor.commit()
    local_cursor.close()

if __name__ == '__main__':
    main()
